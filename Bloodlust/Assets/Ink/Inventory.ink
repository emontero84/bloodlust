LIST Inventory = 
	(QuestLog), MoneyClip, Lockpick, Screwdriver, EmptyPainkillers, Bleach, CheapWatch, BloodBag, BrokenTvRemote, 
	Stake, OldBible, TinyKey, MariansDiary, EmptyCanOfGas, BirthdayCake, IntoxicatedBirthdayCake, Laxatives,
	Newspaper, 
	GoldenWatch, Wrench,
	CharityBox, 
	CdRom, CarltonsNote, Katana, 
	BigScissors, MillionaireDress, 
	Painkillers, SleepingPills, Stethoscope,
	BucketOfYellowPaint, FakeGoldenWatch, LillysDiary, PlasticTubeWithFunnel,
	Frisbee, 
	Crowbar, CanOfGas, BaseballBat,
	Astrolite, MoneyBag, EmptyCoffeeThermos, RatPoison, JarOfWeed, TvRemote, DiamondRing,
	VialOfBlood, DiabolicPropaganda, OldDiabolicBible,
	Steak, IntoxicatedSteak, BoxOfDonuts, CoffeeThermos, PlumberBrosFlyer,
	PileOfCoins,
	TipJar, Knife, LillysPhoto, LillysKey,
	PhilosophyBook1, PhilosophyBook2, PhilosophyBook3, 
	VodkaBottle1, VodkaBottle2, VodkaBottle3, 
	SkillBook1, SkillBook2, SkillBook3, SkillBook4, SkillBook5, SkillBook6, SkillBook7, SkillBook8, SkillBook9, SkillBook10

=== LookAtInventoryItems ====
+ { HasInInventory(MoneyClip) } [ Look at money clip ] -> LookAtMoneyClip
+ { HasInInventory(Lockpick) } [ Look at lockpick ] -> LookAtLockpick
+ { HasInInventory(Screwdriver) } [ Look at screwdriver ] -> LookAtScrewdriver
+ { HasInInventory(EmptyPainkillers) } [ Look at empty painkillers ] -> LookAtEmptyPainkillers
+ { HasInInventory(Bleach) } [ Look at bleach ] -> LookAtBleach
+ { HasInInventory(CheapWatch) } [ Look at cheap watch ] -> LookAtCheapWatch
+ { HasInInventory(BloodBag) } [ Look at blood bag ] -> LookAtBloodBag
+ { HasInInventory(BrokenTvRemote) } [ Look at broken TV remote ] -> LookAtBrokenTvRemote
+ { HasInInventory(Stake) } [ Look at stake ] -> LookAtStake
+ { HasInInventory(OldBible) } [ Look at old Bible ] -> LookAtOldBible
+ { HasInInventory(TinyKey) } [ Look at tiny key ] -> LookAtTinyKey
+ { HasInInventory(MariansDiary) } [ Look at Marian's diary ] -> LookMariansDiary
+ { HasInInventory(EmptyCanOfGas) } [ Look at empty can of gas ] -> LookAtEmptyCanOfGas
+ { HasInInventory(BirthdayCake) } [ Look at birthday cake ] -> LookAtBirthdayCake
+ { HasInInventory(IntoxicatedBirthdayCake) } [ Look at intoxicated birthday cake ] -> LookAtIntoxicatedBirthdayCake
+ { HasInInventory(Laxatives) } [ Look at laxatives ] -> LookAtLaxatives
+ { HasInInventory(Newspaper) } [ Look at newspaper ] -> LookAtNewspaper
+ { HasInInventory(GoldenWatch) } [ Look at golden watch ] -> LookAtGoldenWatch
+ { HasInInventory(Wrench) } [ Look at wrench ] -> LookAtWrench
+ { HasInInventory(CharityBox) } [ Look at charity box ] -> LookAtCharityBox
+ { HasInInventory(CdRom) } [ Look at CD-ROM ] -> LookAtCdRom
+ { HasInInventory(CarltonsNote) } [ Look at Carlton's note ] -> LookAtCarltonsNote
+ { HasInInventory(Katana) } [ Look at katana ] -> LookAtKatana
+ { HasInInventory(BigScissors) } [ Look at big scissors ] -> LookAtBigScissors
+ { HasInInventory(MillionaireDress) } [ Look at dress ] -> LookAtMillionaireDress
+ { HasInInventory(Painkillers) } [ Look at painkillers ] -> LookAtPainkillers
+ { HasInInventory(SleepingPills) } [ Look at sleeping pills ] -> LookAtSleepingPills
+ { HasInInventory(Stethoscope) } [ Look at stethoscope ] -> LookAtStethoscope
+ { HasInInventory(BucketOfYellowPaint) } [ Look at bucket of yellow paint ] -> LookAtBucketOfYellowPaint
+ { HasInInventory(FakeGoldenWatch) } [ Look at fake golden watch ] -> LookAtFakeGoldenWatch
+ { HasInInventory(LillysDiary) } [ Look at Lilly's diary ] -> LookAtLillysDiary
+ { HasInInventory(PlasticTubeWithFunnel) } [ Look at plastic tube with funnel ] -> LookAtPlasticTubeWithFunnel
+ { HasInInventory(Frisbee) } [ Look at frisbee ] -> LookAtFrisbee
+ { HasInInventory(Crowbar) } [ Look at crowbar ] -> LookAtCrowbar
+ { HasInInventory(CanOfGas) } [ Look at can of gas ] -> LookAtCanOfGas
+ { HasInInventory(BaseballBat) } [ Look at baseball bat ] -> LookAtBaseballBat
+ { HasInInventory(Astrolite) } [ Look at Astrolite ] -> LookAtAstrolite
+ { HasInInventory(MoneyBag) } [ Look at money bag ] -> LookAtMoneyBag
+ { HasInInventory(EmptyCoffeeThermos) } [ Look at empty coffee thermos ] -> LookAtEmptyCoffeeThermos
+ { HasInInventory(RatPoison) } [ Look at rat poison ] -> LookAtRatPoison
+ { HasInInventory(JarOfWeed) } [ Look at jar of weed ] -> LookAtJarOfWeed
+ { HasInInventory(TvRemote) } [ Look at TV remote ] -> LookAtTvRemote
+ { HasInInventory(DiamondRing) } [ Look at diamond ring ] -> LookAtDiamondRing
+ { HasInInventory(VialOfBlood) } [ Look at vial of blood ] -> LookAtVialOfBlood
+ { HasInInventory(DiabolicPropaganda) } [ Look at diabolic propaganda ] -> LookAtDiabolicPropaganda
+ { HasInInventory(OldDiabolicBible) } [ Look at old diabolic Bible ] -> LookAtOldDiabolicBible
+ { HasInInventory(Steak) } [ Look at steak ] -> LookAtSteak
+ { HasInInventory(IntoxicatedSteak) } [ Look at intoxicated steak ] -> LookAtIntoxicatedSteak
+ { HasInInventory(BoxOfDonuts) } [ Look at box of donuts ] -> LookAtBoxOfDonuts
+ { HasInInventory(CoffeeThermos) } [ Look at coffee thermos ] -> LookAtCoffeeThermos
+ { HasInInventory(PileOfCoins) } [ Look at pile of coins ] -> LookAtPileOfCoins
+ { HasInInventory(TipJar) } [ Look at tip jar ] -> LookAtTipJar
+ { HasInInventory(Knife) } [ Look at knife ] -> LookAtKnife
+ { HasInInventory(LillysPhoto) } [ Look at Lilly's photo ] -> LookAtLillysPhoto
+ { HasInInventory(LillysKey) } [ Look at Lilly's key ] -> LookAtLillysKey
+ { HasInInventory(PlumberBrosFlyer) } [ Look at Plumber Bros flyer ] -> LookAtPlumberBrosFlyer
+ { HasInInventory(PhilosophyBook1) } [ Look at philosophy book 1 ] -> LookAtPhilosophyBook1
+ { HasInInventory(PhilosophyBook2) } [ Look at philosophy book 2 ] -> LookAtPhilosophyBook2
+ { HasInInventory(PhilosophyBook3) } [ Look at philosophy book 3 ] -> LookAtPhilosophyBook3
+ { HasInInventory(VodkaBottle1) } [ Look at vodka bottle 1 ] -> LookAtVodkaBottle
+ { HasInInventory(VodkaBottle2) } [ Look at vodka bottle 2 ] -> LookAtVodkaBottle
+ { HasInInventory(VodkaBottle3) } [ Look at vodka bottle 3 ] -> LookAtVodkaBottle
* { HasInInventory(SkillBook2) } [ Look at skill book 2 ] -> LookAtSkillBook2
* { HasInInventory(SkillBook4) } [ Look at skill book 4 ] -> LookAtSkillBook4
* { HasInInventory(SkillBook5) } [ Look at skill book 5 ] -> LookAtSkillBook5
* { HasInInventory(SkillBook6) } [ Look at skill book 6 ] -> LookAtSkillBook6
* { HasInInventory(SkillBook7) } [ Look at skill book 7 ] -> LookAtSkillBook7
* { HasInInventory(SkillBook8) } [ Look at skill book 8 ] -> LookAtSkillBook8
* { HasInInventory(SkillBook9) } [ Look at skill book 9 ] -> LookAtSkillBook9
* { HasInInventory(SkillBook10) } [ Look at skill book 10 ] -> LookAtSkillBook10
* { HasInInventory(DiabolicPropaganda) && HasInInventory(OldBible) } [ Use diabolic propaganda with old Bible ] -> CombineOldBibleWithDiabolicPropaganda
* { HasInInventory(DiabolicPropaganda) && HasInInventory(OldBible) } [ Use old Bible with diabolic propaganda ] -> CombineOldBibleWithDiabolicPropaganda
* { HasInInventory(SleepingPills) && HasInInventory(Steak) } [ Use sleeping pills with steak ] -> CombineSleepingPillsWithSteak
* { HasInInventory(SleepingPills) && HasInInventory(Steak) } [ Use steak with sleeping pills ] -> CombineSleepingPillsWithSteak
* { HasInInventory(Laxatives) && HasInInventory(Steak) } [ Use laxatives with steak ] -> CombineLaxativesWithSteak
* { HasInInventory(Laxatives) && HasInInventory(Steak) } [ Use steak with laxatives ] -> CombineLaxativesWithSteak
* { HasInInventory(RatPoison) && HasInInventory(Steak) } [ Use rat poison with steak ] -> CombineRatPoisonWithSteak
* { HasInInventory(RatPoison) && HasInInventory(Steak) } [ Use steak with rat poison ] -> CombineRatPoisonWithSteak
* { HasInInventory(SleepingPills) && HasInInventory(BirthdayCake) } [ Use sleeping pills with birthday cake ] -> CombineSleepingPillsWithBirthdayCake
* { HasInInventory(SleepingPills) && HasInInventory(BirthdayCake) } [ Use birthday cake with sleeping pills ] -> CombineSleepingPillsWithBirthdayCake
* { HasInInventory(Laxatives) && HasInInventory(BirthdayCake) } [ Use laxatives with birthday cake ] -> CombineLaxativesWithBirthdayCake
* { HasInInventory(Laxatives) && HasInInventory(BirthdayCake) } [ Use birthday cake with laxatives ] -> CombineLaxativesWithBirthdayCake
* { HasInInventory(RatPoison) && HasInInventory(BirthdayCake) } [ Use rat poison with birthday cake ] -> CombineRatPoisonWithBirthdayCake
* { HasInInventory(RatPoison) && HasInInventory(BirthdayCake) } [ Use birthday cake with rat poison ] -> CombineRatPoisonWithBirthdayCake
* { HasInInventory(BucketOfYellowPaint) && HasInInventory(CheapWatch) } [ Use bucket of yellow paint with cheap watch ] -> CombineBucketOfYellowPaintWithCheapWatch
* { HasInInventory(CheapWatch) && HasInInventory(BucketOfYellowPaint) } [ Use cheap watch with bucket of yellow paint ] -> CombineBucketOfYellowPaintWithCheapWatch
+ [ Go back ] ->->

=== CombineBucketOfYellowPaintWithCheapWatch ===
~RemoveFromInventory(CheapWatch)
~RemoveFromInventory(BucketOfYellowPaint)
~AddToInventory(FakeGoldenWatch)
->->

=== CombineOldBibleWithDiabolicPropaganda ===
~RemoveFromInventory(DiabolicPropaganda)
~RemoveFromInventory(OldBible)
~AddToInventory(OldDiabolicBible)
->->

=== CombineSleepingPillsWithSteak ===
~RemoveFromInventory(Steak)
~RemoveFromInventory(SleepingPills)
~AddToInventory(IntoxicatedSteak)
->->

=== CombineLaxativesWithSteak ===
~RemoveFromInventory(Steak)
~RemoveFromInventory(Laxatives)
~AddToInventory(IntoxicatedSteak)
->->

=== CombineRatPoisonWithSteak ===
~RemoveFromInventory(Steak)
~RemoveFromInventory(RatPoison)
~AddToInventory(IntoxicatedSteak)
->->

=== CombineSleepingPillsWithBirthdayCake ===
~RemoveFromInventory(BirthdayCake)
~RemoveFromInventory(SleepingPills)
~AddToInventory(IntoxicatedBirthdayCake)
->->

=== CombineLaxativesWithBirthdayCake ===
~RemoveFromInventory(BirthdayCake)
~RemoveFromInventory(Laxatives)
~AddToInventory(IntoxicatedBirthdayCake)
->->

=== CombineRatPoisonWithBirthdayCake ===
~RemoveFromInventory(BirthdayCake)
~RemoveFromInventory(RatPoison)
~AddToInventory(IntoxicatedBirthdayCake)
->->

=== LookAtMoneyClip ===
Player: A clip containing 200 dollars.
->->

=== LookAtLockpick ===
Player: A set of lockpicks embedded in a Swiss knife.
->->

=== LookAtScrewdriver ===
Player: A regular screwdriver. It screws with the screws.
->->

=== LookAtEmptyPainkillers ===
Player: A bottle of painkillers. It's empty.
->->

=== LookAtPainkillers ===
Player: A bottle of painkillers.
->->

=== LookAtBleach ===
Player: A very strong cleaning product.
->->

=== LookAtCheapWatch ===
Player: A cheap watch with a fancy leather strap. It's not fooling anyone.
->->

=== LookAtBloodBag ===
Player: A bag of blood. Type O negative.
->->

=== LookAtBrokenTvRemote ===
Player: A TV remote that doesn't work anymore.
->->

=== LookAtStake ===
Player: A wooden stake used to paralyze vampires.
->->

=== LookAtOldBible ===
Player: A very old Bible.
->->

=== LookAtTinyKey ===
Player: A tiny key.
->->

=== LookMariansDiary ===
Player: It's Marian's diary. The last entry says she was running away from the most vicious vampire in Santa Monica... Tess Voermann.
->->

=== LookAtEmptyCanOfGas ===
Player: A cannister of gasoline. It's empty.
->->

=== LookAtBirthdayCake ===
Player: A birthday cake. It's made of cholocate.
->->

=== LookAtIntoxicatedBirthdayCake ===
Player: A birthday cake. Now with extra of poison.
->->

=== LookAtLaxatives ===
Player: A bottle of strong laxatives.
->->

=== LookAtNewspaper ===
Player: "Carnival of Death. Gruesome remains found in the Santa Monica pier".
Player: Interesting. Maybe I should look into it.
->->

=== LookAtGoldenWatch ===
Player: A fancy golden watch.
->->

=== LookAtWrench ===
Player: A 9/16 wrench.
->->

=== LookAtCharityBox ===
Player: A metal box with the charity funds of Lilith's art exhibition.
->->

=== LookAtCdRom ===
Player: A compact-disc with very sensitive computer data.
->->

=== LookAtCarltonsNote ===
Player: A note from Carlton - "If something happens to me, retrieve this CD-ROM to Arthur Killpatrick".
->->

=== LookAtKatana ===
Player: A traditional samurai sword.
->->

=== LookAtBigScissors ===
Player: A big pair of metallic scissors. They're really sharp.
->->

=== LookAtMillionaireDress ===
Player: The hanger contains a pretty elegant dress. It has to worth a real fortune.
->->

=== LookAtSleepingPills ===
Player: A bottle of sleep-inducing pills.
->->

=== LookAtStethoscope ===
Player: A medical instrument for listening the internal sounds of the body.
->->

=== LookAtBucketOfYellowPaint ===
Player: A bucket full of yellow paint.
->->

=== LookAtFakeGoldenWatch ===
Player: A golden watch with a golden leather strip. This may be fooling someone.
->->

=== LookAtLillysDiary ===
Player: It's the diary of Lilly. The last entry talks about the turning of Elliot.
Player: "I love him more that anything in the world, but maybe it was a mistake to make him a vampire".  
->->

=== LookAtPlasticTubeWithFunnel ===
Player: A plastic tube attached to a big funnel.
->->

=== LookAtFrisbee ===
Player: A flying disc of plastic somebody lost in the sand.
->->

=== LookAtCrowbar ===
Player: A crowbar.
->->

=== LookAtCanOfGas ===
Player: A can full of gasoline.
->->

=== LookAtBaseballBat ===
Player: A wooden baseball bat.
->->

=== LookAtTvRemote ===
Player: A TV remote.
->->

=== LookAtDiamondRing ===
Player: A fancy diamond ring.
->->

=== LookAtAstrolite ===
Player: A very powerful home-made explosive.
->->

=== LookAtMoneyBag ===
Player: A bag full of money.
->->

=== LookAtEmptyCoffeeThermos ===
Player: A thermos for coffee. It's painfully empty.
->->

=== LookAtRatPoison ===
Player: A bottle of poison for rats. In small dosis it's not deadly for humans.
->->

=== LookAtJarOfWeed ===
Player: A jar full of marihuana.
->->

=== LookAtVialOfBlood ===
Player: A vial of blood with the Vampire's Kiss.
->->

=== LookAtDiabolicPropaganda ===
Player: Some pages of demented propaganda about the end of the world, the coming of Satan and other demonic entities.
->->

=== LookAtOldDiabolicBible ===
Player: An old Bible with demonic pages inserted into it.
->->

=== LookAtSteak ===
Player: A fine, raw steak.
->->

=== LookAtBoxOfDonuts ===
Player: A box full of delicious donuts.
->->

=== LookAtCoffeeThermos ===
Player: A thermos full of hot coffee. Ideal for staying up all night.
->->

=== LookAtIntoxicatedSteak ===
Player: A fine poisoned steak.
->->

=== LookAtIntoxicatedBoxOfDonuts ===
Player: A box full of delicious and poisoned donuts.
->->

=== LookAtIntoxicatedCoffeeThermos ===
Player: A thermos full of poisoned hot coffee. Ideal for staying up all night with terrible abdominal pain.
->->

=== LookAtPileOfCoins ===
Player: A pile of coins.
->->

=== LookAtTipJar ===
Player: A jar full of coins.
->->

=== LookAtKnife ===
Player: A knife. Sharp and pointy.
->->

=== LookAtLillysPhoto ===
Player: A pollaroid of Lilly and Elliot.
->->

=== LookAtLillysKey ===
Player: The key of Lilly's car.
->->

=== LookAtPlumberBrosFlyer ===
Player: It's a flyer from the Plumber Bros. "If you need some plumbing. Call us now!".
->->

=== LookAtPhilosophyBook1 ===
Player: 'Being and Nothingness', an essay on phenomenological ontology by Jean-Paul Sartre.
->->

=== LookAtPhilosophyBook2 ===
Player: 'Beyond Good and Evil', a prelude to a philosophy of the future by Friedrich Nietzsche.
->->

=== LookAtPhilosophyBook3 ===
Player: 'Tao Te Ching', the speculations on metaphysics, polity, and morality of the old philosopher Laozi.
->->

=== LookAtVodkaBottle ===
Player: A fine bottle of vodka.
->->

=== LookAtSkillBook2 ===
Player: It's a red book titled 'The Art of Not Asking Twice'. It teaches all the secrets about intimidation.
->->

=== LookAtSkillBook4 ===
Player: A red book titled 'Talking It Through'. It teaches all the secrets about persuasion.
->->

=== LookAtSkillBook5 ===
Player: It's a red book titled 'The Art of Seduction'. It teaches all the secrets about seduction.
->->

=== LookAtSkillBook6 ===
Player: A red book titled 'The Tao of Jeet Kune Do'. It teaches all the secrets about kicking asses.
->->

=== LookAtSkillBook7 ===
Player: It's a red book titled 'Thievery for Dummies'. It teaches all the secrets about stealing things.
->->

=== LookAtSkillBook8 ===
Player: 'Hacking 101'. A red book that teaches all the secrets about hacking into computers.
->->

=== LookAtSkillBook9 ===
Player: It's a red book titled 'Locks & Picks'. It teaches all the secrets about lockpiking.
->->

=== LookAtSkillBook10 ===
Player: A red book titled 'The Young Economist'. It teaches all the secrets about bribing.
->->
