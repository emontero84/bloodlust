=== KickSuccessful ===
~PlayerAnimation("Kick")
~PlaySound("KICK")
Player: {Boom|Kaboom|Oh, Yeah}!
->->

=== StealSuccessful ===
~PlayerAnimation("Interact")
~PlaySound("STEAL")
Player: {Phew|Shhh|Oh-la-la}!
->->

=== LockpickingSuccessful ===
~PlayerAnimation("Interact")
~PlaySound("STEAL")
Player: {Bingo|Score|Jackpot}!
->->

=== BribeSuccessful ===
~PlayerAnimation("Interact")
~PlaySound("STEAL")
Player: {Mmmh|Lovely|Oh-la-la}!
->->