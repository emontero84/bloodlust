INCLUDE Inventory.ink
INCLUDE Hacking.ink
INCLUDE Skills.ink
INCLUDE Rooms/Intro.ink
INCLUDE Rooms/Apartment101.ink
INCLUDE Rooms/Apartment102.ink
INCLUDE Rooms/LodgingHallway.ink
INCLUDE Rooms/MainStreet.ink
INCLUDE Rooms/Pawnshop.ink
INCLUDE Rooms/Hospital.ink
INCLUDE Rooms/HospitalOffice.ink
INCLUDE Rooms/NoirGallery.ink
INCLUDE Rooms/SuitesHallway.ink
INCLUDE Rooms/SuitesMercurius.ink
INCLUDE Rooms/SuitesCarlton.ink
INCLUDE Rooms/ParkingGarage.ink
INCLUDE Rooms/Beach.ink
INCLUDE Rooms/BeachHouseExterior.ink
INCLUDE Rooms/BeachHouseInterior.ink
INCLUDE Rooms/SecondStreet.ink
INCLUDE Rooms/Bailbonds.ink
INCLUDE Rooms/MadhouseClub.ink
INCLUDE Rooms/Elevator.ink
INCLUDE Rooms/MadhouseOffice.ink
INCLUDE Rooms/Diner.ink
INCLUDE Rooms/Silo.ink
INCLUDE Characters/Jack.ink
INCLUDE Characters/OfficerChunk.ink
INCLUDE Characters/OfficerCarter.ink
INCLUDE Characters/Dog.ink
INCLUDE Characters/HomelessDoug.ink
INCLUDE Characters/JimmyJohnson.ink
INCLUDE Characters/Claire.ink
INCLUDE Characters/MrD.ink
INCLUDE Characters/Trip.ink
INCLUDE Characters/OldLady.ink
INCLUDE Characters/Alpha.ink
INCLUDE Characters/SecurityGuard.ink
INCLUDE Characters/Bullock.ink
INCLUDE Characters/Rose.ink
INCLUDE Characters/Cooper.ink
INCLUDE Characters/Jules.ink
INCLUDE Characters/Elliot.ink
INCLUDE Characters/Roberta.ink
INCLUDE Characters/Mercurius.ink
INCLUDE Characters/Kev.ink
INCLUDE Characters/Jay.ink
INCLUDE Characters/Dennis.ink
INCLUDE Characters/Receptionist.ink
INCLUDE Characters/Doctor.ink
INCLUDE Characters/DetectiveCollins.ink
INCLUDE Characters/Photographer.ink
INCLUDE Characters/OfficerJohnson.ink
INCLUDE Characters/YoungButler.ink
INCLUDE Characters/OldButler.ink
INCLUDE Characters/MiamiViceGuy.ink
INCLUDE Characters/Ballerina.ink
INCLUDE Characters/MissYang.ink
INCLUDE Characters/Cowboy.ink
INCLUDE Characters/Shadow.ink
INCLUDE Characters/Dori.ink
INCLUDE Characters/Exorcist.ink
INCLUDE Characters/Cook.ink
INCLUDE Characters/JulietteVoermann.ink
INCLUDE Characters/TessVoermann.ink
INCLUDE Characters/Barman.ink
INCLUDE Characters/OldCop.ink
INCLUDE Characters/MetalGirl.ink
INCLUDE Characters/RichGirl.ink
INCLUDE Characters/GothGirl.ink
INCLUDE Characters/Doomsayer.ink
INCLUDE Characters/ArthurKillpatrick.ink
INCLUDE Characters/BernardThung.ink

// Initialization
VAR IsGameStarted = 0

// Character data
VAR IsFemale = 1
LIST PlayerName = (Ash), Cat, Matt, Stuart, Chloe, Gianna, Jess

// Skills
VAR SkillIntimidate = 1
VAR SkillPersuade = 1
VAR SkillSeduce = 1
VAR SkillKick = 1
VAR SkillSteal = 1
VAR SkillHack = 1
VAR SkillLockpicking = 1
VAR SkillBribe = 1

LIST Rooms = 
	(Room_Introduction), Room_Apartment101, Room_Apartment102, Room_LodgingHallway, Room_MainStreet, Room_Pawnshop, Room_NoirGallery, Room_Hospital, Room_HospitalOffice, Room_ParkingGarage, Room_SuitesHallway, Room_SuitesMercurius, Room_SuitesCarlton, Room_Beach, Room_BeachHouseExterior, Room_BeachHouseInterior, Room_SecondStreet, Room_Bailbonds, Room_MadhouseClub, Room_Elevator, Room_MadhouseOffice, Room_Diner, Room_Silo
	
LIST RoomItemsToShow = (Introduction_Executor), (Introduction_Progenitor), Introduction_ExecutorAnimation, Introduction_FadeIn, Introduction_FadeOut, Introduction_FadeEnd,
	(Apartment101_Door), Apartment101_DoorOpen, (Apartment101_Fridge), Apartment101_FridgeOpen, (Apartment101_KitchenCabinet), Apartment101_KitchenCabinetOpen, (Apartment101_TV), Apartment101_TVTurnedOn, (Apartment101_Stereo), Apartment101_StereoTurnedOn, (Apartment101_TopDrawer), (Apartment101_BottomDrawer), Apartment101_TopDrawerOpen, Apartment101_BottomDrawerOpen, (Apartment101_MoneyClip), Apartment101_Bleach, (Apartment101_CheapWatch), Apartment101_BloodBag, (Apartment101_EmptyPainkillers), (Apartment101_Note), (Apartment101_Laptop), Apartment101_LaptopOn, (Apartment101_Bed), (Apartment101_Pillow), (Apartment101_Jack),
	(Apartment102_ExitDoor), Apartment102_ExitDoorOpen, (Apartment102_DetectiveCollins), (Apartment102_Photographer), (Apartment102_OfficerJohnson), (Apartment102_BurnedBody), (Apartment102_Stake), (Apartment102_TopDrawer), Apartment102_TopDrawerOpen, (Apartment102_BottomDrawer), Apartment102_BottomDrawerOpen, (Apartment102_Phone), (Apartment102_Notebook), (Apartment102_OldBible), (Apartment102_Ashtray), (Apartment102_KitchenCabinet), Apartment102_KitchenCabinetOpen, Apartment102_EmptyCanOfGas, (Apartment102_Fridge), Apartment102_FridgeOpen, Apartment102_BirthdayCake, (Apartment102_ToiletCabinet), Apartment102_ToiletCabinetOpen, Apartment102_Laxatives,
	(LodgingHallway_Apartment101Door), LodgingHallway_Apartment101DoorOpen, (LodgingHallway_Apartment102Door), LodgingHallway_Apartment102DoorOpen, (LodgingHallway_ExitDoor), LodgingHallway_ExitDoorOpen, (LodgingHallway_Newspaper), (LodgingHallway_PoliceLine),  (LodgingHallway_OfficerCarter), (LodgingHallway_OfficerChunk), (LodgingHallway_Plant), (LodgingHallway_LightBulb), (LodgingHallway_MailBoxes),
	MainStreet_SuitesHallwayDoorOpen, (MainStreet_LodgingDoor), MainStreet_LodgingDoorOpen, (MainStreet_PawnshopDoor), MainStreet_PawnshopDoorOpen, MainStreet_DoorToSantaMonicaPalace, (MainStreet_SantaMonicaPalace), (MainStreet_DoorToParkingGarage), MainStreet_DoorToNoirGallery, MainStreet_NoirGalleryDoorOpen, (MainStreet_HospitalDoor), MainStreet_HospitalDoorOpen, (MainStreet_Dog), (MainStreet_DogLeash), MainStreet_DogRunningAnimation, MainStreet_DogSleepingAnimation, (MainStreet_FireHydrant), (MainStreet_HomelessDoug), (MainStreet_MrD), MainStreet_MrDOpen, (MainStreet_Claire), (MainStreet_JimmyJohnson), MainStreet_PlumberBros, MainStreet_Taxi, MainStreet_TaxiLeavingAnimation,
	(Pawnshop_PawnshopDoor), (Pawnshop_BigScissors), (Pawnshop_Trip), (Pawnshop_OldLady), Pawnshop_OldLadyLeavingAnimation, (Pawnshop_Dress), (Pawnshop_OldTV), (Pawnshop_ChineseJar), (Pawnshop_TableClock),
	(Hospital_ExitDoor), Hospital_ExitDoorOpen, (Hospital_DoctorsDoor), Hospital_DoctorsDoorOpen, (Hospital_Receptionist), Hospital_ReceptionistLeavingAnimation, (Hospital_Patients), (Hospital_Tv), (Hospital_Painkillers), (Hospital_MedicalDegrees),  
	(HospitalOffice_ExitDoor), (HospitalOffice_Doctor), (HospitalOffice_Stethoscope), (HospitalOffice_MedicalDiploma), (HospitalOffice_HospitalBed),
	(NoirGallery_ExitDoor), NoirGallery_ExitDoorOpen, (NoirGallery_Guest1), (NoirGallery_Guest2), (NoirGallery_Guest3), (NoirGallery_Guest4), (NoirGallery_Guest5), (NoirGallery_OldButler), (NoirGallery_YoungButler), (NoirGallery_Catering), (NoirGallery_Painting1), NoirGallery_Painting1Slash, (NoirGallery_Painting2), NoirGallery_Painting2Slash, (NoirGallery_Painting3), NoirGallery_Painting3Slash, (NoirGallery_Painting4), NoirGallery_Painting4Slash, (NoirGallery_CharityBox), (NoirGallery_Pc), NoirGallery_PcOn, (NoirGallery_PostIt), (NoirGallery_PileOfBooks), (NoirGallery_TopDrawer), NoirGallery_TopDrawerOpen, (NoirGallery_BottomDrawer), NoirGallery_BottomDrawerOpen, NoirGallery_GuestsLeavingAnimation,
	(ParkingGarage_DoorToMainStreet), (ParkingGarage_DoorToSecondStreet), (ParkingGarage_DoorToTheBeach), ParkingGarage_DoorToTheBeachOpen, (ParkingGarage_SecurityGuard), (ParkingGarage_Alpha), ParkingGarage_AlphaOpen, (ParkingGarage_Bullock), (ParkingGarage_BucketOfYellowPaint), (ParkingGarage_Motorbike), (ParkingGarage_RedCar), ParkingGarage_RedCarOpen, (ParkingGarage_GreenCar), (ParkingGarage_BlueCar), (ParkingGarage_PinkCar),
	(SuitesHallway_ExitDoor), SuitesHallway_ExitDoorOpen, (SuitesHallway_MercuriusDoor), SuitesHallway_MercuriusDoorOpen, (SuitesHallway_CarltonDoor), SuitesHallway_CarltonDoorOpen, (SuitesHallway_BloodStains), (SuitesHallway_Painting), (SuitesHallway_Roberta),
	(SuitesMercurius_ExitDoor), (SuitesMercurius_Mercurius), (SuitesMercurius_Phone), (SuitesMercurius_BloodTrail), (SuitesMercurius_VodkaBottle1), (SuitesMercurius_Cabinet), SuitesMercurius_CabinetOpen,
	(SuitesCarlton_ExitDoor), (SuitesCarlton_OldPicture), SuitesCarlton_Safe, SuitesCarlton_SafeOpen, SuitesCarlton_CdRom, SuitesCarlton_CarltonsNote, (SuitesCarlton_Laptop), SuitesCarlton_LaptopOn, (SuitesCarlton_CarltonsBody), (SuitesCarlton_Katana), (SuitesCarlton_Chest), SuitesCarlton_ChestOpen, (SuitesCarlton_Cabinet), SuitesCarlton_CabinetOpen, (SuitesCarlton_MiniBar), SuitesCarlton_MiniBarOpen, SuitesCarlton_CarltonsDiary, (SuitesCarlton_PileOfBooks),
	(Beach_DoorToParkingGarage), Beach_DoorToBeachHouse, (Beach_Cooper), (Beach_Elliot), (Beach_Jules), (Beach_Rose), (Beach_Stereo), (Beach_CrimeScene), (Beach_Frisbee), (Beach_MetalGate), Beach_MetalGateOpen,
	(BeachHouseExterior_DoorToBeach), (BeachHouseExterior_DoorToBeachHouse), BeachHouseExterior_DoorToBeachHouseOpen, (BeachHouseExterior_Kev), BeachHouseExterior_KevOpen, (BeachHouseExterior_Van), (BeachHouseExterior_VanDoor), BeachHouseExterior_VanDoorOpen, BeachHouseExterior_Crowbar, (BeachHouseExterior_GasDeposit), BeachHouseExterior_GasDepositOpen, BeachHouseExterior_GasCan, BeachHouseExterior_PlasticTubeWithFunnel, (BeachHouseExterior_Bush), BeachHouseExterior_BushOpen, BeachHouseExterior_BushOpenPowerSwitch, BeachHouseExterior_BushOpenPowerOff, (BeachHouseExterior_LightsOn), BeachHouseExterior_LightsOff, (BeachHouseExterior_Fence), (BeachHouseExterior_BaseballBat),
	(BeachHouseInterior_ExitDoor), (BeachHouseInterior_KitchenCabinet), BeachHouseInterior_KitchenCabinetOpen, BeachHouseInterior_JarOfWeed, (BeachHouseInterior_Pc), BeachHouseInterior_PcOn, (BeachHouseInterior_Posters),(BeachHouseInterior_Jay), (BeachHouseInterior_Tv), (BeachHouseInterior_TvRemote), (BeachHouseInterior_RatPoison), (BeachHouseInterior_Food), (BeachHouseInterior_Dennis), (BeachHouseInterior_Safe), BeachHouseInterior_SafeOpen, (BeachHouseInterior_EmptyCoffeeThermos), (BeachHouseInterior_Vent), BeachHouseInterior_VentOpen, BeachHouseInterior_MoneyBag, (BeachHouseInterior_WashingMachine), BeachHouseInterior_WashingMachineMoved, (BeachHouseInterior_Astrolite),
	(SecondStreet_GarageDoor), (SecondStreet_MadhouseClubDoor), SecondStreet_MadhouseClubDoorOpen, (SecondStreet_DinerDoor), SecondStreet_DinerDoorOpen, (SecondStreet_BailbondsDoor), SecondStreet_BailbondsDoorOpen, (SecondStreet_SiloDoor), SecondStreet_SiloDoorOpen, (SecondStreet_Padlock), (SecondStreet_FenceSign), (SecondStreet_Doomsayer),
	(Bailbonds_ExitDoor), (Bailbonds_Fridge), Bailbonds_FridgeOpen, Bailbonds_Steak, Bailbonds_VodkaBottle3, (Bailbonds_Darts), (Bailbonds_FileCabinet), Bailbonds_FileCabinetOpen, (Bailbonds_Pc), Bailbonds_PcOn, (Bailbonds_PostIt), (Bailbonds_OldCalendar), (Bailbonds_Note), (Bailbonds_TopDrawer), Bailbonds_TopDrawerOpen, (Bailbonds_BottomDrawer), Bailbonds_BottomDrawerOpen, (Bailbonds_BoxOfDonuts), (Bailbonds_CoffeeMachine), (Bailbonds_Pizza), (Bailbonds_PileOfBooks), (Bailbonds_ArthurKillpatrick),
	(MadhouseClub_ExitDoor), MadhouseClub_ExitDoorOpen, MadhouseClub_ElevatorDoors, (MadhouseClub_ElevatorDoorsClosed), MadhouseClub_ElevatorDoorsOpen, (MadhouseClub_VodkaBottle2), (MadhouseClub_Barman), (MadhouseClub_OldCop), (MadhouseClub_MetalBoy), (MadhouseClub_MetalGirl), (MadhouseClub_GothBoy), (MadhouseClub_RichGirl), (MadhouseClub_GothGirl), (MadhouseClub_PunkBoy), (MadhouseClub_KissingCouple), (MadhouseClub_DancePit), (MadhouseClub_Arcade), MadhouseClub_ArcadeOpen, MadhouseClub_PileOfCoins, (MadhouseClub_SkillBook), 
	(Elevator_Doors), Elevator_DoorsOpen, (Elevator_Button), Elevator_ButtonPressed, Elevator_WaitingAnimation,
	(MadhouseOffice_ExitDoor), MadhouseOffice_ExitDoorOpen, MadhouseOffice_JulietteVoermann, MadhouseOffice_TessVoermann, (MadhouseOffice_Painting), (MadhouseOffice_JulietteLaptop), MadhouseOffice_JulietteLaptopOn, (MadhouseOffice_TessPc), MadhouseOffice_TessPcOn, (MadhouseOffice_PostIt), (MadhouseOffice_Lipstick), (MadhouseOffice_DeskDrawer), MadhouseOffice_DeskDrawerOpen, (MadhouseOffice_TopDrawer), MadhouseOffice_TopDrawerOpen, (MadhouseOffice_BottomDrawer),MadhouseOffice_BottomDrawerOpen, (MadhouseOffice_PhilosophyBook3),
	(Diner_ExitDoor), (Diner_Dori), (Diner_Cook), (Diner_Exorcist), (Diner_TipJar), (Diner_PublicPhone), (Diner_Jukebox), (Diner_Newspaper), (Diner_Counter), 
	(Silo_Exit), (Silo_BernardThung), (Silo_Pc), Silo_PcOn, (Silo_ActionFigure)
	
LIST Quests = 
	FirstSteps, AstroliteQuest, PainkillersQuest, DogQuest, DrugTrip, OldLadyQuest, ClaireQuest, VodkaQuest, BullockQuest, ElliotQuest, CooperQuest, RoseQuest, JulesQuest,
	FindBernardThungQuest, BarmanQuest, JulietteQuest, TessQuest, VoermannDilemmaQuest, BernardThungQuest, HackingQuest, ArthurKillpatrickQuest, WarehouseQuest, SkillBooksQuest
	
LIST QuestSteps = 
	FirstSteps_1, FirstSteps_2, FirstSteps_3, FirstSteps_4,
	AstroliteQuest_1, AstroliteQuest_2, AstroliteQuest_3, AstroliteQuest_4,
	PainkillersQuest_1, PainkillersQuest_2, 
	DogQuest_1, 
	DrugTrip_1, DrugTrip_2,
	OldLadyQuest_1, OldLadyQuest_2,
	ClaireQuest_1, ClaireQuest_2, ClaireQuest_3,
	VodkaQuest_1, VodkaQuest_2, VodkaQuest_3, 
	BullockQuest_1, BullockQuest_2, BullockQuest_3,
	ElliotQuest_1, ElliotQuest_2, ElliotQuest_3,
	CooperQuest_1, CooperQuest_2, CooperQuest_3, CooperQuest_4,
	RoseQuest_1, RoseQuest_2, RoseQuest_3,
	JulesQuest_1, JulesQuest_2,
	FindBernardThungQuest_1, FindBernardThungQuest_2, FindBernardThungQuest_3,
	BarmanQuest_1, BarmanQuest_2, BarmanQuest_3, BarmanQuest_4,
	JulietteQuest_1, JulietteQuest_2, JulietteQuest_3, JulietteQuest_4,
	TessQuest_1, TessQuest_2, TessQuest_3, TessQuest_4,
	VoermannDilemmaQuest_1, VoermannDilemmaQuest_2, VoermannDilemmaQuest_3,
	BernardThungQuest_1, BernardThungQuest_2,
	HackingQuest_1, HackingQuest_2, HackingQuest_3,
	ArthurKillpatrickQuest_1, ArthurKillpatrickQuest_2, ArthurKillpatrickQuest_3,
	SkillBooksQuest_1, SkillBooksQuest_2, SkillBooksQuest_3, SkillBooksQuest_4

// Starting the story
-> ChoosePlayerCharacter

=== StartGame ===
~GoTo(Room_Apartment101)
-> Apartment101

=== ChoosePlayerCharacter ===
* [ Gianna ] -> ChooseGianna
* [ Matt ] -> ChooseMatt
* [ Cat ] -> ChooseCat
* [ Ash ] -> ChooseAsh
* [ Chloe ] -> ChooseChloe
* [ Jess ] -> ChooseJess
* [ Stuart ] -> ChooseStuart

=== ChooseAsh ===
~IsFemale = 1
~PlayerName = (Ash)
~SkillIntimidate = 1
~SkillPersuade = 0
~SkillSeduce = 0
~SkillKick = 0
~SkillSteal = 0
~SkillHack = 0
~SkillLockpicking = 0
~SkillBribe = 0
-> Introduction

=== ChooseCat ===
~IsFemale = 1
~PlayerName = (Cat)
~SkillIntimidate = 0
~SkillPersuade = 0
~SkillSeduce = 0
~SkillKick = 0
~SkillSteal = 0
~SkillHack = 0
~SkillLockpicking = 1
~SkillBribe = 0
-> Introduction

=== ChooseMatt ===
~IsFemale = 0
~PlayerName = (Matt)
~SkillIntimidate = 0
~SkillPersuade = 1
~SkillSeduce = 0
~SkillKick = 0
~SkillSteal = 0
~SkillHack = 0
~SkillLockpicking = 0
~SkillBribe = 0
-> Introduction

=== ChooseStuart ===
~IsFemale = 0
~PlayerName = (Stuart)
~SkillIntimidate = 0
~SkillPersuade = 0
~SkillSeduce = 0
~SkillKick = 0
~SkillSteal = 0
~SkillHack = 1
~SkillLockpicking = 0
~SkillBribe = 0
-> Introduction

=== ChooseChloe ===
~IsFemale = 1
~PlayerName = (Chloe)
~SkillIntimidate = 0
~SkillPersuade = 0
~SkillSeduce = 1
~SkillKick = 0
~SkillSteal = 0
~SkillHack = 0
~SkillLockpicking = 0
~SkillBribe = 0
-> Introduction

=== ChooseGianna ===
~IsFemale = 1
~PlayerName = (Gianna)
~SkillIntimidate = 0
~SkillPersuade = 0
~SkillSeduce = 0
~SkillKick = 0
~SkillSteal = 0
~SkillHack = 0
~SkillLockpicking = 0
~SkillBribe = 1
-> Introduction

=== ChooseJess ===
~IsFemale = 1
~PlayerName = (Jess)
~SkillIntimidate = 0
~SkillPersuade = 0
~SkillSeduce = 0
~SkillKick = 1
~SkillSteal = 1
~SkillHack = 0
~SkillLockpicking = 0
~SkillBribe = 0
-> Introduction

// Room management
=== function GoTo(x) ===
~Rooms = x
~GoToRoom(x)

=== function Show(x) ===
~RoomItemsToShow += x
~ShowItem(x)

=== function Hide(x) ===
~RoomItemsToShow -= x
~HideItem(x)

// Inventory management
=== function AddToInventory(x) ===
~Inventory += x
~AddItemToInventory(x)

=== function RemoveFromInventory(x) ===
~Inventory -= x
~RemoveItemFromInventory(x)

=== function HasInInventory(x) ===
~return Inventory ? x

// Quest management
=== function AddQuest(x) ===
~Quests += x
~AddQuestToLog(x)

=== function UpdateQuest(x,y) ===
~QuestSteps += y
~UpdateQuestToLog(x, y)

=== function HasQuest(x) ===
~return Quests ? x

=== function IsQuestStepUpdated(x) ===
~return QuestSteps ? x

// External functions
EXTERNAL AddItemToInventory(x)
=== function AddItemToInventory(x) ===
~return x

EXTERNAL RemoveItemFromInventory(x)
=== function RemoveItemFromInventory(x) ===
~return x

EXTERNAL ShowItem(x)
=== function ShowItem(x) ===
~return x

EXTERNAL HideItem(x)
=== function HideItem(x) ===
~return x

EXTERNAL GoToRoom(x)
=== function GoToRoom(x) ===
~return x

EXTERNAL AddQuestToLog(x)
=== function AddQuestToLog(x) ===
~return x

EXTERNAL UpdateQuestToLog(x, y)
=== function UpdateQuestToLog(x, y) ===
~return x

EXTERNAL PlaySound(x)
=== function PlaySound(x) ===
~return x

EXTERNAL StopSound(x)
=== function StopSound(x) ===
~return x

EXTERNAL PlayerAnimation(x)
=== function PlayerAnimation(x) ===
~return x

EXTERNAL Wait(x)
=== function Wait(x) ===
~return x