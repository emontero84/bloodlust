=== TalkToRoberta ===
Roberta: Good evening, {IsFemale:madam}{not IsFemale:sir}.
-> Roberta

=== Roberta ===
#choice
* [ I'm looking for Mercurius. Do you know where to find him? ] -> RobertaMercurius
* [ What are you doing? ] -> RobertaDoing
* [ Tell me more about yourself. ] -> RobertaHerself
+ [ I'm outta here. ] -> SuitesHallway

=== RobertaMercurius ===
Roberta: Of course! Just because I'm cleaning the floor that probably means I have nothing better to do than give you directions. #portrait:angry
Player: I didn't mean it that way.
Roberta: Don't worry. In fact, I have nothing better to do. First room on the right. The hotel only has two rooms so it's difficult to miss.
-> Roberta

=== RobertaDoing ===
Roberta: I'm working on the thermonuclear fusion of the atom. What does it look like? #portrait:angry
Roberta: I'm cleaning the blood off the floor.
-> Roberta

=== RobertaHerself ===
Roberta: I can tell you about my PhD in Philosophy and how it turned out it's worth nothing once you step into America. #portrait:angry
Roberta: But it's not a pretty story. Maybe some other time. I have a lot of cleaning to do.
-> Roberta

=== GiveBleachToRoberta ===
Roberta: Thanks! It's offensive and useful at the same time. #portrait:smile
~RemoveFromInventory(Bleach)
~BloodStainsCleaned = 1
~Hide(SuitesHallway_BloodStains)
Roberta: Here, let me give you something in return. Food for thought. #portrait:smile
~AddToInventory(PhilosophyBook1)
-> SuitesHallway