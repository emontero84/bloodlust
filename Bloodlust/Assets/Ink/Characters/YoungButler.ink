=== TalkToYoungButler ===
Young Butler: Do you want some croquettes, {IsFemale:madam}{not IsFemale:mesieur}? #portrait:smile
-> YoungButler

=== YoungButler ===
#choice
* [ I'm not into croquettes. ] -> YoungButlerNoCroquettes
* { TalkToOldButler } [ Aren't you a younger version of the other butler? ] -> YoungButlerOldButler
* { YoungButlerOldButler } [ Is your father a butler too? ] -> YoungButlerFather
* [ How's the night going? ] -> YoungButlerGossip
* { YoungButlerGossip } [ What do you mean by not interested in art? ] -> YoungButlerGossipMore
+ [ I'm outta here. ] -> NoirGallery

=== YoungButlerNoCroquettes ===
Young Butler: They're made of exquisite Serrano ham. #portrait:smile
-> YoungButler

=== YoungButlerOldButler ===
Young Butler: Well, he's my grampa. We come from a long tradition of butlers.
-> YoungButler

=== YoungButlerFather ===
Young Butler: Nah, our family tradition skips one generation.
-> YoungButler

=== YoungButlerGossip ===
Young Butler: Well, you know how these events usually go. 
Young Butler: Lots of people but very few are really interested in art, if you know what I mean. #portrait:smile
-> YoungButler

=== YoungButlerGossipMore ===
Young Butler: You know, some guests are more interested in... being at the gallery.
Young Butler: It's just another social event for them.
-> YoungButler