
VAR BoxOfDonutsGiven = 0

=== TalkToChunk ===
Officer Chunk: Move along, {IsFemale:madam}{not IsFemale:sir}. *GROWL* There's nothing to see here. 
-> Chunk

=== Chunk ===
#choice
* { not ChunkInvestigation } [ May I ask what is going on? I live in the next apartment. ] -> ChunkInvestigation
* { not ChunkInvestigation } [ Good evening, officer. May I ask what is going on? ] -> ChunkInvestigation
* { ChunkInvestigation } [ I understand. And how long is this investigation gonna take? ] -> ChunkHowLong
* [ What was that sound? ] -> ChunkStomachGrowl
* { ChunkHowLong || ChunkStomachGrowl } [ Maybe I can bring you something to eat? ] -> ChunkSomethingToEat
* { HasQuest(TessQuest) && not IsQuestStepUpdated(TessQuest_1) && not BoxOfDonutsGiven && ChunkInvestigation && SkillSeduce } [ (Seduction) Love your uniform, officer. Can I give you a kiss? ] -> SeduceOfficerChunkInit
* [ Tell me about Santa Monica. ] -> ChunkSantaMonica
+ [ Okay, I'm outta here. ] -> LodgingHallway

=== ChunkInvestigation ===
Officer Chunk: I'm sorry, {IsFemale:madam}{not IsFemale:sir}, but I'm not allowed to discuss the details of the ongoing investigation. *GROWL* #portrait:smile
-> Chunk

=== ChunkHowLong ===
Officer Chunk: I don't really know, we're only following orders. But we've been here since lunchtime and my belly is beginning to growl.
-> Chunk

=== ChunkStomachGrowl ===
Officer Chunk: What sound? *GROWL* I didn't hear anything.
Player: Didn't you hear it? It sounded just like a stomach growl.
Officer Chunk: Maybe it was an EVP. #portrait:smile
-> Chunk

=== ChunkSomethingToEat ===
Officer Chunk: Oh! That would be delightful. *GROWL* #portrait:smile
-> Chunk

=== GiveBoxOfDonutsToOfficerChunk ===
Officer Chunk: Uh, thanks! That's very generous! #portrait:smile
~RemoveFromInventory(BoxOfDonuts)
~BoxOfDonutsGiven = 1
-> CheckBothOfficersAreHappy

=== SeduceOfficerChunkInit ===
Officer Chunk: I'm sorry, {IsFemale:madam}{not IsFemale:sir}. I beg your pardon?
-> SeduceOfficerChunkChoice

=== SeduceOfficerChunkChoice ===
#choice
* [ You heard me already. Wanna give me a kiss, big boy? ] -> SeduceOfficerChunkFail
* [ Come closer, sweetheart. Let me show you something... ] -> SeduceOfficerChunk
* [ I've been so naughty, officer. Will you arrest me? And hold me real tight. ] -> SeduceOfficerChunkFail
* [ You know what I mean, handsome. *WINK* ] -> SeduceOfficerChunkFail

=== SeduceOfficerChunkFail ===
Officer Chunk: I knew the uniform had its own appeal but this is too much... #portrait:angry
Officer Chunk: Move along, {IsFemale:madam}{not IsFemale:sir}! #portrait:angry
-> LodgingHallway

=== SeduceOfficerChunk ===
Officer Chunk: Uh! That tickles! *CHUCKLE* #portrait:smile
Player: I knew you'd like it... come closer... #portrait:smile
Officer Chunk: Whoa! Whoa! Slow down, {IsFemale:madam}{not IsFemale:sir}!
Officer Chunk: I knew the uniform had its own appeal but this is too much...
Officer Chunk: I'll let you in as long as we keep this little incident to ourselves, okay? 
~Apartment102DoorIsOpen = 1
~Hide(LodgingHallway_PoliceLine)
-> LodgingHallway

=== ChunkSantaMonica ===
Officer Chunk: What can I tell you about this beautiful city?
Officer Chunk: There's so much to do. Have you been to Santa Monica beach yet?
Officer Chunk: You can get through the stairs of the parking garage. It's so romantic. #portrait:smile
-> Chunk