=== TalkToDoomsayer ===
{ shuffle:
	- 	Doomsayer: Every second takes us closer to final judgment before the Demon Father!
	- 	Doomsayer: The corruption of the soul is pandemic!
	- 	Doomsayer: The worms are stripping the foulness from the skull of the Earth!
	- 	Doomsayer: There is no hiding for the true face of the Demon Father!
	- 	Doomsayer: I have seen the face of the Demon Father, and it is excited!
	-	Doomsayer: The damned laugh loudest before they lose their heads!
	-	Doomsayer: Hurry on your way to Apocalypse!
	-	Doomsayer: The bones of your unholy ancestors will rise up and reclaim the flesh that they lent their sorry progeny!
	-	Doomsayer: Repent on your sins! The end is near! The Demon Father is hungry for your blood!
}
-> Doomsayer

=== Doomsayer ===
#choice
* [ What are you talking about? ] -> DoomsayerExpainYourself
* { DoomsayerExpainYourself } [ Who is the Demon Father? ] -> DoomsayerDemonFather
* { DoomsayerExpainYourself } [ You're not making any sense. ] -> TalkToDoomsayer
* { DoomsayerExpainYourself } [ Do you really believe in the stuff that you're saying? ] -> DoomsayerExpainYourselfMore
* { DoomsayerExpainYourselfMore } [ You're full of crap. ] -> TalkToDoomsayer
* { DoomsayerExpainYourselfMore } [ That sounds hideously reassuring. Give me a pamphlet. ] -> DoomsayerPamphlet
* { not DoomsayerPamphlet && SkillPersuade } [ (Persuasion) Let's have a little talk about your propaganda. ] -> PersuadeDoomsayerInit
* { HasQuest(BarmanQuest) && MetalGirlWhereToBuyVampireKiss } [ I know you're the pusher of the Vampire's Kiss. ] -> DoomsayerIsThePusher
* { HasQuest(BarmanQuest) && DoomsayerIsThePusher } [ Cut the crap! I know what you're selling. ] -> DoomsayerBusted
* [ Tell me about Santa Monica. ] -> DoomsayerSantaMonica
+ [ Bye. ] -> SecondStreet

=== DoomsayerExpainYourself ===
Doomsayer: We are nothing but unholy worms in the rotten corpse of mankind!
Doomsayer: The Demon Father will devour our souls and cleanse all of our sins! #portrait:angry
-> Doomsayer

=== DoomsayerDemonFather ===
Doomsayer: The cursed one with many names! The whore and the unborned childe, bearer of the Apocalypse!
-> Doomsayer

=== DoomsayerExpainYourselfMore ===
-> TalkToDoomsayer

=== DoomsayerPamphlet ===
Doomsayer: Feel free to pass it on your group of unholy friends and sinners! #portrait:smile
~AddToInventory(DiabolicPropaganda)
-> SecondStreet

=== PersuadeDoomsayerInit ===
Doomsayer: Uh?
-> PersuadeDoomsayerChoice

=== PersuadeDoomsayerChoice ===
#choice
* [ You know that all you say is bullshit, right? *WINK* ] -> PersuadeDoomsayerFail
* [ I'm a secret agent from the Ancient Illuminati from the Outer Space. ] -> PersuadeDoomsayerFail
* [ Aren't all apocalyptic conspiracies just another tool of the oligarchies? ] -> PersuadeDoomsayer
* [ Satan is my master! I'm here on behalf of the Demon Father himself! ] -> PersuadeDoomsayerFail

=== PersuadeDoomsayerFail ===
Doomsayer: Unholy heavens! You're even crazier than me! Ha ha! #portrait:smile
-> SecondStreet

=== PersuadeDoomsayer ===
Doomsayer: ... #portrait:angry
Doomsayer: Maybe you're right...
-> DoomsayerPamphlet

=== DoomsayerIsThePusher ===
Doomsayer: Errr... 
~UpdateQuest(BarmanQuest, BarmanQuest_2)
-> Doomsayer

=== DoomsayerBusted ===
Doomsayer: All right, all right. You got me. I'm selling the devil's blood to the kids in the Madhouse. #portrait:angry
-> DoomsayerBustedChoice

=== DoomsayerBustedChoice ===
#choice
* [ What would the Demon Father think about selling drugs? ] -> DoomsayerStopPushing
* [ You know, winners don't use drugs! ] -> DoomsayerStopPushing
* [ You better stop selling drugs or I'll have to come and teach you lesson. ] -> DoomsayerStopPushing

=== DoomsayerStopPushing ===
Doomsayer: You're right! You're right! I'm so ashamed of myself! #portrait:angry
~UpdateQuest(BarmanQuest, BarmanQuest_3)
-> SecondStreet

=== DoomsayerSantaMonica ===
Doomsayer: This city is a rotten corpse of depraved corruption! #portrait:angry
Doomsayer: Have you been to the beach? You can get there through the stairs in the parking garage. #portrait:smile
-> Doomsayer