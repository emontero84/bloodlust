=== TalkToJack ===
* { not JackFirstEncounter } -> JackFirstEncounter
+ { JackFirstEncounter } -> JackDefault

=== JackFirstEncounter ===
Jack: Hey kiddo, my name's Jack. De la Croix sent me to help you settle in your new apartment. 
-> JackGreetingChoice

=== JackDefault ===
Jack: How you doin', kiddo? #portrait:smile
-> Jack

=== JackGreetingChoice ===
#choice
* [ Hi Jack, nice to meet you. ] -> JackNiceToMeetYou
* [ Fuck off! I'm outta here. ] -> JackFuckOff

=== JackNiceToMeetYou ===
Jack: Well, I'll be around if you need me, all right? #portrait:smile
-> Jack

=== JackFuckOff ===
Jack: Ha ha! You have balls, that's for sure. I'll let you be. #portrait:smile
-> Apartment101

=== Jack ===
#choice
* { NoteRead == 0 } [ What are you doing here? ] -> JackWhatAreYouDoingHere
* { NoteRead == 1 } [ I've read the note. What now? ] -> JackWhatNow
* [ What am I supposed to do now? ] -> JackWhatToDoNow
+ [ I have some questions. ] -> JackQuestions
* { JackWhatToDoNow } [ Can I help you with something? ] -> JackHelpSomething
* { HasQuest(SkillBooksQuest) && HasInInventory(SkillBook2) } [ I have a book about intimidation. ] -> JackGiveSkillBook2
* { HasQuest(SkillBooksQuest) && HasInInventory(SkillBook4) } [ I have a book about persuasion. ] -> JackGiveSkillBook4
* { HasQuest(SkillBooksQuest) && HasInInventory(SkillBook5) } [ I have a book about seduction. ] -> JackGiveSkillBook5
* { HasQuest(SkillBooksQuest) && HasInInventory(SkillBook6) } [ I have a book about kicking. ] -> JackGiveSkillBook6
* { HasQuest(SkillBooksQuest) && HasInInventory(SkillBook7) } [ I have a book about stealing. ] -> JackGiveSkillBook7
* { HasQuest(SkillBooksQuest) && HasInInventory(SkillBook8) } [ I have a book about hacking. ] -> JackGiveSkillBook8
* { HasQuest(SkillBooksQuest) && HasInInventory(SkillBook9) } [ I have a book about lockpiking. ] -> JackGiveSkillBook9
* { HasQuest(SkillBooksQuest) && HasInInventory(SkillBook10) } [ I have a book about bribing. ] -> JackGiveSkillBook10
+ [ I'm outta here. ] -> JackExit

=== JackExit ===
Jack: Alright! See you around. #portrait:smile 
-> Apartment101

=== JackWhatAreYouDoingHere ===
Jack: I'm your vampire babysitter! Ha ha ha! Just kidding. #portrait:smile
Jack: De la Croix sent me to help you start off on your right foot in Santa Monica.
Jack: Also you have to follow his direct orders so I can't let you out until you've read the note on your desk.
-> Jack

=== JackWhatNow ===
Jack: Ha ha! I dunno, kid. Go out a see the world for yourself! I'll be here if you need some advice later on.
-> Jack

=== JackWhatToDoNow ===
Jack: Explore the city, meet the locals, learn your ropes. Have some fun, kiddo! I'm sure you'll find something to do. 
-> Jack
	
=== JackQuestions ===
Jack: Sure. Just shoot. 
-> JackQuestionsChoice

=== JackQuestionsChoice ===
#choice
+ [ Tell me more about the vampires of Santa Monica. ] -> JackVampires
+ [ Tell me more about the factions. ] -> JackFactions
+ [ Let's talk about something else. ] -> Jack

=== JackVampires ===
Jack: Who do you want to talk about?
-> JackVampiresChoice

=== JackVampiresChoice ===
#choice
* [ De la Croix. ] -> TalkAboutDeLaCroix
* { HasQuest(FindBernardThungQuest) || MadhouseOffice } [ Tess. ] -> TalkAboutTessVoermann
* { HasQuest(FindBernardThungQuest) || MadhouseOffice } [ Juliette. ] -> TalkAboutJulietteVoermann
* { HasQuest(FindBernardThungQuest) || SiloDefault } [ Thung. ] -> TalkAboutBernardThung
+ [ That's enough for now. ] -> JackQuestionsChoice

=== JackFactions ===
Jack: Sure. Politics. Ask away.
-> JackFactionsChoice

=== JackFactionsChoice ===
#choice
* [ The Logia of Traditions ] -> Logia
* [ The Blood Cult ] -> BloodCult
* [ The Apostates ] -> Apostates
+ [ That's enough politics for now. ] -> JackQuestionsChoice

=== TalkAboutDeLaCroix ===
Jack: Ha ha! The all mighty king De la Croix. He's the vampire in charge of the city. 
Jack: Or at least he pretends to be. De la Croix is at the same time a tyrant and a puppet in the hands of the Logia. 
-> JackVampires
	
=== TalkAboutTessVoermann ===
Jack: Tess Voermann is the owner of the Asylum club and one of the most influential vampires in the city. 
Jack: She has her hands in almost every dirty business of Santa Monica.
-> JackVampiresChoice

=== TalkAboutJulietteVoermann ===
Jack: Juliette is Tess's twin sister and a hell of a character of her own. 
Jack: Words don't do justice with Juliette so you better pay her a visit at the Asylum club and see for yourself. 
-> JackVampiresChoice
	
=== TalkAboutBernardThung ===
Jack: Bernard Thung is a sneaky sewer rat. Never met the guy in person so I don't have much to say about him. 
-> JackVampiresChoice
	
=== Logia ===
Jack: The Logia is the rightious owner of the vampire traditions, the rules that govern all civilized vampires. 
Jack: They are obsessed with law and order, but often it's all just a cover for their absurd conspiracies. 
-> JackFactionsChoice
	
=== BloodCult ===
Jack: The Cult is like a sect of blood-thirsty vampires worshipping chaos, blood and destruction. #portrait:angry
-> JackFactionsChoice
	
=== Apostates ===
Jack: The Apostates are the misfits of the vampire society. They claim that all vampires are created equal. 
Jack: Which makes a lot of sense when you're losing the war against the other two factions. 
-> JackFactionsChoice

=== JackHelpSomething ===
Jack: Whaddaya mean?
-> JackHelpSomethingChoice

=== JackHelpSomethingChoice ===
#choice
* [ Can I bring you something to eat, maybe? ] -> JackHelpEat
* [ Do you have any errand I could run for you? ] -> JackHelpErrand
* [ You're babysitting a vampire in a motel room. You REALLY need some help. ] -> JackHelpTrapped

=== JackHelpEat ===
Jack: Maybe not in a literal sense but... yeah, I could use some food for thought. #portrait:smile
-> JackQuestExplain

=== JackHelpErrand ===
Jack: Ha! Maybe you're right... and there's an errand you could do for me. #portrait:smile
-> JackQuestExplain

=== JackQuestExplain ===
Jack: You know, I'm building a collection of self-improvement books. 
Jack: If you happen to stumble upon one of them, bring it to me and I'll be eternally grateful. #portrait:smile
-> JackQuestChoice

=== JackHelpTrapped ===
Jack: Ha! You're right! I'm paying a high price for the mistakes I made in the past. #portrait:smile
Jack: But I can't complaint. I'm as happy as a bloodsucker can be. #portrait:smile
-> Jack

=== JackQuestChoice ===
#choice
* [ Of course! I'll do my best. ] -> JackQuestChoiceYes
* [ I'll see what I can do. ] -> JackQuestChoiceYes
* [ Nah, it seems like a total waste of time. ] -> JackQuestChoiceNo

=== JackQuestChoiceYes ===
Jack: Great! Just bring me the books and I'll tell you if they're the ones I'm looking for. #portrait:smile
~AddQuest(SkillBooksQuest)
-> Apartment101

=== JackQuestChoiceNo ===
Jack: All right. You brough it up in the first place, remember? Ha ha! #portrait:smile
-> Jack

=== JackGiveSkillBook2 ===
~RemoveFromInventory(SkillBook2)
~UpdateQuest(SkillBooksQuest, SkillBooksQuest_1)
-> JackReceiveBook

=== JackGiveSkillBook6 ===
~RemoveFromInventory(SkillBook6)
~UpdateQuest(SkillBooksQuest, SkillBooksQuest_1)
-> JackReceiveBook

=== JackGiveSkillBook4 ===
~RemoveFromInventory(SkillBook4)
~UpdateQuest(SkillBooksQuest, SkillBooksQuest_2)
-> JackReceiveBook

=== JackGiveSkillBook5 ===
~RemoveFromInventory(SkillBook5)
~UpdateQuest(SkillBooksQuest, SkillBooksQuest_2)
-> JackReceiveBook

=== JackGiveSkillBook7 ===
~RemoveFromInventory(SkillBook7)
~UpdateQuest(SkillBooksQuest, SkillBooksQuest_3)
-> JackReceiveBook

=== JackGiveSkillBook8 ===
~RemoveFromInventory(SkillBook8)
~UpdateQuest(SkillBooksQuest, SkillBooksQuest_3)
-> JackReceiveBook

=== JackGiveSkillBook9 ===
~RemoveFromInventory(SkillBook9)
~UpdateQuest(SkillBooksQuest, SkillBooksQuest_4)
-> JackReceiveBook

=== JackGiveSkillBook10 ===
~RemoveFromInventory(SkillBook10)
~UpdateQuest(SkillBooksQuest, SkillBooksQuest_4)
-> JackReceiveBook

=== JackReceiveBook ===
Jack: Uh! I was looking forward to reading that book. Thank you! #portrait:smile
-> Apartment101