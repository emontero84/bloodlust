=== TalkToDetectiveCollins ===
+ { not HasQuest(TessQuest) } -> DetectiveCollinsFirstEncounter
* { HasQuest(TessQuest) && not DetectiveCollinsFirstEncounter } -> DetectiveCollinsFirstEncounter
+ { HasQuest(TessQuest) && DetectiveCollinsFirstEncounter } -> DetectiveCollinsDefault

=== DetectiveCollinsFirstEncounter ===
Detective Collins: Hey! What do yo think you're doing here? This is a crime scene! #portrait:angry
-> DetectiveCollinsCrimeSceneChoice

=== DetectiveCollinsDefault ===
Detective Collins: Evening. 
-> DetectiveCollins

=== DetectiveCollinsCrimeSceneChoice ===
#choice
* { HasQuest(TessQuest) } [ I'm here on behalf of Tess Voermann. ] -> DetectiveCollinsCrimeSceneGoAhead
* [ Oh! Sorry, the cops outside just let me in. ] -> DetectiveCollinsCrimeSceneCantEnter
* [ I'm authorized to be here. Who's in charge here? ] -> DetectiveCollinsCrimeSceneCantEnter
+ [ You're right. I'll come back later. ] -> GoToLodgingsHallwayFromApartment102

=== DetectiveCollinsCrimeSceneCantEnter ===
Detective Collins: What?! Get off my sight. This is a crime scene! #portrait:angry
-> GoToLodgingsHallwayFromApartment102

=== DetectiveCollinsCrimeSceneGoAhead ===
Detective Collins: Oh! For god's sake, I can't believe it. #portrait:angry
Detective Collins: Quit the bullshit and join the fucking party. 
Detective Collins: Who cares?
-> Apartment102

=== DetectiveCollins ===
#choice
* [ What are you doing here? ] -> DetectiveCollinsWhatAreYouDoing
* [ What's the current state of the investigation? ] -> DetectiveCollinsOngoingInvestigation
* { DetectiveCollinsOngoingInvestigation } [ Which clues? ] -> DetectiveCollinsClues
* { DetectiveCollinsOngoingInvestigation } [ What can I do to help? ] -> DetectiveCollinsHelp
* { DetectiveCollinsOngoingInvestigation } [ Tell me about the victim. ] -> DetectiveCollinsAboutTheVictim
+ { DetectiveCollinsOngoingInvestigation } [ I think I have some useful information about the case. ] -> DetectiveCollinsInformation
* [ Tell me about Santa Monica. ] -> DetectiveCollinsSantaMonica
+ [ Bye. ] -> Apartment102

=== DetectiveCollinsWhatAreYouDoing ===
Detective Collins: I'm the main detective assigned to this case. The questions is what are YOU doing here? #portrait:angry
Detective Collins: But since we're not making any progress... let's just roll with it.
-> DetectiveCollins

=== DetectiveCollinsOngoingInvestigation ===
Detective Collins: We've established a couple of obvious facts. The victim is dead. And it seems that the body has been burned to the ground. 
Detective Collins: There are other clues that are a bit more... difficult to put into the official report.
-> DetectiveCollins

=== DetectiveCollinsClues ===
Detective Collins: Go ahead and take a look for yourself. I've never seen such a thing in my whole life...
-> DetectiveCollins

=== DetectiveCollinsHelp ===
Detective Collins: Don't touch anything and try not to ask too many questions.
-> DetectiveCollins

=== DetectiveCollinsAboutTheVictim ===
Detective Collins: We haven't established the victim's identity yet. The body is all burned out so it could be anyone.
-> DetectiveCollins

=== DetectiveCollinsSantaMonica ===
Detective Collins: You're asking me for fucking directions in the middle of a crime scene? #portrait:angry
Detective Collins: What can I tell you, there's a house on the beach that looks really suspicious.
Detective Collins: We have some reports pointing to their criminal activities but you know. Things go really slow in the department.
Detective Collins: The house is past the metal gate on the Santa Monica beach.
-> DetectiveCollins

=== DetectiveCollinsInformation ===
Detective Collins: Sure, what do you have?
-> DetectiveCollinsInformationChoice

=== DetectiveCollinsInformationChoice ===
#choice
+ { not OpenApartment102BottomDrawer && not PickUpBirthdayCake && not PickUpEmptyCanOfGas && not LookAtApartment102Notebook } [ Nah, I have nothing. ] -> DetectiveCollins   
* { OpenApartment102BottomDrawer } [ I think I've found the victim's name. ] -> DetectiveCollinsVictimsName
* { PickUpBirthdayCake } [ I think I've found the victim's origin. ] -> DetectiveCollinsVictimsOrigin
* { PickUpEmptyCanOfGas } [ I think I've found the cause of death. ] -> DetectiveCollinsVictimsCauseOfDeath
* { LookAtApartment102Notebook } [ I think I've found who were the murderers. ] -> DetectiveCollinsVictimsMurderes
+ [ That's all for now. ] -> DetectiveCollins

=== DetectiveCollinsVictimsName ===
Detective Collins: Oh! That's actually quite useful... #portrait:smile
Player: The name if the victim is Marian.
-> DetectiveCollinsInformationChoice

=== DetectiveCollinsVictimsOrigin ===
Detective Collins: Really?
Player: The victim is a just regular human. She had food in her fridge, instead of blood. How could she be a vampire, right?
Detective Collins: Oh! You're right! That makes a lot of sense... #portrait:smile
-> DetectiveCollinsInformationChoice

=== DetectiveCollinsVictimsCauseOfDeath ===
Detective Collins: Really?
Player: The victim was burned with gasoline while sleeping.
Detective Collins: Fantastic! That sounds a lot better than our initial theories... #portrait:smile
-> DetectiveCollinsInformationChoice

=== DetectiveCollinsVictimsMurderes ===
Detective Collins: And who might that be?
Player: The victim was being pursued by a secret society. They believe themselves to be vampire hunters.
Player: Can you believe it? So they followed a vampiric ritual to make the killing seem more supernatural.
Detective Collins: Oh! That makes a lot of sense! You have some detective instincts after all. #portrait:smile
-> DetectiveCollinsInformationChoice