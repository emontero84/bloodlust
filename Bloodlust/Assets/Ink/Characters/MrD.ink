=== TalkToMrD ===
Mr. D: Good evening.
-> MrD

=== MrD ===
#choice
* { not MrDSantaMonica && not MrDMainStreet && not MrDGalleryNoir } [ What is this place? ] -> MrDSantaMonica
* { MrDSantaMonica } [ No, I mean what is this particular place? ] -> MrDMainStreet
* { MrDMainStreet } [ Ok, I give up. What's the name of the building behind you? ] -> MrDGalleryNoir
* { MrDGalleryNoir } [ Are you happy now? ] -> MrDRespondsYes
* { MrDGalleryNoir } [ You're not a man of too many words, are you? ] -> MrDRespondsNo
* { HasQuest(JulietteQuest) && not IsQuestStepUpdated(JulietteQuest_1) && not MrDAllowsYouToEnter } [ Can I enter the gallery? ] -> MrDRespondsNoToEnterGallery
* { HasQuest(JulietteQuest) && not IsQuestStepUpdated(JulietteQuest_1) && not MrDAllowsYouToEnter && AlphaHelpToEnterNoirGallery } [ I'm here on behalf of Alpha, can I enter now? ] -> MrDRespondsToAlpha
* { HasQuest(JulietteQuest) && not IsQuestStepUpdated(JulietteQuest_1) && not MrDAllowsYouToEnter && MrDRespondsNoToEnterGallery } [ Why can't I enter the gallery? ] -> MrDExplains
* { HasQuest(JulietteQuest) && not IsQuestStepUpdated(JulietteQuest_1) && not MrDAllowsYouToEnter && MrDExplains && SkillBribe } [ (Bribe) Maybe we can make some sort of economic arrangement. ] -> BribeMrDInit
+ [ Bye. ] -> MainStreet

=== MrDSantaMonica ===
Mr. D: Santa Monica.
-> MrD

=== MrDMainStreet ===
Mr. D: Main street.
-> MrD

=== MrDGalleryNoir ===
Mr. D: Gallery Noir.
-> MrD

=== MrDRespondsYes ===
Mr. D: Yes.
-> MrD

=== MrDRespondsNo ===
Mr. D: Nope.
-> MrD

=== MrDRespondsNoToEnterGallery ===
Mr. D: No.
-> MrD

=== MrDFakeGoldenWatch ===
Player: Check out my truly golden watch. Can I enter now?
Mr. D: No. It's fake.
-> MrD

=== MrDExplains ===
Mr. D: It's a private event. 
Mr. D: Only for the rich and famous.
Mr. D: And you're not either of those.
-> MrD

=== GiveMillionaireDressToMrD ===
Player: Check out my millionaire dress. Can I enter now?
~RemoveFromInventory(MillionaireDress)
-> MrDAllowsYouToEnter

=== GiveDiamondRingToMrD ===
Player: Check out my diamond ring. Can I enter now?
~RemoveFromInventory(DiamondRing)
-> MrDAllowsYouToEnter

=== GiveGoldenWatchToMrD ===
Player: Check out my golden watch. Can I enter now?
~RemoveFromInventory(GoldenWatch)
-> MrDAllowsYouToEnter

=== MrDAllowsYouToEnter ===
Mr. D: Of course.
Mr. D: Come on in.
~Hide(MainStreet_MrD)
~Show(MainStreet_MrDOpen)
~Show(MainStreet_DoorToNoirGallery)
~Show(MainStreet_NoirGalleryDoorOpen)
-> MainStreet

=== BribeMrDInit ===
Mr. D: I'm listening.
-> BribeMrDChoice

=== BribeMrDChoice ===
#choice
* [ Let's say I give a very generous tip. ] -> BribeMrDFail
* [ This pack of Benjamin Franklins are going to sleep in your pocket. ] -> BribeMrDFail
* [ Consider it a small token of my appreciation. In cash, of course. ] -> BribeMrD
* [ Being a doorman must be very tiring. Here, a tip for all your efforts. ] -> BribeMrDFail

=== BribeMrDFail ===
Mr. D: Forget it.
-> MainStreet

=== BribeMrD ===
-> BribeSuccessful -> BribeMrDSuccess

=== BribeMrDSuccess ===
Mr. D: Very generous.
-> MrDAllowsYouToEnter

=== MrDRespondsToAlpha ===
Mr. D: Alpha who?
Mr. D: No.
-> MrD