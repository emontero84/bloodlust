=== TalkToAlpha ===
+ { not AlphaFirstEncounter } -> AlphaFirstEncounter
+ { AlphaFirstEncounter || AlphaBribed == 1 } -> AlphaDefault

=== AlphaDefault ===
Alpha: Hey, pal! How ya doin'? #portrait:smile
-> Alpha

=== AlphaFirstEncounter ===
Alpha: Where do you think you're going? #portrait:angry
-> AlphaFirstEncounterChoice

=== AlphaFirstEncounterChoice ===
#choice
* [ I'd like to go to the beach. ] -> AlphaExplainsToll
* [ I'm going to the beach. ] -> AlphaExplainsToll
* [ It's none of your fucking business. ] -> AlphaExplainsTollAggro

=== AlphaExplainsTollAggro ===
Alpha: Actually, it is of my fucking business since you can't go to the beach at the present moment because of the toll situation. #portrait:angry
-> AlphaTollChoice

=== AlphaExplainsToll ===
Alpha: Well, you can't go to the beach at the present moment because of the toll situation.
-> AlphaTollChoice

=== AlphaTollChoice ===
#choice
* [ What toll situation? ] -> AlphaToll
* [ I don't have time for this. Let me thru. ] -> AlphaIntimidationFail

=== AlphaToll ===
Alpha: The parking garage is imposing a toll on each and every citizen of Santa Monica to grant their access to the beach.
Player: How much?
Alpha: Two hundred dollars and you're free to go. #portrait:smile
-> AlphaTollAgainChoice

=== AlphaIntimidationFail ===
Alpha: Not so fast, knucklehead. I can't let you in. And I'm entitled to use any means necessary. #portrait:angry
-> AlphaToll

=== AlphaTollAgainChoice ===
#choice
* [ Isn't 200 bucks a bit too much? ] -> AlphaTollExpensive
* [ Is there anything I can do to enter? ] -> AlphaSeductionFail
* [ You don't seem like a regular parking lot worker. ] -> AlphaTollWorker
* [ Can you explain me the toll situation again? ] -> AlphaTollAgain
+ [ Okay, let's talk about something else. ] -> Alpha

=== AlphaTollAgain ===
Alpha: The parking garage is imposing a $200 toll on each and every citizen of Santa Monica to grant their access to the beach.
-> AlphaTollAgainChoice

=== AlphaTollExpensive ===
Alpha: Yeah, I've heard some complaints about that. But I'm not the one making the rules. Orders are orders. #portrait:smile
-> AlphaTollAgainChoice

=== AlphaSeductionFail ===
Alpha: You can pay the toll as everyone else. I'm sorry, that's your only option. #portrait:smile
-> AlphaTollAgainChoice

=== AlphaTollWorker ===
Alpha: Why is that? #portrait:angry
-> AlphaWorkerChoice

=== AlphaWorkerChoice ===
#choice
* [ Because of your clothes. ] -> AlphaWorkerClothes
* [ Because of your piercings and chains. ] -> AlphaWorkerPiercings
* [ Because of your mohawk. ] -> AlphaWorkerMohawk
+ [ You're probably right. ] -> AlphaWorkerClothesRight

=== AlphaWorkerClothes ===
Alpha: Oh! You mean because of the uniform? Yeah, mine is in the laundry but the boss is OK with my trenchcoat.
-> AlphaWorkerChoice

=== AlphaWorkerPiercings ===
Alpha: They're just metallic accessories. And they're not against the working policy of the parking garage.
-> AlphaWorkerChoice

=== AlphaWorkerMohawk ===
Alpha: You mean the company cap? They're making a bigger cap specifically designed for the height of my mohawk.
Alpha: But it's not coming anytime soon.
-> AlphaWorkerChoice

=== AlphaWorkerClothesRight ===
Alpha: I hate to say it, but I told you. #portrait:smile
-> AlphaTollAgainChoice

=== Alpha ===
#choice
* { AlphaBribed == 0 && SkillBribe } [ (Bribe) Okay, let me bribe you a little. ] -> BribeAlphaInit
* { AlphaBribed == 0 && SkillPersuade } [ (Persuade) I'm sure we can sort this misunderstanding with a little talk. ] -> PersuadeAlphaInit
* [ Tell me about Santa Monica. ] -> AlphaAboutSantaMonica
* { HasQuest(JulietteQuest) && not IsQuestStepUpdated(JulietteQuest_1) && AlphaAboutSantaMonica } [ Can you help me enter the Noir Gallery? ] -> AlphaHelpToEnterNoirGallery
* { HasQuest(JulietteQuest) && not IsQuestStepUpdated(JulietteQuest_1) && MrDRespondsToAlpha } [ Hey! They didn't let me in the Noir Gallery! ] -> AlphaTrickToEnterNoirGallery
+ { AlphaBribed == 0 } [ About the toll... ] -> AlphaTollAgainChoice
+ [ I'm outta here. ] -> ParkingGarage

=== GiveMoneyClipToAlpha ===
~RemoveFromInventory(MoneyClip)
-> BribeAlpha

=== BribeAlphaInit ===
Alpha: What?! #portrait:angry
-> BribeAlphaChoice

=== BribeAlphaChoice ===
#choice
* [ Quid pro quo, my friend. Have a little tip. You deserve it. ] -> BribeAlphaFail
* [ You've worked hard, and now you deserve a token of my appreciation. ] -> BribeAlpha
* [ Here's a gift for you. In cash. ] -> BribeAlphaFail
* [ Merry Christmas! I have a gift for you! ] -> BribeAlphaFail

=== BribeAlphaFail ===
Alpha: No way! Get off my face, dude! #portrait:angry
-> ParkingGarage

=== BribeAlpha ===
-> BribeSuccessful -> BribeAlphaSuccess

=== BribeAlphaSuccess ===
Alpha: Now we're talking. Enjoy the beach! #portrait:smile
-> AccessToBeachIsOpen

=== PersuadeAlphaInit ===
Alpha: What?! #portrait:angry
-> PersuadeAlphaChoice

=== PersuadeAlphaChoice ===
#choice
* [ Isn't your toll policy a bit unfair? ] -> PersuadeAlphaFail
* [ I've seen some people get to the beach without paying. ] -> PersuadeAlphaFail
* [ You're abusing your position as toll manager! ] -> PersuadeAlphaFail
* [ I'm exempt to pay the toll policy. ] -> PersuadeAlpha

=== PersuadeAlphaFail ===
Alpha: No way! Get off my face, dude! #portrait:angry
-> ParkingGarage

=== PersuadeAlpha ===
Alpha: I'm not so sure about that...
Player: Please, allow me to ellaborate on my position. #portrait:smile
Player: The toll policies that you describe might be subject to the common Santa Monica citizen, such as yourself.
Player: But since I'm not a common Santa Monica citizen, these toll policies must not be applied in my particular case.
Alpha: Well... I guess you're right...
-> AccessToBeachIsOpen

=== AccessToBeachIsOpen ====
~AlphaBribed = 1
~Hide(ParkingGarage_DoorToTheBeach)
~Hide(ParkingGarage_Alpha)
~Show(ParkingGarage_DoorToTheBeachOpen)
~Show(ParkingGarage_AlphaOpen)
-> ParkingGarage

=== AlphaAboutSantaMonica ===
Alpha: This city is awesome. Beach, clubs and everthing is open all night. Even the art galleries! #portrait:smile
Alpha: Have you been to the Noir Gallery? It's at the start of main street and it's absolutely awesome, you should pay them a visit.
-> Alpha

=== AlphaHelpToEnterNoirGallery ===
Alpha: Of course! I've been there a million times and I'm a good friend of the doorman.
Alpha: Just say him you're on behalf of Alpha and they'll let you in. #portrait:smile
-> ParkingGarage

=== AlphaTrickToEnterNoirGallery ===
Alpha: Ha ha ha! Did you really believe it? #portrait:smile
Alpha: Aw man! Noir Gallery is a very exclusive place for the most rich and famous. 
Alpha: You can't just walk in there without a millionaire dress, a nice diamond ring or a fancy golden watch. #portrait:smile
-> ParkingGarage