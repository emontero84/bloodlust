=== TalkToOfficerJohnson ===
Officer Johnson: Yep. How may I help you?
-> OfficerJohnson

=== OfficerJohnson ===
#choice
* [ What's the current state of the investigation? ] -> OfficerJohnsonOngoingInvestigation
* { OfficerJohnsonOngoingInvestigation } [ Which is the cause of death? ] -> OfficerJohnsonCauseOfDeath
* { OfficerJohnsonCauseOfDeath } [ Why are you so sure it's supernatural? ] -> OfficerJohnsonSupernatural
+ { OfficerJohnsonOngoingInvestigation } [ I think I have some useful information about the case. ] -> OfficerJohnsonInformation
* [ Tell me about Santa Monica. ] -> OfficerJohnsonSantaMonica
+ [ Bye. ] -> Apartment102

=== OfficerJohnsonOngoingInvestigation ===
Officer Johnson: Not so good. The detective is having some problems with... the cause of death.
-> OfficerJohnson

=== OfficerJohnsonCauseOfDeath ===
Officer Johnson: I'd say it's obviously supernatural but the detective thinks otherwise. #portrait:angry
-> OfficerJohnson

=== OfficerJohnsonSupernatural ===
Officer Johnson: Well, there's a wooden stake in the victim's heart... and the body is reduced to ashes...
Officer Johnson: The signals could not be any stronger. 
-> OfficerJohnson

=== OfficerJohnsonSantaMonica ===
Officer Johnson: You been to Trip's Pawnshop? It's at the end of main street.
Officer Johnson: There you can find a lot of useful items for a reasonable price.
-> OfficerJohnson

=== OfficerJohnsonInformation ===
-> OfficerJohnsonInformationChoice

=== OfficerJohnsonInformationChoice ===
#choice
+ { not OpenApartment102BottomDrawer && not PickUpBirthdayCake && not PickUpEmptyCanOfGas && not LookAtApartment102Notebook } [ Nah, I have nothing. ] -> OfficerJohnson
* { OpenApartment102BottomDrawer } [ I think I've found the victim's name. ] -> OfficerJohnsonVictimsName
* { PickUpBirthdayCake } [ I think I've found the victim's origin. ] -> OfficerJohnsonVictimsOrigin
* { PickUpEmptyCanOfGas } [ I think I've found the cause of death. ] -> OfficerJohnsonVictimsCauseOfDeath
* { LookAtApartment102Notebook } [ I think I've found who were the murderers. ] -> OfficerJohnsonVictimsMurderes
+ [ That's all for now. ] -> OfficerJohnson

=== OfficerJohnsonVictimsName ===
Officer Johnson: Oh! You mean the vampire's name? #portrait:smile
Player: Errr... The name of the victim is Marian.
Officer Johnson: Uh! Marian the Vampire, that makes a lot of sense.
-> OfficerJohnsonInformationChoice

=== OfficerJohnsonVictimsOrigin ===
Officer Johnson: Do you mean her supernatural origin? #portrait:smile
Player: Not exactly. The victim is a just regular human. She had food in her fridge, instead of blood. 
Officer Johnson: Mmmh! Maybe it was a food-eating vampire or a vampire with a very peculiar diet...
-> OfficerJohnsonInformationChoice

=== OfficerJohnsonVictimsCauseOfDeath ===
Officer Johnson: Oh! Really? #portrait:smile
Player: The victim was burned with gasoline while sleeping.
Officer Johnson: Are you sure it wasn't supernatural fire? #portrait:smile
Player: Nah, I'm pretty sure it was gasoline.
-> OfficerJohnsonInformationChoice

=== OfficerJohnsonVictimsMurderes ===
Officer Johnson: Ah! Who is the vampire slayer? Is it a secret society? A teenage girl? #portrait:smile
Player: ...
Player: You know what? Nevermind.
-> OfficerJohnsonInformationChoice