=== TalkToDog ===
Dog: BARK BARK BARK!
-> Dog

=== Dog ===
#choice
* [ Hey doggy, gimme your paw. ] -> DogBarks
* [ You seem like a good St. Bernard. Any news from the Alps? ] -> DogBarks 
* [ There's a strong prejudice in the dog community about vampires. ] -> DogBarks
+ [ What am I doing? Talking to a dog? ] -> MainStreet

=== DogBarks ===
Dog: BARK BARK BARK!
-> Dog

=== BeginGiveSteakToDog ===
Player: Here, doggy! Have some steak!
-> GiveSteakToDog

=== GiveSteakToDog ===
Dog: *NOM* *NOM* *NOM* 
~DogIsUnleashed = 1
~RemoveFromInventory(Steak)
~Hide(MainStreet_Dog)
~Hide(MainStreet_DogLeash)
~Show(MainStreet_DogSleepingAnimation)
Dog: ZzzzZZZzzzZZZ....
-> DogQuestUpdated

=== BeginGiveIntoxicatedSteakToDog ===
Player: Here, doggy! Have some poisonous steak!
-> GiveIntoxicatedSteakToDog

=== GiveIntoxicatedSteakToDog ===
Dog: *NOM* *NOM* *NOM*
~DogIsUnleashed = 1
~RemoveFromInventory(IntoxicatedSteak)
~Hide(MainStreet_Dog)
~Hide(MainStreet_DogLeash)
~Show(MainStreet_DogSleepingAnimation)
Dog: ZzzzZZZzzzZZZ....
-> DogQuestUpdated