=== TalkToExorcist ===
* { not ExorcistFirstEncounter } -> ExorcistFirstEncounter
+ { ExorcistFirstEncounter } -> ExorcistDefault

=== ExorcistFirstEncounter ===
Father Joachim: Good evening, my child. My name is Joachim. How may I help you? #portrait:smile
-> ExorcistGreetingChoice

=== ExorcistGreetingChoice ===
#choice
* [ Nice to meet you... father. ] -> FatherJoachimMeeting
* [ Nice collar. May I call you father Joachim? ] -> FatherJoachimMeeting
* [ Are you some kind of priest? ] -> FatherJoachimMeeting

=== FatherJoachimMeeting ===
Father Joachim: Ja! You can call me father Joachim, my child. #portrait:smile
Father Joachim: I've just landed in Santa Monica and I'm still getting used to the American habits.
~UpdateQuest(RoseQuest, RoseQuest_1)
-> Exorcist

=== ExorcistDefault ===
Father Joachim: Good evening, my child. What can I do for you? #portrait:smile
-> Exorcist

=== Exorcist ===
#choice
* [ I want to make a confession. ] -> ExorcistConfession
* [ What kind of priest are you? ] -> ExorcistPriest
* [ What brings you to Santa Monica? ] -> ExorcistWhySantaMonica
* { ExorcistPriest || ExorcistSantaMonica } [ Have you exorcised any little girls, lately? ] -> ExorcistLittleGirls
* { ExorcistPriest || ExorcistSantaMonica } [ How is the battle against evil? ] -> ExorcistBattleAgainstEvil
* { ExorcistPriest || ExorcistSantaMonica } [ What do you think about vampires? ] -> ExorcistVampires
* { HasQuest(RoseQuest) && ExorcistBattleAgainstEvil } [ How long are you planning on staying in Santa Monica? ] -> ExorcistStayingInSantaMonica
* [ Tell me about Santa Monica. ] -> ExorcistSantaMonica
+ [ Good bye. ] -> Diner

=== ExorcistConfession ===
Father Joachim: Nein. I'm afraid it's not the proper place to take a confession.
Father Joachim: But of course we can have a friendly talk. I'm a good listener. #portrait:smile
-> ExorcistConfessionChoice

=== ExorcistConfessionChoice ===
#choice
* [ I'm no longer mortal and I feel above all humans.  ] -> ExorcistConfessionVampire
* [ I feel an uncontrollable hunger of human blood. ] -> ExorcistConfessionVampire
* [ I have propably killed some innocents in the last nights. ] -> ExorcistConfessionVampire
* [ I'm cursed and I cannot walk the earth on the face of the sun. ] -> ExorcistConfessionVampire
* [ Lately I have been very proud of myself. Does it count as a sin? ] -> ExorcistConfessionVampire
+ [ Let's talk about something else. ] -> Exorcist

=== ExorcistConfessionVampire ===
Father Joachim: Well, nobody's perfect. Ja? Don't you worry, it's not a sin if you're really committed to improve yourself. #portrait:smile
-> Exorcist

=== ExorcistPriest ===
Father Joachim: I'm just a regular priest. Nothing more than a humble servant to God.
Father Joachim: I have been recently promoted to master exorcist so now I have the priviledge to serve God in this beautiful city.
-> Exorcist

=== ExorcistWhySantaMonica ===
Father Joachim: I have been asigned to Santa Monica as my first destination.  
Father Joachim: The Church has detected some unusual spiritual activities in the Ocean House Hotel, at the outskirts of Santa Monica. 
Father Joachim: My duty is to investigate the place and proceed with any exorcisms that may be necessary. #portrait:angry
-> Exorcist

=== ExorcistLittleGirls ===
Father Joachim: Ha ha ha! Meine mutter! Exorcisms are not as they show in the movies, with the spinning heads and the green vomit. #portrait:smile
Father Joachim: You can find some broken bones and nonsensical talking. But usually it's all tears and suffering.  
Father Joachim: I'm sorry to disappoint you. No spinning heads lately. #portrait:smile
-> Exorcist

=== ExorcistBattleAgainstEvil ===
Father Joachim: Being an exorcist is mostly an intellectual endeavor.
Father Joachim: The old writings are filled with demonic entities that we need to research before doing the actual exorcisms.
-> Exorcist

=== ExorcistStayingInSantaMonica ===
Father Joachim: My orders are clear: unless I find other signs of demonic activity, I must remain focused on Santa Monica.
-> Exorcist

=== ExorcistVampires ===
Father Joachim: I'm not sure if they even exist. But if I ever find one, I'd use my holy water as with the other creatures of evil.
-> Exorcist

=== GiveCakeToFatherJoachim ===
Player: Here's some cake... #portrait:smile
Father Joachim: I'm sorry, I'm not hungry. I've had more than enough hamburguers! #portrait:smile
-> Diner

=== GiveDiabolicPropagandaToFatherJoachim ===
Player: Here's a solid sign of demonic activity. Look at this pamphlet! 
Father Joachim: Let me se...
Father Joachim: Nein! This is just foolish diabolical propaganda. Without a solid historical background, it's worth nothing. #portrait:angry
-> Diner

=== GiveOldBibleToFatherJoachim ===
Player: Here's a solid sign of demonic activity. Look at this old Bible!
Father Joachim: Let me se...
Father Joachim: Nein! It's just an old Bible. It serves as a solid historical background, but nothing more.
-> Diner

=== GiveOldDiabolicBibleToFatherJoachim ===
Player: Here's a solid sign of demonic activity. Look at this old diabolical Bible!
Father Joachim: Let me see...
Father Joachim: Uh! Wunderbar! An old bible with demonic references to the Demon Father... #portrait:smile
Father Joachim: This is indeed a solid sign of demonic activity! I have to report to the Vatican! #portrait:angry
~RemoveFromInventory(OldDiabolicBible)
~Hide(Diner_Exorcist)
~UpdateQuest(RoseQuest, RoseQuest_2)
-> Diner

=== ExorcistSantaMonica ===
Father Joachim: Oh! I wish I had more time to do some tourism. I have barely left this diner.
-> Exorcist