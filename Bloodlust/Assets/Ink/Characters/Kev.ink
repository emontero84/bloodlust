
VAR TalkAboutDennis = 0

=== TalkToKev ===
Kev: Hey! Step back! This is private property! #portrait:angry
-> Kev

=== Kev ===
#choice
* [ Excuse me, who are you exactly? ] -> KevWhoAreYou
* { TalkAboutDennis == 0 } [ I'm looking for Dennis. ] -> KevLookingForDennis
* { TalkAboutDennis == 1 } [ Listen, I know Dennis is in here. He's expecting me. ] -> KevDennisExpectingMe
* { TalkAboutDennis == 2 } [ What about me? ] -> KevWhatAboutMe
* { TalkAboutDennis == 2 } [ Sure, go ask him if you want. I can wait here. ] -> KevNoWay
* { TalkAboutDennis == 2 } [ Didn't he tell you? I'm here to discuss some business. ] -> KevNoWay
* { TalkAboutDennis == 3 } [ I told you, we have some business to discuss. Please, let me in. ] -> KevNoWay
* [ Move away! I have no time to waste on you. ] -> KevWhoDoYouThinkYouAre
* { not IntimidateKev && not KevSeduction && TalkAboutDennis == 3 && SkillIntimidate } [ (Intimidate) You're running out of my patience! Let. Me. In. ] -> IntimidateKevInit
* { not IntimidateKev && not KevSeduction && TalkAboutDennis == 3 && SkillSeduce } [ (Seduction) Maybe we can find some kind of... arrangement. ] -> KevSeductionInit
+ [ I'm outta here. ] -> BeachHouseExterior

=== KevWhoAreYou ===
Kev: I'm Kev and this is private property! Turn around and fuck off before I lose my nerve! #portrait:angry
-> Kev

=== KevLookingForDennis ===
Kev: There's no Dennis here! Fuck off! #portrait:angry
~TalkAboutDennis = 1
-> Kev

=== KevDennisExpectingMe ===
Kev: What? He's expecting YOU? #portrait:angry
~TalkAboutDennis = 2
-> Kev

=== KevWhatAboutMe ===
Kev: Nah, you just don't look like the kind of client of Dennis.
~TalkAboutDennis = 3
-> Kev

=== KevNoWay ===
Kev: No way! Get outta my sight or I'll beat you with my baseball bat! #portrait:angry
-> Kev
	
=== KevWhoDoYouThinkYouAre ===
Kev: What? Who do you think you are? This is private property! #portrait:angry
-> Kev

=== KevScared ===
Kev: Ahhh! Get off me, freak! #portrait:angry
-> KevMovesAway

=== KevMovesAway ===
~BeachHouseOpen = 1
~Hide(BeachHouseExterior_Kev)
~Show(BeachHouseExterior_KevOpen)
-> BeachHouseExterior

=== IntimidateKevInit ===
Kev: What?! #portrait:angry
-> IntimidateKevChoice

=== IntimidateKevChoice ===
#choice
* [ You heard me. Fuck off or I'll break your skull. ] -> IntimidateKevFail
* [ It's your last chance. Move away or suffer the consequences. ] -> IntimidateKevFail
* [ You're gonna taste your own baseball bat! Let. Me. In. ] -> IntimidateKev
* [ Are you deaf? Let me in, you little prick. Let me in!  ] -> IntimidateKevFail

=== IntimidateKevFail ===
Kev: #portrait:angry
-> BeachHouseExterior

=== IntimidateKev ===
Kev: Alright, alright. There's no need to make a scene out of it. 
Kev: I'll let you in.
-> KevMovesAway

=== IntimidateKevWithKatana ===
Player: I'll cut you like a tuna head! Let. Me. In. #portrait:blood
~RemoveFromInventory(Katana)
-> IntimidateKev

=== IntimidateKevWithBaseballBat ===
Player: You're gonna taste your own baseball bat! Let. Me. In. #portrait:blood
~RemoveFromInventory(BaseballBat)
-> IntimidateKev

=== KevSeductionInit ===
Kev: Err... Arrangement?
-> KevSeductionChoice

=== KevSeductionChoice ===
#choice
* [ You know, the kind of arrangement where we get closer and sexy. ] -> KevSeduction
* [ Aren't you lonely all by yourself? I can make you sweet company. *WINK* ] -> KevSeductionFail
* [ You're so strong and handsome. I wanna eat you all, sweetheart! ] -> KevSeductionFail
* [ Come closer, big boy. I want to show you something... ] -> KevSeductionFail

=== KevSeductionFail ===
Kev: No way! Fuck off, you freak! #portrait:angry
-> BeachHouseExterior

=== KevSeduction ===
Kev: You mean?... *CHUCKLES* #portrait:smile
Player: I really mean it. #portrait:smile
Kev: Uh! *CHUCKLES* That's awesome... #portrait:smile
-> KevMovesAway