=== TalkToOldLady ===
Miss Webster: Yeeees? #portrait:smile
-> OldLady

=== OldLady ===
#choice
* [ What are you doing in a place like this? ] -> OldLadyWhatAreYouDoing
* [ Do you need help, madam? ] -> OldLadyNeedHelp
* { OldLadyWhatAreYouDoing } [ How was Santa Monica different in your times? ] -> OldLadySantaMonicaInYourTimes
* { OldLadySantaMonicaInYourTimes } [ Maybe I can help you find what you need. ] -> OldLadyHusbandsWatch
* { HasQuest(OldLadyQuest) } [ The pawnshop owner wants you to leave. ] -> OldLadyRespondsNo
* { HasQuest(OldLadyQuest) && not IsQuestStepUpdated(OldLadyQuest_1) && OldLadyHusbandsWatch && SkillPersuade } [ (Persuasion) Your husband's watch is not in this store. ] -> PersuadeOldLadyInit
* { HasQuest(OldLadyQuest) && not IsQuestStepUpdated(OldLadyQuest_1) && OldLadyHusbandsWatch && SkillIntimidate } [ (Intimidate) It's time for you to leave. Or die. ] -> IntimidateOldLadyInit
+ [ I'm outta here. ] -> Pawnshop

=== OldLadyWhatAreYouDoing ===
Miss Webster: Oh! I'm just browsing. This convenience store... and the whole city... they were very different in my times.
Miss Webster: But don't worry, {IsFemale:darling}{not IsFemale:son}. I can manage myself. #portrait:smile
-> OldLady

=== OldLadyNeedHelp ===
Miss Webster: Oh no! I'm just browsing, {IsFemale:darling}{not IsFemale:son}. Don't worry, I can manage myself, thank you. #portrait:smile
-> OldLady

=== OldLadySantaMonicaInYourTimes ===
Miss Webster: The city was cleaner and quieter. You could walk the streets peacefully. 
Miss Webster: Now it's all noise and pollution. Even this store is almost unrecognizable. #portrait:angry
Miss Webster: Where's Mr. Huggle? I may need his assistance...
-> OldLady

=== OldLadyHusbandsWatch ===
Miss Webster: Oh! Would you be so kind, {IsFemale:darling}{not IsFemale:son}? #portrait:smile
Miss Webster: I need to find my husband's watch. It's a beautiful, golden watch. He left it when he went to the war.
-> OldLady

=== OldLadyRespondsNo ===
Miss Webster: Can't hear you, {IsFemale:darling}{not IsFemale:son}! #portrait:smile
-> OldLady

=== GiveCheapWatchToOldLady ===
Player: I found your husband's watch! More or less...  #portrait:confused
Miss Webster: Oh dear! I'm afraid it's not the watch of my husband. 
Miss Webster: This watch is not golden and it's pretty ugly.
-> Pawnshop

=== GiveGoldenWatchToOldLady ===
~RemoveFromInventory(GoldenWatch)
Player: I found your husband's watch! #portrait:smile
-> SolveOldLadyPuzzle

=== GiveFakeGoldenWatchToOldLady ===
~RemoveFromInventory(FakeGoldenWatch)
Player: I found your husband's watch! As good as new. #portrait:smile
-> SolveOldLadyPuzzle

=== SolveOldLadyPuzzle ===
Miss Webster: Oh dear! Is that so? #portrait:smile
Miss Webster: I can go home at last! #portrait:smile
-> OldLadyLeaves

=== PersuadeOldLadyInit ===
Miss Webster: Oh! How is it so?
-> PersuadeOldLadyChoice

=== PersuadeOldLadyChoice ===
#choice
* [ Your husband's watch is already sold. I'm sorry. ] -> PersuadeOldLady
* [ Your watch has never been in this store, madam. It's in your home. ] -> PersuadeOldLadyFail
* [ Your husband's watch was melt to cover some golden platter. ] -> PersuadeOldLadyFail
* [ Your watch was sent to another pawnshop, in the other side of town. ] -> PersuadeOldLady

=== PersuadeOldLadyFail ===
Miss Webster: Oh dear! How dare you say something like that? portrait:angry
-> Pawnshop

=== PersuadeOldLady ===
Miss Webster: Oh dear! What a shame! #portrait:angry
Miss Webster: Well, at least I can go home at last. #portrait:smile
-> OldLadyLeaves

=== IntimidateOldLadyInit ===
Player: What?
-> IntimidateOldLadyChoice

=== IntimidateOldLadyChoice ===
#choice
* [ It's time for you to leave, madam! ] -> IntimidateOldLady
* [ Go home, gramma! You're not welcome here anymore. ] -> IntimidateOldLadyFail
* [ Don't make me yell again, gramma. Leave. Now.] -> IntimidateOldLadyFail
* [ Fuck off! And never come back! ] -> IntimidateOldLadyFail

=== IntimidateOldLadyFail ===
Miss Webster: Oh dear! How rude! #portrait:angry
-> Pawnshop

=== IntimidateOldLady ===
Miss Webster: Oh dear! I'd better be going then. #portrait:angry
-> OldLadyLeaves

=== GiveBirthdayCakeToOldLady ===
Player: Happy birthday, Miss Webster!
~RemoveFromInventory(BirthdayCake)
-> OldLadyEatsBirthdayCake

=== GiveIntoxicatedBirthdayCakeToOldLady ===
Player: Happy birthday, Miss Webster! With all my poisonous love.
~RemoveFromInventory(IntoxicatedBirthdayCake)
-> OldLadyEatsBirthdayCake

=== OldLadyEatsBirthdayCake ===
Miss Webster: Oh dear! I forgot about my birthday... #portrait:smile
Miss Webster: That's so sweet of you! #portrait:smile
Miss Webster: *YUMMY* *YUMMY* #portrait:smile
Miss Webster: Oh my! I must go to the toilet! Now!
-> OldLadyLeaves

=== OldLadyLeaves ===
~Show(Pawnshop_OldLadyLeavingAnimation)
~Hide(Pawnshop_OldLady)
~IsOldLadyGone = 1
~UpdateQuest(OldLadyQuest, OldLadyQuest_1)
-> PawnshopDefault