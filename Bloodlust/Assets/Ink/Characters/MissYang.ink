=== TalkToMissYang ===
Miss Yang: Hi, it' very nice meeting you. #portrait:smile
-> MissYang

=== MissYang ===
#choice
* [ I like your hat. ] -> MissYangHat
* [ What do you think about this painting? ] -> MissYangPainting
* { MissYangPainting } [ What do you know about the author? ] -> MissYangAuthor
+ [ I'm outta here. ] -> NoirGallery

=== MissYangHat ===
Miss Yang: Well, thank you! It's a coiffe, mon {IsFemale:amie}{not IsFemale:ami}. A headdress. #portrait:smile
-> MissYang

=== MissYangPainting ===
Miss Yang: Distressful. But nonetheless beautiful. #portrait:smile
Miss Yang: They're definitely the best of the author.
-> MissYang

=== MissYangAuthor ===
Miss Yang: Well, I've read some stories about the artist. A very peculiar character, indeed.
Miss Yang: They say he spent the last twenty years painting in his attic.
Miss Yang: No doubt he has lost his mind. And it really shows in the paintings.
-> MissYang