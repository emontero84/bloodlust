=== TalkToJules ===
Jules: E-e-e-evening.
-> Jules

=== Jules ===
#choice
* [ Is this some kind of beach party? ] -> JulesParty
* { JulesParty } [ Tell me more about the half-bloods. ] -> JulesHalfBloods
* [ Are you okay? ] -> JulesOkay
* { JulesScared } [ Do you need some help? ] -> JulesHelp
+ [ See ya! ] -> Beach

=== JulesParty ===
Jules: A p-p-p-p-arty? I wish it was a p-p-p-party. We are a-a-a-all half-bloods.
Jules: This b-b-b-beach is the only place where we can h-h-h-hang out. #portrait:angry
-> Jules

=== JulesHalfBloods ===
Jules: T-t-t-they call us half-bloods since our b-b-b-blood is too weak.
-> Jules

=== JulesOkay ===
Jules: It's no-no-no-nothing.
-> JulesScaredChoice

=== JulesScaredChoice ===
#choice
* [ You seem a bit scared. ] -> JulesScared
* [ Okay, I was just asking. ] -> JulesJustAsking
* [ Are you sure you're o-o-o-okay? ] -> JulesOffended

=== JulesScared ===
Jules: M-m-m-maybe.
-> Jules

=== JulesJustAsking ===
Jules: Just a s-s-s-small speech problem.
-> Jules

=== JulesOffended ===
Jules: V-v-v-very funny! Making a l-l-l-laugh of my s-s-s-speech problem. #portrait:angry
-> Jules

=== JulesHelp ===
Jules: Y-y-y-yeah, maybe you could bring me s-s-s-something to eat. #portrait:smile
Jules: I'm s-s-s-starving but I'm afraid of leaving the safety of the b-b-b-beach.
~AddQuest(JulesQuest)
-> Beach

=== GiveBloodBagToJules ===
Jules: Oh! You're a l-l-l-life-saver! #portrait:smile
~RemoveFromInventory(BloodBag)
~UpdateQuest(JulesQuest, JulesQuest_1)
-> Beach

=== GiveStakeToJules ===
Jules: A s-s-s-stake? Well, it might be useful against other v-v-v-vampires. #portrait:smile
~RemoveFromInventory(Stake)
~UpdateQuest(JulesQuest, JulesQuest_2)
-> Beach