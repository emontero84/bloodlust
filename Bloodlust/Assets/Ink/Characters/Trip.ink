=== TalkToTrip ===
* { not TripFirsEncounter } -> TripFirsEncounter
+ { TripFirsEncounter } -> TripDefault

=== TripFirsEncounter ===
Trip: Welcome to Trip's Pawnshop. My name is Trip. You buying or selling?
-> Trip

=== TripDefault ===
Trip: Hey. Thanks for coming back. Waddaya need? #portrait:smile
-> Trip

=== Trip ===
#choice
* { TripBuy && LookAtBigScissorsAtCounter } [ Gimme these big scissors. ] -> TripGivesBigScissors
* { TripOldLadyQuestNo } [ About the old lady...] -> TripOldLadyQuestRetry
* { HasQuest(OldLadyQuest) && IsOldLadyGone == 1 } [ I solved your problem with the old lady. ] -> TripOldLadySolved
* [ What is this place? ] -> TripPawnshopExplanation
* [ I'm here to buy things. What do you have for sale? ] -> TripBuy
* [ I want to sell some of my junk. ] -> TripSell
* { TripSellNotInterested } [ What was that about the special junk? ] -> TripRedHerringChoice
* { not HasQuest(DrugTrip) } [ You seem rather bored. ] -> TripBored
* { TripBored } [ Is there anything I can do to help you with the boredom? ] -> TripAsksForWeed
* { LookAtOldLady || TalkToOldLady } [ Who's that old lady? Look like your only customer. ] -> TripOldLadyQuest
* [ Tell me about Santa Monica. ] -> TripSantaMonica
+ [ I'm outta here. ] -> Pawnshop

=== TripGivesBigScissors ===
Trip: Yeah, sure. The first is on the house. They are a bit rusty but they are still great for cutting your toenails. #portrait:smile
~AddToInventory(BigScissors)
~Hide(Pawnshop_BigScissors)
-> Pawnshop

=== GiveJarOfWeedToTrip ===
Player: I've got your special cigarettes, Trip. #portrait:smile
Trip: Oh! Thanks, dude! That means a lot! #portrait:smile
~RemoveFromInventory(JarOfWeed)
~UpdateQuest(DrugTrip, DrugTrip_1)
~UpdateQuest(DrugTrip, DrugTrip_2)
-> Pawnshop

=== TripPawnshopExplanation ===
Trip: This is Trip's Pawnshop. A place to trade things for cash. Are you new in town or something? #portrait:smile
Player: Yeah, sort of.
Trip: Welcome to Santa Monica then. #portrait:smile
-> Trip

=== TripBuy ===
Trip: We have a fine selection of useless crap. But feel free to look around. #portrait:smile
-> Trip

=== TripSell ===
Trip: More crap? Nah, I already have the counter full of worthless garbage. 
Trip: However, there's a very specific piece of junk that I'm looking for.
-> TripRedHerringChoice

=== TripRedHerringChoice ===
#choice
* [ What junk are you looking for? ] -> TripRedHerring
* [ Tell me already! What is it? ] -> TripRedHerring
* [ I'm not interested in junk-hunting. ] -> TripSellNotInterested

=== TripRedHerring ===
Trip: It's called "red herring". I'm not entirely sure what it looks like but there's some folks asking for them lately.
Player: Okay. I'll see what I can do.
-> Trip

=== TripSellNotInterested ===
Trip: No worries, dude. Maybe some other time. #portrait:smile
-> Trip

=== TripBored ===
Trip: Yeah, it's mostly the same thing night after night. And I'm always stuck in this side of the bulletproof glass, you know.
-> Trip

=== TripAsksForWeed ===
Trip: Well, maybe you could bring me something to smoke. #portrait:smile
Player: You mean a pack of cigarettes? #portrait:confused
Trip: I was thinking of another kind of cigarettes. You know, something more... happy. #portrait:smile
-> TripWeedChoice

=== TripWeedChoice ===
#choice
* [ Oh, no problem! I'll get you some dope. ] -> TripWeedYes
* [ Ok, I'll see what I can do. ] -> TripWeedYes
* [ Winners don't use drugs, Trip. ] -> TripWeedNo

=== TripWeedYes ===
Trip: Thanks! That means a lot. #portrait:smile
~AddQuest(DrugTrip)
-> Pawnshop

=== TripWeedNo ===
Trip: You're right, you're right. I should've not asked you something like that...
-> Trip

=== TripOldLadyQuest ===
Trip: Oh! Don't talk me about it! #portrait:angry
Trip: She's just standing there... aging... and she's scaring the shit out of me...  #portrait:angry
Trip: What does she want?! Why is she staring at my stuff?! I can't stand it anymore... #portrait:angry
Trip: Can you make the old geezer leave? I'll give you my best product if you get rid of her.
-> TripOldLadyQuestChoice

=== TripOldLadyQuestChoice ===
#choice
* [ Consider it done. ] -> TripOldLadyQuestYes
* [ Sure, I'll see what I can do. ] -> TripOldLadyQuestYes
* [ I'm gonna pass on that. ] -> TripOldLadyQuestNo

=== TripOldLadyQuestYes ===
Trip: Great! Try not to make a scene out of it. The shop has a reputation to keep, you know.
~AddQuest(OldLadyQuest)
-> Pawnshop

=== TripOldLadyQuestNo ===
Trip: All right, I understand. She gives me the creeps too. #portrait:smile
-> Trip

=== TripOldLadyQuestRetry ===
Trip: Have your reconsidered the offer? My best product for getting rid of the old lady. #portrait:smile
-> TripOldLadyQuestChoice

=== TripOldLadySolved ===
Trip: Uh! You've done it! #portrait:smile
Trip: A promise, a promise. Lemme see... here's my best product. #portrait:smile
~AddToInventory(MillionaireDress)
~Hide(Pawnshop_Dress)
~UpdateQuest(OldLadyQuest, OldLadyQuest_2)
-> LookAtMillionaireDress -> Pawnshop

=== TripSantaMonica ===
Trip: Have you been to the beach? It's absolutely amazing at night! You can get there through the stairs in the parking garage.
-> Trip