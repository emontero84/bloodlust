=== TalkToDennis ===
* { not DennisFirstEncounter } -> DennisFirstEncounter
+ { DennisFirstEncounter } -> DennisDefault

=== DennisFirstEncounter ===
Dennis: Before we get into business, let's keep all things clear. 
Dennis: If you cross me, I'll fuck you. If you call the cops about me, I'll fuck you. And if you're a cop, I'll fuck even harder. #portrait:angry
Dennis: Are we clear?
-> DennisGreetingChoice

=== DennisDefault ===
Dennis: Hey, you're back. What are you buying, honey? #portrait:smile
-> Dennis

=== DennisGreetingChoice ===
#choice
* [ We're clear. ] -> Dennis
* [ What if I fuck you first? ] -> DennisGreetingFuckYou

=== DennisGreetingFuckYou ===
Dennis: He he! I'd really like to see that one coming. #portrait:smile
-> Dennis

=== Dennis ===
#choice
* [ What do you have for sale? ] -> DennisForSale
* { HasQuest(AstroliteQuest) && not IsQuestStepUpdated(AstroliteQuest_2) } [ I'm here for the Astrolite. ] -> DennisAstrolite
* { TalkToMercurius } [ Do you know a guy by the name of Mercurius? ] -> DennisMercurius
* { HasQuest(ClaireQuest) } [ Do you know Claire? ] -> DennisClaire
* { HasQuest(ClaireQuest) && not IsQuestStepUpdated(ClaireQuest_2) && DennisClaire && SkillIntimidate } [ (Intimidate) I'm here for Claire. Let's make some things absolutely clear. ] -> IntimidateDennisInit
* { HasQuest(ClaireQuest) && not IsQuestStepUpdated(ClaireQuest_2) && DennisClaire && SkillPersuade } [ (Persuasion) Let's talk about Claire. ] -> PersuadeDennisInit
* { HasQuest(ClaireQuest) && not IsQuestStepUpdated(ClaireQuest_2) && DennisClaire && SkillBribe } [ (Bribe) Let's make some sort of economic arrangement about Claire. ] -> BribeDennisInit
+ [ I'm gonna get going. ] -> BeachHouseInterior

=== DennisForSale ===
Dennis: The catalogue's wide and extensive. We sell all kinds of drugs, traditional and synthetic.  
Dennis: We also cook some medicines and chemical explosives, if that's your kind of thing.
-> Dennis

=== DennisAstrolite ===
Dennis: Ooh! Great choice! If you liked TNT, you're gonna love Astrolite. It's powerful as hell. #portrait:smile
-> PickUpAstrolite

=== DennisMercurius ===
Dennis: Uh yeah! Our beloved friend Mercurius. I do know him. How's he holding up? #portrait:smile
-> Dennis

=== DennisClaire ===
Dennis: Claire? Yeah, that whore used to work for me not so long ago. #portrait:angry
~UpdateQuest(ClaireQuest, ClaireQuest_1)
-> Dennis

=== UseBaseballBatWithDennis ===
~RemoveFromInventory(BaseballBat)
Player: I'm here for Claire. And I've got a baseball bat to speak on her behalf. #portrait:blood
Dennis: Whoa whoa whoa! Not so quick, can't we talk it out?
Player: The baseball bat's going to do all the talking. #portrait:angry
Dennis: Aaaaaaarg! #portrait:angry
Dennis: Okay, okay! I'll stop! Just don't hurt me! Please!
~UpdateQuest(ClaireQuest, ClaireQuest_2)
-> BeachHouseInterior

=== UseKatanaWithDennis ===
~RemoveFromInventory(Katana)
Player: I'm here for Claire. Let her go or I'll cut you like a tuna head! #portrait:blood
Dennis: Whoa whoa whoa! Not so quick, can't we talk it out?
Player: The samurai sword is going to do all the talking. #portrait:angry
Dennis: Aaaaaaarg! #portrait:angry
Dennis: Okay, okay! I'll stop! Just don't hurt me! Please!
~UpdateQuest(ClaireQuest, ClaireQuest_2)
-> BeachHouseInterior

=== IntimidateDennisInit ===
Dennis: I'm afraid I don't follow...
-> IntimidateDennisChoice

=== IntimidateDennisChoice ===
#choice
* [ Now you listen to me, motherfucker! Let Claire go! ] -> IntimidateDennisFail
* [ Shut up and listen or I'll have to crunch you like a filthy cockroach. ] -> IntimidateDennisFail
* [ Are you deaf or something? Don't make me kill you, you piece of shit! ] -> IntimidateDennisFail
* [ Let's talk. And you better follow my words very fucking carefully. ] -> IntimidateDennis

=== IntimidateDennisFail ===
Dennis: Oh boy! You're so wrong. Get outta my sight before I lose my temper. #portrait:angry
-> BeachHouseInterior

=== IntimidateDennis ===
Player: If you cross Claire, I'll fuck you. #portrait:angry
Player: If you think about Claire, I'll fuck you. #portrait:angry
Player: And if you ever pimp again, I'll fuck even harder. #portrait:angry
Player: Are we absolutely clear? #portrait:smile
Dennis: Yeah... I'm sorry! It won't happen again, I swear.
Player: All right. #portrait:smile
~UpdateQuest(ClaireQuest, ClaireQuest_2)
-> BeachHouseInterior

=== PersuadeDennisInit ===
Dennis: What about her? #portrait:angry
-> PersuadeDennisChoice

=== PersuadeDennisChoice ===
#choice
* [ Let's talk from businessman to {IsFemale:businesswoman}{not IsFemale:businessman}. ] -> PersuadeDennis
* [ You know what happens when a pimp gets caught by the police? ] -> PersuadeDennisFail
* [ You're doing nasty things to Claire and my associates might not like it. ] -> PersuadeDennisFail
* [ Selling drugs is one thing. But selling people... that's a terrible sin. ] -> PersuadeDennisFail

=== PersuadeDennisFail ===
Dennis: Who do you think you are?! Mind your own fucking business and get out of my property! #portrait:angry
-> BeachHouseInterior

=== PersuadeDennis ===
Player: You're a smart businessman. And Claire has become a problem more than a resource. #portrait:smile
Player: Let her go and you'll save yourself a ton of time and headaches. Which, as you know, are no good for business.
Dennis: Mmmm... maybe you're right...
Player: You know I'm right. #portrait:smile
~UpdateQuest(ClaireQuest, ClaireQuest_2)
-> BeachHouseInterior

=== BribeDennisInit ===
Dennis: Okay, I'm listening...
-> BribeDennisChoice

=== BribeDennisChoice ===
#choice
* [ Here's a monetary compensation for your troubles with Claire. ] -> BribeDennis
* [ I know I can buy your services. How much for leaving Claire alone? ] -> BribeDennisFail
* [ Quid pro quo, my friend. I give you cash and you forget about Claire. ] -> BribeDennisFail
* [ Let me bribe you a little. Then you must forget about Claire. Forever. ] -> BribeDennisFail

=== BribeDennisFail ===
Dennis: What?! No way! #portrait:angry
-> BeachHouseInterior

=== BribeDennis ===
Player: Wouldn't it be perfect for both parties? You forget about Claire, and I forget about the fact that I've bribed you. #portrait:smile
-> BribeSuccessful -> BribeDennisSuccess

=== BribeDennisSuccess ===
Dennis: That sounds certainly interesting. Okay, I'll forget about Claire for that amount. #portrait:smile
~UpdateQuest(ClaireQuest, ClaireQuest_2)
-> BeachHouseInterior