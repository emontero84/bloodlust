=== TalkToBallerina ===
Ballerina: Evening. #portrait:smile
-> Ballerina

=== Ballerina ===
#choice
* [ I like your dress. ] -> BallerinaDress
* { BallerinaDress } [ What's your show about? ] -> BallerinaBalletDancer
* { BallerinaDress } [ Tell me more about Paris. ] -> BallerinaParis
* { BallerinaBalletDancer } [ Is ballet so boring as they say? ] -> BallerinaBalletIsBoring
* [ What do you think about this painting? ] -> BallerinaPainting
+ [ I'm outta here. ] -> NoirGallery

=== BallerinaDress ===
Ballerina: Oh, thank you! It's from my last show at Paris. #portrait:smile
-> Ballerina

=== BallerinaBalletDancer ===
Ballerina: I'm a proffessional ballet dancer. We tour all over Europe on a regular basis.
-> Ballerina

=== BallerinaParis ===
Ballerina: Paris is my favorite city. It's so full of life and culture. #portrait:smile
-> Ballerina

=== BallerinaBalletIsBoring ===
Ballerina: Of course not! #portrait:angry
Ballerina: It's a classical art, filled with emotions and movement. #portrait:smile
-> Ballerina

=== BallerinaPainting ===
Ballerina: Yes, they are very intense. And the theme is fascinating.
-> Ballerina