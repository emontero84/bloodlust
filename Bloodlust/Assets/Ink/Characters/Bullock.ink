VAR BullockHasWrench = 0
VAR BullockHasCanOfGas = 0

=== TalkToBullockTheMotorhead ===
* { not BullockFirstEncounter } -> BullockFirstEncounter
+ { BullockFirstEncounter } -> BullockDefault

=== BullockFirstEncounter ===
Bullock: Hey, you! Can you help me?
-> BullockHelpChoice

=== BullockHelpChoice ===
#choice
* [ Of course. What can I do to help? ] -> BullockHelpYes
* [ What kind of help do you need? ] -> BullockHelpYes
* [ It depends. How much are you paying? ] -> BullockHelpNo

=== BullockDefault ===
Bullock: Hey. How's it goin', pal? #portrait:smile
-> BullockTheMotorhead

=== BullockTheMotorhead ===
#choice
* [ Why are you leaving Santa Monica? ] -> BullockLeavingTown
* [ Are you in some kind of trouble? ] -> BullockTrouble
* [ Tell me about Santa Monica. ] -> BullockSantaMonica
+ [ See you later. ] -> ParkingGarage

=== BullockHelpYes ===
Bullock: I'm leaving Los Angeles tonight. My bike needs some fixing and I can't find my 9/16 wrench.
Bullock: Also, I need some gas for the ride.
~AddQuest(BullockQuest)
-> ParkingGarage

=== BullockHelpNo ===
Bullock: Nice try, but I'm not paying. #portrait:angry
-> BullockHelpYes

=== BullockLeavingTown ===
Bullock: Well, let's say I must be somewhere else... as soon as possible...
-> BullockTheMotorhead

=== BullockTrouble ===
Bullock: Not if I leave town.
-> BullockTheMotorhead 

=== BullockSantaMonica ===
Bullock: This city is crazy! Have you been to the Madhouse Club? It's fucking unbelievable! #portrait:smile
Bullock: You should pay them a visit. They're in the middle of second street. Can't miss it.
-> BullockTheMotorhead

=== GiveWrenchToBullockTheMotorhead ===
Bullock: Thanks! That's precisely what I needed! #portrait:smile
~RemoveFromInventory(Wrench)
~BullockHasWrench = 1
-> CheckBullockQuest

=== GiveCanOfGasToBullockTheMotorhead ===
Bullock: Yeah! Thanks a lot! #portrait:smile
~RemoveFromInventory(CanOfGas)
~BullockHasCanOfGas = 1
-> CheckBullockQuest

=== CheckBullockQuest ===
{ 
	- BullockHasWrench == 1 && BullockHasCanOfGas == 1 : -> BullockQuestCompleted
	- else : -> ParkingGarage
}

=== BullockQuestCompleted ===
Bullock: That's awesome! Now I can leave town! #portrait:smile
~UpdateQuest(BullockQuest, BullockQuest_3)
~Hide(ParkingGarage_Bullock)
~Hide(ParkingGarage_Motorbike)
-> ParkingGarage