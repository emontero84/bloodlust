=== TalkToBernardThung ===
* { not BernardThungFirstEncounter } -> BernardThungFirstEncounter
+ { BernardThungFirstEncounter } -> BernardThungDefault

=== BernardThungFirstEncounter ===
Bernard Thung: Look who finally made it. Thought you'd never find me, huh? #portrait:smile
~UpdateQuest(FindBernardThungQuest, FindBernardThungQuest_1)
~UpdateQuest(FindBernardThungQuest, FindBernardThungQuest_2)
~UpdateQuest(FindBernardThungQuest, FindBernardThungQuest_3)
-> BernardThungGreetingChoice

=== BernardThungGreetingChoice ===
#choice
* [ Are you Bernard Thung? ] -> BernardThungGreetingWhoAreYou
* [ You must be Bernard Thung, I presume. ] -> BernardThungGreetingWhoAreYou
* [ And you think you're so fucking funny. ] -> BernardThungGreetingIntimidation 

=== BernardThungGreetingWhoAreYou ===
Bernard Thung: The one and only. Don't bother with the introductions. I know who you are.
-> BernardThung

=== BernardThungGreetingIntimidation ===
Bernard Thung: Ha ha ha! C'mon, don't take it too personal. Let's get down to business. #portrait:smile
-> BernardThung

=== BernardThungDefault ===
Bernard Thung: What do yo need? #portrait:smile
-> BernardThung

=== BernardThung ===
#choice
* [ I need to go to that warehouse for Mercurius. ] -> BernardThungWarehouse
* { BernardThungWarehouse } [ Can you get me there? ] -> BernardThungWarehouseGetMeThere
* { BernardThungCdRomQuestNo } [ About the missing CD... ] -> BernardThungCdRomQuestRetry
* { BernardThungPCReinstallSpyware } [ It's done. You have all the passwords and the spyware is running. ] -> HackingQuestComplete
* [ Do you really live in this silo? ] -> BernardThungSilo
* { GiveCdRomToBernardThung } [ Tell me about the data in the CD-ROM. ] -> BernardThungCdRom
* [ What hapenned between you and Tess Voermann? ] -> BernardThungTessVoermann
* [ What hapenned between you and Juliette Voermann? ] -> BernardThungJulietteVoermann
+ [ Bye bye! ] -> Silo

=== BernardThungWarehouse ===
Bernard Thung: I've been watching the place lately. The Cult has a bunch of lowlife humans working day and night to move stuff in there.
Bernard Thung: I don't know what they're up to. But there's a major staging going on. That's for sure. #portrait:angry
-> BernardThung

=== BernardThungWarehouseGetMeThere ===
Bernard Thung: Yes I can. But first, I need to know I can trust you.
Bernard Thung: Here's the deal. Somebody stole me a CD-ROM with a lot of sensitive information about Santa Monica's VIPs.
Bernard Thung: There's a folder for everyone in this city. So I need the disc back before the info gets published. Will you help me? #portrait:angry
-> BernardThungCdRomQuestChoice

=== BernardThungCdRomQuestRetry ===
Bernard Thung: Did you think it over?
-> BernardThungCdRomQuestChoice

=== BernardThungCdRomQuestChoice ===
#choice
* [ You can trust me. I'll do it. ] -> BernardThungCdRomQuestYes
* [ I can't promise anything. But I'll try. ] -> BernardThungCdRomQuestYes
* [ Forget it. I'm outta here. ] -> BernardThungCdRomQuestNo

=== BernardThungCdRomQuestYes ===
Bernard Thung: Excellent! Come back when you've found the CD-ROM. #portrait:smile
~AddQuest(BernardThungQuest)
-> Silo

=== BernardThungCdRomQuestNo ===
BernardThung: All right, all right. Think it over and come back if you change your mind.
-> BernardThung

=== GiveCdRomToBernardThung ===
Bernard Thung: Excellent! You're a life saver. And I know I can trust you. #portrait:smile
Bernard Thung: A promise's a promise. I can get you to the warehouse but once you're there, you're on your own. 
Bernard Thung: There's a cab waiting for you in main street. It will take you to the place.  
~RemoveFromInventory(CdRom)
~UpdateQuest(BernardThungQuest, BernardThungQuest_1)
~UpdateQuest(BernardThungQuest, BernardThungQuest_2)
~Show(MainStreet_Taxi)
-> Silo

=== BernardThungSilo ===
Bernard Thung: I know it looks like a small rat-hole in the middle of nowhere. But I really like it here! #portrait:smile
Bernard Thung: It's dark, cozy, there's no annoying neighbours... and the internet connection is great! #portrait:smile
-> BernardThung

=== BernardThungCdRom ===
Bernard Thung: I don't want to sound presumptuous but I'm afraid that you wouldn't understand it even if I tried.
-> BernardThungCdRomChoice

=== BernardThungCdRomChoice ===
#choice
* [ Try me. I'm smarter than you think. ] -> BernardThungAddHackingQuest
* [ Stop using that word if you're not trying to sound stupid. ] -> BernardThungCdRomNo
* [ Sometimes I forget you're a total douchebag. Forget it. ] -> BernardThungCdRomNo

=== BernardThungAddHackingQuest ===
Bernard Thung: You're right. You've proven yourself more than capable to tackle this issue. #portrait:smile
Bernard Thung: The CD-ROM was full of passwords and personal data about Santa Monica's VIPs.
Bernard Thung: The problem is that by now, most of the passwords are already obsolete. 
Bernard Thung: I need someone to hack into all the computers of Santa Monica and re-install my spyware software again. #portrait:angry
Bernard Thung: With the passwords and the spyware software running, I'll be untouchable again.
~AddQuest(HackingQuest)
-> Silo

=== HackingQuestComplete ===
Bernard Thung: You made it! I can't thank you enough, I really owe you big time. #portrait:smile
~UpdateQuest(HackingQuest, HackingQuest_3)
-> Silo

=== BernardThungCdRomNo ===
Bernard Thung: Nevermind.
-> BernardThung

=== BernardThungTessVoermann ===
Bernard Thung: Nothing, really. I never did anything wrong to her. I barely knew her, for god's sake!
Bernard Thung: But she never liked me. And she just found a great excuse in her sister to pick on me. #portrait:angry
-> BernardThung

=== BernardThungJulietteVoermann ===
Bernard Thung: That's kind of a long story. We had a peculiar relationship back in the day. #portrait:angry
Bernard Thung: We remained being close friends until her sister got in the middle, a couple of months ago.
-> BernardThung