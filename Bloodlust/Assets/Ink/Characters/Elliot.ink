=== TalkToElliot ===
* { not ElliotFirstEncounter } -> ElliotFirstEncounter
+ { ElliotFirstEncounter } -> ElliotDefault

=== ElliotFirstEncounter ===
Elliot: I'm sorry, now it's not a good time for talking...
-> Elliot

=== ElliotDefault ===
Elliot: How you doing, mate? #portrait:smile
-> Elliot

=== Elliot ===
#choice
* [ What are you doing here at the beach? ] -> ElliotWhatAreYouDoing
* [ What's troubling you? ] -> ElliotTroubles
* { ElliotTroubles } [ Tell me more about Lilly. ] -> ElliotMoreAboutLilly
* { ElliotQuestRejected } [ About your girlfriend... ] -> ElliotQuestChoice
+ [ Okay, let's talk some other time. ] -> Beach

=== ElliotWhatAreYouDoing ===
Elliot: We're all half-vampires, mate. This is the only place where we can hang out without being chased by other vampires.
-> Elliot

=== ElliotTroubles ===
Elliot: It's nothing, mate. My girlfriend just stood me up. I haven't heard from her since then. 
Elliot: Her name's Lilly and she's the most beautiful girl I've ever seen. #portrait:smile
Elliot: We met during a surfing contest and we instantly fell in love. Spent the nights talking at the Sunset Diner. 
Elliot: She told me she was a vampire and I begged her to make me one of her kind so we could spend eternity together.
Elliot: She finally turned me. But then she vanished. #portrait:angry
-> ElliotQuestChoice

=== ElliotQuestChoice ===
#choice
* [ Why don't you go searching for her? ] -> ElliotSearchForHer
* [ Maybe I can help you to find Lilly. ] -> ElliotQuestStarted
* [ You're boring me to death. She's gone. Just move on! ] -> ElliotQuestRejected

=== ElliotSearchForHer ===
Elliot: I can't! That's what's eating me up! #portrait:angry
Elliot: This beach is the only place that we half-vampires can hang out peacefully. 
Elliot: Your kind is hunting us down like we're animals. So I'm basically trapped here at the beach. #portrait:angry
-> ElliotQuestChoice

=== ElliotQuestStarted ===
Elliot: Really? Would you do that for me? That would be awesome! #portrait:smile
Elliot: I last saw her at the Sunset Diner. We hang out there a lot so maybe it's a good place to start searching.
~AddQuest(ElliotQuest)
-> Beach

=== ElliotQuestRejected ===
Elliot: All right! All right! There's no need to be such a jerk. #portrait:angry
-> Elliot

=== ElliotMoreAboutLilly ===
Elliot: Oh, Lilly! She was so unbelievable. Pale and charming, beautiful and funny. The moment I saw her I knew we were ment to be. #portrait:smile
Elliot: Now it really pains me... I'm stuck in this beach... And she's who knows where... #portrait:angry
-> Elliot

=== ElliotLillysCar ===
Player: I think I found some keys belonging to Lilly. #portrait:confused
Elliot: Yeah. These are her car keys. She had an old '64 Cadillac. Red and white. Absolutly stunning. 
Elliot: She used to park it at the garage near the beach...
-> Elliot

=== ElliotLillysPhoto ===
Player: I fond a photo of Lilly... #portrait:sad
Elliot: Oh! Thank you, mate. This brings me a lot of memories...
~RemoveFromInventory(LillysPhoto)
-> Elliot

=== GiveLillysDiaryToElliot ===
Elliot: You made it! Did you find something about where she's right now? Why she stood me up? #portrait:smile
Elliot: Wait! I'm not sure if I want to know...
Elliot: But thanks, mate, I owe you big time. I'll never forget it. #portrait:smile
Player: My pleasure. #portrait:smile
~RemoveFromInventory(LillysPhoto)
~RemoveFromInventory(LillysKey)
~RemoveFromInventory(LillysDiary)
~UpdateQuest(ElliotQuest, ElliotQuest_1)
~UpdateQuest(ElliotQuest, ElliotQuest_2)
~UpdateQuest(ElliotQuest, ElliotQuest_3)
-> Beach
