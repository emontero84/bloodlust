=== TalkToJulietteVoermann ===
* { not JulietteVoermannFirstEncounter } -> JulietteVoermannFirstEncounter
+ { JulietteVoermannFirstEncounter } -> JulietteVoermannDefault

=== JulietteVoermannFirstEncounter ===
Juliette Voermann: Oooh! what do we have here? Another young plaything lured into my club? #portrait:smile
Juliette Voermann: Mmmm... you smell fresh and new, little {IsFemale:girl}{not IsFemale:boy}. Straight outta the coffin, right? #portrait:smile
Juliette Voermann: Oh! I'm not frightening you, am I? #portrait:smile
-> JulietteVoermannGreetingChoice

=== JulietteVoermannGreetingChoice ===
#choice
* [ Who are you? ] -> JulietteVoermannWhoAreYou
* [ Frightening me? I love it! ] -> JulietteVoermannWhoAreYou
* [ What is your problem? ] -> JulietteVoermannWhoAreYou

=== JulietteVoermannWhoAreYou ===
Juliette Voermann: I'm the finger down your spine when all the lights are out. I'm the name on all the men's room walls. 
Juliette Voermann: When I smile, the whole world turns it neck. And everyone always wants to know, who... is...that...girl? #portrait:smile
-> JulietteVoermannWhoAreYouChoice

=== JulietteVoermannWhoAreYouChoice ===
#choice
* [ And who are you? ] -> JulietteVoermannPresentation
* { HasQuest(FindBernardThungQuest) } [ You must be Juliette. ] -> JulietteVoermannRuinedPresentation
* [ I don't have time for this. ] -> MadhouseOffice

=== JulietteVoermannPresentation ===
Juliette Voermann: I...am...Juliette. And this beautiful bit of chaos is my club. Wellcome to the Madhouse Club. #portrait:smile
-> JulietteVoermann

=== JulietteVoermannRuinedPresentation ===
Juliette Voermann: I am, indeed! And I'm glad that my reputation precedes me. #portrait:smile
Juliette Voermann: I hope that the stories you've heard about me were not... disappointing. #portrait:smile
-> JulietteVoermann

=== JulietteVoermannDefault ===
Juliette Voermann: How you doin', sweety? #portrait:smile
-> JulietteVoermann

=== JulietteVoermann ===
#choice
* { HasQuest(FindBernardThungQuest) } [ I need you to call off the feud with Bernard Thung. ] -> JulietteVoermannFeud
* { JulietteVoermannQuestNo } [ About Noir Gallery... ] -> JulietteVoermannQuestChoice
* [ Somebody told me your sister is in charge of the Madhouse Club. ] -> JulietteVoermannInChargeOfTheClub
+ [ I have to go. ] -> MadhouseOffice

=== JulietteVoermannFeud ===
Juliette Voermann: That's my sister's feud, not mine!  #portrait:angry
Juliette Voermann: But I can talk to her in your behalf. If you help me with a tiny little favor.
Juliette Voermann: Do you know Noir Gallery, in main street? I happen to know there's a charity event being organized there. #portrait:smile
Juliette Voermann: Lots of rich and famous Santa Monicans will gather there, sipping drinks and eating expensive sandwhiches.
Juliette Voermann: But there's a thing they dont know... The whole event is setup by a vampire trying to move up in Santa Monica's social ladder.
Juliette Voermann: And we can't let that happen, can we? So I need you to step in and bring the party to the next level. #portrait:angry
Juliette Voermann: Give the paintings in the gallery a good slashing. Don't get caught and don't turn it into a massacre. #portrait:smile
Juliette Voermann: Let the slashing speak for itself. And steal the charity box, would you? Buy yourself something velvet. #portrait:smile
-> JulietteVoermannQuestChoice

=== JulietteVoermannQuestChoice ===
#choice
* [ Consider it done. ] -> JulietteVoermannQuestYes
* [ Ok, I'll do it. ] -> JulietteVoermannQuestYes
* [ Forget it. I'm out. ] -> JulietteVoermannQuestNo

=== JulietteVoermannQuestYes ===
Juliette Voermann: Splendid! Hurry up, I can only amuse myslef for so long. #portrait:smile
~AddQuest(JulietteQuest)
-> MadhouseOffice

=== JulietteVoermannQuestNo ===
Juliette Voermann: Don't be such a party pooper! Promise me you'll think about it...
-> JulietteVoermann

=== JulietteVoermannInChargeOfTheClub ===
Juliette Voermann: And did you just swallowed it all? Oh baby, you're so sweet and tender! #portrait:smile
Juliette Voermann: My sister likes to pretend she owns the club all by herself, but we're both in the contract. #portrait:angry
Juliette Voermann: I'm in charge of the social aspects of the business while she takes care of the numbers. #portrait:smile
-> JulietteVoermann

=== StartVoermannDilemma ===
Juliette Voermann: You! What have you done?! #portrait:angry
Tess Voermann: It's not {IsFemale:her}{not IsFemale:his} fault! #portrait:angry
Juliette Voermann: You've help us both! And you've ruined our plans! #portrait:angry
~RemoveFromInventory(MariansDiary)
~UpdateQuest(TessQuest, TessQuest_1)
~UpdateQuest(TessQuest, TessQuest_3)
~UpdateQuest(TessQuest, TessQuest_4)
~AddQuest(VoermannDilemmaQuest)
-> VoermannDilemmaQuestChoice

=== VoermannDilemmaQuestChoice ===
#choice
* [ What's going on here? ] -> VoermannDilemmaExplanation
* { VoermannDilemmaExplanation } [ Juliette, tell me your side of things. ] -> VoermannDilemmaJuliette
* { VoermannDilemmaExplanation } [ Tess, tell me your side of things. ] -> VoermannDilemmaTess
* { VoermannDilemmaJuliette && VoermannDilemmaTess } [ Juliette, your sister's right. You have to go. ] -> VoermannDilemmaTessWins
* { VoermannDilemmaJuliette && VoermannDilemmaTess } [ Tess, your sister's right. You have to go. ] -> VoermannDilemmaJulietteWins
* { VoermannDilemmaJuliette && VoermannDilemmaTess } [ You're both right. You're stronger together. ] -> VoermannDilemmaBothWins

=== VoermannDilemmaExplanation ===
Juliette Voermann: I'll tell you what's going on! My sisters is a monster. She even tried to kill me! #portrait:angry
Tess Voermann: Don't you dare try to fool me, little sister! You're the one plotting against me. You know you have it coming! #portrait:angry
-> VoermannDilemmaQuestChoice

=== VoermannDilemmaJuliette ===
Juliette Voermann: My sister is a control freak. And I'm the only factor that she can't control. #portrait:angry
Tess Voermann: Juliette, you're a danger to the business. You fornicate with mortals! #portrait:angry
Juliette Voermann: All I do is for you and for the Madhouse. Without my touch, the club would be just another dull discotheque. 
-> VoermannDilemmaQuestChoice

=== VoermannDilemmaTess ===
Tess Voermann: My sister is a danger to the business. She's plotting against me, first with Thung and now with you. #portrait:angry
Juliette Voermann: Tess, you're a control freak! I wasn't plotting against you! Thung was dancing on my hand. I mean you no harm! #portrait:angry
Tess Voermann: I've worked my way up to the top. Now half of Santa Monica is ours. The last thing I need is my sister trying to sink us both.
-> VoermannDilemmaQuestChoice

=== VoermannDilemmaTessWins ===
Juliette Voermann: Maybe you're right. Santa Monica is ours, after all. What's the point in fighting? 
Tess Voermann: We made it to the top together, little sister. But now it's time to settle down and rule with an iron fist. 
Juliette Voermann: I will miss you, sister. But you're right. It's time for me to go. #portrait:smile
Tess Voermann: I'll call off the feud with Bernard Thung.
~UpdateQuest(VoermannDilemmaQuest, VoermannDilemmaQuest_2)
~UpdateQuest(VoermannDilemmaQuest, VoermannDilemmaQuest_3)
~UpdateQuest(FindBernardThungQuest, FindBernardThungQuest_2)
~IsSiloDoorOpen = 1
~Hide(SecondStreet_Padlock)
~Hide(MadhouseOffice_JulietteVoermann)
-> MadhouseOffice

=== VoermannDilemmaJulietteWins ===
Tess Voermann: Maybe you're right. I never gave you credit for anything. And you've proven yourself worthy more than enough times.
Juliette Voermann: We made it to the top together, sister. But now it's time for me to take the lead and rule with an iron fist.
Tess Voermann: I will miss you, little sister. But you're right. It's time for me to go. #portrait:smile
Tess Voermann: I'll call off the feud with Bernard Thung.
~UpdateQuest(VoermannDilemmaQuest, VoermannDilemmaQuest_1)
~UpdateQuest(VoermannDilemmaQuest, VoermannDilemmaQuest_3)
~UpdateQuest(FindBernardThungQuest, FindBernardThungQuest_2)
~IsSiloDoorOpen = 1
~Hide(SecondStreet_Padlock)
~Hide(MadhouseOffice_TessVoermann)
-> MadhouseOffice

=== VoermannDilemmaBothWins ===
Juliette Voermann: You're right! With your business skills and my contacts, we will be invincible! #portrait:smile
Tess Voermann: Of course! Together we can outsmart our enemies. #portrait:smile
Juliette Voermann: Thank you! You've teached us a very important lesson. #portrait:smile
Tess Voermann: I'll call off the feud with Bernard Thung.
~UpdateQuest(VoermannDilemmaQuest, VoermannDilemmaQuest_1)
~UpdateQuest(VoermannDilemmaQuest, VoermannDilemmaQuest_2)
~UpdateQuest(VoermannDilemmaQuest, VoermannDilemmaQuest_3)
~UpdateQuest(FindBernardThungQuest, FindBernardThungQuest_2)
~IsSiloDoorOpen = 1
~Hide(SecondStreet_Padlock)
-> MadhouseOffice