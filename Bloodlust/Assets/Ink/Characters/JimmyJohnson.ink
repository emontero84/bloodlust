=== TalkToJimmyJohnson ===
* { not JimmyJohnsonFirstEncounter } -> JimmyJohnsonFirstEncounter
+ { JimmyJohnsonFirstEncounter } -> JimmyJohnsonDefault

=== JimmyJohnsonFirstEncounter ===
Jimmy Johnson: Excuse me, are you the so-called black-market peddler?
-> JimmyJohnsonPeddlerChoice

=== JimmyJohnsonDefault ===
Jimmy Johnson: What can I do for you? #portrait:smile
-> JimmyJohnson

=== JimmyJohnsonPeddlerChoice ===
#choice 
* [ Do I look like a drug dealer? ] -> JimmyJohnsonDrugDealer
* [ Yes, I can sell you anything you need. ] -> JimmyJohnsonAnythingYouNeed
* [ No, I'm afraid I'm not your black-market peddler. ] -> JimmyJohnsonNoPeddler
+ [ I don't have time for this. ] -> MainStreet

=== JimmyJohnsonDrugDealer ===
Jimmy Johnson: Well, yes, in a certain way, I suppose. I've been waiting here for hours and the so-called peddler is not showing up.
-> JimmyPresentation

=== JimmyJohnsonAnythingYouNeed ===
Jimmy Johnson: Excellent! #portrait:smile
-> JimmyPresentation

=== JimmyJohnsonNoPeddler ===
Jimmy Johnson: I know, I've been waiting here for hours and the so-called peddler is not showing up. 
-> JimmyPresentation

=== JimmyPresentation ===
Jimmy Johnson: My name's Jimmy. Jimmy Johnson, attorney-at-law. #portrait:smile
-> JimmyJohnson

=== JimmyJohnson ===
#choice
* { JimmyJohnsonAnythingYouNeed } [ Let's get down to business. What can I hit you with, my friend? ] -> JimmyJohnsonDrugs
* { not SeduceJimmy && not IntimidateJimmy && SkillIntimidate } [ (Intimidate) I like your watch. Give it to me! ] -> IntimidateJimmyInit
* { not SeduceJimmy && not IntimidateJimmy && SkillSeduce } [ (Seduction) I love your watch. Trade it for a kiss? ] -> SeduceJimmyInit
* [ Tell me about yourself. ] -> JimmyYourself
* [ Tell me about Santa Monica. ] -> JimmySantaMonica
+ [ See you around, Jimmy. ] -> MainStreet

=== JimmyJohnsonDrugs ===
Jimmy Johnson: Uh! I like it when they go straight down to business. Powder, I'm looking for some nose candy, if you know what I mean.
Player: Yeah, but I'm pretty sure it's ilegal to buy drugs in the middle of the street. #portrait:confused
Jimmy Johnson: How come?
Player: Winners don't use drugs. #portrait:angry
-> JimmyJohnson

=== JimmyYourself ===
Jimmy Johnson: We're in the middle of the night. Maybe it's not a good time to trade life stories.
-> JimmyJohnson

=== JimmySantaMonica ===
Jimmy Johnson: This city is amazing. Everything is open all night long. Even the bailbonds! #portrait:smile
Jimmy Johnson: You should pay them a visit. They're at the start of second street. Say hi to Arthur Killpatrick on my behalf.
-> JimmyJohnson

=== IntimidateJimmyInit ===
Player: What?! #portrait:angry
-> IntimidateJimmyChoice

=== IntimidateJimmyChoice ===
#choice
* [ You heard me, motherfucker! Gimme that watch! ] -> IntimidateJimmyFail
* [ I don't have time to waste on you, mortal. Give it to me! Now! ] -> IntimidateJimmyFail
* [ The watch or your life. The choice is yours. ] -> IntimidateJimmy
* [ I'll crush your skull and drink all of your blood. The watch! Nooow! ] -> IntimidateJimmyFail

=== IntimidateJimmyFail ===
Jimmy Johnson: Ha! You don't know who you're talking to. I'm Jimmy Johnson. Attorney-at-law! #portrait:angry
Jimmy Johnson: Never fuck with a lawyer, {IsFemale:girl}{not IsFemale:boy}! #portrait:angry
-> MainStreet

=== IntimidateJimmy ===
Jimmy Johnson: But that's my father's watch! #portrait:angry
Player: Don't make me ask twice. #portrait:blood
Jimmy Johnson: All right, all right... here you go. #portrait:angry
~AddToInventory(GoldenWatch)
-> MainStreet

=== IntimidateJimmyWithBaseballBat ===
Player: I like your watch. Give it to me or I'll break your skull like a fucking piñata!
~RemoveFromInventory(BaseballBat)
-> IntimidateJimmy

=== IntimidateJimmyWithKatana ===
Player: I like your watch. Give it to me or I'll cut you like a rotten tuna!
~RemoveFromInventory(Katana)
-> IntimidateJimmy

=== SeduceJimmyInit ===
Jimmy Johnson: Errr... A kiss?
-> SeduceJimmyChoice

=== SeduceJimmyChoice ===
#choice
* [ Yeah! A sweet, long and wet kiss that you'll never forget. ] -> SeduceJimmyFail
* [ Come closer, big boy. I'm gonna give you unspeakable pleasure! ] -> SeduceJimmyFail
* [ A kiss means rough sex. *WINK* ] -> SeduceJimmyFail
* [ I've never kissed a lawyer before. I wonder how it tastes. ] -> SeduceJimmy

=== SeduceJimmyFail ===
Jimmy Johnson: Oh, my gosh! You're a psycho! Get off me, you freak! #portrait:angry
-> MainStreet

=== SeduceJimmy ===
Jimmy Johnson: Mmmm... That tickles! *CHUCKLE* #portrait:smile
~AddToInventory(GoldenWatch)
-> MainStreet