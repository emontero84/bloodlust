=== TalkToJay ===
Jay: Hey, whassap!
-> Jay

=== Jay ===
#choice
* [ Hey, what are you doing? ] -> JayIsWatchingTV
* { JayIsWatchingTV } [ What are you watching? ] -> JayWatchingCartoons
* { JayWatchingCartoons } [ Aren't you a bit old to watch cartoons? ] -> JayTooOldForCartoons
* { LookAtJayPC || UseBeachHouseInteriorPc } [ I like your computer. ] -> JayComputer
* { JayComputer } [ Do you know much about computers? ] -> JayComputerExpert
* { JayComputerExpert } [ Would you teach me to hack into computers? ] -> JayGiveSkillBook
* [ Tell me about your boss. ] -> JayAboutDennis
* [ Tell me about Santa Monica. ] -> JaySantaMonica
+ [ See ya! ] -> BeachHouseInterior

=== JayIsWatchingTV ===
Jay: Shut up. I'm watching TV. #portrait:angry
-> Jay

=== JayWatchingCartoons ===
Jay: Cartoons. #portrait:smile
-> Jay

=== JayTooOldForCartoons ===
Jay: Nah, I like cartoons.
-> Jay

=== JayComputer ===
Jay: Yeah, I like it too. #portrait:smile
-> Jay

=== JayComputerExpert ===
Jay: Yeah, I'm a big expert. #portrait:smile
-> Jay

=== JayGiveSkillBook ===
Jay: Sure, I'll teach you everything I know about computers.
Jay: Here. All you need is in this book. #portrait:smile
~AddToInventory(SkillBook8)
-> LookAtSkillBook8 -> BeachHouseInterior

=== JayAboutDennis ===
Jay: Dennis is the best. 
Jay: He pays for the weed and we play videogames. #portrait:smile
-> Jay

=== JaySantaMonica ===
Jay: You been to the Sunset Diner?
Jay: They have the best burgers in all Santa Monica. #portrait:smile
Jay: It's at the end of seconds street. Can't miss it.
-> Jay