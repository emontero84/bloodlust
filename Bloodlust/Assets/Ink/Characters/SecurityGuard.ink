=== TalkToSecurityGuard ===
* { not SecurityGuardFirstEncounter } -> SecurityGuardFirstEncounter
+ { SecurityGuardFirstEncounter } -> SecurityGuardDefault

=== SecurityGuardFirstEncounter ===
Security Guard: Good evening, what can I do for you? #portrait:smile
-> SecurityGuard

=== SecurityGuardDefault ===
Security Guard: Good evening, {IsFemale:madam}{not IsFemale:sir}. #portrait:smile
-> SecurityGuard

=== SecurityGuard ===
#choice
* [ What are you doing? ] -> SecurityGuardDoing
* { AlphaTollWorker && SecurityGuardDoing } [ Do you know the big guy with the mohawk? ] -> SecurityGuardAlpha
* { AlphaTollWorker && SecurityGuardAlpha } [ Do you work together? ] -> SecurityGuardAlphaWork
* [ Tell me about Santa Monica. ] -> SecurityGuardSantaMonica
* { SecurityGuardSantaMonica && AlphaBribed == 0 } [ I can't go to the beach because of the big guy with the mohawk! ] -> SecurityGuardAlphaToll
+ [ See you later. ] -> ParkingGarage

=== SecurityGuardDoing ===
Security Guard: Well, I'm doing the night shift. Safeguard your vehicles with a vigilant eye.
-> SecurityGuard

=== SecurityGuardAlpha ===
Security Guard: Yeah, of course. These punks spend all day and night here at the parking lot.
Security Guard: Drinking beer and sit on their asses. That's all they do. #portrait:angry
-> SecurityGuard

=== SecurityGuardAlphaWork ===
Security Guard: What?! No, that punk has never worked a single day of his entire life. #portrait:angry
Security Guard: They must be playing some kind of joke on you. #portrait:smile
-> SecurityGuard

=== SecurityGuardSantaMonica ===
Security Guard: I'm not much of a swimmer but you have to see the beach of Santa Monica. #portrait:smile
Security Guard: It's just through the stairs on the left. You can't miss it.
-> SecurityGuard

=== SecurityGuardAlphaToll ===
Security Guard: I wish I could help you, but they only pay me to watch over the cars.
-> SecurityGuard