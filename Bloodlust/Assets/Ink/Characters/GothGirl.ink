=== TalkToGothGirl ===
Goth Girl: Hey! #portrait:smile
-> GothGirl

=== GothGirl ===
#choice
* [ How you doing? ] -> GothGirlHowYouDoing
* [ What's your opinion about the Madhouse Club? ] -> GothGirlMadhouseClub
* [ What are you drinking? ] -> GothGirlSpecialCocktail
* { HasQuest(BarmanQuest) && SkillSeduce } [ (Seduction) Tell me, beautiful. What if we trade some vampire kisses? ] -> SeduceGothGirlInit
* { HasQuest(BarmanQuest) } [ Have you heard about the Vampire's Kiss? ] -> GothGirlHeardVampireKiss
* { HasQuest(BarmanQuest) && GothGirlGetVampireKiss } [ What if you give me a little sip of your Vampire's Kiss? ] -> GothGirlVampireKissSample
+ [ See you around. ] -> MadhouseClub

=== GothGirlHowYouDoing ===
Goth Girl: The night's coming along pretty nicely. Thanks for asking. #portrait:smile
-> GothGirl

=== GothGirlMadhouseClub ===
Goth Girl: It's the real deal in Santa Monica. Great music, cheap drinks and it's open all night long.
-> GothGirl

=== GothGirlVampireKissSample ===
Goth Girl: Mmmm... All right, how about a little sample? #portrait:smile
~AddToInventory(VialOfBlood)
~UpdateQuest(BarmanQuest, BarmanQuest_1)
-> MadhouseClub

=== GothGirlSpecialCocktail ===
Goth Girl: Uhm, it's a... special cocktail.
-> GothGirlSpecialCocktailChoice

=== GothGirlSpecialCocktailChoice ===
#choice
* [ Why is it special? ] -> GothGirlSpecialCocktailWhy
* [ Is your special cocktail a Vampire's Kiss, by any chance? ] -> GothGirlSpecialCocktailIsVampireKiss
* [ Where can I get some of your special cocktail? ] -> GothGirlGetVampireKiss

=== GothGirlSpecialCocktailWhy ===
Goth Girl: It tastes like blood. #portrait:smile
-> GothGirl

=== GothGirlSpecialCocktailIsVampireKiss ===
Goth Girl: Uhm... maybe?
-> GothGirl

=== GothGirlHeardVampireKiss ===
Goth Girl: Yeah, everybody is talking about it. It's the new shit. And it's pretty wild. #portrait:smile
Goth Girl: I mean, I've heard it's pretty wild.
-> GothGirlHeardVampireKissChoice

=== GothGirlHeardVampireKissChoice ===
#choice
* [ Can you get me some Vampire's Kiss? ] -> GothGirlGetVampireKiss
* [ The barman has told me that there's a new drug in town. ] -> GothGirlGetVampireKiss
* [ I can give you a real Vampire's Kiss. ] -> GothGirlRealVampireKiss

=== GothGirlGetVampireKiss ===
Goth Girl: I'm not supposed to...
-> GothGirl

=== GothGirlGetVampireKissChoice ===
#choice
* [ Where did you get it? ] -> GothGirlRunAway
* [ Can you tell who's your pusher? ] -> GothGirlRunAway
* [ Okay, thank you. ] -> MadhouseClub

=== SeduceGothGirlInit ===
Goth Girl: What do you mean? #portrait:smile
-> SeduceGothGirlChoice

=== SeduceGothGirlChoice ===
#choice
* [ You know what I mean, darling. *WINK* ] -> SeduceGothGirlFail
* [ Can't you see it, dear? I'm an undead trading kisses for a blood vial. ] -> SeduceGothGirl
* [ I mean sweet kisses and dirty games in the dark. ] -> SeduceGothGirlFail
* [ Wanna trade kisses for blood, honey? ] -> SeduceGothGirlFail

=== SeduceGothGirlFail ===
Goth Girl: No way! Get outta here! #portrait:angry
-> MadhouseClub

=== SeduceGothGirl ===
Goth Girl: Uh! That sounds really... amusing! *CHUCKLE* Show me what you have, babe. #portrait:smile
Player: All right! #portrait:blood
~AddToInventory(VialOfBlood)
~UpdateQuest(BarmanQuest, BarmanQuest_1)
-> MadhouseClub

=== GothGirlRunAway ===
Goth Girl: I don't know nothing! I swear! #portrait:angry
-> MadhouseClub

=== GothGirlRealVampireKiss ===
Goth Girl: Ha ha ha! You're so funny. #portrait:smile
Player: It's not a joke. #portrait:blood
Goth Girl: Nice make up! You gotta tell me where did you buy those fangs. They're so cool! #portrait:smile
-> GothGirl