VAR CoffeThermosGiven = 0

=== TalkToCarter ===
Officer Carter: Move along, please. *YAWN* There's nothing to see here.
-> Carter

=== Carter ===
#choice
* { not CarterInvestigation } [ What is going on? I live in the next apartment. ] -> CarterInvestigation
* { not CarterInvestigation } [ Good evening, officer. May I ask what is going on? ] -> CarterInvestigation
* { CarterInvestigation } [ I understand. And how long is this investigation gonna take? ] -> CarterHowLong
* [ You seem a bit tired, officer. ] -> CarterTired
* [ Tell me about Santa Monica. ] -> CarterSantaMonica
+ [ Okay, I'm outta here. ] -> LodgingHallway

=== CarterInvestigation ===
Officer Carter: I'm not allowed to discuss the details of the ongoing *YAWN* investigation.
-> Carter

=== CarterHowLong ===
Officer Carter: Don't know. *YAWN* We're only following orders, {IsFemale:madam}{not IsFemale:sir}. 
-> Carter

=== CarterTired ===
Officer Carter: Yes. *YAWN* I must admit I'm a bit sleepy.
-> Carter

=== GiveEmptyCoffeThermosToOfficerCarter ===
Officer Carter: Oh, coffee! #portrait:smile
Player: Yes, I'll come back when the thermos is not empty... #portrait:confused
-> LodgingHallway

=== GiveCoffeThermosToOfficerCarter ===
Officer Carter: Oh, thanks! That was very thoughtful of you. #portrait:smile
~RemoveFromInventory(CoffeeThermos)
~CoffeThermosGiven = 1
-> CheckBothOfficersAreHappy

=== CarterSantaMonica ===
Officer Carter: This city is amazing, everything is open all night.
Officer Carter: Hey Chunk, what was the name of the place with the awesome burguers?
Officer Chunk: Sunset Diner. #portrait:smile
Officer Carter: Yeah, they have the best burguers in the whole bay area. #portrait:smile
Officer Carter: You should pay them a visit. They're at the end of second street.
-> Carter