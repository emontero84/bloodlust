=== TalkToCowboy ===
Cowboy: Yep?
-> Cowboy

=== Cowboy ===
#choice
* [ Nice hat. ] -> CowboyHat
* [ What do you think about this painting? ] -> CowboyPainting
+ [ I'm outta here. ] -> NoirGallery

=== CowboyHat ===
Cowboy: D'ya like it, {IsFemale:gal}{not IsFemale:son}? 
Cowboy: I'm so glad to hear it! #portrait:smile
-> Cowboy

=== CowboyPainting ===
Cowboy: Well, they're all very colorful and such. 
Cowboy: But I'm not too interested in art. I'm here only for the sandwiches.
-> Cowboy