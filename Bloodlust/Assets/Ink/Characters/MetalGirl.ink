=== TalkToMetalGirl ===
Metal Girl: Hi! #portrait:smile
-> MetalGirl

=== MetalGirl ===
#choice
* [ How you doing? ] -> MetalGirlHowYouDoing
* [ What's your opinion about the Madhouse Club? ] -> MetalGirlMadhouseClub
* [ What are you drinking? ] -> MetalGirlDrinking
* { HasQuest(BarmanQuest) } [ Have you heard about the Vampire's Kiss? ] -> MetalGirlHeardVampireKiss
* { HasQuest(BarmanQuest) && MetalGirlHeardVampireKiss } [ Can you get me some Vampire's Kiss? ] -> MetalGirlNoVampireKiss
* { HasQuest(BarmanQuest) && not HasInInventory(VialOfBlood) } [ Do you know where I can buy some Vampire's Kiss? ] -> MetalGirlNoBuyVampireKiss
* { HasQuest(BarmanQuest) && HasInInventory(VialOfBlood) } [ I have some Vampire's Kiss and I need to find more! ] -> MetalGirlWhereToBuyVampireKiss
+ [ See you around. ] -> MadhouseClub

=== MetalGirlHowYouDoing ===
Metal Girl: Great! Nice music and lots of beer! How could it be better? #portrait:smile
-> MetalGirl

=== MetalGirlMadhouseClub ===
Metal Girl: Awesome! This club is really awesome! It's allways open, and allways full of people! #portrait:smile
-> MetalGirl

=== MetalGirlDrinking ===
Metal Girl: Rum, gin and a couple of shots of whiskey.
-> MetalGirl

=== MetalGirlHeardVampireKiss ===
Metal Girl: Of course! Who doesn't? #portrait:smile
-> MetalGirl

=== MetalGirlNoVampireKiss ===
Metal Girl: I'm sorry! Right now I don't have any!
-> MetalGirl

=== MetalGirlNoBuyVampireKiss ===
Metal Girl: What?! Are you a cop or something? #portrait:angry
-> MetalGirlCopChoice

=== MetalGirlCopChoice ===
#choice
* [ Yes, I'm a police officer on vacation. ] -> MetalGirlCopAnswer
* [ Do I look like a cop? ] -> MetalGirlCopAnswer
* [ No, I'm not a fucking cop. Just a corcerned citizen. ] -> MetalGirlCopAnswer

=== MetalGirlCopAnswer ===
Metal Girl: Yeah! Whatever.
-> MetalGirl

=== MetalGirlWhereToBuyVampireKiss ===
Metal Girl: Well done, Columbo! I've heard you can get more vials from the doomsayer outside the club. #portrait:smile
-> MadhouseClub