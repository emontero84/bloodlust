=== TalkToRose ===
* { not RoseFirstEncounter } -> RoseFirstEncounter
+ { RoseFirstEncounter } -> RoseDefault

=== RoseFirstEncounter ===
Rose: I see you... I see you in China.
-> RoseFirstVisionChoice

=== RoseDefault ===
Rose: Hi again. #portrait:smile
-> Rose

=== RoseFirstVisionChoice ===
#choice
* [ What? I've never been to China. ] -> RoseFirstVision
* [ Maybe you're mistaking me for somebody else. ] -> RoseFirstVision
* [ Are you on drugs? ] -> RoseFirstVision
* [ I don't have time for this. ] -> Beach

=== Rose ===
#choice
* { RoseFirstVision } [ Seems like you have a gift. Please, tell me what you see about me. ] -> RoseSecondVision
* [ What are you all doing here in the beach? ] -> RoseBeach
* { RoseBeach } [ Why are you hiding in the beach? ] -> RoseHalfBlood
* { RoseHalfBlood } [ Tell me about the bigger problem that is yet to come. ] -> RoseProblemsToCome
* { HasQuest(RoseQuest) && IsQuestStepUpdated(RoseQuest_2) } [ It's done. The holy man is leaving the bay. ] -> RoseQuestCompleted
+ [ Goodbye. ] -> Beach

=== RoseFirstVision ===
Rose: The sins of the fathers... will be washed away... by their children. 
Rose: Oh! Forgive me. My words are not making any sense. But they speak the truth that I... foresee.
-> Rose

=== RoseSecondVision ===
Rose: Your shadow rips through a million worlds. In all of them you are given the unique opportunity... to make things matter.
Rose: The sarcophagus... The father of our kind... That cannot be! Or is it meant to be?
Rose: Oh! Please, forget it. I don't really know what I'm saying. #portrait:smile
-> Rose

=== RoseBeach ===
Rose: We're all hiding. Hiding from the light and the anger of our own kind. These shores are silent and... reassuring. #portrait:smile
-> Rose

=== RoseHalfBlood ===
Rose: Because we're half-vampires... the outcast of the damned.
Rose: They say we are the weakest breed of our kind. And that's why they hunt us down.
Rose: But they don't know there're bigger problems yet to come... for all vampires.
-> Rose

=== RoseProblemsToCome ===
Rose: Of course! You're no clairvoyant, but maybe you can help. #portrait:smile
Rose: A fiercesome holy man has arrived to Santa Monica. He doesn't know it yet... but he may end up with all vampires in the bay.  
Rose: You must stop the holy man... before it's too late. And we all become his prey. #portrait:angry
~AddQuest(RoseQuest) 
-> Beach

=== RoseQuestCompleted ===
Rose: Oh! Magnificient! We can all rest assured... for now.
Rose: Thanks a lot for your efforts!
~UpdateQuest(RoseQuest, RoseQuest_1)
~UpdateQuest(RoseQuest, RoseQuest_2)
~UpdateQuest(RoseQuest, RoseQuest_3)
-> Beach