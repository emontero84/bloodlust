=== TalkToOldCop ===
* { not OldCopFirstEncounter } -> OldCopFirstEncounter
+ { OldCopFirstEncounter } -> OldCopDefault

=== OldCopFirstEncounter ===
Old James: Hi, my name's James. I used to be a cop, and now look at me.
Old James: Drinking Scotch, by myself, in a filthy bar...
-> OldCopFirstEncounterChoice

=== OldCopFirstEncounterChoice ===
#choice
* [ Nice to meet you, James. Need some help? ] -> OldCopNiceToMeetYou
* [ It's not so bad. Now you have some company. ] -> OldCopNiceToMeetYou
* [ Don't you cops always end like this? ] -> OldCopNiceToMeetYou

=== OldCopNiceToMeetYou ===
Old James: Oh god! You're right! Sorry about that, I didn't mean it to sound so depressive. 
Old James: It's just the booze talking. And the retirement. My back's giving me some pains lately.
Old James: But enough about me. How can I help you, friend? #portrait:smile
-> OldCop

=== OldCopDefault ===
Old Cop: Good evening, friend. #portrait:smile
-> OldCop

=== OldCop ===
#choice
* [ How you doing? ] -> OldCopHowYouDoing
* [ What's your opinion about the Madhouse Club? ] -> OldCopMadhouseClub
* [ What are you drinking? ] -> OldCopDrinking
* { HasQuest(BarmanQuest) } [ Have you heard about the Vampire's Kiss? ] -> OldCopHeardVampireKiss
+ [ I'm outta here. ] -> MadhouseClub

=== OldCopHowYouDoing ===
Old James: There are some good days, and some bad days. But mostly I hang out here, by myself.
Old James: I'd kill to get a case again... #portrait:angry
Old James: Not literally! I'm still a cop in my heart...
-> OldCop

=== OldCopMadhouseClub ===
Old James: It's not the best bar in town, but the booze is cheap and the music is so loud you can barely think.
Old James: It's a shame the rest of the clientele is around thirteen years old. #portrait:angry
Old James: Just kidding. I'm the one who's outta place here... but this Scotch is really good. #portrait:smile
-> OldCop

=== OldCopDrinking ===
Old James: I'm not giving trademarks, but it's from Scotland and my name is James...
-> OldCop

=== OldCopHeardVampireKiss ===
Old James: Is that a new videogame or something?
Old James: I'm sorry kid, I'm a bit outta fashion with these things...
-> OldCop