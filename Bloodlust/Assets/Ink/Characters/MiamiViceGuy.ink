=== TalkToMiamiViceGuy ===
Sonny: Hey, what's up? #portrait:smile
-> MiamiViceGuy

=== MiamiViceGuy ===
#choice
* [ Nice jacket. ] -> MiamiViceGuyJacket
* { MiamiViceGuyJacket && !MiamiViceGuyPolyesterHate } [ I love polyester. ] -> MiamiViceGuyPolyesterLove
* { MiamiViceGuyJacket && !MiamiViceGuyPolyesterLove } [ I hate polyester. ] -> MiamiViceGuyPolyesterHate
* { MiamiViceGuyPolyesterLove } [ Maybe we should start a band together. ] -> MiamiViceGuyDiscoBand
* [ I think I have seen you somewhere else... ] -> MiamiViceGuyRegularFace
* [ What do you think about this painting? ] -> MiamiViceGuyPainting
+ [ I'm outta here. ] -> NoirGallery

=== MiamiViceGuyJacket ===
Sonny: Oh! Thank you, it's all polyester. #portrait:smile
-> MiamiViceGuy

=== MiamiViceGuyPolyesterLove ===
Sonny: Oh dude! We have so much in common! #portrait:smile
-> MiamiViceGuy

=== MiamiViceGuyPolyesterHate ===
Sonny: What? Polyester is the best! #portrait:angry
-> MiamiViceGuy

=== MiamiViceGuyDiscoBand ===
Sonny: Totally! I can't play any instruments but I'm all in with the attitude! #portrait:smile
-> MiamiViceGuy

=== MiamiViceGuyRegularFace ===
Sonny: Yeah, it happens a lot! Maybe I have a very familiar face. #portrait:smile
-> MiamiViceGuy

=== MiamiViceGuyPainting ===
Sonny: Honestly, I don't know much about art. I'm here just for the drinks. #portrait:smile
-> MiamiViceGuy