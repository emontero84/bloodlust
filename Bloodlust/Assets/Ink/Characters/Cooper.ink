VAR CooperHasBooks = 0

=== TalkToCooper ===
* { not CooperFirstEncounter } -> CooperFirstEncounter
+ { CooperFirstEncounter } -> CooperDefault

=== CooperFirstEncounter ===
Cooper: Aw man! Did you know we all have to live only by night?
-> CooperByNightChoice

=== CooperByNightChoice ===
#choice
* [ I'm afraid I don't follow. ] -> CooperVampires
* [ What do you mean by only by night? ] -> CooperVampires
* [ Of course! That's why we are vampires! ] -> CooperVampires

=== CooperVampires ===
Cooper: But that's not fair! I didn't choose to be a... you know... a vampire.
Cooper: I can live with the blood-sucking and the immortality. That's the real thing!
Cooper: But only for the nights?... That screwed me up, man!
-> CooperNightChoice

=== CooperNightChoice ===
#choice
* [ But how about living each night as the last one? ] -> CooperGivesQuest
* [ What are you complaining about? ] -> CooperGivesQuest
* [ You're right, it's not worth it. I can help you end your misery. ] -> CooperGivesQuest

=== CooperGivesQuest ===
Cooper: Maybe you're right. I should look on the bright side... 
Cooper: Now I can read all the books in the world!
Cooper: Would you bring me something interesting to read? Maybe it's time to finish my philosophy studies.
~AddQuest(CooperQuest)
-> Beach

=== CooperDefault ===
Cooper: Hey, it's you again.
-> Cooper

=== Cooper ===
#choice
* [ Tell me more about yourself. ] -> CooperHimself
+ [ See ya! ] -> Beach

=== CooperHimself ===
Cooper: There's not much to tell. I moved to Santa Monica a few years ago. Been studying philosophy since then. 
-> Cooper

=== GivePhilosophyBookToCooper1 ====
Cooper: Thanks! I always wanted to read this one!
~RemoveFromInventory(PhilosophyBook1)
~UpdateQuest(CooperQuest, CooperQuest_1)
-> CooperGetsBook

=== GivePhilosophyBookToCooper2 ====
Cooper: Uh! Great one!
~RemoveFromInventory(PhilosophyBook2)
~UpdateQuest(CooperQuest, CooperQuest_2)
-> CooperGetsBook

=== GivePhilosophyBookToCooper3 ====
Cooper: Nice! I have so much to read!
~RemoveFromInventory(PhilosophyBook3)
~UpdateQuest(CooperQuest, CooperQuest_3)
-> CooperGetsBook

=== CooperGetsBook ===
~CooperHasBooks++
{ 
	- CooperHasBooks == 3 : -> CooperQuestCompleted 
	- CooperHasBooks < 3 : -> Beach
}

=== CooperQuestCompleted ===
~UpdateQuest(CooperQuest, CooperQuest_4)
-> Beach