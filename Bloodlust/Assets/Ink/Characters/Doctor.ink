=== TalkToDoctor ===
* { not DoctorFirstEncounter } -> DoctorFirstEncounter
+ { DoctorFirstEncounter } -> DoctorDefault

=== DoctorFirstEncounter ===
Doctor: Hi, my name's doctor Smith. #portrait:smile
Doctor: How can I help you? #portrait:smile
-> Doctor

=== DoctorDefault ===
Doctor: Hi, again. How can I help you? #portrait:smile
-> Doctor

=== Doctor ===
#choice
+ { not DoctorVampire } [ I feel really sick, doctor. ] -> DoctorSymptoms
* { HasInInventory(RatPoison) } [ Can I kill a human with rat poison? ] -> DoctorRatPoison
* { HasInInventory(Laxatives) } [ Can I kill a human with laxatives? ] -> DoctorLaxatives
* { HasInInventory(SleepingPills) } [ Can I kill a human with sleeping pills? ] -> DoctorSleepingPills
+ [ See ya around, doc! ] -> HospitalOffice

=== DoctorSymptoms === 
Doctor: What are your symptoms?
-> DoctorSymptomsChoice

=== DoctorSymptomsChoice ===
#choice
* [ I don't sleep by night. ] -> DoctorSleep
* [ I can't eat food. ] -> DoctorEat
* [ I can't touch sunlight. ] -> DoctorSun
* [ My heart has stopped. ] -> DoctorHeart
* { DoctorSleep && DoctorEat && DoctorHeart && DoctorSun } [ I might be a vampire, doc. ] -> DoctorVampire
+ [ That's all for now. ] -> Doctor

=== DoctorSleep ===
Doctor: That's a pretty common problem. Don't you worry, there are plenty of remedies for insomnia.
-> DoctorSymptomsChoice

=== DoctorEat ===
Doctor: Well, that's a bit more problematic. But don't you worry, there are plenty of remedies for anorexia.
-> DoctorSymptomsChoice

=== DoctorSun ===
Doctor: It's not very common, but it happens... Don't worry, there are some remedies for skin hypersensitivity.
-> DoctorSymptomsChoice

=== DoctorHeart ===
Doctor: Err... That's certainly odd...
Doctor: But I'm sure we can find a remedy for that too! #portrait:smile
-> DoctorSymptomsChoice

=== DoctorVampire ===
Doctor: It certainly looks like a complex medical problem.
Doctor: Let me see...
Doctor: Maybe this pills help you to regain some sleep. #portrait:smile
Doctor: It will not turn into a human again, but at least you will be very well rested.
~AddToInventory(SleepingPills)
Player: Thank you, doctor. #portrait:smile
-> HospitalOffice

=== DoctorRatPoison ===
Doctor: Well, rat poison is a strong chemical. A small dose of rat poison will get you stomach pain, but not death.
Doctor: However, don't try to make a mojito with rat poison or you'll end up running to the toilet all night long. #portrait:smile
-> Doctor

=== DoctorLaxatives ===
Doctor: Laxatives are ment to get your vowels moving. A big dose of laxatives might get you stomach pain, but not death.
Doctor: However, don't try to make a margarita with laxatives or your guests will end up running to the toilet all night long. #portrait:smile
-> Doctor

=== DoctorSleepingPills ===
Doctor: Sleeping pill are ment to get you sleeping. 
Doctor: You're not going to kill a character with sleeping pills in a graphic adventure. 
Doctor: But don't try it in the real world. It's a party pooper. #portrait:smile
-> Doctor