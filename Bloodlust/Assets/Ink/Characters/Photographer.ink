=== TalkToPhotographer ===
* { not PhotographerFirstEncounter } -> PhotographerFirstEncounter
+ { PhotographerFirstEncounter } -> PhotographerDefault

=== PhotographerFirstEncounter ===
Photographer: I'm busy! Make it quick! #portrait:angry
-> Photographer

=== PhotographerDefault ===
Photographer: You again? 
-> Photographer

=== Photographer ===
#choice
* [ How's the investigation going on? ] -> PhotographerInvestigation
* [ I have a couple of questions. ] -> PhotographerNoQuestions
+ [ I'm outta here. ] -> Apartment102

=== PhotographerNoQuestions ===
Photographer: I don't have time for questions. Sorry.
-> Photographer

=== PhotographerInvestigation ===
Photographer: Really bad! The detective and the officer can't agree on what to put in the report. #portrait:angry
Photographer: And I'm doing photos of all the evidences in all possible angles.
Photographer: Can you believe it?
-> Photographer