=== TalkToTessVoermann ===
* { not TessVoermannFirstEncounter } -> TessVoermannFirstEncounter
+ { TessVoermannFirstEncounter } -> TessVoermannDefault

=== TessVoermannFirstEncounter ===
Tess Voermann: What have you done?! It was my event at the Noir Gallery! #portrait:angry
~UpdateQuest(FindBernardThungQuest, FindBernardThungQuest_1)
-> TessVoermannGivesYouNoChoice

=== TessVoermannGivesYouNoChoice ===
#choice
* [ It wasn't me! ] -> TessVoermannExpainsEverything
* [ Let me tell you the whole story. ] -> TessVoermannExpainsEverything
* [ I can explain! ] -> TessVoermannExpainsEverything
* [ ... ] -> TessVoermannExpainsEverything

=== TessVoermannExpainsEverything ===
Tess Voermann: I already know what happened, don't waste my precious time with your futile excuses, fledgeling. #portrait:angry
Tess Voermann: Juliette told me everything that there is to know. You have ruined years of careful preparations. #portrait:angry
Tess Voermann: And still you want me to call off the feud with Thung! #portrait:angry
Tess Voermann: But of course, I'm well aware of the fact that Juliette played you just like she plays with everyone else.
Tess Voermann: I am magnanimous. Let us forgive and forget... but first, there's a small favor a have to ask of you. #portrait:smile
Tess Voermann: There's a police investigation at the apartment 102 of the Lodgings. A very unclean affair, I must add.
Tess Voermann: A young vampire who goes by the name of Marian was turned into ashes at her apartment. 
Tess Voermann: Apparently some vampire hunters took care of her during her sleep. The hunters left some leads at the crime scene that you must remove.
Tess Voermann: You have to go and investigate the crime scene. We don't want the police to find any supernatural evidences, right? #portrait:smile
~RemoveFromInventory(CharityBox)
~UpdateQuest(JulietteQuest, JulietteQuest_1)
~UpdateQuest(JulietteQuest, JulietteQuest_2)
~UpdateQuest(JulietteQuest, JulietteQuest_3)
~UpdateQuest(JulietteQuest, JulietteQuest_4)
-> TessVoermannQuestChoice

=== TessVoermannQuestChoice ===
#choice
* [ Sure thing. It's the least I can do. ] -> TessVoermannQuestYes
* [ Let me see what I can do. ] -> TessVoermannQuestYes
* [ Forget it. I'm outta here. ] -> TessVoermannQuestNo

=== TessVoermannQuestYes ===
Tess Voermann: Excellent! Come back when it's done. #portrait:smile
~AddQuest(TessQuest)
-> MadhouseOffice

=== TessVoermannQuestNo ===
Tess Voermann: I won't take no for an answer. #portrait:smile
-> TessVoermannQuestChoice

=== TessVoermannDefault ===
Tess Voermann: Yes?
-> TessVoermann

=== TessVoermann ===
#choice
* [ Somebody told me your sister is in charge of the Madhouse Club. ] -> TessVoermannInChargeOfTheClub
+ [ I'm outta here. ] -> MadhouseOffice

=== TessVoermannInChargeOfTheClub ===
Tess Voermann: Nonsense! My sister is just the pretty face behind the counter giving free shots of Tequila on the house. #portrait:angry
Tess Voermann: I'm the one doing all the hard work behind the curtains. The meetings, the management and the finances. 
Tess Voermann: I understand your misunderstanding but in fact I am the only proprietor of the Madhouse Club. #portrait:smile
-> TessVoermann