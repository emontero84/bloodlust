=== TalkToRichGirl ===
Rich Girl: Yes? Gosh, I can barely hear you with that noise. #portrait:angry
-> RichGirl

=== RichGirl ===
#choice
* [ How you doing? ] -> RichGirlHowYouDoing
* [ What's your opinion about the Madhouse Club? ] -> RichGirlMadhouseClub
* [ What are you drinking? ] -> RichGirlDrinking
* { HasQuest(BarmanQuest) } [ Have you heard about the Vampire's Kiss? ] -> RichGirlHeardVampireKiss
+ [ See you around. ] -> MadhouseClub

=== RichGirlHowYouDoing ===
Rich Girl: I don't know. It's my first time in this club...
Rich Girl: It smells funny in here. And the floor is all sticky. Oh my gosh! #portrait:angry
-> RichGirl

=== RichGirlMadhouseClub ===
Rich Girl: I don't know. It's too dark, too loud and too dirty for my taste. #portrait:angry
-> RichGirl

=== RichGirlDrinking ===
Rich Girl: I'm not sure, but it's pretty strong. And cheap. #portrait:smile
-> RichGirl

=== RichGirlHeardVampireKiss ===
Rich Girl: Not really. Is some kind of metal band? They all sound the same to me, you know.
-> RichGirl

