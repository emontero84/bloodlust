
VAR VodkaBottlesDelivered = 0

=== TalkToHomelessDoug ===
* { not HomelessDougFirstEncounter } -> HomelessDougFirstEncounter
+ { HomelessDougFirstEncounter } -> HomelessDougDefault

=== HomelessDougFirstEncounter ===
Homeless Doug: Spare some change?
-> HomelessDougSpareChangeChoice

=== HomelessDougDefault ===
Homeless Doug: Hey! How ya doing, friend? #portrait:smile
-> HomelessDoug

=== HomelessDougSpareChangeChoice ===
#choice
* [ Sure, let me see what I have. ] -> HomelessDougGiveChange
* [ Let me give you a piece of advice, instead. Winners don't use drugs. ] -> HomelessDougWinnersAndDrugs
* [ Yikes! Don't get too close. You stink of rotten tuna. ] -> HomelessDougRottenTuna

=== HomelessDougGiveChange ===
Homeless Doug: No rush here. My name's Doug, by the way. But everyone calls me Homeless Doug because of apparent reasons. #portrait:smile
-> HomelessDoug

=== HomelessDougWinnersAndDrugs ===
Homeless Doug: Ha! That's a good one. Winners don't use drugs. They sell them! And become filthy rich. #portrait:smile
-> HomelessDoug

=== HomelessDougRottenTuna ===
Homeless Doug: I know. Actually there's a real tuna rotting in my cart.
-> HomelessDoug

=== HomelessDoug ===
#choice
* [ There's a lot of stuff in your cart. Do you really need all those things? ] -> HomelessDougArtProject
* { HomelessDougArtProject } [ Is there anything you need for your art project? ] -> HomelessDougVodkaBottles
* [ How did you end up living in the street? ] -> LivingInTheStreet
* { HasQuest(VodkaQuest) } [ Your art project seems like a good excuse to get free booze. ] -> HomelessDougFreeBooze
+ [ See you around. ] -> MainStreet

=== HomelessDougArtProject ===
Homeless Doug: Look at me. I don't need much. But I enjoy art and I'm working on a new project for the Noir Gallery. 
Homeless Doug: It's called cumulative art. It represents the emptiness of the modern world. #portrait:smile
{ 
	- not HomelessDougRottenTuna : -> HomelessDoug
	- HomelessDougRottenTuna : -> HomelessDougArtProjectAboutTheTuna
}

=== HomelessDougArtProjectAboutTheTuna ===
Homeless Doug: And hence the rotten tuna. You know, it's all metaphorical.
-> HomelessDoug

=== HomelessDougVodkaBottles ===
Homeless Doug: Now that you mention it, I've been looking for three particular pieces that I can't find.
Homeless Doug: Russian vodka bottles. They represent the end of the cold war and the tryumph of capitalism.
~AddQuest(VodkaQuest)
-> MainStreet

=== LivingInTheStreet ===
Homeless Doug: You know, there's always a tragic story. Mine involves hookers and casinos. Oh! I enjoyed my five seconds of fame. #portrait:smile
-> HomelessDoug

=== GiveVodkaBottle1ToHomelessDoug ===
Player: I have a bottle of vodka for you.
~RemoveFromInventory(VodkaBottle1)
~UpdateQuest(VodkaQuest, VodkaQuest_1)
-> GiveVodkaBottleToHomelessDoug

=== GiveVodkaBottle2ToHomelessDoug ===
Player: I have another bottle of vodka for you.
~RemoveFromInventory(VodkaBottle2)
~UpdateQuest(VodkaQuest, VodkaQuest_2)
-> GiveVodkaBottleToHomelessDoug

=== GiveVodkaBottle3ToHomelessDoug ===
Player: I have another bottle of vodka.
~RemoveFromInventory(VodkaBottle3)
~UpdateQuest(VodkaQuest, VodkaQuest_3)
-> GiveVodkaBottleToHomelessDoug

=== GiveVodkaBottleToHomelessDoug ===
~VodkaBottlesDelivered++
{ 
	- VodkaBottlesDelivered == 1 : -> HomelessDougVodkaDelivered1
	
	- VodkaBottlesDelivered == 2 : -> HomelessDougVodkaDelivered2
	
	- VodkaBottlesDelivered == 3 : -> HomelessDougVodkaDelivered3
}

=== HomelessDougVodkaDelivered1 ===
Homeless Doug: Excellent! Exactly what I needed... for my art project, of course. And don't forget I need two more bottles. #portrait:smile
-> MainStreet

=== HomelessDougVodkaDelivered2 ===
Homeless Doug: Uh! Another bottle of fine Russian vodka. Only one more left to go. #portrait:smile
-> MainStreet

=== HomelessDougVodkaDelivered3 ===
Homeless Doug: Nice! All three bottles ready to go. Now if you'll excuse me, I need a moment alone... #portrait:smile
-> MainStreet

=== HomelessDougFreeBooze ===
Homeless Doug: Free booze! You have no idea what you're talking about. It's a meaningful piece of art! #portrait:angry
Homeless Doug: And I'm a homeless person, I don't need excuses to drink vodka! #portrait:angry
-> HomelessDoug
