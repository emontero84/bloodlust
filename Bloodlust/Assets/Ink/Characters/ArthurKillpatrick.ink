=== TalkToArthurKillpatrick ===
* { not ArthurKillpatrickFirstEncounter } -> ArthurKillpatrickFirstEncounter
+ { ArthurKillpatrickFirstEncounter } -> ArthurKillpatrickDefault

=== ArthurKillpatrickFirstEncounter ===
Arthur Killpatrick: Wellcome to Arthur Killpatrick's 24 Hour Bail Bonds. How can I help you? 
-> ArthurKillpatrick

=== ArthurKillpatrickDefault ===
Arthur Killpatrick: Hey, you're back. #portrait:smile
-> ArthurKillpatrick

=== ArthurKillpatrick ===
#choice
* [ Tell me more about the bail bonds business. ] -> ArthurKillpatrickBailbonds
* [ Are you a bounty hunter yourself? ] -> ArthurKillpatrickBailbondsYourself
* { ArthurKillpatrickBailbondsYourself } [ Tell me more about your bounty hunter. ] -> ArthurKillpatrickLostBountyHunter
* { ArthurKillpatrickNoQuest } [ About the lost bounty hunter... ] -> ArthurKillpatrickRepeatQuest
* { UseBailbondsPc && SkillPersuade } [ (Persuasion) Let's have a talk about your crime database. ] -> PersuadeArthurKillpatrickInit
* [ Tell me about Santa Monica. ] -> ArthurKillpatrickSantaMonica
+ [ Bye, bye. ] -> Bailbonds

=== ArthurKillpatrickBailbonds ===
Arthur Killpatrick: The place's got my name on the front so yeah, I'm in charge of the business. #portrait:smile
Arthur Killpatrick: I've been running the bail bonds myself for quite some time now. 
Arthur Killpatrick: We're kinda doing a service to the whole society at large, you know. And it's a honest pay for a honest work. #portrait:smile
-> ArthurKillpatrick

=== ArthurKillpatrickBailbondsYourself ===
Arthur Killpatrick: Oh! You betcha. I used to do some bounty huntin' myself back in the day. #portrait:smile
Arthur Killpatrick: Now I'm too old for the nuts and bolts of hunting. So I got a pretty young bounty hunter to do that for me.
-> ArthurKillpatrick

=== ArthurKillpatrickLostBountyHunter ===
Arthur Killpatrick: His name's Carlton and he's great at what he does... when he wants to do it. The kid's a bit lazy, you know.
Arthur Killpatrick: I can't seem to find him now, though. And I'm starting to worry. #portrait:angry
Arthur Killpatrick: Since I'm stuck here, why don't you go look for him? Just real quick. I'll pay ya for your time. #portrait:smile
-> ArthurKillpatrickAcceptQuestChoice

=== ArthurKillpatrickRepeatQuest ===
Arthur Killpatrick: The job's still open. You interested now? #portrait:smile
-> ArthurKillpatrickAcceptQuestChoice

=== ArthurKillpatrickAcceptQuestChoice ===
#choice
* [ Of course! I'll bring your hunter back. ] -> ArthurKillpatrickBountyHunterQuest
* [ Maybe I can help you find your bounty hunter. ] -> ArthurKillpatrickBountyHunterQuest
* { not ArthurKillpatrickNoQuest } [ I don't have time for this. ] -> ArthurKillpatrickNoQuest
* { ArthurKillpatrickNoQuest } [ Still not interested. ] -> ArthurKillpatrickNoQuest

=== ArthurKillpatrickNoQuest ===
Arthur Killpatrick: Okay. No problem. Just let me know if you change your mind. 
-> ArthurKillpatrick

=== ArthurKillpatrickBountyHunterQuest ===
Arthur Killpatrick: Great! I suppose the first place you should look is his apartment in Santa Monica Palace suites. #portrait:smile
~CarltonSuiteOpen = 1
~AddQuest(ArthurKillpatrickQuest) 
-> Bailbonds

=== GiveCarltonsNoteToArthurKillpatrick ===
Arthur Killpatrick: Oh my god! He was not sleeping... he was actually working...
Arthur Killpatrick: Poor Carlton. He was such a good fella... #portrait:angry
~RemoveFromInventory(CarltonsNote)
~UpdateQuest(ArthurKillpatrickQuest, ArthurKillpatrickQuest_1)
~UpdateQuest(ArthurKillpatrickQuest, ArthurKillpatrickQuest_2)
~UpdateQuest(ArthurKillpatrickQuest, ArthurKillpatrickQuest_3)
-> Bailbonds

=== GiveCdRomToArthurKillpatrick ===
Arthur Killpatrick: Thank you. That means a lot... Now if you'll excuse me. #portrait:angry
~RemoveFromInventory(CdRom)
-> Bailbonds

=== PersuadeArthurKillpatrickInit ===
Arthur Killpatrick: What crime database? What are you talking about?
-> PersuadeArthurKillpatrickChoice

=== PersuadeArthurKillpatrickChoice ===
#choice
* [ I wonder what would the police think about a bailbond's crime database? ] -> PersuadeArthurKillpatrick
* [ Can I take a peek at your crime database? I'm doing a private investigation. ] -> PersuadeArthurKillpatrickFail
* [ I'm a concerned citizen and I wonder why this city has so many crimes. ] -> PersuadeArthurKillpatrickFail
* [ I've heard a lot of rumors about your database. Isn't it a bit illegal? ] -> PersuadeArthurKillpatrick

=== PersuadeArthurKillpatrickFail ===
Arthur Killpatrick: You're full of shit! You have nothing against me! Nothing! #portrait:angry
-> Bailbonds

=== PersuadeArthurKillpatrick ===
Player: Isn't a crime database supposed to help the common citizen in the task of avoiding dangerous crimes? #portrait:smile
Player: One could argue that hiding a crime database behind a password is almost like a crime in itself...
Player: What is it hiding, I wonder? Maybe the police has the same reasonable doubt...
Arthur Killpatrick: All right, all right! Here's the pass to the database: 'Crime-Killer85'. #portrait:angry
Arthur Killpatrick: Just don't tell the cops, ok? #portrait:angry
-> Bailbonds

=== ArthurKillpatrickSantaMonica ===
Arthur Killpatrick: What can I tell ya? I'm a central part of the Santa Monica city life. #portrait:smile
Arthur Killpatrick: I've devoted more hours to the community than the Hospital Clinic.
Arthur Killpatrick: Have you been to the Hospital Clinic? It's at the end of main street.
Arthur Killpatrick: Doctor Smith is a really nice fella. You should pay him a visit. You don't look too good. #portrait:smile
-> ArthurKillpatrick