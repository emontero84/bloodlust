VAR MercuriusIsAngry = 0

=== TalkToMercurius ===
* { not MercuriusFirstEncounter } -> MercuriusFirstEncounter
+ { MercuriusFirstEncounter } -> MercuriusDefault

=== MercuriusFirstEncounter ===
~UpdateQuest(FirstSteps, FirstSteps_4)
Mercurius: Oh! These motherfuckers ripped me apart... I'm dying here! #portrait:angry
-> MercuriusFirstEncounterChoice

=== MercuriusDefault ===
Mercurius: Arg! Oh! I'm still bleeding... #portrait:angry
-> Mercurius

=== MercuriusFirstEncounterChoice ===
#choice
* [ Are you Mercurius? ] -> MercuriusAreYouMercurius
* [ You want me to call an ambulance? ] -> MercuriusAmbulance
* [ What happened? ] -> MercuriusWhatHappened

=== MercuriusAreYouMercurius ===
Mercurius: Yeah, and you must be the guy De la Croix send to help me with the Astrolite.
-> MercuriusFirstEncounterChoice
	
=== MercuriusAmbulance ===
Mercurius: What? No! I have a police record back in the east. No goddamn cops! Alright? #portrait:angry
-> MercuriusFirstEncounterChoice
	
=== MercuriusWhatHappened ===
Mercurius: Fucking pricks! They beat me up and left me for dead. I think I lost a rib or two... #portrait:angry
Mercurius: Oh shit! Something started leaking. You gotta look and tell me... How does it look?
-> MercuriusWhatHappenedChoice

=== MercuriusWhatHappenedChoice ===
#choice
* [ It looks bad. Really bad. Now tell me who did this to you? ] -> MercuriusWhoDidThis
* [ You have five minutes to live, tops. ] -> MercuriusCrackingWise
* [ If you don't stop crying like a little baby I'll finish the job myself! ] -> MercuriusFinishTheJob

=== MercuriusCrackingWise ===
Mercurius: Geez! I'm laying in a pool of my own blood and you still have time for cracking jokes. Un-fucking-believable! #portrait:angry
-> MercuriusWhoDidThis
	
=== MercuriusFinishTheJob ===
~MercuriusIsAngry++
Mercurius: Hey! No need to be a such a fuckin' prick! I'm losing my guts here, just give me a goddamn minute to breath... #portrait:angry
-> MercuriusWhoDidThis
	
=== MercuriusWhoDidThis ===
Mercurius: They jumped out of nowhere! The chemist and maybe three or four of 'em. Hit me with baseball bats, like a filthy rat.
Mercurius: You have to get the Astrolite back. And the money! The cocksuckers stole my money too! #portrait:angry
-> MercuriusWhoDidThisChoice

=== MercuriusWhoDidThisChoice ===
#choice
* [ What is this Astrolite thing? ] -> MercuriusWhatIsAstrolite
* [ Where are they? ] -> MercuriusWhereAreThey
* [ So you want me to get the Astrolite for you? ] -> MercuriusGetTheAstrolite
* [ You're screwed. ] -> MercuriusYouAreScrewed

=== MercuriusWhatIsAstrolite ===
Mercurius: A liquid explosive... twice as powerful as TNT. If you want to blow something really big, Astrolite's your best choice.
-> MercuriusWhoDidThisChoice

=== MercuriusWhereAreThey ===
Mercurius: They live in a dump near the beach. The chemist is a local dealer who goes by the name of Dennis. 
Mercurius: You gotta find him and make him suffer. I want him to suffer! Oh, my leg is going numb... #portrait:angry
->  MercuriusWhereAreTheyChoice

=== MercuriusWhereAreTheyChoice ===
#choice
* [ Don't worry, I'll get the Astrolite for you. ] -> MercuriusAstroliteQuestAdded
* [ Let me think about it. ] -> MercuriusYouAreScrewed
	
=== MercuriusYouAreScrewed ===
~MercuriusIsAngry++
Mercurius: I know I fucked up. Believe me, in more ways than you can imagine. 
-> MercuriusBegging
	
=== MercuriusBegging ===
Mercurius: If De La Croix finds out, I'm done. Please, you have to cover me! I'll do anything for you.
#choice
* [ I'll see what I can do. ] -> MercuriusAstroliteQuestAdded
* { MercuriusIsAngry > 0 } [ You deserve it. ] -> SuitesMercurius

=== MercuriusGetTheAstrolite ===
Mercurius: Yeah, get the Astrolite and come back in one piece. 
-> MercuriusGetTheAstroliteChoice

=== MercuriusGetTheAstroliteChoice ===
#choice
* [ Don't worry, I'll get the Astrolite for you. ] -> MercuriusAstroliteQuestAdded
* [ Forget about it. I'm outta here. ] -> SuitesMercurius

=== MercuriusAstroliteQuestAdded ===
~AddQuest(AstroliteQuest)
-> SuitesMercurius

=== Mercurius ===
#choice
* [ How are you holding up, Mercurius? ] -> MercuriusHoldingUp
* [ Is there anything I can do to help you? ] -> MercuriusNeedsPainkillers
* { HasQuest(FindBernardThungQuest) } [ Tell me more about Tess Voermann. ] -> MercuriusAboutTess
* { HasQuest(FindBernardThungQuest) } [ Tell me more about Juliette Voermann. ] -> MercuriusAboutJuliette
* { HasQuest(FindBernardThungQuest) } [ Tell me more about Bernard Thung. ] -> MercuriusAboutThung
+ [ I'm outta here. ] -> SuitesMercurius

=== MercuriusHoldingUp ===
Mercurius: Ahh... I can't barely breathe. And every time I move something starts bleeding. Other from that, I'm having a great night...
-> Mercurius

=== MercuriusNeedsPainkillers ===
Mercurius: Yeah, now that you mention it... I could use some painkillers. This leaking is killing me...
~AddQuest(PainkillersQuest)
-> SuitesMercurius

=== GiveAstroliteToMercurius ===
Player: Guess who's brought the Astrolite back.
Mercurius: Oh! Is that what I think it is? #portrait:smile
Mercurius: Aw man! You've saved my life!
Mercurius: Now that we have the astrolite back... we can finally blow the Blood Cult hideout... but first...
~RemoveFromInventory(Astrolite)
~UpdateQuest(AstroliteQuest, AstroliteQuest_1)
~UpdateQuest(AstroliteQuest, AstroliteQuest_2)
~UpdateQuest(AstroliteQuest, AstroliteQuest_4)
-> FindBernardThungQuestChoice

=== FindBernardThungQuestChoice ===
#choice
* [ You can count on me. Tell me. ] -> FindBernardThungQuestGiven
* [ What is it now? ] -> FindBernardThungQuestGiven
* [ I don't wanna know. ] -> SuitesMercurius

=== FindBernardThungQuestGiven ===
Mercurius: You need to find a nasty sewer rat called Bernard Thung. He's the only one who knows how to reach the hideout. #portrait:angry
Mercurius: Unfortunately, Thung has banished... and all we know is that the Voermanns may have something to do with that.
Mercurius: Oh! I forgot you're new in town.
Mercurius: Tess and Juliette Voermann. Two sister vampires who run the Madhouse club, on second street.
Mercurius: You should pay them a visit and see if you can call off the feud against Thung. 
~AddQuest(FindBernardThungQuest)
-> SuitesMercurius

=== MercuriusAboutTess ===
Mercurius: Tess Voermann is one of the most influential vampires in Santa Monica.
Mercurius: She runs the Madhouse club and owns half of the business in the whole bay area.
Mercurius: Never met her in person so I can't give any more details about her. #portrait:smile
-> Mercurius

=== MercuriusAboutJuliette ===
Mercurius: Juliette is the evil twin sister of Tess Voermann. She's beautiful and apparently more passionate than the average vampire.
Mercurius: They say she's into some nasty perversions with mortals... but who knows, maybe it's all just gossip.
-> Mercurius

=== MercuriusAboutThung ===
Mercurius: I've heard a lot of stories about this Bernard Thung fella. He was a spy for De la Croix and then he went freelance.
Mercurius: The poor bastard has crossed too many interests and now he's hiding in some shitty rathole, waiting for the storm to pass.
-> Mercurius

=== GiveMoneyBagToMercurius ===
Player: Here's the money you left behind at the beach house.
Mercurius: Oh boy! You've saved my life!... Twice in the same night... #portrait:smile
Mercurius: I won't forget about that. I owe you big. #portrait:smile
~RemoveFromInventory(MoneyBag)
~UpdateQuest(AstroliteQuest, AstroliteQuest_3)
-> SuitesMercurius

=== GivePainkillersToMercurius ===
Player: I've found some painkillers. They'll ease your pain.
Mercurius: Oh! Thanks! Let me grab some *MUNCH MUNCH* #portrait:smile
Mercurius: Ahh... I feel much better already! You're a lifesaver! #portrait:smile
~RemoveFromInventory(Painkillers)
~UpdateQuest(PainkillersQuest, PainkillersQuest_1)
~UpdateQuest(PainkillersQuest, PainkillersQuest_2)
-> SuitesMercurius