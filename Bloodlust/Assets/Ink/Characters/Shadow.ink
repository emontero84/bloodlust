=== TalkToShadow ===
Shadow: ...
-> Shadow

=== Shadow ===
#choice
* [ Nice trenchcoat. ] -> ShadowTrenchcoat
* [ What do you think about this painting? ] -> ShadowPainting
* { MissYangAuthor } [ What do you know about the author? ] -> ShadowAuthor
+ [ Bye. ] -> NoirGallery

=== ShadowTrenchcoat ===
Shadow: ...
Shadow: Thank you.
-> Shadow

=== ShadowPainting ===
Shadow: ...
Shadow: I like how the vibrant colors add texture to the inner drama of Lilith.
-> Shadow

=== ShadowAuthor ===
Shadow: The author?
Shadow: Why would I know ANYTHING about the author?!
Shadow: ...
Shadow: Excuse me.
-> NoirGallery