=== TalkToDori ===
Dori: What can I get you, honey? #portrait:smile
-> Dori

=== Dori ===
#choice
* [ Do you run this place? ] -> DoriRunSunsetDiner
* { DoriRunSunsetDiner && TalkToCook } [ Tell me about the cook. ] -> DoriAboutGilberto
* { HasQuest(ElliotQuest) && not IsQuestStepUpdated(ElliotQuest_3) && DoriRunSunsetDiner && not DoriRemembersLilly } [ I'm looking for a girl called Lilly. ] -> DoriAboutLilly
* [ Tell me about Santa Monica. ] -> DoriSantaMonica
+ [ I'm outta here. ] -> Diner

=== DoriRunSunsetDiner ===
Dori: Yeah, I am the owner of the Sunset Diner since 1944. #portrait:smile
Dori: An in this last forty years I've seen it all.
Dori: Good time, bad times and times somewhere in between. You name it!
-> Dori

=== DoriAboutGilberto ===
Dori: Oh! You mean Gilberto? He has a strange accent but, believe me, he does the best burguers in the whole bay area. 
Dori: And he's a great singer too! Sometimes he's a bit obsessive with the cutting, but mostly he's doing fine. #portrait:smile
-> Dori

=== DoriAboutLilly ===
Dori: I'm sorry, sweetheart. There's no Lilly here. Just me and Gilberto.
-> DoriAboutLillyChoice

=== DoriAboutLillyChoice ===
#choice
* [ She was last seen here in one of your booths. ] -> DoriRemembersLilly
* [ She came here with a surfer boy called Elliot. ] -> DoriRemembersLilly
* [ She was a vampire. Doesn't ring a bell? ] -> DoriLillyVampire
+ [ Let's talk about something else. ] -> Dori

=== DoriLillyVampire ===
Dori: A vampire? You mean it was Halloween or something? 
Player: Nevermind. 
-> DoriAboutLillyChoice

=== DoriRemembersLilly ===
Dori: Oh yeah, I remember her. Beautiful girl. Short hair, pale skin, big brown eyes. #portrait:smile
Dori: She left a tip in jar but she didn't order a thing. Poor thing. She looked so lonely.
Dori: I let her stay in the booth until we closed. She left some stuff here.
Dori: Why don't you give it back to her? #portrait:smile
~AddToInventory(LillysPhoto)
~AddToInventory(LillysKey)
~UpdateQuest(ElliotQuest, ElliotQuest_1)
-> Diner

=== DoriSantaMonica ===
Dori: What can I tell you? I've been half a century behind this counter and this city never gets old.
Dori: Have you been to the beach already? You can get there through the stairs in the parking garage.
Dori: There're surfing contests and whatnot. Even during the night! Youngsters these days... #portrait:angry
-> Dori