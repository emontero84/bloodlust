=== TalkToClaire ===
* { not ClaireFirstEncounter } -> ClaireFirstEncounter
+ { ClaireFirstEncounter } -> ClaireDefaultEncounter

=== ClaireFirstEncounter ===
Claire: Good night, honey. Looking for a date? #portrait:smile
-> ClaireDateChoice

=== ClaireDateChoice ===
#choice
* [ I'm not really interested. ] -> ClaireRespondsNo
* [ Sure! How much? ] -> ClaireRespondsHowMuch

=== ClaireRespondsNo ===
Claire: All right. I'll be right here in case you change your mind.
-> ClaireDefault

=== ClaireRespondsHowMuch ===
Claire: It depends on the services provided, sweetheart. There's no flat rate in this line of work.
Claire: I do French for 10 bucks, Greek for 50 and German for 100 dollars. No kissing and no cuddles.
Player: Oh snap! I'm here for the cuddles.
Claire: My corner, my rules.
-> ClaireDefault

=== ClaireDefaultEncounter ===
Claire: How you doin', honey? #portrait:smile
-> ClaireDefault

=== ClaireDefault ===
#choice
* { ClaireRespondsNo } [ On second thought, how much was that date? ] -> ClaireRespondsHowMuch
* [ Tell me about yourself. ] -> ClaireYourself
* [ Tell me about Santa Monica. ] -> ChaireSantaMonica
* [ Tell me about your work. ] -> ClaireWork
* { ClaireWork } [ Tell me about your pimp. ] -> ClairePimp
* { ClairePimp } [ Maybe I could help you with Dennis. ] -> ClaireGiveQuest
* { HasQuest(ClaireQuest) && IsQuestStepUpdated(ClaireQuest_2) } [ It's done. Dennis won't bother you anymore. ] -> ClaireQuestComplete
+ [ Enough talking. ] -> MainStreet

=== ClaireYourself ===
Claire: There's not much to tell. My name's Claire and I work the streets since I came to America.
-> ClaireDefault

=== ClaireWork ===
Claire: Prostitution is never easy, honey. But at least it's an honest way to put food on the table.
Claire: I have a child, you know. #portrait:smile
-> ClaireDefault

=== ClairePimp ===
Claire: Don't make me talk. This fucking Dennis is killing me. Literally. #portrait:angry
Claire: But I'm planning to go freelance. As soon as I found a way to say it to Dennis.
-> ClaireDefault

=== ClaireGiveQuest ===
Claire: That would be wonderful! Maybe you could talk some sense into him. Or break him a bone or two. Either way is fine to me. #portrait:angry
~AddQuest(ClaireQuest)
-> MainStreet

=== ClaireQuestComplete ===
Claire: Oh! You did that for me? Thank you! You're a lifesaver! #portrait:smile
~UpdateQuest(ClaireQuest, ClaireQuest_1)
~UpdateQuest(ClaireQuest, ClaireQuest_2)
~UpdateQuest(ClaireQuest, ClaireQuest_3)
-> MainStreet

=== ChaireSantaMonica ===
Claire: This city is a beautiful mess. There's always a place open to grab a bite, even in the middle of the night.
Claire: Do you know the Sunset Diner? It's at the end of second street. You should pay them a visit. Best burgers in the whole bay area.
-> ClaireDefault