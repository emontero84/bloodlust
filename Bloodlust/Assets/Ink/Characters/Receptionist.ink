=== TalkToReceptionist ===
Receptionist: Wellcome to Santa Monica Hospital Clinic. How may I help you? #portrait:smile
-> Receptionist

=== Receptionist ===
#choice
* { not IsHospitalOfficeDoorOpen } [ I need to see the doctor. ] -> ReceptionistRespondsNo
* { not IsHospitalOfficeDoorOpen && ReceptionistRespondsNo && SkillIntimidate } [ (Intimidate) Doctor! Now! Or I'll break some bones. ] -> IntimidateReceptionistInit
* { not IsHospitalOfficeDoorOpen && ReceptionistRespondsNo && SkillSeduce } [ (Seduction) I love your smile. Can I see the doctor, honey? ] -> SeduceReceptionistInit
* { not IsHospitalOfficeDoorOpen && ReceptionistRespondsNo && SkillBribe } [ (Bribe) Let's say I give you a very generous tip. Can I see the doctor now? ] -> BribeReceptionistInit
* [ How's the night going? ] -> ReceptionistHowsItGoing
* [ Tell me about Santa Monica. ] -> ReceptionistSantaMonica
+ [ I'm outta here. ] -> Hospital

=== ReceptionistRespondsNo ===
Receptionist: I'm sorry, {IsFemale:madam}{not IsFemale:sir}. You'll have to wait your turn to enter the doctor's office.
-> Receptionist

=== IntimidateReceptionistInit ===
Receptionist: Excuse me? #portrait:angry
-> IntimidateReceptionistChoice

=== IntimidateReceptionistChoice ===
#choice
* [ Don't make me lose my temper. Open the door. Now! ] -> IntimidateReceptionistFail
* [ I said: Let. Me. In. ] -> IntimidateReceptionistFail
* [ It's your last chance. Buzz me in and I'll spare your miserable life. ] -> IntimidateReceptionist
* [ You have three seconds to let me in. Three... Two... One... ] -> IntimidateReceptionistFail

=== IntimidateReceptionistFail ===
Receptionist: Please, remain calm and wait for your turn, {IsFemale:madam}{not IsFemale:sir}.
-> Hospital

=== IntimidateReceptionist ===
Receptionist: Okay, okay... please, don't make a scene. I'll let you in the doctor's office.
~PlaySound("BUZZER")
~IsHospitalOfficeDoorOpen = 1
-> Hospital

=== SeduceReceptionistInit ===
Receptionist: Excuse me?
-> SeduceReceptionistChoice

=== SeduceReceptionistChoice ===
#choice
* [ I said you have a very beautiful smile. Would you let me see the doctor? ] -> SeduceReceptionistFail
* [ I'm sorry, I can't repress the urge to admire your beauty. ] -> SeduceReceptionistFail
* [ You have a lovely smile, honey. You know it, right? *WINK* ] -> SeduceReceptionist
* [ I'm in love with your smile. Please, let me in. ] -> SeduceReceptionistFail

=== SeduceReceptionistFail ===
Receptionist: Eww! That was totally inappropriate, {IsFemale:madam}{not IsFemale:sir}. #portrait:angry
-> Hospital

=== SeduceReceptionist ===
Receptionist: Oh Gosh! You're gonna make me blush! *CHUCKLE* #portrait:smile
Receptionist: You know I can't let you in but... maybe we can make an exception. #portrait:smile
Receptionist: It'll be our little secret. *CHUCKLE* #portrait:smile
~PlaySound("BUZZER")
~IsHospitalOfficeDoorOpen = 1
-> Hospital

=== BribeReceptionistInit ===
Receptionist: Excuse me?
-> BribeReceptionistChoice

=== BribeReceptionistChoice ===
#choice
* [ Let's say I give you some money for moving my name up in the waiting list. ] -> BribeReceptionist
* [ Consider it a gift for your invaluable services to the community. ] -> BribeReceptionistFail
* [ I'm gonna leave some cash in your desk. Who knows what will happen next? ] -> BribeReceptionistFail
* [ Everybody has a price, dear. Let's see which is yours. ] -> BribeReceptionistFail

=== BribeReceptionistFail ===
Receptionist: Oh! Let's forget you even said that, {IsFemale:madam}{not IsFemale:sir}. #portrait:angry
-> Hospital

=== BribeReceptionist ===
Receptionist: We don't accept bribes! How dare you! #portrait:angry
Receptionist: *WHISPERING* Leave your tip on the counter and I'll buzz you in. *WINK* #portrait:smile
-> BribeSuccessful -> BribeReceptionistSuccess

=== BribeReceptionistSuccess ===
~PlaySound("BUZZER")
~IsHospitalOfficeDoorOpen = 1
-> Hospital

=== BeginGiveBirthdayCakeToReceptionist ===
Player: Happy birthday, dear receptionist! #portrait:smile
-> GiveBirthdayCakeToReceptionist

=== GiveBirthdayCakeToReceptionist ===
Receptionist: Oh my! Thank you! *CHUCKLE* #portrait:smile
Receptionist: Since it's you, let me buzz you in. *WINK* #portrait:smile
~RemoveFromInventory(BirthdayCake)
~PlaySound("BUZZER")
~IsHospitalOfficeDoorOpen = 1
-> Hospital

=== BeginGiveIntoxicatedBirthdayCakeToReceptionist ===
Player: Happy birthday, dear receptionist! With all my poisonous love. #portrait:smile
-> GiveIntoxicatedBirthdayCakeToReceptionist

=== GiveIntoxicatedBirthdayCakeToReceptionist ===
Receptionist: Oh my! Thank you! *CHUCKLE* #portrait:smile
~RemoveFromInventory(IntoxicatedBirthdayCake)
Receptionist: *YUMMY* *YUMMY* #portrait:smile
Receptionist: I think... I'm gonna be... sick... #portrait:angry
~Hide(Hospital_Receptionist)
~Show(Hospital_ReceptionistLeavingAnimation)
~IsHospitalOfficeDoorOpen = 1
-> Hospital

=== ReceptionistHowsItGoing ===
Receptionist: Well, nothing out of the ordinary. 
Receptionist: There's some stabbing, major injuries, and house accidents but mostly we deal with old people.
Receptionist: They feel pretty lonely and watching TV in here is their only social activity.
-> Receptionist

=== ReceptionistSantaMonica ===
Receptionist: You been to the Santa Monica Palace yet? It's a luxury hotel in the middle of main street.
Receptionist: It's so classy and romantic! I wish I could afford a single night in there. #portrait:smile
-> Receptionist