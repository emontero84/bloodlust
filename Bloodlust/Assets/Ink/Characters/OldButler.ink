=== TalkToOldButler ===
Old Butler: Do you want a cocktail, {IsFemale:madam}{not IsFemale:mesieur}? #portrait:smile
-> OldButler

=== OldButler ===
#choice
* [ What cocktails do you have? ] -> OldButlerWhatDoYouHave
* [ I'm sorry, I don't drink. ] -> OldButlerNoDrink
* { TalkToYoungButler } [ I've seen some resemblance between you and the younger butler. ] -> OldButlerYoungerButler
* { OldButlerYoungerButler } [ Is your son a butler too? ] -> OldButlerSon
* [ How's the night going? ] -> OldButlerGossip
+ [ I'm outta here. ] -> NoirGallery

=== OldButlerWhatDoYouHave ===
Old Butler: We have the best selection of alcoholic beverages. Gin, rum and everything in between. #portrait:smile
-> OldButler

=== OldButlerNoDrink ===
Old Butler: Oh, I'm sorry then. #portrait:smile
-> OldButler

=== OldButlerYoungerButler ===
Old Butler: Of course, {IsFemale:madam}{not IsFemale:mesieur}. He is my grandson, indeed. #portrait:smile
Old Butler: We have a long family tradition of butlers.
-> OldButler

=== OldButlerSon ===
Old Butler: Oh, I'm afraid not. The tradition skips one generation.
-> OldButler

=== OldButlerGossip ===
Old Butler: Oh, the event is going pretty well. Lots of guests and the evening is going smoothly. #portrait:smile
-> OldButler