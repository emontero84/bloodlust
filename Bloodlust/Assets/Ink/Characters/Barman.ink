=== TalkToBarman ===
Barman: Whaddaya want?
-> Barman

=== Barman ===
#choice
* { HasQuest(FindBernardThungQuest) } [ I'm looking for your boss. ] -> BarmanYourBoss
+ [ Let's talk about drinking. ] -> BarmanDrinksChoice
* { HasQuest(BarmanQuest) && DoomsayerStopPushing } [ Good news, I found the pusher of the Vampire's Kiss. ] -> BarmanQuestComplete
* [ Tell me more about the Madhouse Club. ] -> BarmanMadhouseClub
* { HasQuest(FindBernardThungQuest) && BarmanYourBoss } [ Do you know that your boss is... well, a vampire? ] -> BarmanVampireBoss
* { BarmanVampireBoss } [ Do you have many vampire customers? ] -> BarmanVampires
* [ Tell me about Santa Monica. ] -> BarmanSantaMonica
* { BarmanQuestMaybeLater } [ About your drug problem...] -> BarmanQuestChoice
+ [ See ya. ] -> MadhouseClub

=== BarmanYourBoss ===
Barman: And who's that? #portrait:angry
-> BarmanYourBossChoice

=== BarmanYourBossChoice ===
#choice
* [ Juliette Voermann. ] -> BarmanYourBossJuliette
* [ Tess Voermann. ] -> BarmanYourBossTess

=== BarmanYourBossJuliette ===
Barman: Ha! That would be fun to see! Juliette's a regular at the Madhouse but she's not in charge around here. #portrait:smile
Barman: Tess is the real owner of the whole fuckin' club.
-> BarmanGivesQuest

=== BarmanYourBossTess ===
Barman: Yeah, she's the boss. Bingo! Go get your fuckin' prize.
-> BarmanGivesQuest

=== BarmanGivesQuest ===
Barman: But she's too busy running the club. If you want to meet with Tess, you have to help me with a little problem first.
-> BarmanQuestChoice

=== BarmanQuestChoice ===
#choice
* [ Okay. What's the problem? ] -> BarmanQuestYes
* [ Maybe some other time.] -> BarmanQuestMaybeLater
* [ I don't have time for that. ] -> BarmanQuestMaybeLater

=== BarmanQuestYes ===
Barman: There's a new drug coming into the club. It's some kind of chemical shit that fucks with your brains. #portrait:angry
Barman: They call it the "Vampire's Kiss". Fuck me if I know why. You gotta find who's pushing the drug and make it stop. 
Barman: Then I'll give you an audience with Tess.
~AddQuest(BarmanQuest)
-> MadhouseClub

=== BarmanQuestMaybeLater ===
Barman: Sure! Come back when you've grown a pair. #portrait:angry
-> MadhouseClub

=== BarmanMadhouseClub ===
Barman: There's not much to tell. Club's running like a bitch for the last thirty years or so.
Barman: The place was a small theatre before the boss turned it into a gothic golden egg hen.
-> Barman

=== BarmanVampireBoss ===
Barman: Ha! Of course! I've been working most of my life behind this bar. #portrait:smile
Barman: Do you think I'm an idiot? She's not aged a single day in the last three decades.
-> Barman

=== BarmanVampires ===
Barman: Well, not many but there are some blood-suckers in our exquisite clientele.
Barman: They like the music just as the regular folk. And it's pretty dark in here so they just feel at home.
-> Barman

=== BarmanDrinksChoice ===
#choice
* [ What do you have? ] -> BarmanAllDrinks
* { BarmanVampireBoss } [ Pour me a bloody drink, if you catch my drift. ] -> BarmanBloodDrink
* { LookAtMadhouseVodkaBottle } [ I want a bottle of your finest vodka. ] -> BarmanAskForVodkaBottle
+ [ On second thought, I'm not drinking tonight. ] -> BarmanDrinksChoiceBack

=== BarmanAllDrinks ===
Barman: I have all kinds of alcoholic beverages. Good and evil, young and old. Pick your poison.
-> BarmanDrinksChoice

=== BarmanBloodDrink ===
Barman: Hey! You lost your fuckin' mind? #portrait:angry
Barman: You can't just walk into a bar and order a pint o' blood. This ain't a fuckin' Dracula joke! #portrait:angry
Player: Sorry, I thought...
Barman: Nah, don't even say it.
-> Barman

=== BarmanAskForVodkaBottle ===
Barman: Sure! Lemme give you the whole fuckin' bottle for free because that's how you make a profit in the clubbing business. #portrait:angry
-> BarmanVodkaBottleChoice

=== BarmanVodkaBottleChoice ===
#choice
* { not BarmanVodkaBottle && HasInInventory(MoneyClip) } [ You're right, let me pay for it. Here's more than enough. ] -> UseMoneyClipWithBarman
* { not BarmanVodkaBottle && SkillBribe } [ (Bribe) You're right, let me pay for it. Here's more than enough. ] -> BribeBarmanInit
* { not BarmanVodkaBottle && HasQuest(VodkaQuest) } [ But I need it for a homeless person. ] -> BarmanVodkaBottlePlease1
* { not BarmanVodkaBottle && BarmanVodkaBottlePlease1 } [ And he needs it very badly for his art project. ] -> BarmanVodkaBottlePlease2
* { not BarmanVodkaBottle && BarmanVodkaBottlePlease2 } [ He is poor and needs vodka. Don't you have a heart? ] -> BarmanVodkaBottlePlease3
* { not BarmanVodkaBottle && BarmanVodkaBottlePlease3 } [ Please? I'm begging you. ] -> BarmanVodkaBottlePlease4
* { not BarmanVodkaBottle && BarmanVodkaBottlePlease4 } [ Pretty please? ] -> BarmanVodkaBottlePlease5
+ [ Maybe you're right. I'll be back. ] -> MadhouseClub

=== BarmanVodkaBottlePlease1 ===
Barman: Yeah, sure! That's the worst excuse I've ever heard.
-> BarmanVodkaBottleChoice

=== BarmanVodkaBottlePlease2 ===
Barman: Oh jeez! Have some dignity.
-> BarmanVodkaBottleChoice

=== BarmanVodkaBottlePlease3 ===
Barman: Do you even hear what you're saying? You're making no fuckin' sense.
-> BarmanVodkaBottleChoice

=== BarmanVodkaBottlePlease4 ===
Barman: I can keep saying no all night long.
-> BarmanVodkaBottleChoice

=== BarmanVodkaBottlePlease5 ===
Barman: Stop it already! I'll give you the fuckin' vodka if you just shut up and disappear. #portrait:angry
Player: Deal.
-> BarmanVodkaBottle

=== BarmanVodkaBottle ===
Barman: Okay, here's your vodka. Don't drink it all by yourself!
~AddToInventory(VodkaBottle2)
~Hide(MadhouseClub_VodkaBottle2)
-> MadhouseClub

=== UseMoneyClipWithBarman ===
~RemoveFromInventory(MoneyClip)
-> BribeBarman

=== BribeBarmanInit ===
Barman: What?! #portrait:angry
-> BribeBarmanChoice

=== BribeBarmanChoice ===
#choice
* [ Let me pay you for the vodka bottle. ] -> BribeBarman
* [ I have money burning in my pocket. Let's trade if for the vodka bottle. ] -> BribeBarmanFail
* [ Let's pretend I give you this very generous tip for the bottle of vodka. ] -> BribeBarmanFail
* [ Isn't trading money for products the very foundation of capitalism? ] -> BribeBarmanFail

=== BribeBarmanFail ===
Barman: Ha ha! You're so full of shit! #portrait:smile
-> MadhouseClub

=== BribeBarman ===
-> BribeSuccessful -> BribeBarmanSuccess

=== BribeBarmanSuccess ===
Barman: Thank you! That's very fucking generous. #portrait:smile
-> BarmanVodkaBottle

=== BarmanDrinksChoiceBack ===
Barman: Alrighty.
-> Barman

=== BarmanQuestComplete ===
Barman: Is it done?
Player: You can bet on it. #portrait:smile
Barman: Great! Then lemme give you access to the Madhouse office. You've earned your audition with the boss. #portrait:smile
Barman: She's in the second floor. Straight thru the elevator.
~RemoveFromInventory(VialOfBlood)
~UpdateQuest(BarmanQuest, BarmanQuest_1)
~UpdateQuest(BarmanQuest, BarmanQuest_2)
~UpdateQuest(BarmanQuest, BarmanQuest_3)
~UpdateQuest(BarmanQuest, BarmanQuest_4)
~Show(MadhouseClub_ElevatorDoors)
~IsMadhouseClubOfficeOpen = 1
-> MadhouseClub

=== BarmanSantaMonica ===
Barman: Do I look like a fuckin' travel agent?
Barman: This city is rotten and most Santa Monicans are just crazy.
Barman: But they pay their drinks so I ain't complaining.
-> Barman

=== UseVialOfBloodWithBarman ===
Player: Here, I got a vial of the Vampire's Kiss.
Barman: Excellent! Now go an find the pusher, will ya? #portrait:smile
-> MadhouseClub