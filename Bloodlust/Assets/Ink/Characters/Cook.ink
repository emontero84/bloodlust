=== TalkToCook ===
Gilberto: O sole mio! What can I do for you?
-> Cook

=== Cook ===
#choice
* [ What are you doing? ] -> CookCutingOnions
* { CookCutingOnions } [ With all these onions, don't you think it's enough? ] -> CookEnoughOnions
* { CookCutingOnions } [ Why aren't you crying with all these onions? ] -> CookNotCrying
+ { HasQuest(JulietteQuest) && CookCutingOnions && not CookBorrowKnife } [ I need to borrow a knife. ] -> CookDontBorrowKnife
* [ Tell me about Santa Monica. ] -> CookSantaMonica
* [ Tell me about your job here at the Sunset Diner. ] -> CookJobAtSunsetDiner
+ [ I'm outta here. ] -> Diner

=== CookCutingOnions ===
Gilberto: Mamma mia! What do you think I'm doing? Cutting onions for the burguers.
Gilberto: Lots of burguers mean a lot of onions.
-> Cook

=== CookEnoughOnions ===
Gilberto: Ha! Enough onions! There is no such a thing. #portrait:smile
Gilberto: They always want more burguers. And more burguers mean more onions!
-> Cook

=== CookNotCrying ===
Gilberto: I'm not crying because of the magic trick with onions. Singing! #portrait:smile
Gilberto: The joy of singing alliviates the effect of the onions.
Gilberto: O sole mio! You see? #portrait:smile
-> Cook

=== CookDontBorrowKnife ===
Gilberto: Borrow the knife? And what I am supposed to do with all these onions?
-> CookDontBorrowKnifeChoice

=== CookDontBorrowKnifeChoice ===
#choice
* [ I think you have more than enough onions. ] -> CookMoreThanEnoughOnions
* [ Don't you have more knives to use? ] -> CookMoreKnives
* { CookMoreThanEnoughOnions || CookMoreKnives } [ Maybe you can cut some lettuce instead. ] -> CookCutLettuce
* { CookMoreThanEnoughOnions } [ Maybe you can do some burguers instead. ] -> CookDoSomeBurguers
* [ It's not my problem. Just give me the knife. ] -> CookCantGiveKnife
+ [ You're right. Let's forget about the knife. ] -> Cook

=== CookTradeKnifeForKatana ===
Gilberto: Mamma mia! You crazy? I can't cut onions with a samurai sword! #portrait:angry
-> Diner

=== CookMoreKnives ===
Gilberto: Of course I have more knives! I have many things to cut!
-> Diner

=== CookMoreThanEnoughOnions ===
Gilberto: Ha! Enough onions... You are funny! #portrait:smile
-> Diner

=== CookCutLettuce ===
Gilberto: Mmmh... You might be right...
Gilberto: Onions are starting to pile up and there's no lettuce for the burguers...
Gilberto: Maybe I can cut more lettuce! #portrait:smile
-> CookBorrowKnife

=== CookDoSomeBurguers ===
Gilberto: Mmmh... You might be right...
Gilberto: Onions are starting to pile up and there're no burguers in the fire...
Gilberto: Maybe I can cut more tomatoes instead! #portrait:smile
-> CookBorrowKnife

=== CookBorrowKnife ===
Gilberto: Here, bring it back when you're done. There's still a lot of things to cut!
~AddToInventory(Knife)
-> Diner

=== CookCantGiveKnife ===
Gilberto: Nah, I'm gonna pass. I got many onions to cut!
Gilberto: O Sole Mio! #portrait:smile
-> Diner

=== CookSantaMonica ===
Gilberto: What can I tell you? I love this city! There's food everywhere. #portrait:smile
Gilberto: Have you been to the Madhouse Club? Even they had some great sandwiches!
-> Cook

=== CookJobAtSunsetDiner ===
Gilberto: I like it here at the Diner. Lots of onions to cut, but the job is still OK.
-> Cook