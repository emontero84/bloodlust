VAR NumberOfPasswords = 0

=== HackSuccessful ===
~PlaySound("HACKING")
~PlayerAnimation("Hack")
~Wait(1)
Player: I'm in!
->->

=== HackFailed ===
~PlaySound("HACKING")
~PlayerAnimation("Hack")
~Wait(1)
Player: And... {Shit|Dang|Crap}! It's wrong!
->->

=== UseApartment101Laptop ===
~PlayerAnimation("Hack")
~Show(Apartment101_LaptopOn)
#choice #hack
* [ Read e-mail. ] -> HackSuccessful -> ReadLaptopEmail
* { not ReadLaptopEmail } [ Type random command. ] -> HackFailed -> UseApartment101Laptop
+ [ Quit. ] -> QuitApartment101Laptop

=== QuitApartment101Laptop ===
~Hide(Apartment101_LaptopOn)
~StopSound("HACKING")
-> Apartment101

=== ReadLaptopEmail ===
Player: There's nothing in the e-mail inbox.
-> UseApartment101Laptop

=== UseBeachHouseInteriorPc ===
~PlayerAnimation("Hack")
~Show(BeachHouseInterior_PcOn)
#choice #hack
* { not HackIntoBeachHouseInteriorPc && LookAtPosters } [ Password: 'SilverSurfer'] -> HackIntoBeachHouseInteriorPc
* { SkillHack } [ Hack the password. ] -> HackIntoBeachHouseInteriorPc
* { HackIntoBeachHouseInteriorPc } [ Read private files. ] -> ReadPrivateFilesInBeachHouseInteriorPc
* { not HackIntoBeachHouseInteriorPc } [ Type random password. ] -> HackFailed -> UseBeachHouseInteriorPc
+ [ Quit. ] -> QuitBeachHouseInteriorPc

=== QuitBeachHouseInteriorPc ===
~Hide(BeachHouseInterior_PcOn)
~StopSound("HACKING")
-> BeachHouseInterior

=== HackIntoBeachHouseInteriorPc ===
-> HackSuccessful -> UseBeachHouseInteriorPc

=== ReadPrivateFilesInBeachHouseInteriorPc ===
Player: Here's the code to open the safe.
-> BeachHouseInterior

=== UseBailbondsPc ===
~PlayerAnimation("Hack")
~Show(Bailbonds_PcOn)
#choice #hack
* { not HackIntoBailbondsPc && LookAtBailbondsPostItNote } [ Password: 'Crime-Buster' ] -> HackFailed -> UseBailbondsPc
* { not HackIntoBailbondsPc && LookAtBailbondsPostItNote } [ Password: 'Crime-Crusher' ] -> HackFailed -> UseBailbondsPc
* { not HackIntoBailbondsPc && LookAtBailbondsPostItNote } [ Password: 'Crime-Killer' ] -> HackFailed -> UseBailbondsPc
* { not HackIntoBailbondsPc && ((LookAtBailbondsPostItNote && LookAtBailbondsOldCalendar) || PersuadeArthurKillpatrick ) } [ Password: 'Crime-Killer85' ] -> HackIntoBailbondsPc
* { SkillHack } [ Hack the password. ] -> HackIntoBailbondsPc
* { HackIntoBailbondsPc } [ Read criminal files. ] -> ReadCriminalFilesInBailbondsPc
* { not HackIntoBailbondsPc } [ Type random password. ] -> HackFailed -> UseBailbondsPc
+ [ Quit. ] -> QuitBailbondsPc

=== QuitBailbondsPc ===
~Hide(Bailbonds_PcOn) 
~StopSound("HACKING")
-> Bailbonds

=== HackIntoBailbondsPc ===
-> HackSuccessful -> UseBailbondsPc

=== ReadCriminalFilesInBailbondsPc ===
Player: There're a lot of criminal files in this database.
Player: I can't read it outloud. I don't want to get sued.
-> Bailbonds

=== UseNoirGalleryPc ===
~PlayerAnimation("Hack")
~Show(NoirGallery_PcOn)
#choice #hack
* { not HackIntoNoirGalleryPC && (LookAtNoirGalleryPostIt || LookAtPileOfBooks) } [ Password: 'Lilith' ] -> HackFailed -> UseNoirGalleryPc
* { not HackIntoNoirGalleryPC && LookAtPainting1 } [ Password: 'AdamsFirstWife' ] -> HackFailed -> UseNoirGalleryPc
* { not HackIntoNoirGalleryPC && LookAtPainting2 } [ Password: 'LilithLeftAdam' ] -> HackFailed -> UseNoirGalleryPc
* { not HackIntoNoirGalleryPC && LookAtPainting3 } [ Password: 'GardenOfEden' ] -> HackFailed -> UseNoirGalleryPc
* { not HackIntoNoirGalleryPC && LookAtPainting4 } [ Password: 'ChildrenOfTheNight' ] -> HackFailed -> UseNoirGalleryPc
* { not HackIntoNoirGalleryPC && (LookAtNoirGalleryPostIt || LookAtPileOfBooks ) && LookAtPainting1 && LookAtPainting2 && LookAtPainting3 && LookAtPainting4 } [ Password: 'CreatureOfTheNight' ] -> HackIntoNoirGalleryPC
* { SkillHack } [ Hack the password. ] -> HackIntoNoirGalleryPC
* { HackIntoNoirGalleryPC } [ Read private files. ] -> ReadPrivateFilesInNoirGalleryPc
* { not HackIntoNoirGalleryPC } [ Type random password. ] -> HackFailed -> UseNoirGalleryPc
+ [ Quit. ] -> QuitNoirGalleryPc

=== QuitNoirGalleryPc ===
~Hide(NoirGallery_PcOn)
~StopSound("HACKING")
-> NoirGallery

=== HackIntoNoirGalleryPC ===
-> HackSuccessful -> UseNoirGalleryPc

=== ReadPrivateFilesInNoirGalleryPc ===
Player: This file contains private information about the gallery and their artists.
Player: I can't read it outloud. I don't want to get sued.
-> NoirGallery

=== UseSuitesCarltonLaptop ===
~PlayerAnimation("Hack")
~Show(SuitesCarlton_LaptopOn)
#choice #hack
* { not HackIntoCarltonLaptop && OpenSuitesCarltonCabinet } [ Password: 'Sherlock' ] -> HackFailed -> UseSuitesCarltonLaptop
* { not HackIntoCarltonLaptop && LookAtCarltonsDiary } [ Password: '1965' ] -> HackFailed -> UseSuitesCarltonLaptop
* { not HackIntoCarltonLaptop && LookAtCarltonsDiary && OpenSuitesCarltonCabinet } [ Password: 'Sherlock65' ] -> HackIntoCarltonLaptop
* { SkillHack } [ Hack the password. ] -> HackIntoCarltonLaptop
* { HackIntoCarltonLaptop } [ Read private files. ] -> ReadPrivateFilesInCarltonLaptop
* { not HackIntoCarltonLaptop } [ Type random password. ] -> HackFailed -> UseSuitesCarltonLaptop
+ [ Quit. ] -> QuitSuitesCarltonLaptop 

=== QuitSuitesCarltonLaptop ===
~Hide(SuitesCarlton_LaptopOn)
~StopSound("HACKING")
-> SuitesCarlton

=== HackIntoCarltonLaptop ===
-> HackSuccessful -> UseSuitesCarltonLaptop

=== ReadPrivateFilesInCarltonLaptop ===
Player: Here's the code to open the safe.
-> UseSuitesCarltonLaptop

=== UseJulietteLaptop ===
~PlayerAnimation("Hack")
~Show(MadhouseOffice_JulietteLaptopOn)
#choice #hack
* { not HackIntoJulietteLaptop && LookAtJuliettePostit } [ Password: 'Password3' ] -> HackIntoJulietteLaptop
* { SkillHack } [ Hack the password. ] -> HackIntoJulietteLaptop
* { HackIntoJulietteLaptop } [ Read private files. ] -> ReadPrivateFilesInJulietteLaptop
* { not HackIntoJulietteLaptop } [ Type random password. ] -> HackFailed -> UseJulietteLaptop
+ [ Quit. ] -> QuitJulietteLaptop

=== QuitJulietteLaptop ===
~Hide(MadhouseOffice_JulietteLaptopOn)
~StopSound("HACKING")
-> MadhouseOffice

=== HackIntoJulietteLaptop ===
-> HackSuccessful -> UseJulietteLaptop

=== ReadPrivateFilesInJulietteLaptop ===
Player: There's a diary of Juliette's most recent private activities.
Player: I can't read it outloud. It's too explicit.
-> UseJulietteLaptop

=== UseTessPc ===
~PlayerAnimation("Hack")
~Show(MadhouseOffice_TessPcOn)
#choice #hack
* { SkillHack } [ Hack the password. ] -> HackIntoTessPc
* { HackIntoTessPc } [ Read private files. ] -> ReadPrivateFilesInTessPc
* { not HackIntoTessPc } [ Type random password. ] -> HackFailed -> UseTessPc
+ [ Quit. ] -> QuitTessPc 

=== QuitTessPc ===
~Hide(MadhouseOffice_TessPcOn)
~StopSound("HACKING")
-> MadhouseOffice

=== HackIntoTessPc ===
-> HackSuccessful -> UseTessPc

=== ReadPrivateFilesInTessPc ===
Player: There's a log of Tess' most private financial activities.
Player: I can't read it outloud. I'm sure they're ilegal, at least in some states.
-> UseTessPc

=== UseBernardThungPc ===
~PlayerAnimation("Hack")
~Show(Silo_PcOn)
#choice #hack
+ [ List all passwords. ] -> BernardThungPCListAll
* { HackIntoBeachHouseInteriorPc } [ Add Jay's password. ] -> BernardThungPCAddPassword
* { HackIntoBailbondsPc } [ Add Arthur Killpatrick's password. ] -> BernardThungPCAddPassword
* { HackIntoNoirGalleryPC } [ Add Noir Gallery's password. ] -> BernardThungPCAddPassword
* { HackIntoCarltonLaptop } [ Add Carlton's password. ] -> BernardThungPCAddPassword
* { HackIntoJulietteLaptop } [ Add Juliette's password. ] -> BernardThungPCAddPassword
* { HackIntoTessPc } [ Add Tess' password. ] -> BernardThungPCAddPassword
* { BernardThungPCAllPasswordAdded } [ Re-install spyware software. ] -> BernardThungPCReinstallSpyware
+ [ Quit. ] -> QuitBernardThungPc

=== QuitBernardThungPc === 
~Hide(Silo_PcOn)
~StopSound("HACKING")
-> Silo

=== BernardThungPCListAll ===
Player: The screen says that there are {NumberOfPasswords} of 6 passwords successfully added.
-> UseBernardThungPc

=== BernardThungPCAddPassword ===
~NumberOfPasswords++
{
	- NumberOfPasswords == 6 : -> BernardThungPCAllPasswordAdded
	- else : -> BernardThungPCNotEnoughPasswords
}

=== BernardThungPCAllPasswordAdded ===
Player: That was the last password! Time to re-install the spyware.
~UpdateQuest(HackingQuest, HackingQuest_1)
-> UseBernardThungPc

=== BernardThungPCNotEnoughPasswords ===
Player: Okay, one more password in the system.
-> UseBernardThungPc

=== BernardThungPCReinstallSpyware ===
Player: And... it's done!
~UpdateQuest(HackingQuest, HackingQuest_2)
-> UseBernardThungPc