VAR IsDinerPublicPhoneWithCoins = 0

=== Diner ===
* { not DinerFirstEncounter } -> DinerFirstEncounter
+ { DinerFirstEncounter } -> DinerDefault

=== DinerFirstEncounter ===
-> DinerDefault

=== DinerDefault ===
+ [ Use exit door ] -> GoToSecondStreetFromDiner
+ [ Look at Dori ] -> LookAtDori
+ [ Talk to Dori ] -> TalkToDori
+ [ Look at Gilberto ] -> LookAtCook
+ [ Talk to Gilberto ] -> TalkToCook
+ [ Look at Father Joachim ] -> LookAtExorcist
+ [ Talk to Father Joachim ] -> TalkToExorcist
* { HasQuest(RoseQuest) && ExorcistStayingInSantaMonica && HasInInventory(BirthdayCake) } [ Give birthday cake to Father Joachim ] -> GiveCakeToFatherJoachim
* { HasQuest(RoseQuest) && ExorcistStayingInSantaMonica && HasInInventory(IntoxicatedBirthdayCake) } [ Give  intoxicated birthday cake to Father Joachim ] -> GiveCakeToFatherJoachim
* { HasQuest(RoseQuest) && ExorcistStayingInSantaMonica && HasInInventory(OldBible) } [ Give old Bible to Father Joachim ] -> GiveOldBibleToFatherJoachim
* { HasQuest(RoseQuest) && ExorcistStayingInSantaMonica && HasInInventory(DiabolicPropaganda) } [ Give diabolic propaganda to Father Joachim ] -> GiveDiabolicPropagandaToFatherJoachim
* { HasQuest(RoseQuest) && ExorcistStayingInSantaMonica && HasInInventory(OldDiabolicBible) } [ Give old diabolic Bible to Father Joachim ] -> GiveOldDiabolicBibleToFatherJoachim
+ [ Look at tip jar ] -> LookAtTipJar -> Diner
+ { not SkillSteal } [ Pick up tip jar ] -> CantPickUpTipJar
* { SkillSteal } [ Pick up tip jar ] -> StealTipJar
+ [ Look at public phone ] -> LookAtPublicPhone
+ { HasInInventory(TipJar) } [ Use tip jar with public phone ] -> CoinPublicPhone
+ { HasInInventory(PileOfCoins) } [ Use pile of coins with public phone ] -> CoinPublicPhone
* { IsDinerPublicPhoneWithCoins && HasInInventory(PlumberBrosFlyer) } [ Use Plumber Bros flyer with public phone ] -> UsePlumberBrosFlyerWithPublicPhone
+ { not IsDinerPublicPhoneWithCoins && HasInInventory(PlumberBrosFlyer) } [ Use Plumber Bros flyer with public phone ] -> CantUsePlumberBrosFlyerWithPublicPhone
+ [ Look at jukebox ] -> LookAtJukebox
+ [ Look at counter ] -> LookAtCounter
+ [ Look at newspaper ] -> LookAtDinerNewspaper
+ [ Look at inventory items ] -> LookAtInventoryItems -> Diner

=== GoToSecondStreetFromDiner ===
~PlaySound("OPEN_DOOR")
~GoTo(Room_SecondStreet)
-> SecondStreet

=== LookAtDori ===
Player: A gloomy old lady is attending the diner.
-> Diner

=== LookAtCook ===
Player: A fat cook is working hard on the kitchen.
-> Diner

=== LookAtExorcist ===
Player: A priest is having a cup of coffee.
-> Diner

=== CantPickUpTipJar === 
Dori: Hey! Get your hands off the tip jar!
-> Diner

=== StealTipJar ===
Player: Look behind you! A three-headed monkey! #portrait:smile
Dori: What?
Player: It never gets old. #portrait:smile
-> StealSuccessful -> PickUpTipJar

=== PickUpTipJar === 
~AddToInventory(TipJar)
~Hide(Diner_TipJar)
-> LookAtTipJar -> Diner

=== LookAtPublicPhone ===
Player: A public phone.
-> Diner

=== CoinPublicPhone ===
~IsDinerPublicPhoneWithCoins = 1
Player: Here we go. A coin goes into the phone.
-> Diner

=== CantUsePlumberBrosFlyerWithPublicPhone ===
Player: Mmmm... I can call the plumbers in the flyer, but first I need to add some coins to the public phone.
-> Diner

=== UsePlumberBrosFlyerWithPublicPhone ===
Player: Yeah? The Plumber Bros? We need some serious plumbing on main street. 
Player: Aha. Right away, perfect. #portrait:smile
~RemoveFromInventory(TipJar)
~RemoveFromInventory(PileOfCoins)
~RemoveFromInventory(PlumberBrosFlyer)
~Hide(Diner_PublicPhone)
~Show(MainStreet_PlumberBros)
~PlumberBrosInMainStreet = 1
-> Diner

=== LookAtJukebox ===
Player: A coin-operated jukebox machine.
-> Diner

=== LookAtCounter ===
Player: Dirty plates and moldy sandwiches.
-> Diner

=== LookAtDinerNewspaper ===
Player: A rolled up edition of the local newspaper.
-> Diner