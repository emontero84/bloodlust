VAR IsJulietteInMadhouseOffice = 0
VAR IsTessInMadhouseOffice = 0
VAR IsVoermanDilemmaInMadhouseOffice = 0

=== MadhouseOffice ===
~Hide(MadhouseOffice_ExitDoorOpen)
* { not MadhouseOfficeJulietteFirstEncounter && not MadhouseOfficeTessFirstEncounter &&  not MadhouseOfficeVoermannDilemmaFirstEncounter } -> MadhouseOfficeJulietteFirstEncounter
* { MadhouseOfficeJulietteFirstEncounter && not MadhouseOfficeTessFirstEncounter &&  not MadhouseOfficeVoermannDilemmaFirstEncounter && IsQuestStepUpdated(JulietteQuest_1) && IsQuestStepUpdated(JulietteQuest_2) && IsQuestStepUpdated(JulietteQuest_3) && HasInInventory(CharityBox) } -> MadhouseOfficeTessFirstEncounter
* { MadhouseOfficeJulietteFirstEncounter && MadhouseOfficeTessFirstEncounter &&  not MadhouseOfficeVoermannDilemmaFirstEncounter  && IsQuestStepUpdated(TessQuest_1) && IsQuestStepUpdated(TessQuest_2) && IsQuestStepUpdated(TessQuest_3) && HasInInventory(MariansDiary) } -> MadhouseOfficeVoermannDilemmaFirstEncounter
+ { MadhouseOfficeJulietteFirstEncounter || MadhouseOfficeTessFirstEncounter } -> MadhouseOfficeDefault

=== MadhouseOfficeJulietteFirstEncounter ===
~IsJulietteInMadhouseOffice = 1
~IsTessInMadhouseOffice = 0
~IsVoermanDilemmaInMadhouseOffice = 0
~Show(MadhouseOffice_JulietteVoermann)
~Hide(MadhouseOffice_TessVoermann)
-> MadhouseOfficeDefault

=== MadhouseOfficeTessFirstEncounter ===
~IsTessInMadhouseOffice = 1
~IsJulietteInMadhouseOffice = 0
~IsVoermanDilemmaInMadhouseOffice = 0
~Show(MadhouseOffice_TessVoermann)
~Hide(MadhouseOffice_JulietteVoermann)
-> MadhouseOfficeDefault

=== MadhouseOfficeVoermannDilemmaFirstEncounter ===
~IsVoermanDilemmaInMadhouseOffice = 1
~IsTessInMadhouseOffice = 0
~IsJulietteInMadhouseOffice = 0
~Show(MadhouseOffice_JulietteVoermann)
~Show(MadhouseOffice_TessVoermann)
-> StartVoermannDilemma

=== MadhouseOfficeDefault ===
+ [ Use exit door ] -> GoToElevatorFromMadhouseOffice
+ { IsJulietteInMadhouseOffice == 1 } [ Look at Juliette Voermann ] -> LookAtJulietteVoermann
+ { IsJulietteInMadhouseOffice == 1 } [ Talk to Juliette Voermann ] -> TalkToJulietteVoermann
+ { IsTessInMadhouseOffice == 1 } [ Look at Tess Voermann ] -> LookAtTessVoermann
+ { IsTessInMadhouseOffice == 1 } [ Talk to Tess Voermann ] -> TalkToTessVoermann
+ [ Look at painting ] -> LookAtMadhouseOfficePainting
+ [ Look at laptop ] -> LookAtJulietteLaptop
+ { IsTessInMadhouseOffice == 1 } [ Use laptop ] -> UseJulietteLaptop
+ { IsTessInMadhouseOffice == 0 } [ Use laptop ] -> CantUseJulietteLaptop
+ [ Look at PC ] -> LookAtTessPc
+ { IsJulietteInMadhouseOffice == 1 } [ Use PC ] -> UseTessPc
+ { IsJulietteInMadhouseOffice == 0 } [ Use PC ] -> CantUseTessPc
+ [ Look at post-it ] -> LookAtJuliettePostit
+ [ Look at lipstick ] -> LookAtLipstick
* [ Use desk drawer ] -> OpenMadhouseOfficeDeskDrawer
* [ Use top drawer ] -> OpenMadhouseOfficeTopDrawer
* [ Use bottom drawer ] -> OpenMadhouseOfficeBottomDrawer
+ [ Look at philosophy book ] -> LookAtPhilosophyBook3 -> MadhouseOffice
* [ Pick up philosophy book ] -> PickUpPhilosophyBook3
+ [ Look at inventory items ] -> LookAtInventoryItems -> MadhouseOffice

=== GoToElevatorFromMadhouseOffice ===
~PlayerAnimation("Interact")
~Wait(1)
~Hide(MadhouseOffice_ExitDoorOpen)
~PlaySound("OPEN_DOOR")
~GoTo(Room_Elevator)
-> Elevator

=== LookAtMadhouseOfficePainting ===
Player: A big painting of Juliette and Tess when they were just two little girls.
-> MadhouseOffice

=== LookAtJulietteVoermann ===
Player: Juliette Voermann. She's sexy, deadly and beautiful.
-> MadhouseOffice

=== LookAtTessVoermann === 
Player: Tess Voermann. A fierce business-woman turned into a vampire.
-> MadhouseOffice

=== LookAtJulietteLaptop ===
Player: A laptop.
-> MadhouseOffice

=== CantUseJulietteLaptop ===
Juliette Voermann: Hey! Don't touch my laptop!
-> MadhouseOffice

=== LookAtTessPc ===
Player: A working PC station.
-> MadhouseOffice

=== CantUseTessPc ===
Tess Voermann: Please, don't touch my PC.
-> MadhouseOffice

=== LookAtJuliettePostit ===
Player: A post-it note with the words 'Password1' and 'Password2' slashed.
-> MadhouseOffice

=== LookAtLipstick ===
Player: A pink lipstick.
-> MadhouseOffice

=== OpenMadhouseOfficeDeskDrawer ===
~Hide(MadhouseOffice_DeskDrawer)
~Show(MadhouseOffice_DeskDrawerOpen)
Player: There's a book in here. It seems rather important.
~AddToInventory(SkillBook5)
-> LookAtSkillBook5 -> MadhouseOffice

=== OpenMadhouseOfficeTopDrawer ===
~Hide(MadhouseOffice_TopDrawer)
~Show(MadhouseOffice_TopDrawerOpen)
-> MadhouseOffice

=== OpenMadhouseOfficeBottomDrawer ===
~Hide(MadhouseOffice_BottomDrawer)
~Show(MadhouseOffice_BottomDrawerOpen)
-> MadhouseOffice

=== PickUpPhilosophyBook3 ===
~AddToInventory(PhilosophyBook3)
~Hide(MadhouseOffice_PhilosophyBook3)
-> LookAtPhilosophyBook3 -> MadhouseOffice