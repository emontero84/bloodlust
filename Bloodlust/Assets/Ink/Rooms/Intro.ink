=== Introduction ===
~IsGameStarted = 1
~Show(Introduction_FadeIn)
~Wait(5)
Sebastien De la Croix: Good evening, my fellow kindred. My apologies for disrupting or interfering with any business you may have had this evening. #portrait:smile
Sebastien De la Croix: We are here because the laws that bind our society have been broken. #portrait:angry
Sebastien De la Croix: As the regent of Los Angeles, I am within my right to grant or deny the vampires of this city the privilige of progeny.
Sebastien De la Croix: Many of you have come to me seeking this permission, and I have endorsed some of these requests. #portrait:smile
Sebastien De la Croix: However, the accused that sits before you tonight was not refused my permission. Indeed, my permission was never sought at all! #portrait:angry
Sebastien De la Croix: It pains me to announce the sentence, since as some of you may know, the penalty of this transgression is death. #portrait:angry
Sebastien De la Croix: Let the penalty commence! #portrait:smile
~Hide(Introduction_Executor)
~Hide(Introduction_Progenitor)
~Show(Introduction_ExecutorAnimation)
~Wait(3)
~PlaySound("SLASH_SWORD")
Sebastien De la Croix: Which leads to the fate of the ill-begotten progeny.
Sebastien De la Croix: Without a progenitor, most childe are doomed to walk the earth never knowing their place, their responsability.
Sebastien De la Croix: And most importantly the laws they must obey. Therefore, I have decided that...
Jack: This is bullshit! #portrait:angry
Sebastien De la Croix: ...
Sebastien De la Croix: If Mr. Jack would let me finish... I have decided to let this vampire live. #portrait:smile
Sebastien De la Croix: {IsFemale:She}{not IsFemale:He} shall be instructed in the ways of our kind and be granted the same rights.
Sebastien De la Croix: I thank you all for attending these proceedings, and I hope their significance is not lost. Good evening. #portrait:smile
Player: ... #portrait:confused
~AddQuest(FirstSteps)
~UpdateQuest(FirstSteps, FirstSteps_1)
-> StartGame