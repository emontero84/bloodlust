VAR IsOldLadyGone = 0

=== Pawnshop ===
~Show(Pawnshop_PawnshopDoor)
~Hide(Pawnshop_OldLadyLeavingAnimation)
* { not InitPawnshop } -> InitPawnshop
+ { InitPawnshop } -> PawnshopDefault

=== InitPawnshop ===
-> PawnshopDefault

=== PawnshopDefault ===
+ [ Use exit door ] -> GoToMainStreetFromPawnshop
+ [ Look at Trip ] -> LookAtTrip
+ [ Talk to Trip ] -> TalkToTrip
* { HasQuest(DrugTrip) && HasInInventory(JarOfWeed) } [ Give jar of weed to Trip ] -> GiveJarOfWeedToTrip
+ [ Look at Miss Webster ] -> LookAtOldLady
+ [ Talk to Miss Webster ] -> TalkToOldLady
* { HasQuest(OldLadyQuest) && not IsQuestStepUpdated(OldLadyQuest_1) && OldLadyHusbandsWatch && HasInInventory(CheapWatch) } [ Give cheap watch to Miss Webster ] -> GiveCheapWatchToOldLady
* { HasQuest(OldLadyQuest) && not IsQuestStepUpdated(OldLadyQuest_1) && OldLadyHusbandsWatch && HasInInventory(GoldenWatch) } [ Give golden watch to Miss Webster ] -> GiveGoldenWatchToOldLady
* { HasQuest(OldLadyQuest) && not IsQuestStepUpdated(OldLadyQuest_1) && OldLadyHusbandsWatch && HasInInventory(FakeGoldenWatch) } [ Give fake golden watch to Miss Webster ] -> GiveFakeGoldenWatchToOldLady
* { HasQuest(OldLadyQuest) && not IsQuestStepUpdated(OldLadyQuest_1) && OldLadyHusbandsWatch && HasInInventory(BirthdayCake) } [ Give birthday cake to Miss Webster ] -> GiveBirthdayCakeToOldLady
* { HasQuest(OldLadyQuest) && not IsQuestStepUpdated(OldLadyQuest_1) && OldLadyHusbandsWatch && HasInInventory(IntoxicatedBirthdayCake) } [ Give intoxicated birthday cake to Miss Webster ] -> GiveIntoxicatedBirthdayCakeToOldLady
+ [ Look at big scissors ] -> LookAtBigScissorsAtCounter
+ [ Pick up big scissors ] -> PickUpBigScissors
+ [ Look at dress ] -> LookAtDressPawnshop
+ [ Look at old TV ] -> LookAtOldTV
+ [ Look at Chinese jar ] -> LookAtChineseJar
+ [ Look at table clock ] -> LookAtTableClock
* { HasQuest(DrugTrip) && HasInInventory(JarOfWeed) } [ Give jar of weed to Trip ] -> GiveJarOfWeedToTrip
+ [ Look at inventory items ] -> LookAtInventoryItems -> Pawnshop

=== GoToMainStreetFromPawnshop ===
~PlayerAnimation("Interact")
~Wait(1)
~Hide(Pawnshop_PawnshopDoor)
~PlaySound("OPEN_DOOR")
~GoTo(Room_MainStreet)
-> MainStreet

=== LookAtBigScissorsAtCounter ===
Player: There's a big pair of metallic scissors behind the pawnshop counter.
-> Pawnshop

=== PickUpBigScissors ===
Player: I can't. The scissors are behind the pawnshop counter.
-> Pawnshop

=== LookAtDressPawnshop ===
Player: There's a couple of dresses for sale.
-> Pawnshop

=== LookAtOldTV ===
Player: A pretty old television.
-> Pawnshop

=== LookAtChineseJar ===
Player: A beautiful Chinese jar that has been broken and glued again a couple of times.
-> Pawnshop

=== LookAtTableClock ===
Player: A robust table clock that does not tick. 
-> Pawnshop

=== LookAtTrip ===
Player: A guy waits on the customers from behind the bullet-proof glass.
-> Pawnshop

=== LookAtOldLady ===
Player: A very old lady is waiting at the Pawnshop.
-> Pawnshop