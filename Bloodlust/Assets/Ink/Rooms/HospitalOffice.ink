
=== HospitalOffice ===
* { not HospitalOfficeFirstEncounter } -> HospitalOfficeFirstEncounter
+ { HospitalOfficeFirstEncounter } -> HospitalOfficeDefault

=== HospitalOfficeFirstEncounter ===
-> HospitalOfficeDefault

=== HospitalOfficeDefault ===
+ [ Use exit door ] -> GoToHospitalFromHospitalOffice
+ [ Look at doctor ] -> LookAtDoctor
+ [ Talk to doctor ] -> TalkToDoctor
+ [ Look at stethoscope ] -> LookAtStethoscope -> HospitalOffice
* [ Pick up stethoscope ] -> PickUpStethoscope
+ [ Look at medical diploma ] -> LookAtMedicalDiploma
+ [ Look at hospital bed ] -> LookAtHospitalBed
+ [ Look at inventory items ] -> LookAtInventoryItems -> HospitalOffice

=== GoToHospitalFromHospitalOffice ===
~PlaySound("OPEN_DOOR")
~GoTo(Room_Hospital)
-> Hospital

=== LookAtDoctor ===
Player: The doctor is waiting behind his desk.
-> Hospital

=== PickUpStethoscope ===
~AddToInventory(Stethoscope)
~Hide(HospitalOffice_Stethoscope)
-> LookAtStethoscope -> HospitalOffice

=== LookAtMedicalDiploma ===
Player: A medical diploma.
-> HospitalOffice

=== LookAtHospitalBed ===
Player: A hospital bed.
-> HospitalOffice