=== SuitesMercurius ===
+ [ Use exit door ] -> GoToSuitesHallway
+ [ Look at Mercurius ] -> LookAtMercurius
+ [ Talk to Mercurius ] -> TalkToMercurius
* { HasQuest(AstroliteQuest) && HasInInventory(Astrolite) } [ Give Astrolite to Mercurius ] -> GiveAstroliteToMercurius
* { HasQuest(AstroliteQuest) && HasInInventory(MoneyBag) } [ Give money bag to Mercurius ] -> GiveMoneyBagToMercurius
* { MercuriusNeedsPainkillers && HasInInventory(Painkillers) } [ Give painkillers to Mercurius ] -> GivePainkillersToMercurius
+ [ Look at blood trail ] -> LookAtBloodTrail
+ [ Look at vodka bottle ] -> LookAtVodkaBottle -> SuitesMercurius
* [ Pick up vodka bottle ] -> PickUpVodkaBottle1
* [ Use cabinet ] -> OpenSuitesMercuriusCabinet
+ [ Look at phone ] -> LookAtMercuriusPhone
+ [ Look at inventory items ] -> LookAtInventoryItems -> SuitesMercurius

=== LookAtMercurius ===
Player: {not TalkToMercurius:This poor man}{TalkToMercurius: Mercurius} is bleeding like a stuck pig.
-> SuitesMercurius

=== LookAtBloodTrail ===
Player: A trail of blood goes from the entrance door to the couch.
-> SuitesMercurius

=== PickUpVodkaBottle1 ===
~AddToInventory(VodkaBottle1)
~Hide(SuitesMercurius_VodkaBottle1)
-> LookAtVodkaBottle -> SuitesMercurius

=== UseVodkaBottle ===
Player: I'm not drinking that. I don't want to puke.
-> SuitesMercurius

=== OpenSuitesMercuriusCabinet ===
~Hide(SuitesMercurius_Cabinet)
~Show(SuitesMercurius_CabinetOpen)
~PlaySound("OPEN_CONTAINER")
Player: There's a book in here that seems rather important.
~AddToInventory(SkillBook4)
-> LookAtSkillBook4 -> SuitesMercurius

=== LookAtMercuriusPhone ===
Player: A phone. Out of order.
-> SuitesMercurius