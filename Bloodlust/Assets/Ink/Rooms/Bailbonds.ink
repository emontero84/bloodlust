
=== Bailbonds ===
* { not BailbondsFirstEncounter } -> BailbondsFirstEncounter
+ { BailbondsFirstEncounter } -> BailbondsDefault

=== BailbondsFirstEncounter ===
-> BailbondsDefault

=== BailbondsDefault ===
+ [ Use exit door ] -> GoToSecondStreetFromBailbonds
* [ Use fridge ] -> OpenBailbondsFridge
+ [ Look at steak ] -> LookAtSteak -> Bailbonds
* { not IsQuestStepUpdated(DogQuest_1) } [ Pick up steak ] -> PickUpSteak
+ { IsQuestStepUpdated(DogQuest_1) } [ Pick up steak ] -> LookAtSteak -> Bailbonds
+ [ Look at vodka bottle ] -> LookAtVodkaBottle -> Bailbonds
* [ Pick up vodka bottle ] -> PickUpVodkaBottle3
+ [ Look at darts ] -> LookAtBailbondsDarts
* [ Use file cabinet ] -> OpenBailbondsFileCabinet
+ [ Look at PC ] -> LookAtBailbondsPc
+ [ Use PC ] -> UseBailbondsPc
+ [ Look at post-it ] -> LookAtBailbondsPostItNote
+ [ Look at old calendar ] -> LookAtBailbondsOldCalendar
+ [ Look at note ] -> LookAtBailbondsNote
* [ Use top drawer ] -> OpenBailbondsTopDrawer
* [ Use bottom drawer ] -> OpenBailbondsBottomDrawer
+ [ Look at box of donuts ] -> LookAtBoxOfDonuts -> Bailbonds
* [ Pick up box of donuts ] -> PickUpBoxOfDonuts
+ [ Look at coffee machine ] -> LookAtBailbondsCoffeeMachine
* { HasInInventory(EmptyCoffeeThermos) } [ Use empty coffee thermos with coffee machine ] -> UseEmptyCoffeeThermosWithCoffeeMachine
+ [ Look at pizza ] -> LookAtBailbondsPizza
* [ Look at pile of books ] -> LookAtBailbondsPileOfBooks
+ [ Look at Arthur Killpatrick ] -> LookAtArthurKillpatrick
+ [ Talk to Arthur Killpatrick ] -> TalkToArthurKillpatrick
* { HasQuest(ArthurKillpatrickQuest) && HasInInventory(CarltonsNote) } [ Give Carlton's note to Arthur Killpatrick ] -> GiveCarltonsNoteToArthurKillpatrick
* { HasQuest(ArthurKillpatrickQuest) && HasInInventory(CdRom) && GiveCarltonsNoteToArthurKillpatrick } [ Give CD-ROM to Arthur Killpatrick ] -> GiveCdRomToArthurKillpatrick
+ [ Look at inventory items ] -> LookAtInventoryItems -> Bailbonds

=== GoToSecondStreetFromBailbonds ===
~GoTo(Room_SecondStreet)
-> SecondStreet

=== OpenBailbondsFridge ===
~Hide(Bailbonds_Fridge)
~Show(Bailbonds_FridgeOpen)
~Show(Bailbonds_VodkaBottle3)
~Show(Bailbonds_Steak)
~PlaySound("OPEN_CONTAINER")
-> Bailbonds

=== PickUpVodkaBottle3 ===
~AddToInventory(VodkaBottle3)
~Hide(Bailbonds_VodkaBottle3)
-> LookAtVodkaBottle -> Bailbonds

=== PickUpSteak ===
~AddToInventory(Steak)
~Hide(Bailbonds_Steak)
-> Bailbonds

=== LookAtBailbondsDarts ===
Player: A target and some darts.
-> Bailbonds

=== OpenBailbondsFileCabinet ===
Player: Let's see...
~Hide(Bailbonds_FileCabinet)
~Show(Bailbonds_FileCabinetOpen) 
~PlaySound("OPEN_CONTAINER")
Player: Nothing of any particular interest.
-> Bailbonds

=== LookAtBailbondsPc ===
Player: A clunky PC terminal.
-> Bailbonds

=== LookAtBailbondsPostItNote ===
Player: There are several passwords scratched in the post-it note - Crime-Buster, Crime-Crusher and Crime-Killer.
-> Bailbonds

=== LookAtBailbondsOldCalendar ===
Player: It's a pretty old calendar. It says "Killpatrick Bailbonds, since 1985".
-> Bailbonds

=== LookAtBailbondsNote ===
Player: It's a TO-DO list. "Open the bailbonds. Put up the crime. Become rich and famous". 
Player: What could have gone wrong?
-> Bailbonds

=== OpenBailbondsTopDrawer ===
~Hide(Bailbonds_TopDrawer)
~Show(Bailbonds_TopDrawerOpen)
-> Bailbonds

=== OpenBailbondsBottomDrawer ===
~Hide(Bailbonds_BottomDrawer)
~Show(Bailbonds_BottomDrawerOpen)
Player: There's a book in here. It seems rather important.
~AddToInventory(SkillBook10)
-> LookAtSkillBook10 -> Bailbonds

=== PickUpBoxOfDonuts ===
~AddToInventory(BoxOfDonuts)
~Hide(Bailbonds_BoxOfDonuts)
-> LookAtBoxOfDonuts -> Bailbonds

=== LookAtBailbondsCoffeeMachine ===
Player: A coffee machine. It's full of hot coffee.
-> Bailbonds

=== UseEmptyCoffeeThermosWithCoffeeMachine ===
Player: Mmmm. It's full of coffe now.
~RemoveFromInventory(EmptyCoffeeThermos)
~AddToInventory(CoffeeThermos)
-> Bailbonds

=== LookAtBailbondsPizza ===
Player: Greasy old pizza.
-> Bailbonds

=== LookAtBailbondsPileOfBooks ===
Player: There's a bookmark in the books. It's a flyer.
~AddToInventory(PlumberBrosFlyer)
~Hide(Bailbonds_PileOfBooks)
-> Bailbonds

=== LookAtArthurKillpatrick ===
Player: A thick man with dark glasses waits on the customers of the bailbonds.
-> Bailbonds