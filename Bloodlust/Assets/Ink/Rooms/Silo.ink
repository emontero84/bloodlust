=== Silo ===
* { not SiloFirstEncounter } -> SiloFirstEncounter
+ { SiloFirstEncounter } -> SiloDefault

=== SiloFirstEncounter ===
-> SiloDefault

=== SiloDefault ===
+ [ Walk to exit ] -> GoToSecondStreetFromSilo
+ [ Look at Bernard Thung ] -> LookAtBernardThung
+ [ Talk to Bernard Thung ] -> TalkToBernardThung
* { HasQuest(BernardThungQuest) && HasInInventory(CdRom) } [ Give CD-ROM to Bernard Thung ] -> GiveCdRomToBernardThung
+ [ Look at PC ] -> LookAtBernardThungPc
+ { not BernardThungAddHackingQuest } [ Use PC ] -> CantUseBernardThungPc
+ { BernardThungAddHackingQuest } [ Use PC ] -> UseBernardThungPc
+ [ Look at action figure ] -> LookAtActionFigure
+ [ Look at inventory items ] -> LookAtInventoryItems -> Silo

=== GoToSecondStreetFromSilo ===
~GoTo(Room_SecondStreet)
-> SecondStreet

=== LookAtBernardThung ===
Player: A monstrous vampire with pointy ears and rat-like teeth.
Bernard Thung: I can hear you.
Player: Oh...
-> Silo

=== LookAtBernardThungPc ===
Player: A pretty impressive computer station.
-> Silo

=== CantUseBernardThungPc ===
Bernard Thung: Don't touch my PC!
Player: Okay...
-> Silo

=== LookAtActionFigure ===
Player: A limited edition action figurine of the critically acclaimed "Nuclear Wastelander".
-> Silo 