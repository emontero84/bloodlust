
VAR AlphaBribed = 0

=== ParkingGarage ===
+ [ Walk to Main street ] -> GoToMainStreet
+ [ Walk to Second street ] -> GoToSecondStreet
+ { AlphaBribed == 0 } [ Use stairs to the beach ] -> CantGoToBeach
+ { AlphaBribed == 0 } [ Walk to stairs to the beach ] -> CantGoToBeach
+ { AlphaBribed == 1 } [ Use stairs to the beach ] -> GoToBeach
+ { AlphaBribed == 1 } [ Walk to stairs to the beach ] -> GoToBeach
+ { AlphaBribed == 0 } [ Look at Alpha ] -> LookAtAlpha
+ { AlphaBribed == 1 } [ Look at Alpha ] -> LookAtAlphaBribed
+ [ Talk to Alpha ] -> TalkToAlpha
* { AlphaToll && AlphaBribed == 0 && HasInInventory(MoneyClip) } [ Give money clip to Alpha ] -> GiveMoneyClipToAlpha
+ [ Look at security guard ] -> LookAtSecurityGuard
+ [ Talk to security guard ] -> TalkToSecurityGuard
+ [ Look at Bullock ] -> LookAtBullock
+ [ Talk to Bullock ] -> TalkToBullockTheMotorhead
* { HasQuest(BullockQuest) && HasInInventory(Wrench) }  [ Give wrench to Bullock ] -> GiveWrenchToBullockTheMotorhead
* { HasQuest(BullockQuest) && HasInInventory(CanOfGas) }  [ Give can of gas to Bullock ] -> GiveCanOfGasToBullockTheMotorhead
+ [ Look at bucket of yellow paint ] -> LookAtBucketOfYellowPaint -> ParkingGarage
* [ Pick up bucket of yellow paint ] -> PickUpBucketOfYellowPaint
+ [ Look at green car ] -> LookAtGreenCar
+ [ Look at blue car ] -> LookAtBlueCar
+ [ Look at pink car ] -> LookAtPinkCar
+ { not SkillKick && not SkillLockpicking } [ Look at red car ] -> LookAtRedCar
* { SkillKick } [ Look at red car ] -> KickRedCarTrunk
* { SkillLockpicking } [ Look at red car ] -> LockpickRedCarTrunk
* { HasInInventory(Crowbar) } [ Use crowbar with red car ] -> OpenRedCarTrunkWithCrowbar
* { HasInInventory(LillysKey) } [ Use Lilly's key with red car ] -> OpenRedCarTrunkWithLillysKey
+ [ Look at motorbike ] -> LookAtMotorbike
+ [ Use motorbike ] -> UseMotorbike
+ [ Look at inventory items ] -> LookAtInventoryItems -> ParkingGarage

=== GoToMainStreet ===
~GoTo(Room_MainStreet)
-> MainStreet

=== GoToSecondStreet ===
~GoTo(Room_SecondStreet)
-> SecondStreet

=== GoToBeach ===
~GoTo(Room_Beach)
-> Beach

=== CantGoToBeach ===
-> TalkToAlpha

=== PickUpBucketOfYellowPaint ===
~AddToInventory(BucketOfYellowPaint)
~Hide(ParkingGarage_BucketOfYellowPaint)
-> LookAtBucketOfYellowPaint -> ParkingGarage

=== LookAtAlpha ===
Player: This big fellow is blocking the stairs to the beach.
-> ParkingGarage

=== LookAtAlphaBribed ===
Player: A punk with a heart of gold.
-> ParkingGarage

=== LookAtSecurityGuard ===
Player: A security guard is waiting at the parking entrance.
-> ParkingGarage

=== LookAtBullock ===
Player: A motorhead and his bike.
-> ParkingGarage

=== LookAtMotorbike ===
Player: A beautiful motorbike. 
-> ParkingGarage

=== UseMotorbike ===
Player: It's clearly not working. Bullock is trying to fix it.
-> ParkingGarage

=== LookAtGreenCar ===
Player: A green car.
-> ParkingGarage

=== LookAtBlueCar ===
Player: A blue car.
-> ParkingGarage

=== LookAtPinkCar ===
Player: A pink car.
-> ParkingGarage

=== LookAtRedCar ===
Player: A red '64 Cadillac with a white top and silver tires.
-> ParkingGarage

=== OpenRedCarTrunkWithCrowbar ===
-> OpenRedCarTrunk

=== OpenRedCarTrunkWithLillysKey ===
~RemoveFromInventory(LillysKey)
-> OpenRedCarTrunk

=== KickRedCarTrunk ===
-> KickSuccessful -> OpenRedCarTrunk

=== LockpickRedCarTrunk ===
Player: Let's see...
~Wait(2)
-> LockpickingSuccessful -> OpenRedCarTrunk

=== OpenRedCarTrunk ===
Player: Voilá.
~PlaySound("OPEN_CONTAINER")
~Show(ParkingGarage_RedCarOpen)
Player: There's a diary and a plastic tube in the trunk.
~AddToInventory(LillysDiary)
~AddToInventory(PlasticTubeWithFunnel)
~UpdateQuest(ElliotQuest, ElliotQuest_2)
-> ParkingGarage