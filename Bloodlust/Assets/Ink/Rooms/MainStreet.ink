
VAR DogIsGone = 0
VAR DogIsUnleashed = 0
VAR PlumberBrosInMainStreet = 0

=== MainStreet ===
~Hide(MainStreet_SuitesHallwayDoorOpen)
~Hide(MainStreet_LodgingDoorOpen)
~Hide(MainStreet_PawnshopDoorOpen)
~Hide(MainStreet_HospitalDoorOpen)
~Hide(MainStreet_DogRunningAnimation)
* { not InitMainStreet } -> InitMainStreet
+ { InitMainStreet } -> MainStreetDefault

=== InitMainStreet ===
~Hide(MainStreet_DoorToSantaMonicaPalace)
~Show(MainStreet_SantaMonicaPalace)
~Show(MainStreet_Dog)
~Show(MainStreet_DogLeash)
-> MainStreetDefault

=== MainStreetDefault ===
+ { DogIsUnleashed == 0 && DogIsGone == 0 } [ Walk to Santa Monica Palace ] -> LookAtDog
+ { DogIsUnleashed == 1 && DogIsGone == 0 } [ Walk to Santa Monica Palace ] -> LookAtDogUnleashed
+ { DogIsUnleashed == 1 && DogIsGone == 1 } [ Use doors to Santa Monica Palace ] -> GoToSuitesHallway
+ [ Use door to Lodgings ] -> GoToLodgingHallwayFromMainStreet
+ [ Walk to parking garage ] -> GoToParkingGarage
+ [ Use door to Pawnshop ] -> GoToPawnshop
+ [ Use door to Noir Gallery ] -> GoToNoirGallery
+ [ Use door to Hospital ] -> GoToHospital
+ { DogIsUnleashed == 0 && DogIsGone == 0 } [ Look at dog ] -> LookAtDog
+ { DogIsUnleashed == 1 && DogIsGone == 0 } [ Look at dog ] -> LookAtDogUnleashed
+ { DogIsUnleashed == 1 && DogIsGone == 1 } [ Look at dog ] -> LookAtDogSleeping
+ { DogIsUnleashed == 1 && DogIsGone == 0 } [ Use dog ] -> DontTouchDog
+ [ Pick up dog ] -> DontTouchDog
+ [ Talk to dog ] -> TalkToDog
+ [ Look at leash ] -> LookAtLeash
+ [ Pick up leash ] -> PickUpLeash
+ [ Use leash ] -> PickUpLeash
* { HasInInventory(BigScissors) } [ Use big scissors with leash ] -> UseBigScissorsWithLeash
+ [ Look at Jimmy Johnson ] -> LookAtJimmyJohnson
+ [ Talk to Jimmy Johnson ] -> TalkToJimmyJohnson
* { HasInInventory(BaseballBat) } [ Use baseball bat with Jimmy Johnson ] -> IntimidateJimmyWithBaseballBat
* { HasInInventory(Katana) } [ Use katana with Jimmy Johnson ] -> IntimidateJimmyWithKatana
+ [ Look at Homeless Doug ] -> LookAtHomelessDoug
+ [ Talk to Homeless Doug ] -> TalkToHomelessDoug
* { HasQuest(VodkaQuest) && HasInInventory(VodkaBottle1) } [ Give vodka bottle 1 to Homeless Doug ] -> GiveVodkaBottle1ToHomelessDoug
* { HasQuest(VodkaQuest) && HasInInventory(VodkaBottle2) } [ Give vodka bottle 2 to Homeless Doug ] -> GiveVodkaBottle2ToHomelessDoug
* { HasQuest(VodkaQuest) && HasInInventory(VodkaBottle3) } [ Give vodka bottle 3 to Homeless Doug ] -> GiveVodkaBottle3ToHomelessDoug
+ [ Look at Claire ] -> LookAtClaire
+ [ Talk to Claire ] -> TalkToClaire
+ [ Look at Mr. D ] -> LookAtMrD
+ [ Talk to Mr. D ] -> TalkToMrD
* { HasQuest(JulietteQuest) && not IsQuestStepUpdated(JulietteQuest_1) && not MrDAllowsYouToEnter && MrDExplains && HasInInventory(MillionaireDress) } [ Give dress to Mr. D ] -> GiveMillionaireDressToMrD
* { HasQuest(JulietteQuest) && not IsQuestStepUpdated(JulietteQuest_1) && not MrDAllowsYouToEnter && MrDExplains && HasInInventory(DiamondRing) } [ Give diamond ring to Mr. D ] -> GiveDiamondRingToMrD
* { HasQuest(JulietteQuest) && not IsQuestStepUpdated(JulietteQuest_1) && not MrDAllowsYouToEnter && MrDExplains && HasInInventory(GoldenWatch) } [ Give golden watch to Mr. D ] -> GiveGoldenWatchToMrD
* { HasQuest(JulietteQuest) && not IsQuestStepUpdated(JulietteQuest_1) && not MrDAllowsYouToEnter && MrDExplains && HasInInventory(FakeGoldenWatch) } [ Give fake golden watch to Mr. D ] -> MrDFakeGoldenWatch
+ [ Look at fire hydrant ] -> LookAtFireHydrant
+ [ Use fire hydrant ] -> UseFireHydrant
* { HasInInventory(PlumberBrosFlyer) } [ Use Plumber Bros flyer with fire hydrant ] -> UsePlumberBrosFlyerWithFireHydrant
+ { PlumberBrosInMainStreet } [ Look at Plumber Bros van ] -> LookAtPlumberBrosVan
+ { PlumberBrosInMainStreet } [ Pick up Plumber Bros van ] -> PickUpPlumberBrosVan
+ [ Use taxi ] -> UseTaxi
+ [ Look at inventory items ] -> LookAtInventoryItems -> MainStreet

=== GoToLodgingHallwayFromMainStreet ===
~PlayerAnimation("Interact")
~Wait(1)
~Show(MainStreet_LodgingDoorOpen)
~PlaySound("OPEN_DOOR")
~GoTo(Room_LodgingHallway)
-> LodgingHallway

=== GoToSuitesHallway ===
~PlayerAnimation("Interact")
~Wait(1)
~Show(MainStreet_SuitesHallwayDoorOpen)
~PlaySound("OPEN_DOOR")
~GoTo(Room_SuitesHallway)
-> SuitesHallway

=== GoToParkingGarage ===
~GoTo(Room_ParkingGarage)
-> ParkingGarage

=== GoToPawnshop ===
~PlayerAnimation("Interact")
~Wait(1)
~Show(MainStreet_PawnshopDoorOpen)
~PlaySound("OPEN_DOOR")
~GoTo(Room_Pawnshop)
-> Pawnshop

=== GoToHospital ===
~PlayerAnimation("Interact")
~Wait(1)
~Show(MainStreet_HospitalDoorOpen)
~PlaySound("OPEN_DOOR")
~GoTo(Room_Hospital)
-> Hospital

=== GoToNoirGallery ===
~PlayerAnimation("Interact")
~Wait(1)
~PlaySound("OPEN_DOOR")
~GoTo(Room_NoirGallery)
-> NoirGallery

=== LookAtDog ===
~AddQuest(DogQuest)
Dog: BARK BARK BARK!
Player: There's a big St. Bernard blocking the entrance to the Santa Monica Palace.
-> MainStreet

=== LookAtDogUnleashed ===
Player: The big St. Bernard has no leash but he's still blocking the entrance to the Santa Monica Palace.
Dog: BARK BARK BARK!
-> MainStreet

=== LookAtDogSleeping ===
Player: The big St. Bernard is sleeping like a baby.
-> MainStreet

=== DontTouchDog ===
Dog: BARK BARK BARK!
Player: I'm not touching THAT.
-> MainStreet

=== LookAtLeash ===
Player: The dog is leashed to a streetlight. 
-> MainStreet

=== PickUpLeash ===
Player: The leash is tightly tied to the streetlight. I can't open it with my bare hands.
-> MainStreet

=== UseBigScissorsWithLeash ===
Player: Here we go. 
~DogIsUnleashed = 1
~Hide(MainStreet_DogLeash)
~RemoveFromInventory(BigScissors)
~UpdateQuest(DogQuest, DogQuest_1)
-> DogQuestUpdated

=== DogQuestUpdated ===
~DogIsGone = 1
~Hide(MainStreet_Dog)
~Hide(MainStreet_SantaMonicaPalace)
~Show(MainStreet_DoorToSantaMonicaPalace)
~Show(MainStreet_DogRunningAnimation)
->MainStreetDefault

=== LookAtFireHydrant ===
Player: The fire hydrant is broken.
-> MainStreet

=== UseFireHydrant ===
Player: I can't fix this with my bare hands.
-> MainStreet

=== UsePlumberBrosFlyerWithFireHydrant ===
Player: Maybe I could call the Plumber Bros to fix this fire hydrant, but first I'll need to find a phone.
-> MainStreet

=== LookAtJimmyJohnson ===
Player: A business man in a three-piece suit is waiting in the middle of the street.
-> MainStreet

=== LookAtHomelessDoug ===
Player: A homeless person and his cart full of garbage.
-> MainStreet

=== LookAtClaire ===
Player: A prostitute is waiting in the alley.
-> MainStreet

=== LookAtMrD ===
Player: A big doorman guards the entrance of this building.
-> MainStreet

=== LookAtPlumberBrosVan ===
Player: The Plumber Bros van. It has a big wrench logo on it.
-> MainStreet

=== PickUpPlumberBrosVan ===
Player: They're not going to miss a single wrench.
~AddToInventory(Wrench)
~UpdateQuest(BullockQuest, BullockQuest_1)
-> LookAtWrench -> MainStreet

=== UseTaxi ===
Player: I have the strong feeling that if I enter this taxi I will never come back to Santa Monica. 
-> UseTaxiChoice

=== UseTaxiChoice ===
#choice
* [ Leave Santa Monica forever. ] -> Ending
+ [ Wait, I have things to do in Santa Monica. ] -> MainStreet

=== Ending ===
Player: The game is over. Thanks for playing! #portrait:smile #end 
~Hide(MainStreet_Taxi)
~Show(MainStreet_TaxiLeavingAnimation)
-> DONE