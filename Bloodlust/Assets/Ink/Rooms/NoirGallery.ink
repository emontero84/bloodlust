
VAR NoirGalleryGuestsAreOut = 0
VAR NoirGalleryPaintingsSlashed = 0

=== NoirGallery ===
~Hide(NoirGallery_ExitDoorOpen)
* { not NoirGalleryFirstEncounter } -> NoirGalleryFirstEncounter
+ { NoirGalleryFirstEncounter } -> NoirGalleryDefault

=== NoirGalleryFirstEncounter ===
~UpdateQuest(JulietteQuest, JulietteQuest_1)
-> NoirGalleryDefault

=== NoirGalleryDefault ===
+ [ Use exit door ] -> GoToMainStreetFromNoirGallery
+ [ Look at catering ] -> LookAtCatering
* { HasInInventory(SleepingPills) } [ Use sleeping pills with catering ] -> UseSleepingPillsWithCatering
* { HasInInventory(Laxatives) } [ Use laxatives with catering ] -> UseLaxativesWithCatering
* { HasInInventory(RatPoison) } [ Use rat poison with catering ] -> UseRatPoisonWithCatering
+ [ Look at Guest 1 ] -> LookAtMiamiViceGuy
+ [ Look at Guest 2 ] -> LookAtMissYang
+ [ Look at Guest 3 ] -> LookAtCowboy
+ [ Look at Guest 4 ] -> LookAtBallerina
+ [ Look at Guest 5 ] -> LookAtShadow
+ [ Look at young butler ] -> LookAtYoungButler
+ [ Look at old butler ] -> LookAtOldButler
+ [ Talk to Guest 1 ] -> TalkToMiamiViceGuy
+ [ Talk to Guest 2 ] -> TalkToMissYang
+ [ Talk to Guest 3 ] -> TalkToCowboy
+ [ Talk to Guest 4 ] -> TalkToBallerina
+ [ Talk to Guest 5 ] -> TalkToShadow
+ [ Talk to young butler ] -> TalkToYoungButler
+ [ Talk to old butler ] -> TalkToOldButler
+ [ Look at painting 1 ] -> LookAtPainting1
+ [ Look at painting 2 ] -> LookAtPainting2
+ [ Look at painting 3 ] -> LookAtPainting3
+ [ Look at painting 4 ] -> LookAtPainting4
+ { NoirGalleryGuestsAreOut == 0 && HasInInventory(Knife) } [ Use knife with painting 1 ] -> CantSlashPainting
+ { NoirGalleryGuestsAreOut == 0 && HasInInventory(Knife) } [ Use knife with painting 2 ] -> CantSlashPainting
+ { NoirGalleryGuestsAreOut == 0 && HasInInventory(Knife) } [ Use knife with painting 3 ] -> CantSlashPainting
+ { NoirGalleryGuestsAreOut == 0 && HasInInventory(Knife) } [ Use knife with painting 4 ] -> CantSlashPainting
* { NoirGalleryGuestsAreOut == 1 && HasInInventory(Knife) } [ Use knife with painting 1 ] -> SlashPainting1
* { NoirGalleryGuestsAreOut == 1 && HasInInventory(Knife) } [ Use knife with painting 2 ] -> SlashPainting2
* { NoirGalleryGuestsAreOut == 1 && HasInInventory(Knife) } [ Use knife with painting 3 ] -> SlashPainting3
* { NoirGalleryGuestsAreOut == 1 && HasInInventory(Knife) } [ Use knife with painting 4 ] -> SlashPainting4
+ [ Look at charity box ] -> LookAtCharityBox -> NoirGallery
+ { NoirGalleryGuestsAreOut == 0 } [ Pick up charity box ] -> CantPickUpCharityBox
* { NoirGalleryGuestsAreOut == 1 } [ Pick up charity box ] -> PickUpCharityBox
+ [ Look at PC ] -> LookAtNoirGalleryPC
+ [ Use PC ] -> UseNoirGalleryPc
+ [ Look at post-it ] -> LookAtNoirGalleryPostIt
+ [ Look at pile of books ] -> LookAtPileOfBooks
* [ Use top drawer ] -> OpenNoirGalleryTopDrawer
* [ Use bottom drawer ] -> OpenNoirGalleryBottomDrawer
+ [ Look at inventory items ] -> LookAtInventoryItems -> NoirGallery

=== GoToMainStreetFromNoirGallery ===
~PlayerAnimation("Interact")
~Wait(1)
~Show(NoirGallery_ExitDoorOpen)
~PlaySound("OPEN_DOOR")
~GoTo(Room_MainStreet)
-> MainStreet

=== LookAtCatering ===
Player: The catering for the event includes croquettes and cocktails.
-> NoirGallery

=== UseSleepingPillsWithCatering ===
~RemoveFromInventory(SleepingPills)
-> PoisonCatering

=== UseLaxativesWithCatering ===
~RemoveFromInventory(Laxatives)
-> PoisonCatering

=== UseRatPoisonWithCatering ===
~RemoveFromInventory(RatPoison)
-> PoisonCatering

=== PoisonCatering ===
Player: A bit of poison in the catering should be enough to get rid of the guests... #portrait:smile
~Show(NoirGallery_GuestsLeavingAnimation)
~Hide(NoirGallery_Guest1)
~Hide(NoirGallery_Guest2)
~Hide(NoirGallery_Guest3)
~Hide(NoirGallery_Guest4)
~Hide(NoirGallery_Guest5)
~Hide(NoirGallery_OldButler)
~Hide(NoirGallery_YoungButler)
~NoirGalleryGuestsAreOut = 1
~UpdateQuest(JulietteQuest, JulietteQuest_2)
-> NoirGallery

=== LookAtMiamiViceGuy ===
Player: A guy dressed like a character from Miami Vice.
-> NoirGallery

=== LookAtMissYang ===
Player: An elegant business woman in a turquoise dress.
-> NoirGallery

=== LookAtCowboy ===
Player: A fat business man dressed like a cowboy.
-> NoirGallery

=== LookAtBallerina ===
Player: A beautiful and athletic woman dressed like a ballerina.
-> NoirGallery

=== LookAtShadow ===
Player: A shadowy person dressed in a dark trenchcoat.
-> NoirGallery

=== LookAtYoungButler ===
Player: A young butler carrying a silver platter.
-> NoirGallery

=== LookAtOldButler ===
Player: An old butler carrying a silver platter.
-> NoirGallery

=== LookAtPainting1 ===
Player: "Adam's first wife".
-> NoirGallery

=== LookAtPainting2 ===
Player: "Lilith left Adam after she refused to become subservient to him".
-> NoirGallery

=== LookAtPainting3 ===
Player: "Lilith is expelled from the Garden of Eden and becomes a creature of the night".
-> NoirGallery

=== LookAtPainting4 ===
Player: "Lilith spawns the first children of the night".
-> NoirGallery

=== CantSlashPainting ===
Player: There's a lot of witnesses around. I'd better get rid of the guests first.
-> NoirGallery

=== SlashPainting1 ===
~PlaySound("SLASH")
~Show(NoirGallery_Painting1Slash)
~NoirGalleryPaintingsSlashed++
-> CheckAllPaintingsAreSlashed

=== SlashPainting2 ===
~PlaySound("SLASH")
~Show(NoirGallery_Painting2Slash)
~NoirGalleryPaintingsSlashed++
-> CheckAllPaintingsAreSlashed

=== SlashPainting3 ===
~PlaySound("SLASH")
~Show(NoirGallery_Painting3Slash)
~NoirGalleryPaintingsSlashed++
-> CheckAllPaintingsAreSlashed

=== SlashPainting4 ===
~PlaySound("SLASH")
~Show(NoirGallery_Painting4Slash)
~NoirGalleryPaintingsSlashed++
-> CheckAllPaintingsAreSlashed

=== CheckAllPaintingsAreSlashed ===
{ 
	- NoirGalleryPaintingsSlashed == 4 : -> AllPaintingsAreSlashed
	- NoirGalleryPaintingsSlashed < 4 : -> NoirGallery
}

=== AllPaintingsAreSlashed ===
~RemoveFromInventory(Knife)
~UpdateQuest(JulietteQuest, JulietteQuest_3)
-> NoirGallery

=== CantPickUpCharityBox ===
Player: There's a lot of witnesses around. I'd better get rid of the guests first.
-> NoirGallery

=== PickUpCharityBox ===
~Hide(NoirGallery_CharityBox)
~AddToInventory(CharityBox)
-> LookAtCharityBox -> NoirGallery

=== LookAtNoirGalleryPC ===
Player: A powerful computer.
-> NoirGallery

=== LookAtNoirGalleryPostIt ===
Player: There's a post-it with the word Lilith scratched.
-> NoirGallery

=== LookAtPileOfBooks ===
Player: All these books are somewhat related to the figure of Lilith in ancient manuscripts.
-> NoirGallery

=== OpenNoirGalleryTopDrawer ===
~Hide(NoirGallery_TopDrawer)
~Show(NoirGallery_TopDrawerOpen)
~PlaySound("OPEN_CONTAINER")
-> NoirGallery

=== OpenNoirGalleryBottomDrawer ===
~Hide(NoirGallery_BottomDrawer)
~Show(NoirGallery_BottomDrawerOpen)
~PlaySound("OPEN_CONTAINER")
Player: There's a book in here that seems rather important.
~AddToInventory(SkillBook6)
-> LookAtSkillBook6 -> NoirGallery