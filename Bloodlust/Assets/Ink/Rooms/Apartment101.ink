
VAR NoteRead = 0

=== Apartment101 ===
~Hide(Apartment101_DoorOpen)
* { not InitApartment101 } -> InitApartment101
+ { InitApartment101 } -> Apartment101Default

=== InitApartment101 ===
-> Apartment101Default

=== Apartment101Default ===
+ { NoteRead == 1 } [ Use door ] -> GoToLodgingHallway
+ { NoteRead == 0 } [ Use door ] -> CantGoToLodgingHallway
+ [ Look at Jack ] -> LookAtJack
+ [ Talk to Jack ] -> TalkToJack
+ [ Look at note ] -> LookAtApartment101Note
+ [ Look at bottle of wine ] -> LookAtBottleOfWine
+ [ Look at money clip ] -> LookAtMoneyClip -> Apartment101
* [ Pick up money clip ] -> PickUpMoneyClip
+ [ Look at laptop ] -> LookAtApartment101Laptop
+ [ Use laptop ] -> UseApartment101Laptop
* [ Use top drawer ] -> OpenApartment101TopDrawer
* [ Use bottom drawer ] -> OpenApartment101BottomDrawer
+ [ Look at TV ] -> LookAtApartment101TV
+ [ Use TV ] -> UseApartment101TV
+ [ Look at stereo ] -> LookAtApartment101Stereo
+ [ Use stereo ] -> UseApartment101Stereo
* [ Use kitchen cabinet ] -> OpenApartment101KitchenCabinet
+ [ Look at plunger ] -> LookAtPlunger
+ [ Look at bleach ] -> LookAtBleach -> Apartment101
+ [ Pick up bleach ] -> PickUpBleach
+ [ Look at cheap watch ] -> LookAtCheapWatch -> Apartment101
* [ Pick up cheap watch ] -> PickUpCheapWatch
+ [ Look at bed ] -> LookAtApartment101Bed
+ [ Use bed ] -> UseApartment101Bed
* [ Use fridge ] -> OpenApartment101Fridge
+ [ Look at blood bag ] -> LookAtBloodBag -> Apartment101
+ [ Use blood bag ] -> UseBloodBag
* [ Pick up blood bag ] -> PickUpBloodBag
+ [ Look at empty painkillers ] -> LookAtEmptyPainkillers -> Apartment101
* [ Pick up empty painkillers ] -> PickUpEmptyPainkillers
* [ Pick up pillow ] -> PickUpPillow
+ [ Look at inventory items ] -> LookAtInventoryItems -> Apartment101

=== GoToLodgingHallway ===
~PlayerAnimation("Interact")
~Wait(1)
~Show(Apartment101_DoorOpen)
~PlaySound("OPEN_DOOR")
~GoTo(Room_LodgingHallway)
-> LodgingHallway

=== CantGoToLodgingHallway ===
Jack: I'm sorry, kiddo, but I can't let you out until you've read the note on your desk. 
Jack: De la Croix was quite specific about that, so... just give it a look, OK?
-> Apartment101

=== LookAtJack ===
Player: A vampire is guarding the door of my apartment.
-> Apartment101

=== LookAtBottleOfWine ===
Player: A cheap bottle of red wine.
-> Apartment101

=== LookAtApartment101Note ===
Player: Somebody left a note for me on the desk. How romantic. #portrait:confused
Player: "Welcome to the city of Los Angeles. I hope you find your new accommodation suitable to your humble position in Santa Monica".
Player: "Head to the suites in Santa Monica Palace and meet my agent Mercurius. He will provide you with the details of your first mission".
Player: "Yours sincerily, Sebastien De la Croix".
~NoteRead = 1
~UpdateQuest(FirstSteps, FirstSteps_2)
-> Apartment101

=== PickUpMoneyClip ===
~AddToInventory(MoneyClip)
~Hide(Apartment101_MoneyClip)
-> LookAtMoneyClip -> Apartment101

=== LookAtApartment101Laptop ===
Player: A splendid next-generation laptop computer with a blasting 80 MegaBytes of RAM.
-> Apartment101

=== OpenApartment101TopDrawer ===
~Hide(Apartment101_TopDrawer)
~Show(Apartment101_TopDrawerOpen)
~AddToInventory(Screwdriver)
Player: A screwdriver. It might become handy.
-> Apartment101

=== OpenApartment101BottomDrawer ===
~Hide(Apartment101_BottomDrawer)
~Show(Apartment101_BottomDrawerOpen)
~AddToInventory(Lockpick)
-> LookAtLockpick -> Apartment101

=== LookAtApartment101TV ===
Player: A rather old television. It's turned off.
-> Apartment101

=== UseApartment101TV ===
Player: Let's see...
~Show(Apartment101_TVTurnedOn)
TV Reporter: ... The slaughter in Santa Monica pier continues to baffle the police investigators of Los Angeles.
TV Reporter: As stated by the head of the Police Department, it's still too early to discard any explanation. 
TV Reporter: We will continue to inform about these and other pressing affairs after the break ...
Player: Enough.
~Hide(Apartment101_TVTurnedOn)
-> Apartment101

=== LookAtApartment101Stereo ===
Player: A fancy stereo with dual cassette and a radio. It's turned off.
-> Apartment101

=== UseApartment101Stereo ===
Player: Let's see...
~Show(Apartment101_StereoTurnedOn)
Radio Broadcast: ... Stuck in a difficult situation? Kilpatrick's Bail Bond can put your feet back on the street! Call now 988-BAIL-OUT ... 
-> Apartment101

=== OpenApartment101KitchenCabinet ===
~Hide(Apartment101_KitchenCabinet)
~Show(Apartment101_KitchenCabinetOpen)
~Show(Apartment101_Bleach)
~PlaySound("OPEN_CONTAINER")
-> Apartment101

=== LookAtPlunger ===
Player: It's a plunger.
-> Apartment101

=== PickUpBleach ===
~AddToInventory(Bleach)
~Hide(Apartment101_Bleach)
-> LookAtBleach -> Apartment101

=== PickUpCheapWatch ===
~AddToInventory(CheapWatch)
~Hide(Apartment101_CheapWatch)
-> LookAtCheapWatch -> Apartment101

=== LookAtApartment101Bed ===
Player: This is the filthiest motel bed I've ever seen in my life. #portrait:confused
-> Apartment101

=== UseApartment101Bed ===
Player: I don't need to rest yet.
-> Apartment101

=== OpenApartment101Fridge ===
~Hide(Apartment101_Fridge)
~Show(Apartment101_FridgeOpen)
~Show(Apartment101_BloodBag)
~PlaySound("OPEN_CONTAINER")
-> Apartment101

=== UseBloodBag ===
Player: Mmmm! Looks tasty, but I'm not hungry. #portrait:blood
-> Apartment101

=== PickUpBloodBag ===
~AddToInventory(BloodBag)
~Hide(Apartment101_BloodBag)
-> LookAtBloodBag -> Apartment101

=== PickUpEmptyPainkillers ===
~AddToInventory(EmptyPainkillers)
~Hide(Apartment101_EmptyPainkillers)
-> LookAtEmptyPainkillers -> Apartment101

=== PickUpPillow ===
Player: There's something below the pillow.
~AddToInventory(BrokenTvRemote)
~Hide(Apartment101_Pillow)
-> Apartment101
