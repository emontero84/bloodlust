
VAR IsCarltonsSafeOpen = 0

=== SuitesCarlton ===
* { not SuitesCarltonFirstEncounter } -> SuitesCarltonFirstEncounter
+ { SuitesCarltonFirstEncounter } -> SuitesCarltonDefault

=== SuitesCarltonFirstEncounter ===
-> SuitesCarltonDefault

=== SuitesCarltonDefault ===
+ [ Use exit door ] -> GoToSuitesHallwayFromSuitesCarlton
+ [ Look at old picture ] -> LookAtSuitesCarltonOldPicture
* [ Use old picture ] -> PickUpSuitesCarltonOldPicture
+ [ Look at safe ] -> LookAtSuitesCarltonSafe
+ { not ReadPrivateFilesInCarltonLaptop && not SkillLockpicking} [ Use safe ] -> CantUseSuitesCarltonSafe
* { SkillLockpicking } [ Use safe ] -> LockpickSafe
* { ReadPrivateFilesInCarltonLaptop && not SkillLockpicking } [ Use safe ] -> UseSuitesCarltonSafe
* { HasInInventory(Stethoscope) } [ Use stethoscope with safe ] -> UseStethoscopeWithSafe
+ [ Look at CD-ROM ] -> LookAtCdRom -> SuitesCarlton
* [ Pick up CD-ROM ] -> PickUpCdRom
+ [ Look at Carlton's note ] -> LookAtCarltonsNote -> SuitesCarlton
* [ Pick up Carlton's note ] -> PickUpCarltonsNote
+ [ Look at laptop ] -> LookAtSuitesCarltonLaptop
+ [ Use laptop ] -> UseSuitesCarltonLaptop
+ [ Look at Carlton's body ] -> LookAtCarltonsBody
+ [ Look at katana ] -> LookAtKatanaInSofa
* [ Pick up katana ] -> PickUpKatana
+ { not SkillKick } [ Use chest ] -> CantOpenSuitesCarltonChest
* { SkillKick } [ Use chest ] -> KickSuitesCarltonChest
* { HasInInventory(Crowbar) } [ Use crowbar with chest ] -> UseCrowbarWithChest
* [ Use cabinet ] -> OpenSuitesCarltonCabinet
* [ Use mini-bar ] -> OpenSuitesCarltonMiniBar
+ [ Look at Carlton's diary ] -> LookAtCarltonsDiary
+ [ Look at pile of books ] -> LookAtSuitesCarltonPileOfBooks
+ [ Look at inventory items ] -> LookAtInventoryItems -> SuitesCarlton

=== GoToSuitesHallwayFromSuitesCarlton ===
~PlaySound("OPEN_DOOR")
~GoTo(Room_SuitesHallway)
-> SuitesHallway

=== LookAtSuitesCarltonOldPicture ===
Player: There's a poor replica of the famous painting of a girl with a pearl earring by Johannes Vermeer.
-> SuitesCarlton

=== PickUpSuitesCarltonOldPicture ===
Player: The painting moves.
~Hide(SuitesCarlton_OldPicture)
~Show(SuitesCarlton_Safe)
-> SuitesCarlton

=== LookAtSuitesCarltonSafe ===
Player: A metal safe with a turning dial.
-> SuitesCarlton

=== CantUseSuitesCarltonSafe ===
Player: I don't know the code. I can't open it.
-> SuitesCarlton

=== UseSuitesCarltonSafe ===
Player: Let me see... here's a click...
Player: Let me see... here's another click...
Player: And... Bingo!
-> OpenSuitesCarltonSafe

=== UseStethoscopeWithSafe ===
Player: Let me see... here's a click...
Player: Let me see... here's another click...
Player: And... Bingo!
~RemoveFromInventory(Stethoscope)
-> OpenSuitesCarltonSafe

=== LockpickSafe ===
Player: Let me see... here's a click...
Player: Let me see... here's another click...
Player: And... Bingo!
-> LockpickingSuccessful -> OpenSuitesCarltonSafe

=== OpenSuitesCarltonSafe ===
~Hide(SuitesCarlton_Safe)
~Show(SuitesCarlton_SafeOpen)
~Show(SuitesCarlton_CdRom)
~Show(SuitesCarlton_CarltonsNote)
~PlaySound("OPEN_CONTAINER")
-> SuitesCarlton

=== PickUpCdRom ===
~Hide(SuitesCarlton_CdRom)
~AddToInventory(CdRom)
~UpdateQuest(BernardThungQuest, BernardThungQuest_1)
-> LookAtCdRom -> SuitesCarlton

=== PickUpCarltonsNote ===
~Hide(SuitesCarlton_CarltonsNote)
~AddToInventory(CarltonsNote)
~UpdateQuest(ArthurKillpatrickQuest, ArthurKillpatrickQuest_2)
-> LookAtCarltonsNote -> SuitesCarlton

=== LookAtSuitesCarltonLaptop ===
Player: A laptop, stained with Carlton's blood.
-> SuitesCarlton

=== LookAtCarltonsBody ===
Player: The poor guy had a terrible death. The body has several severe injuries and there's blood everywhere.
~UpdateQuest(ArthurKillpatrickQuest, ArthurKillpatrickQuest_1)
-> SuitesCarlton

=== LookAtKatanaInSofa ===
Player: There's a katana stuck in the sofa.
-> SuitesCarlton

=== PickUpKatana ===
~Hide(SuitesCarlton_Katana)
~AddToInventory(Katana)
-> LookAtKatana -> SuitesCarlton

=== CantOpenSuitesCarltonChest ===
Player: The chest is wide shut.
-> SuitesCarlton

=== UseCrowbarWithChest ===
Player: Volià!
-> OpenSuitesCarltonChest

=== KickSuitesCarltonChest ===
-> KickSuccessful -> OpenSuitesCarltonChest

=== OpenSuitesCarltonChest ===
~Hide(SuitesCarlton_Chest)
~Show(SuitesCarlton_ChestOpen)
~PlaySound("OPEN_CONTAINER")
Player: Its empty. How disappointing. #portrait:sad
-> SuitesCarlton

=== OpenSuitesCarltonCabinet ===
~Hide(SuitesCarlton_Cabinet)
~Show(SuitesCarlton_CabinetOpen)
~PlaySound("OPEN_CONTAINER")
Player: Inside the cabinet there's only books of Sherlock Holmes. Maybe Carlton was a bit obsessed with fictional investigators.
-> SuitesCarlton

=== OpenSuitesCarltonMiniBar ===
~Hide(SuitesCarlton_MiniBar)
~Show(SuitesCarlton_MiniBarOpen)
~Show(SuitesCarlton_CarltonsDiary)
~PlaySound("OPEN_CONTAINER")
-> SuitesCarlton

=== LookAtCarltonsDiary ===
Player: There's a diary that seems to belong to Carlton. 
Player: There's nothing about the course of his investigations. It's mainly all personal stuff. 
Player: The number 65 is written all over the place. Maybe it was his favorite number or his birthday.
-> SuitesCarlton

=== LookAtSuitesCarltonPileOfBooks ===
Player: There's a book in here that seems rather important.
~AddToInventory(SkillBook7)
-> LookAtSkillBook7 -> SuitesCarlton