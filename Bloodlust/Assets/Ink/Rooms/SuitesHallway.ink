
VAR BloodStainsCleaned = 0
VAR CarltonSuiteOpen = 0

=== SuitesHallway ===
~Hide(SuitesHallway_ExitDoorOpen)
~Hide(SuitesHallway_MercuriusDoorOpen)
* { not SuitesHallwayInit } -> SuitesHallwayInit
+ { SuitesHallwayInit } -> SuitesHallwayDefault

=== SuitesHallwayInit ===
~UpdateQuest(FirstSteps, FirstSteps_3)
-> SuitesHallwayDefault

=== SuitesHallwayDefault ===
+ [ Use exit door ] -> GoToMainStreetFromSuitesHallway
+ [ Use door to suite 2 ] -> GoToSuitesMercurius
+ { CarltonSuiteOpen == 0 } [ Use door to suite 1 ] -> CantGoToSuitesCarlton
+ { CarltonSuiteOpen == 1 } [ Use door to suite 1 ] -> GoToSuitesCarlton
+ [ Look at Roberta ] -> LookAtRoberta
+ [ Talk to Roberta ] -> TalkToRoberta
* { RobertaHerself && HasInInventory(Bleach) } [ Give bleach to Roberta ] -> GiveBleachToRoberta
+ { BloodStainsCleaned == 0 } [ Look at blood stains ] -> LookAtBloodStains
+ [ Look at painting ] -> LookAtPainting
+ [ Look at inventory items ] -> LookAtInventoryItems -> SuitesHallway

=== GoToMainStreetFromSuitesHallway ===
~PlayerAnimation("Interact")
~Wait(1)
~Show(SuitesHallway_ExitDoorOpen)
~PlaySound("OPEN_DOOR")
~GoTo(Room_MainStreet)
-> MainStreet

=== GoToSuitesMercurius ===
~PlayerAnimation("Interact")
~Wait(1)
~Show(SuitesHallway_MercuriusDoorOpen)
~PlaySound("OPEN_DOOR")
~GoTo(Room_SuitesMercurius)
-> SuitesMercurius

=== CantGoToSuitesCarlton ===
Player: It's locked.
-> SuitesHallway

=== GoToSuitesCarlton ===
~PlayerAnimation("Interact")
~Wait(1)
~Show(SuitesHallway_CarltonDoorOpen)
~PlaySound("OPEN_DOOR")
~GoTo(Room_SuitesCarlton)
-> SuitesCarlton

=== LookAtBloodStains ===
Player: Someone has left a fresh trail of blood on the floor.
-> SuitesHallway

=== LookAtPainting ===
Player: A very disturbing painting.
-> SuitesHallway

=== LookAtRoberta ===
Player: {not TalkToRoberta:A woman in uniform}{TalkToRoberta:Roberta} is cleaning the floor.
-> SuitesHallway