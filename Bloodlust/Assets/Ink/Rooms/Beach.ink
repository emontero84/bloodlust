
VAR BeachGateOpen = 0

=== Beach ===
* { not InitBeach } -> InitBeach
+ { InitBeach } -> BeachDefault

=== InitBeach ===
~Hide(Beach_DoorToBeachHouse)
~Hide(Beach_MetalGateOpen)
~Show(Beach_MetalGate)
-> BeachDefault

=== BeachDefault ===
+ [ Walk to parking garage ] -> GoToParkingGarage
+ { BeachGateOpen == 1 } [ Use stairs to beach house ] -> GoToBeachHouseExterior
+ { BeachGateOpen == 0 } [ Use stairs to beach house ] -> CantGoToBeachHouseExterior
+ [ Look at Rose ] -> LookAtRose
+ [ Talk to Rose ] -> TalkToRose
+ [ Look at Cooper ] -> LookAtCooper
+ [ Talk to Cooper ] -> TalkToCooper
* { HasQuest(CooperQuest) && HasInInventory(PhilosophyBook1) } [ Give philosophy book 1 to Cooper ] -> GivePhilosophyBookToCooper1
* { HasQuest(CooperQuest) && HasInInventory(PhilosophyBook2) } [ Give philosophy book 2 to Cooper ] -> GivePhilosophyBookToCooper2
* { HasQuest(CooperQuest) && HasInInventory(PhilosophyBook3) } [ Give philosophy book 3 to Cooper ] -> GivePhilosophyBookToCooper3
+ [ Look at Jules ] -> LookAtJules
+ [ Talk to Jules ] -> TalkToJules
* { HasQuest(JulesQuest) && HasInInventory(BloodBag) } [ Give blood bag to Jules ] -> GiveBloodBagToJules
* { HasQuest(JulesQuest) && HasInInventory(Stake) } [ Give stake to Jules ] -> GiveStakeToJules
+ [ Look at Elliot ] -> LookAtElliot
+ [ Talk to Elliot ] -> TalkToElliot
* { HasQuest(ElliotQuest) && HasInInventory(LillysKey) } [ Give Lilly's key to Elliot ] -> ElliotLillysCar
* { HasQuest(ElliotQuest) && HasInInventory(LillysPhoto) } [ Give Lilly's photo to Elliot ] -> ElliotLillysPhoto
* { HasQuest(ElliotQuest) && HasInInventory(LillysDiary) } [ Give Lilly's diary to Elliot ] -> GiveLillysDiaryToElliot
+ [ Look at frisbee ] -> LookAtFrisbee -> Beach
+ [ Pick up frisbee ] -> PickUpFrisbee
+ [ Look at crime scene ] -> LookAtCrimeScene
+ [ Look at stereo ] -> LookAtBeachStereo
+ { not HasQuest(AstroliteQuest) } [ Look at metal gate ] -> LookAtMetalGateNoQuest
+ { HasQuest(AstroliteQuest) } [ Look at metal gate ] -> LookAtMetalGate
* { HasQuest(AstroliteQuest) && SkillKick } [ Use metal gate ] -> KickMetalGate
* { HasQuest(AstroliteQuest) && SkillLockpicking } [ Use metal gate ] -> LockpickMetalGate
+ { not HasQuest(AstroliteQuest) } [ Use metal gate ] -> LookAtMetalGateNoQuest
+ { HasQuest(AstroliteQuest) && not SkillKick && not SkillLockpicking } [ Use metal gate ] -> CantGoToBeachHouseExterior
* { HasQuest(AstroliteQuest) && HasInInventory(Lockpick) } [ Use lockpick with metal gate ] -> UseLockpickOnMetalGate
+ [ Look at inventory items ] -> LookAtInventoryItems -> Beach

=== GoToBeachHouseExterior ===
~GoTo(Room_BeachHouseExterior)
-> BeachHouseExterior

=== CantGoToBeachHouseExterior ===
Player: The metal gate is closed. I can't go any further.
-> Beach

=== PickUpFrisbee ===
~AddToInventory(Frisbee)
~Hide(Beach_Frisbee)
-> LookAtFrisbee -> Beach

=== LookAtMetalGateNoQuest ===
Player: A metal gate blocking the access to the some private property up there in the mountains.
-> Beach

=== LookAtMetalGate ===
Player: This metal gate blocks the access to the beach house up in the mountains.
-> Beach

=== UseLockpickOnMetalGate ===
~RemoveFromInventory(Lockpick)
-> LockpickMetalGate

=== LockpickMetalGate ===
Player: Let me put my lockpicking skills to the test... #portrait:smile
Player: Let's see...
~Wait(2)
-> LockpickingSuccessful -> OpenMetalGate

=== KickMetalGate ===
-> KickSuccessful -> OpenMetalGate

=== OpenMetalGate ===
~BeachGateOpen = 1
~Hide(Beach_MetalGate)
~Show(Beach_MetalGateOpen)
~Show(Beach_DoorToBeachHouse)
-> Beach

=== LookAtCrimeScene ===
Player: From here all I can see is a dead body torn into pieces.
-> Beach

=== LookAtRose ===
Player: A woman stands on the sand with an lifeless gaze into infinity.
-> Beach

=== LookAtCooper ===
Player: A troubled man is sitting near the fire.
-> Beach

=== LookAtJules ===
Player: A frightened man is sitting near the fire.
-> Beach

=== LookAtElliot ===
Player: A man naked from the waist up. 
-> Beach

=== LookAtBeachStereo ===
Player: They have a stereo playing some music.
-> Beach