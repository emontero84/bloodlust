
VAR Apartment102DoorIsOpen = 0

=== LodgingHallway ===
~Hide(LodgingHallway_Apartment101DoorOpen)
~Hide(LodgingHallway_Apartment102DoorOpen)
~Hide(LodgingHallway_ExitDoorOpen)

* { not InitLodgingHallway } -> InitLodgingHallway
+ { InitLodgingHallway } -> LodgingHallwayDefault

=== InitLodgingHallway ===
-> LodgingHallwayDefault

=== LodgingHallwayDefault ===
+ [ Use door to apartment 101 ] -> GoToApartment101
+ { Apartment102DoorIsOpen == 0 } [ Use door to apartment 102 ] -> CantGoToApartment102
+ { Apartment102DoorIsOpen == 1 } [ Use door to apartment 102 ] -> GoToApartment102
+ [ Use exit door ] -> GoToMainStreetFromLodgingHall
+ [ Look at newspaper ] -> LookAtNewspaper -> LodgingHallway
+ [ Pick up newspaper ] -> PickUpNewspaper
+ [ Look at officer Chunk ] -> LookAtChunk
+ [ Look at officer Carter ] -> LookAtCarter
+ [ Talk to officer Chunk ] -> TalkToChunk
+ [ Talk to officer Carter ] -> TalkToCarter
+ [ Look at plant ] -> LookAtPlant
+ [ Look at light bulb ] -> LookAtLightBulb
+ [ Look at mail boxes ] -> LookAtMailBoxes
+ [ Use mail boxes ] -> LookAtMailBoxes
* { CarterTired && HasInInventory(EmptyCoffeeThermos) } [ Give empty coffee thermos to officer Carter ] -> GiveEmptyCoffeThermosToOfficerCarter
* { CarterTired && HasInInventory(CoffeeThermos) } [ Give coffee thermos to officer Carter ] -> GiveCoffeThermosToOfficerCarter
* { ChunkSomethingToEat && HasInInventory(BoxOfDonuts) } [ Give box of donuts to officer Chunk ] -> GiveBoxOfDonutsToOfficerChunk
+ [ Look at inventory items ] -> LookAtInventoryItems -> LodgingHallway

=== GoToApartment101 ===
~PlayerAnimation("Interact")
~Wait(1)
~Show(LodgingHallway_Apartment101DoorOpen)
~PlaySound("OPEN_DOOR")
~GoTo(Room_Apartment101)
-> Apartment101

=== GoToApartment102 ===
~PlayerAnimation("Interact")
~Wait(1)
~Show(LodgingHallway_Apartment102DoorOpen)
~PlaySound("OPEN_DOOR")
~GoTo(Room_Apartment102)
-> Apartment102

=== GoToMainStreetFromLodgingHall ===
~PlayerAnimation("Interact")
~Wait(1)
~Show(LodgingHallway_ExitDoorOpen)
~PlaySound("OPEN_DOOR")
-> GoToMainStreet

=== CantGoToApartment102 ===
Player: The door to the apartment 102 is heavily guarded by the two police officers.
-> LodgingHallway

=== PickUpNewspaper ===
~AddToInventory(Newspaper)
~Hide(LodgingHallway_Newspaper)
-> LookAtNewspaper -> LodgingHallway

=== LookAtChunk ===
Player: An overweighted police officer guarding the door to apartment 102.
-> LodgingHallway

=== LookAtCarter ===
Player: A hard-boiled police officer guarding the door to apartment 102.
-> LodgingHallway

=== LookAtPlant ===
Player: A very sad decoration.
-> LodgingHallway

=== LookAtLightBulb ===
Player: I don't want to go blind.
-> LodgingHallway

=== CheckBothOfficersAreHappy ===
{ 
	- CoffeThermosGiven == 1 && BoxOfDonutsGiven == 1 : -> BothOfficersAreHappy
	- CoffeThermosGiven == 0 || BoxOfDonutsGiven == 0 : -> LodgingHallway
}

=== BothOfficersAreHappy ===
Officer Chunk: Maybe we started with the wrong foot. #portrait:smile
Officer Carter: Yeah, you're doing a great service to the city. #portrait:smile
~Apartment102DoorIsOpen = 1
~Hide(LodgingHallway_PoliceLine)
-> LodgingHallway

=== LookAtMailBoxes ===
Player: A set of mail boxes. Mine is empty.
-> LodgingHallway