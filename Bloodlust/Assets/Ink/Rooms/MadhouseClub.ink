VAR IsMadhouseClubOfficeOpen = 0

=== MadhouseClub ===
~Hide(MadhouseClub_ExitDoorOpen)
~Show(MadhouseClub_ElevatorDoorsClosed)
~Hide(MadhouseClub_ElevatorDoorsOpen)
* { not MadhouseClubFirstEncounter } -> MadhouseClubFirstEncounter
+ { MadhouseClubFirstEncounter } -> MadhouseClubDefault

=== MadhouseClubFirstEncounter ===
-> MadhouseClubDefault

=== MadhouseClubDefault ===
+ [ Use exit door ] -> GoToSecondStreetFromMadhouseClub
+ { IsMadhouseClubOfficeOpen == 1 } [ Use elevator doors ] -> GoToElevatorFromMadhouseClub
+ [ Look at Barman ] -> LookAtBarman
+ [ Talk to Barman ] -> TalkToBarman
* { BarmanAskForVodkaBottle && not BarmanVodkaBottle && HasInInventory(MoneyClip) } [ Give money clip to Barman ] -> UseMoneyClipWithBarman
* { HasQuest(BarmanQuest) && HasInInventory(VialOfBlood) } [ Give vial of blood to Barman ] -> UseVialOfBloodWithBarman
+ [ Look at old James ] -> LookAtOldCop
+ [ Talk to old James ] -> TalkToOldCop
+ [ Look at metal girl ] -> LookAtMetalGirl
+ [ Talk to metal girl ] -> TalkToMetalGirl
+ [ Look at metal boy ] -> LookAtMetalBoy
+ [ Talk to metal boy ] -> TalkToMetalBoy
+ [ Look at goth boy ] -> LookAtGothBoy
+ [ Talk to goth boy ] -> TalkToGothBoy
+ [ Look at rich girl ] -> LookAtRichGirl
+ [ Talk to rich girl ] -> TalkToRichGirl
+ [ Look at goth girl ] -> LookAtGothGirl
+ [ Talk to goth girl ] -> TalkToGothGirl
+ [ Look at punk boy ] -> LookAtPunkBoy
+ [ Talk to punk boy ] -> TalkToPunkBoy
+ [ Look at kissing couple ] -> LookAtKissingCouple
+ [ Look at dance pit ] -> LookAtDancePit
+ [ Look at vodka bottle ] -> LookAtMadhouseVodkaBottle
+ { not SkillKick } [ Look at arcade ] -> LookAtArcade
* { SkillKick } [ Look at arcade ] -> KickArcade
* { HasInInventory(Crowbar) } [ Use crowbar with arcade ] -> OpenArcade
+ [ Look at pile of coins ] -> LookAtPileOfCoins -> MadhouseClub
* [ Pick up pile of coins ] -> PickUpPileOfCoins
* [ Pick up book ] -> PickUpMadhouseClubSkillBook
+ [ Look at inventory items ] -> LookAtInventoryItems -> MadhouseClub

=== GoToSecondStreetFromMadhouseClub ===
~PlayerAnimation("Interact")
~Wait(1)
~Show(MadhouseClub_ExitDoorOpen)
~PlaySound("OPEN_DOOR")
~GoTo(Room_SecondStreet)
-> SecondStreet

=== GoToElevatorFromMadhouseClub ===
~PlayerAnimation("Interact")
~Wait(1)
~Hide(MadhouseClub_ElevatorDoorsClosed)
~Show(MadhouseClub_ElevatorDoorsOpen)
~PlaySound("ELEVATOR_DOOR")
~GoTo(Room_Elevator)
-> Elevator

=== LookAtBarman ===
Player: A fat man waits behind the bar.
-> MadhouseClub

=== LookAtOldCop ===
Player: A hard-boiled man is standing near a glass of whisky.
-> MadhouseClub

=== LookAtMetalBoy ===
Player: A young metalhead sitting at the bar.
-> MadhouseClub

=== TalkToMetalBoy ===
Player: He's so into headbanging I'd better not disturb him.
-> MadhouseClub

=== LookAtMetalGirl ===
Player: A young metalhead with lots of tattoos.
-> MadhouseClub

=== LookAtGothBoy ===
Player: A young gothic boy with a pretty nice leather jacket.
-> MadhouseClub

=== TalkToGothBoy ===
Player: He's paying attention somewhere else. Better leave him be. 
-> MadhouseClub

=== LookAtRichGirl ===
Player: A young girl with too many jewels to be in this club.
-> MadhouseClub

=== LookAtGothGirl ===
Player: A young gothic girl with leather boots and a corsage.
-> MadhouseClub

=== LookAtPunkBoy ===
Player: A young punk with combat boots and lots of attitude.
-> MadhouseClub

=== TalkToPunkBoy ===
Player: He's doing some kind of ritual dance. I don't want to interrupt.
-> MadhouseClub

=== LookAtKissingCouple ===
Player: Are they even breathing?
-> MadhouseClub

=== LookAtDancePit ===
Player: The people in the dance pit are having a blast.
-> MadhouseClub

=== LookAtMadhouseVodkaBottle ===
Player: There's a fine bottle of vodka behind the counter. Maybe I should ask the barman about it.
-> MadhouseClub

=== LookAtArcade ===
Player: An arcade cabinet with the 8-bit classic "Alien Invaders".
-> MadhouseClub

=== OpenArcade ===
~PlaySound("KICK")
~Show(MadhouseClub_ArcadeOpen)
~Show(MadhouseClub_PileOfCoins)
-> MadhouseClub

=== KickArcade ===
-> KickSuccessful -> KickToOpenArcade

=== KickToOpenArcade ===
~Show(MadhouseClub_ArcadeOpen)
~Show(MadhouseClub_PileOfCoins)
-> MadhouseClub

=== PickUpPileOfCoins ===
~AddToInventory(PileOfCoins)
~Hide(MadhouseClub_PileOfCoins)
-> LookAtPileOfCoins -> MadhouseClub

=== PickUpMadhouseClubSkillBook ===
Player: There's a book in here. It seems rather important.
~AddToInventory(SkillBook2)
~Hide(MadhouseClub_SkillBook)
-> LookAtSkillBook2 -> MadhouseClub

=== PickUpBloodVial ===
~AddToInventory(VialOfBlood)
~UpdateQuest(BarmanQuest, BarmanQuest_1)
-> MadhouseClub