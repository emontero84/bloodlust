
VAR WashingMachineIsMoved = 0
VAR VentIsOpen = 0

=== BeachHouseInterior ===
* { not BeachHouseInteriorFirstEncounter } -> BeachHouseInteriorFirstEncounter
+ { BeachHouseInteriorFirstEncounter } -> BeachHouseInteriorDefault

=== BeachHouseInteriorFirstEncounter ===
~UpdateQuest(AstroliteQuest, AstroliteQuest_1)
-> BeachHouseInteriorDefault

=== BeachHouseInteriorDefault ===
+ [ Use exit door ] -> GoToBeachHouseExterior
+ [ Use kitchen cabinet ] -> OpenBeachHouseKitchenCabinet
+ [ Look at jar of weed ] -> LookAtJarOfWeed -> BeachHouseInterior
+ [ Pick up jar of weed ] -> PickUpJarOfWeed
+ [ Look at PC ] -> LookAtJayPC
+ [ Use PC ] -> UseBeachHouseInteriorPc
+ [ Look at posters ] -> LookAtPosters
+ [ Talk to Jay ] -> TalkToJay
+ [ Look at TV ] -> LookAtJaysTv
+ [ Look at TV remote ] -> LookAtTvRemoteOnTheSofa
+ { not SkillSteal } [ Pick up TV remote ] -> CantPickUpTvRemote
* { SkillSteal } [ Pick up TV remote ] -> StealTvRemote
* { HasInInventory(BrokenTvRemote) } [ Use broken TV remote with TV remote ] -> UseBrokenTvRemoteWithTvRemote
+ [ Look at rat poison ] -> LookAtRatPoison -> BeachHouseInterior
+ [ Pick up rat poison ] -> PickUpRatPoison
+ [ Look at food ] -> LookAtFood
+ [ Use food ] -> UseFood
+ [ Talk to Dennis ] -> TalkToDennis
* { HasQuest(ClaireQuest) && not IsQuestStepUpdated(ClaireQuest_2) && HasInInventory(BaseballBat) } [ Give baseball bat to Dennis ] -> UseBaseballBatWithDennis
* { HasQuest(ClaireQuest) && not IsQuestStepUpdated(ClaireQuest_2) && HasInInventory(Katana) } [ Give katana to Dennis ] -> UseKatanaWithDennis
+ [ Look at safe ] -> LookAtSafe
+ { not HackIntoBeachHouseInteriorPc && not SkillLockpicking } [ Use safe ] -> CantUseBeachHouseInteriorSafe
* { SkillLockpicking } [ Use safe ] -> LockpickBeachHouseInteriorSafe
* { HackIntoBeachHouseInteriorPc && not SkillLockpicking } [ Use safe ] -> UseBeachHouseInteriorSafe
+ [ Look at empty coffee thermos ] -> LookAtEmptyCoffeeThermos -> BeachHouseInterior
* [ Pick up empty coffee thermos ] -> PickUpEmptyCoffeeThermos
+ [ Look at vent ] -> LookAtVent
+ { WashingMachineIsMoved == 0 } [ Use vent ] -> CantReachVent
+ { WashingMachineIsMoved == 1 } [ Use vent ] -> CantOpenVent
* { WashingMachineIsMoved == 1 && HasInInventory(Screwdriver) } [ Use screwdriver with vent ] -> UseScrewdriverOnVent
+ { WashingMachineIsMoved == 1 && VentIsOpen } [ Look at money bag ] -> LookAtMoneyBag -> BeachHouseInterior
* { WashingMachineIsMoved == 1 && VentIsOpen } [ Pick up money bag ] -> PickUpMoneyBag
* [ Use washing machine ] -> PushWashingMachine
+ [ Look at Astrolite ] -> LookAtAstrolite -> BeachHouseInterior
* [ Pick up Astrolite ] -> PickUpAstrolite
+ [ Look at inventory items ] -> LookAtInventoryItems -> BeachHouseInterior

=== OpenBeachHouseKitchenCabinet ===
~Hide(BeachHouseInterior_KitchenCabinet)
~Show(BeachHouseInterior_KitchenCabinetOpen)
~Show(BeachHouseInterior_JarOfWeed)
~PlaySound("OPEN_CONTAINER")
-> BeachHouseInterior

=== PickUpJarOfWeed ===
~AddToInventory(JarOfWeed)
~Hide(BeachHouseInterior_JarOfWeed)
~UpdateQuest(DrugTrip, DrugTrip_1)
-> LookAtJarOfWeed -> BeachHouseInterior

=== LookAtJayPC ===
Player: A desk with a computer.
-> BeachHouseInterior

=== LookAtPosters ===
Player: A sexy calendar and a poster of Silver Surfer.
-> BeachHouseInterior

=== LookAtTvRemoteOnTheSofa ===
Player: There's a TV remote on the sofa.
-> BeachHouseInterior

=== CantPickUpTvRemote ===
Jay: Hey! I'm watching TV!
-> BeachHouseInterior

=== UseBrokenTvRemoteWithTvRemote ===
~RemoveFromInventory(BrokenTvRemote)
~AddToInventory(TvRemote)
-> BeachHouseInterior

=== StealTvRemote ===
Player: No one will notice... #portrait:smile
-> StealSuccessful -> PickUpTvRemote

=== PickUpTvRemote ===
~AddToInventory(TvRemote)
-> BeachHouseInterior

=== PickUpRatPoison ===
~AddToInventory(RatPoison)
~Hide(BeachHouseInterior_RatPoison)
-> LookAtRatPoison -> BeachHouseInterior

=== LookAtFood ===
Player: This food is rotten and poisoned. Someone is trying to lazily catch a rat. #portrait:confused
-> BeachHouseInterior

=== UseFood ===
Player: I'm not eating that! 
-> BeachHouseInterior

=== LookAtSafe ===
Player: A small safe with a numeric pad.
-> BeachHouseInterior

=== CantUseBeachHouseInteriorSafe ===
Player: Er... I don't know the code so I can't open the safe.
-> BeachHouseInterior

=== UseBeachHouseInteriorSafe ===
Player: Open!
~Show(BeachHouseInterior_SafeOpen)
Player: There's a little shiny thing inside the safe.
~AddToInventory(DiamondRing)
-> BeachHouseInterior

=== LockpickBeachHouseInteriorSafe ===
Player: Let me put my lockpicking skills to the test...
Player: Let's see...
~Wait(2)
-> LockpickingSuccessful -> UseBeachHouseInteriorSafe

=== PickUpEmptyCoffeeThermos ===
~AddToInventory(EmptyCoffeeThermos)
~Hide(BeachHouseInterior_EmptyCoffeeThermos)
-> LookAtEmptyCoffeeThermos -> BeachHouseInterior

=== LookAtVent ===
Player: A ventilation grill.
-> BeachHouseInterior

=== PushWashingMachine ===
Player: Maybe I can move it a little bit.
~Wait(1)
~PlaySound("OPEN_CONTAINER")
~Hide(BeachHouseInterior_WashingMachine)
~Show(BeachHouseInterior_WashingMachineMoved)
~WashingMachineIsMoved = 1
-> BeachHouseInterior

=== CantReachVent ===
Player: I can't reach the air vent.
-> BeachHouseInterior

=== CantOpenVent ===
Player: I can't open the air vent with my bare hands. The screws are too tiny.
-> BeachHouseInterior

=== UseScrewdriverOnVent ===
Player: Showtime!
~Hide(BeachHouseInterior_Vent)
~Show(BeachHouseInterior_VentOpen)
~Show(BeachHouseInterior_MoneyBag)
~RemoveFromInventory(Screwdriver)
~PlaySound("OPEN_CONTAINER")
~VentIsOpen = 1
-> BeachHouseInterior

=== PickUpMoneyBag ===
~AddToInventory(MoneyBag)
~Hide(BeachHouseInterior_MoneyBag)
-> LookAtMoneyBag -> BeachHouseInterior

=== PickUpAstrolite ===
~AddToInventory(Astrolite)
~Hide(BeachHouseInterior_Astrolite)
Player: Gotcha! #portrait:smile
~UpdateQuest(AstroliteQuest, AstroliteQuest_2)
-> BeachHouseInterior

=== LookAtJaysTv ===
Player: There's an infinite stream of cartoons in the TV.
-> BeachHouseInterior