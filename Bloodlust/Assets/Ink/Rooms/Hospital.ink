VAR IsHospitalOfficeDoorOpen = 0

=== Hospital ===
~Hide(Hospital_ExitDoorOpen)
~Hide(Hospital_DoctorsDoorOpen)
* { not HospitalFirstEncounter } -> HospitalFirstEncounter
+ { HospitalFirstEncounter } -> HospitalDefault

=== HospitalFirstEncounter ===
-> HospitalDefault

=== HospitalDefault ===
+ [ Use exit door ] -> GoToMainStreetFromHospital
+ { IsHospitalOfficeDoorOpen == 0 } [ Use doctor's office door ] -> CantGoToHospitalOfficeFromHospital
+ { IsHospitalOfficeDoorOpen == 1 } [ Use doctor's office door ] -> GoToHospitalOfficeFromHospital
+ [ Look at receptionist ] -> LookAtReceptionist
+ [ Talk to receptionist ] -> TalkToReceptionist
* { HasInInventory(BirthdayCake) } [ Give birthday cake to receptionist ] -> BeginGiveBirthdayCakeToReceptionist
* { HasInInventory(IntoxicatedBirthdayCake) } [ Give intoxicated birthday cake to receptionist ] -> BeginGiveIntoxicatedBirthdayCakeToReceptionist
+ [ Look at patients ] -> LookAtPatients
+ [ Look at TV ] -> LookAtHospitalTv
+ { HasInInventory(BrokenTvRemote) } [ Use broken TV remote with TV ] -> UseBrokenTvRemoteWithTv
* { HasInInventory(TvRemote) } [ Use TV remote with TV ] -> UseTvRemoteWithTv
+ [ Look at medical degrees ] -> LookAtMedicalDegrees
+ [ Look at painkillers ] -> LookAtPainkillersTray
+ { not SkillSteal } [ Pick up painkillers ] -> CantPickUpPainkillers
* { SkillSteal } [ Pick up painkillers ] -> StealPainkillers
* { HasInInventory(EmptyPainkillers) } [ Use empty painkillers with painkillers ] -> UseEmptyPainkillersWithPainkillers
+ [ Look at inventory items ] -> LookAtInventoryItems -> Hospital

=== GoToMainStreetFromHospital ===
~PlayerAnimation("Interact")
~Wait(1)
~Show(Hospital_ExitDoorOpen)
~PlaySound("OPEN_DOOR")
~GoTo(Room_MainStreet)
-> MainStreet

=== GoToHospitalOfficeFromHospital ===
~PlayerAnimation("Interact")
~Wait(1)
~Show(Hospital_DoctorsDoorOpen)
~PlaySound("OPEN_DOOR")
~GoTo(Room_HospitalOffice)
-> HospitalOffice

=== CantGoToHospitalOfficeFromHospital ===
Receptionist: I'm sorry, {IsFemale:madam}{not IsFemale:sir}. You'll have to wait your turn to enter the doctor's office.
-> Hospital

=== LookAtReceptionist ===
Player: A woman waits behind the hospital counter.
-> Hospital

=== LookAtPatients ===
Player: Three patients wait for their turn to the doctor. They're very entertained by the news on the TV.
-> Hospital

=== LookAtHospitalTv ===
Player: The hospital TV is broadcasting the news.
TV Reporter: ... The slaughter in Santa Monica pier continues to baffle the police investigators of Los Angeles.
TV Reporter: As stated by the head of the Police Department, it's still too early to discard any explanation ... 
-> Hospital

=== UseBrokenTvRemoteWithTv ===
Player: It's not a bad idea, but the remote is broken.
-> Hospital

=== UseTvRemoteWithTv ===
Player: Maybe if I turn off the TV...
~Hide(Hospital_Tv)
Receptionist: Oh, snap.
~IsHospitalOfficeDoorOpen = 1
-> Hospital

=== LookAtPainkillersTray ===
Player: There are some painkillers in the medical tray.
-> Hospital

=== CantPickUpPainkillers ===
Receptionist: I'm sorry, {IsFemale:madam}{not IsFemale:sir}. You'll need a prescription for that.
-> Hospital

=== UseEmptyPainkillersWithPainkillers ===
~RemoveFromInventory(EmptyPainkillers)
~AddToInventory(Painkillers)
~UpdateQuest(PainkillersQuest, PainkillersQuest_1)
-> Hospital

=== StealPainkillers ===
Player: Look behind you! A three-headed monkey! #portrait:smile
Receptionist: What?
-> StealSuccessful -> PickUpPainkillers

=== PickUpPainkillers ===
~AddToInventory(Painkillers)
~UpdateQuest(PainkillersQuest, PainkillersQuest_1)
-> Hospital

=== LookAtMedicalDegrees ===
Player: There're some medical degrees from the University of Misktonic. #portrait:confused
-> Hospital 