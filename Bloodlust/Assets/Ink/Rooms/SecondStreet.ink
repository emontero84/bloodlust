VAR IsSiloDoorOpen = 0

=== SecondStreet ===
~Hide(SecondStreet_SiloDoorOpen)
~Hide(SecondStreet_BailbondsDoorOpen)
~Hide(SecondStreet_MadhouseClubDoorOpen)
~Hide(SecondStreet_DinerDoorOpen)
* { not SecondStreetFirstEncounter } -> SecondStreetFirstEncounter
+ { SecondStreetFirstEncounter } -> SecondStreetDefault

=== SecondStreetFirstEncounter ===
-> SecondStreetDefault

=== SecondStreetDefault ===
+ [ Walk to parking garage ] -> GoToParkingGarage
+ [ Use door to Bailbonds ] -> GoToBailbonds
+ [ Use door to Madhouse Club ] -> GoToMadhouseClub
+ [ Use door to Sunset Diner ] -> GoToSunsetDiner
+ { IsSiloDoorOpen == 0 } [ Use fence door ] -> CantGoToSilo
+ { IsSiloDoorOpen == 1 } [ Use fence door ] -> GoToSilo
+ [ Look at sign ] -> LookAtFenceSign
+ [ Look at Doomsayer ] -> LookAtDoomsayer
+ [ Talk to Doomsayer ] -> TalkToDoomsayer
+ [ Look at inventory items ] -> LookAtInventoryItems -> SecondStreet

=== CantGoToSilo ===
Player: The door is locked. And the padlock has been melted. 
-> SecondStreet

=== GoToBailbonds ===
~PlayerAnimation("Interact")
~Wait(1)
~Show(SecondStreet_BailbondsDoorOpen)
~PlaySound("OPEN_DOOR")
~GoTo(Room_Bailbonds)
-> Bailbonds

=== GoToSilo ===
~PlayerAnimation("Interact")
~Wait(1)
~Show(SecondStreet_SiloDoorOpen)
~PlaySound("OPEN_DOOR")
~GoTo(Room_Silo)
-> Silo

=== GoToMadhouseClub ===
~PlayerAnimation("Interact")
~Wait(1)
~Show(SecondStreet_MadhouseClubDoorOpen)
~PlaySound("OPEN_DOOR")
~GoTo(Room_MadhouseClub)
-> MadhouseClub

=== GoToSunsetDiner ===
~PlayerAnimation("Interact")
~Wait(1)
~Show(SecondStreet_DinerDoorOpen)
~PlaySound("OPEN_DOOR")
~GoTo(Room_Diner)
-> Diner

=== LookAtFenceSign ===
Player: The sign says "Danger. Electric fence. Keep away".
-> SecondStreet

=== LookAtDoomsayer ===
Player: A homeless man is holding a sign that says "The End is Near".
-> SecondStreet
