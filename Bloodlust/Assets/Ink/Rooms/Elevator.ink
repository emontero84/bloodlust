
VAR IsElevatorOnFirstFloor = 1

=== Elevator ===
~Show(Elevator_Doors)
~Hide(Elevator_DoorsOpen)
~Hide(Elevator_ButtonPressed)
+ { IsElevatorOnFirstFloor == 1 } [ Use doors ] -> GoToMadhouseClubFromElevator
+ { IsElevatorOnFirstFloor == 0 } [ Use doors ] -> GoToMadhouseOfficeFromElevator
+ { IsElevatorOnFirstFloor == 1 } [ Use button ] -> PushButtonUp
+ { IsElevatorOnFirstFloor == 0 } [ Use button ] -> PushButtonDown
+ [ Look at inventory items ] -> LookAtInventoryItems -> Elevator

=== GoToMadhouseClubFromElevator ===
~PlayerAnimation("Interact")
~Wait(1)
~PlaySound("ELEVATOR_DOOR")
~Hide(Elevator_Doors)
~Show(Elevator_DoorsOpen)
~GoTo(Room_MadhouseClub)
-> MadhouseClub

=== GoToMadhouseOfficeFromElevator ===
~PlayerAnimation("Interact")
~Wait(1)
~PlaySound("ELEVATOR_DOOR")
~Hide(Elevator_Doors)
~Show(Elevator_DoorsOpen)
~GoTo(Room_MadhouseOffice)
-> MadhouseOffice

=== PushButtonUp ===
~Show(Elevator_ButtonPressed)
~Wait(2)
~PlaySound("ELEVATOR_DING")
~Hide(Elevator_ButtonPressed)
~IsElevatorOnFirstFloor = 0
Player: Second floor. Ding!
-> Elevator

=== PushButtonDown ===
~Show(Elevator_ButtonPressed)
~Wait(2)
~PlaySound("ELEVATOR_DING")
~Hide(Elevator_ButtonPressed)
~IsElevatorOnFirstFloor = 1
Player: First floor. Ding!
-> Elevator