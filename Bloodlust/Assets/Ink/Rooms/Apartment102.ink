
=== Apartment102 ===
~Hide(Apartment102_ExitDoorOpen)
+ { not HasQuest(TessQuest) } -> Apartment102FirstEncounterNoQuest
* { HasQuest(TessQuest) && not Apartment102FirstEncounter } -> Apartment102FirstEncounter
+ { HasQuest(TessQuest) && Apartment102FirstEncounter } -> Apartment102Default

=== Apartment102FirstEncounterNoQuest ===
-> TalkToDetectiveCollins

=== Apartment102FirstEncounter ===
~UpdateQuest(TessQuest, TessQuest_1)
-> TalkToDetectiveCollins

=== Apartment102Default ===
+ [ Use exit door ] -> GoToLodgingsHallwayFromApartment102
+ [ Look at Detective Collins ] -> LookAtDetectiveCollins
+ [ Talk to Detective Collins ] -> TalkToDetectiveCollins
+ [ Look at Photographer ] -> LookAtPhotographer
+ [ Talk to Photographer ] -> TalkToPhotographer
+ [ Look at Officer Johnson ] -> LookAtOfficerJohnson
+ [ Talk to Officer Johnson ] -> TalkToOfficerJohnson
+ [ Look at burned body ] -> LookAtBurnedBody
+ [ Look at stake ] -> LookAtStakeInTheBody
* [ Pick up stake ] -> PickUpStake
* [ Use top drawer ] -> OpenApartment102TopDrawer
+ { not SkillLockpicking } [ Use bottom drawer ] -> CantOpenApartment102BottomDrawer
* { SkillLockpicking } [ Use bottom drawer ] -> UseLockpickingSkillOnApartment102BottomDrawer
* { HasInInventory(Lockpick) } [ Use lockpick with bottom drawer ] -> LockpickApartment102BottomDrawer
* { HasInInventory(TinyKey) } [ Use tiny key with bottom drawer ] -> OpenApartment102BottomDrawerWithTinyKey
+ [ Look at phone ] -> LookAtApartment102Phone
+ [ Look at notebook ] -> LookAtApartment102Notebook
+ [ Look at old Bible ] -> LookAtOldBible -> Apartment102
* [ Pick up old Bible ] -> PickUpOldBible
* [ Pick up ashtray ] -> PickUpAshTray
* [ Use kitchen cabinet ] -> OpenApartment102KitchenCabinet
+ [ Look at empty can of gas ] -> LookAtEmptyCanOfGas -> Apartment102
* [ Pick up empty can of gas ] -> PickUpEmptyCanOfGas
* [ Use fridge ] -> OpenApartment102Fridge
+ [ Look at birthday cake ] -> LookAtBirthdayCake -> Apartment102
* [ Pick up birthday cake ] -> PickUpBirthdayCake
* [ Use toilet cabinet ] -> OpenApartment102ToiletCabinet
+ [ Look at laxatives ] -> LookAtLaxatives -> Apartment102
* [ Pick up laxatives ] -> PickUpLaxatives
+ [ Look at inventory items ] -> LookAtInventoryItems -> Apartment102

=== GoToLodgingsHallwayFromApartment102 ===
~PlayerAnimation("Interact")
~Wait(1)
~Show(Apartment102_ExitDoorOpen)
~PlaySound("OPEN_DOOR")
~GoTo(Room_LodgingHallway)
-> LodgingHallway

=== LookAtDetectiveCollins ===
Player: A hard-boiled detective with a gritty gloom in the eyes.
-> Apartment102

=== LookAtPhotographer ===
Player: A photographer taking pictures of the crime scene.
-> Apartment102

=== LookAtOfficerJohnson ===
Player: A strong female police officer is guarding the crime scene.
-> Apartment102

=== LookAtBurnedBody ===
Player: There's a dead body in the bed. It's completely burned out.
-> Apartment102

=== LookAtStakeInTheBody ===
Player: There's a wooden stake at the heart of the burned body. It definitely looks like a vampire was killed here.
-> Apartment102

=== PickUpStake ===
~Hide(Apartment102_Stake)
~AddToInventory(Stake)
~UpdateQuest(TessQuest, TessQuest_2)
-> LookAtStake -> Apartment102

=== OpenApartment102TopDrawer ===
~Hide(Apartment102_TopDrawer)
~Show(Apartment102_TopDrawerOpen)
~PlaySound("OPEN_CONTAINER")
Player: There's a book in here that seems rather important.
~AddToInventory(SkillBook9)
-> LookAtSkillBook9 -> Apartment102

=== CantOpenApartment102BottomDrawer ===
Player: It's locked.
-> Apartment102

=== OpenApartment102BottomDrawerWithTinyKey ===
~RemoveFromInventory(TinyKey)
-> OpenApartment102BottomDrawer

=== UseLockpickingSkillOnApartment102BottomDrawer ===
Player: Let me put my lockpicking skills to the test...
Player: Let's see...
~Wait(2)
-> LockpickingSuccessful -> OpenApartment102BottomDrawer

=== LockpickApartment102BottomDrawer ===
~RemoveFromInventory(Lockpick)
-> LockpickingSuccessful -> OpenApartment102BottomDrawer

=== OpenApartment102BottomDrawer ===
~Hide(Apartment102_BottomDrawer)
~Show(Apartment102_BottomDrawerOpen)
~PlaySound("OPEN_CONTAINER")
Player: There's a book inside the drawer.
~AddToInventory(MariansDiary)
~UpdateQuest(TessQuest, TessQuest_3)
-> Apartment102

=== LookAtApartment102Phone ===
Player: A phone. There's no line. It has been probably sabotaged.
-> Apartment102

=== LookAtApartment102Notebook ===
Player: There's a notebook with a handwritten journal from the victim. 
Player: "They are getting closer. The vampire hunters are following my every step."
-> Apartment102

=== PickUpOldBible ===
~Hide(Apartment102_OldBible)
~AddToInventory(OldBible)
-> LookAtOldBible -> Apartment102

=== PickUpAshTray ===
Player: There's something behind the ashtray.
~AddToInventory(TinyKey)
-> LookAtTinyKey -> Apartment102

=== OpenApartment102KitchenCabinet ===
~Hide(Apartment102_KitchenCabinet)
~Show(Apartment102_KitchenCabinetOpen)
~Show(Apartment102_EmptyCanOfGas)
~PlaySound("OPEN_CONTAINER")
-> Apartment102

=== PickUpEmptyCanOfGas ===
~Hide(Apartment102_EmptyCanOfGas)
~AddToInventory(EmptyCanOfGas)
-> LookAtEmptyCanOfGas -> Apartment102

=== OpenApartment102Fridge ===
~Hide(Apartment102_Fridge)
~Show(Apartment102_FridgeOpen)
~Show(Apartment102_BirthdayCake)
~PlaySound("OPEN_CONTAINER")
-> Apartment102

=== PickUpBirthdayCake ===
~Hide(Apartment102_BirthdayCake)
~AddToInventory(BirthdayCake)
-> LookAtBirthdayCake -> Apartment102

=== OpenApartment102ToiletCabinet ===
~Hide(Apartment102_ToiletCabinet)
~Show(Apartment102_ToiletCabinetOpen)
~Show(Apartment102_Laxatives)
~PlaySound("OPEN_CONTAINER")
-> Apartment102

=== PickUpLaxatives ===
~Hide(Apartment102_Laxatives)
~AddToInventory(Laxatives)
-> LookAtLaxatives -> Apartment102