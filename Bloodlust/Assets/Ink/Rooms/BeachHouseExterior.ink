
VAR BeachHouseLights = 1
VAR BeachHouseOpen = 0
VAR BeachHouseGasDepositOpen = 0

=== BeachHouseExterior ===
+ [ Walk to the beach ] -> GoToBeach
+ { BeachHouseOpen == 1 } [ Use door to beach house ] -> GoToBeachHouseInterior
+ { BeachHouseOpen == 0 } [ Use door to beach house ] -> CantGoToBeachHouseInterior
+ [ Look at Kev ] -> LookAtKev
+ [ Talk to Kev ] -> TalkToKev
* { not IntimidateKev && not KevSeduction && HasInInventory(Katana) } [ Use katana with Kev ] -> IntimidateKevWithKatana
* { not IntimidateKev && not KevSeduction && HasInInventory(BaseballBat) } [ Use baseball bat with Kev ] -> IntimidateKevWithBaseballBat
+ [ Look at van ] -> LookAtVan
* [ Use van door ] -> OpenVan
+ [ Look at crowbar ] -> LookAtCrowbar -> BeachHouseExterior
+ [ Pick up crowbar ] -> PickUpCrowbar
+ [ Look at bush ] -> LookAtBush
* [ Use bush ] -> PushBush
+ [ Look at power switch ] -> LookAtPowerSwitch
* { BeachHouseLights == 1 } [ Use power switch ] -> UsePowerSwitch
* { not SkillKick && BeachHouseLights == 1 } [ Look at fence ] -> LookAtFence
* { not SkillKick && BeachHouseLights == 1 } [ Pick up fence ] -> KevWarning
* { not SkillKick && BeachHouseLights == 0 } [ Pick up fence ] -> CantOpenFence
* { SkillKick } [ Pick up fence ] -> KickFence
+ { BeachHouseLights == 1 && HasInInventory(Crowbar) } [ Use crowbar with fence ] -> KevWarning
* { BeachHouseLights == 0 && HasInInventory(Crowbar) } [ Use crowbar with fence ] -> UseCrowbarOnFence
+ [ Look at baseball bat ] -> LookAtBaseballBat -> BeachHouseExterior
+ { BeachHouseLights == 1 && not SkillSteal } [ Pick up baseball bat ] -> KevWarning
* { BeachHouseLights == 0 && not SkillSteal} [ Pick up baseball bat ] -> PickUpBaseballBat
* { SkillSteal } [ Pick up baseball bat ] -> StealBaseballBat
+ [ Look at gas deposit ] -> LookAtGasDeposit
* [ Use gas deposit ] -> OpenGasDeposit
* { BeachHouseGasDepositOpen && HasInInventory(EmptyCanOfGas) } [ Use empty can of gas with gas deposit ] -> UseEmptyCanOfGasWithGasDeposit
+ { BeachHouseGasDepositOpen && not UseEmptyCanOfGasWithGasDeposit && HasInInventory(PlasticTubeWithFunnel) } [ Use plastic tube with funnel with gas deposit ] -> CantUsePlasticTubeWithFunnelWithGasDeposit
* { BeachHouseGasDepositOpen && UseEmptyCanOfGasWithGasDeposit && HasInInventory(PlasticTubeWithFunnel) } [ Use plastic tube with funnel with gas deposit ] -> UsePlasticTubeWithFunnelWithGasDeposit
+ [ Look at inventory items ] -> LookAtInventoryItems -> BeachHouseExterior

=== GoToBeachHouseInterior ===
~PlayerAnimation("Interact")
~Wait(1)
~Show(BeachHouseExterior_DoorToBeachHouseOpen)
~PlaySound("OPEN_DOOR")
~GoTo(Room_BeachHouseInterior)
-> BeachHouseInterior

=== CantGoToBeachHouseInterior ===
Player: I can't get to the door. The entrance is heavily guarded.
-> BeachHouseExterior

=== LookAtKev ===
Player: A surfer is guarding the entrance of the house.
-> BeachHouseExterior

=== LookAtVan ===
Player: A classic Volkswagen van.
-> BeachHouseExterior

=== OpenVan ===
~Hide(BeachHouseExterior_VanDoor)
~Show(BeachHouseExterior_VanDoorOpen)
~Show(BeachHouseExterior_Crowbar)
~PlaySound("OPEN_CONTAINER")
-> BeachHouseExterior

=== PickUpCrowbar ===
~AddToInventory(Crowbar)
~Hide(BeachHouseExterior_Crowbar)
-> LookAtCrowbar -> BeachHouseExterior

=== LookAtBush ===
Player: The beautiful work of mother nature.
-> BeachHouseExterior

=== PushBush ===
~Hide(BeachHouseExterior_Bush)
~Show(BeachHouseExterior_BushOpen)
~Show(BeachHouseExterior_BushOpenPowerSwitch)
Player: Hmmmm. Interesting. #portrait:smile
-> BeachHouseExterior

=== LookAtPowerSwitch ===
Player: Seems like a power switch controlling the fence lights.
-> BeachHouseExterior

=== UsePowerSwitch ===
Player: Lights off! #portrait:blood
~BeachHouseLights = 0
~Hide(BeachHouseExterior_LightsOn)
~Show(BeachHouseExterior_LightsOff)
~PlaySound("OPEN_CONTAINER")
Kev: What the hell...?
-> BeachHouseExterior

=== LookAtFence ===
Player: The fence has a hole poorly repaired.
-> BeachHouseExterior

=== KevWarning ===
Kev: Hey! Don't even think about it! I can see your intentions from here.
-> BeachHouseExterior

=== CantOpenFence ===
Kev: ...
Player: Now I can try to open the fence. But I can't do it with just my bare hands. 
-> BeachHouseExterior

=== UseCrowbarOnFence ===
Player: All right.
~PlaySound("OPEN_CONTAINER")
-> OpenFence

=== OpenFence ===
~BeachHouseOpen = 1
~Hide(BeachHouseExterior_Fence)
-> BeachHouseExterior

=== KickFence ===
-> KickSuccessful -> OpenFence

=== PickUpBaseballBat ===
~AddToInventory(BaseballBat)
~Hide(BeachHouseExterior_BaseballBat)
-> LookAtBaseballBat -> BeachHouseExterior

=== StealBaseballBat ===
Player: Look, a three-headed monkey! #portrait:smile
Kev: What?!
-> StealSuccessful -> PickUpBaseballBat

=== LookAtGasDeposit ===
Player: The gas deposit of the van.
-> BeachHouseExterior

=== OpenGasDeposit ===
Player: Voliá! It's open.
~Show(BeachHouseExterior_GasDepositOpen)
~PlaySound("OPEN_CONTAINER")
~BeachHouseGasDepositOpen = 1
-> BeachHouseExterior

=== UseEmptyCanOfGasWithGasDeposit ===
~RemoveFromInventory(EmptyCanOfGas)
~Show(BeachHouseExterior_GasCan)
-> BeachHouseExterior

=== CantUsePlasticTubeWithFunnelWithGasDeposit ===
Player: Not a bad idea. But first, I need to place some kind of container for the gas. #portrait:confused
-> BeachHouseExterior

=== UsePlasticTubeWithFunnelWithGasDeposit ===
Player: And here comes the gas! #portrait:smile
~RemoveFromInventory(PlasticTubeWithFunnel)
~Show(BeachHouseExterior_PlasticTubeWithFunnel)
~Wait(2)
~Hide(BeachHouseExterior_GasCan)
~Hide(BeachHouseExterior_PlasticTubeWithFunnel)
~AddToInventory(CanOfGas)
~UpdateQuest(BullockQuest, BullockQuest_2)
-> BeachHouseExterior
