using MainMenu;
using MainMenu.CharacterSelection;
using MainMenu.Options;
using Project.Loading;
using Project.Progression;
using Services.Analytics;
using Services.FileLoader;
using Services.SceneLoader;
using Utils;
using Zenject;

namespace Project
{
	public class ProjectInstaller : MonoInstaller
	{
		public LoadingScreenAdapter loadingScreenAdapter;

		public override void InstallBindings()
		{
			Container.BindInterfacesAndSelfTo<AsyncProcessor>().FromNewComponentOnNewGameObject().AsSingle();
			SignalBusInstaller.Install(Container);

			AnalyticsInstaller.Install(Container);

			SceneLoaderInstaller.Install(Container);

			// File loader service
			Container.Bind<FileLoaderService>().FromNew().AsSingle();

			// Saved games & Progression
			Container.Bind<PlayerModel>().FromNew().AsSingle();
			Container.Bind<ScreenshotModel>().FromNew().AsSingle();
			Container.Bind<SavedGamesModel>().FromNew().AsSingle();
			Container.Bind<CharacterModel>().FromNew().AsSingle();
			Container.BindInterfacesAndSelfTo<ProgressionController>().FromNew().AsSingle();

			// Loading screen
			Container.BindInterfacesAndSelfTo<LoadingScreenAdapter>().FromInstance(loadingScreenAdapter).AsSingle();
			Container.BindInterfacesAndSelfTo<LoadingScreenController>().FromNew().AsSingle();
			Container.DeclareSignal<ShowLoadingScreenSignal>();
			Container.BindSignal<ShowLoadingScreenSignal>().ToMethod<ShowLoadingScreenCommand>(x => x.OnSignal).FromResolve();
			Container.BindInterfacesAndSelfTo<ShowLoadingScreenCommand>().FromNew().AsSingle();

			// Options
			Container.Bind<OptionsModel>().FromNew().AsSingle();

            Container.BindInterfacesAndSelfTo<MainMenuModel>().FromNew().AsSingle();
        }
	}
}