using System;
using System.Globalization;
using System.IO;
using MainMenu;
using MainMenu.CharacterSelection;
using Project.Loading;
using Services.FileLoader;
using Services.SceneLoader;
using Services.Story;
using UnityEngine;
using Utils;
using Zenject;

namespace Project.Progression
{
	public class ProgressionController : IInitializable, IDisposable
	{
		[Inject] public SavedGamesModel savedGamesModel { get; private set; }
		[Inject] public StoryService storyService { get; private set; }
		[Inject] public SceneLoaderService sceneLoaderService { get; private set; }
		[Inject] public FileLoaderService fileLoaderService { get; private set; }
		[Inject] public PlayerModel playerModel { get; private set; }
		[Inject] public ScreenshotModel screenshotModel { get; private set; }
        [Inject] public CharacterModel characterModel { get; private set; }
        [Inject] public MainMenuModel mainMenuModel { get; private set; }
        [Inject] public AsyncProcessor asyncProcessor { get; private set; }
		[Inject] public SignalBus signalBus { get; private set; }

		private Coroutine _coroutine;

		public void Initialize()
		{
		}

		public void Dispose()
		{
			fileLoaderService.FileLoaded -= OnFileLoaded;
			StopAllCoroutines();
		}

		private void StopAllCoroutines()
		{
			if (_coroutine != null)
			{
				if (asyncProcessor != null)
				{
					asyncProcessor.StopCoroutine(_coroutine);
				}

				_coroutine = null;
			}
		}

		public bool IsFirstExecution()
		{
			return savedGamesModel.savedGamesProgressionData.index == -1;
		}

		public SavedGameData GetSavedGame(int index)
		{
			if (savedGamesModel.savedGamesProgressionData == null || savedGamesModel.savedGamesProgressionData.savedGames == null || savedGamesModel.savedGamesProgressionData.savedGames == null)
			{
				Debug.LogError("[SavedGamesController] Saved game is null at index " + index);
				return null;
			}

			if (index < 0 || index >= savedGamesModel.savedGamesProgressionData.savedGames.Count)
			{
				Debug.LogError("[SavedGamesController] Saved game index out or range: " + index);
				return null;
			}

			return savedGamesModel.savedGamesProgressionData.savedGames[index];
		}

		public void LoadGameProgression()
		{
			var path = Application.streamingAssetsPath + StringConstants.SAVE_GAMES_FILE_PATH;
			StopAllCoroutines();
			fileLoaderService.FileLoaded += OnFileLoaded;
			fileLoaderService.LoadStreamingAsset(path, StringConstants.SAVE_GAMES_FILE_ID);
		}

		private void OnFileLoaded(string fileName, string fileId, string json)
		{
			if (fileId != StringConstants.SAVE_GAMES_FILE_ID)
				return;
			fileLoaderService.FileLoaded -= OnFileLoaded;

			if (string.IsNullOrEmpty(json))
			{
				json = StringConstants.SAVE_GAMES_EMPTY;
				fileLoaderService.SaveStreamingAsset(json, fileName, fileId);
			}

			SavedGamesProgressionData progression = JsonUtility.FromJson<SavedGamesProgressionData>(json);
			savedGamesModel.savedGamesProgressionData = progression;
		}

		public void SaveGameProgression()
		{
			string json = JsonUtility.ToJson(savedGamesModel.savedGamesProgressionData, false);
			try
			{
				fileLoaderService.SaveStreamingAsset(json, Application.streamingAssetsPath + StringConstants.SAVE_GAMES_FILE_PATH, StringConstants.SAVE_GAMES_FILE_ID);
			}
			catch (Exception e)
			{
				Debug.LogError("[ProgressionController] Exception in SaveGameProgression! " + e.Message);
				signalBus.Fire(new ShowLoadingScreenSignal(false));
			}
		}

		public void SaveGame(int index)
		{
			var json = storyService.SaveStory();
			var newSavedGame = new SavedGameData();
			newSavedGame.date = DateTime.Now.ToString(CultureInfo.CurrentCulture);
			newSavedGame.name = "Saved Game #" + (index + 1);
			newSavedGame.json = json;
			newSavedGame.x = playerModel.x;
			newSavedGame.y = playerModel.y;
            newSavedGame.characterIndex = characterModel.characterIndex;
#if UNITY_WEBGL
			newSavedGame.screenshot = new byte[0];
#else
			newSavedGame.screenshot = screenshotModel.screenshot.EncodeToJPG();
#endif
			savedGamesModel.savedGamesProgressionData.savedGames[index] = newSavedGame;
			savedGamesModel.savedGamesProgressionData.index = index;
			SaveGameProgression();
			Debug.Log("Save game " + newSavedGame.name + ", date: " + newSavedGame.date);

            mainMenuModel.screen = MainMenuModel.MainMenuScreen.Gameplay;
            sceneLoaderService.LoadScene(SceneLoaderModel.GAMEPLAY_SCENE);
		}

		public void LoadGame(int index)
		{
			if (index < 0 || index >= savedGamesModel.savedGamesProgressionData.savedGames.Count)
			{
				Debug.LogError("[SavedGamesController] Load game index out or range: " + index);
				return;
			}

			var currentSavedGame = savedGamesModel.savedGamesProgressionData.savedGames[index];
			if (currentSavedGame.json == "")
			{
				Debug.LogError("[SavedGamesController] Trying to load an empty game slot: " + index);
				signalBus.Fire(new ShowLoadingScreenSignal(false));
				return;
			}
			storyService.LoadStory(currentSavedGame.json);
			savedGamesModel.savedGamesProgressionData.index = index;
			playerModel.x = currentSavedGame.x;
			playerModel.y = currentSavedGame.y;
            characterModel.InitializeCharacterModel(currentSavedGame.characterIndex);
            playerModel.isInitialized = true;
			Debug.Log("Load game " + currentSavedGame.name + ", date: " + currentSavedGame.date);

            mainMenuModel.screen = MainMenuModel.MainMenuScreen.Gameplay;
            sceneLoaderService.LoadScene(SceneLoaderModel.GAMEPLAY_SCENE);
		}

		public void ResetProgression()
		{
			storyService.ResetStory();
			playerModel.x = 0;
			playerModel.y = 0;
			playerModel.isInitialized = false;
		}
	}
}