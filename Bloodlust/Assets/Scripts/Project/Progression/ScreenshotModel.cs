using UnityEngine;

namespace Project.Progression
{
	public class ScreenshotModel
	{
		public Texture2D screenshot;
	}
}