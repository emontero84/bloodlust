namespace Project.Progression
{
	[System.Serializable]
	public class SavedGameData
	{
		public string name;
		public string json;
		public string date;
		public float x;
		public float y;
		public byte[] screenshot;
        public int characterIndex;
	}
}