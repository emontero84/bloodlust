using System.Collections.Generic;

namespace Project.Progression
{
	[System.Serializable]
	public class SavedGamesProgressionData
	{
		public List<SavedGameData> savedGames;
		public int index;
	}
}