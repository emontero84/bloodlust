namespace Project.Loading
{
	public class ShowLoadingScreenSignal
	{
		public bool showLoading;

		public ShowLoadingScreenSignal(bool showLoading)
		{
			this.showLoading = showLoading;
		}
	}
}