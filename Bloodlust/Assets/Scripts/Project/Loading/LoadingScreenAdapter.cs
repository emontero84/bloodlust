using UnityEngine;

namespace Project.Loading
{
	public class LoadingScreenAdapter : MonoBehaviour
	{
		public Canvas loadingCanvas;

		public void ShowLoadingScreen()
		{
			loadingCanvas.gameObject.SetActive(true);
		}

		public void HideLoadingScreen()
		{
			loadingCanvas.gameObject.SetActive(false);
		}
	}
}