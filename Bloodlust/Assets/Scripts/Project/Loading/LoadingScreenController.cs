using System;
using UnityEngine;
using Zenject;

namespace Project.Loading
{
	public class LoadingScreenController : IInitializable
	{
		[Inject] public LoadingScreenAdapter loadingScreenAdapter { get; private set; }

		public void Initialize()
		{
			ShowLoadingScreen();
		}

		public void ShowLoadingScreen()
		{
			loadingScreenAdapter.ShowLoadingScreen();
		}

		public void HideLoadingScreen()
		{
			loadingScreenAdapter.HideLoadingScreen();
		}
	}
}