using Zenject;

namespace Project.Loading
{
	public class ShowLoadingScreenCommand
	{
		[Inject] public LoadingScreenController loadingScreenController { get; private set; }

		public void OnSignal(ShowLoadingScreenSignal signal)
		{
			if (signal.showLoading)
			{
				loadingScreenController.ShowLoadingScreen();
			}
			else
			{
				loadingScreenController.HideLoadingScreen();
			}
		}
	}
}