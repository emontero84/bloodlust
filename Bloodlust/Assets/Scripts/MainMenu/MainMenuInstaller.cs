using MainMenu.CharacterSelection;
using MainMenu.Disclaimer;
using MainMenu.HowToPlay;
using MainMenu.Options;
using MainMenu.Save;
using MainMenu.Title;
using Zenject;

namespace MainMenu
{
	public class MainMenuInstaller : MonoInstaller
	{
		public TitleScreenAdapter titleScreenAdapter;
		public CharacterSelectionScreenAdapter characterSelectionScreenAdapter;
		public SaveLoadGameScreenAdapter saveLoadGameScreenAdapter;
		public OptionsScreenAdapter optionsScreenAdapter;
		public DisclaimerScreenAdapter disclaimerScreenAdapter;
		public HowToPlayScreenAdapter howToPlayScreenAdapter;

		public override void InstallBindings()
		{
			Container.BindInterfacesAndSelfTo<TitleScreenAdapter>().FromInstance(titleScreenAdapter).AsSingle();
			Container.BindInterfacesAndSelfTo<TitleScreenController>().FromNew().AsSingle();

			Container.BindInterfacesAndSelfTo<CharacterSelectionScreenAdapter>().FromInstance(characterSelectionScreenAdapter).AsSingle();
			Container.BindInterfacesAndSelfTo<CharacterSelectionScreenController>().FromNew().AsSingle();

			Container.BindInterfacesAndSelfTo<SaveLoadGameScreenAdapter>().FromInstance(saveLoadGameScreenAdapter).AsSingle();
			Container.BindInterfacesAndSelfTo<SaveLoadGameScreenController>().FromNew().AsSingle();

			Container.BindInterfacesAndSelfTo<OptionsScreenAdapter>().FromInstance(optionsScreenAdapter).AsSingle();
			Container.BindInterfacesAndSelfTo<OptionsScreenController>().FromNew().AsSingle();
			
			Container.BindInterfacesAndSelfTo<DisclaimerScreenAdapter>().FromInstance(disclaimerScreenAdapter).AsSingle();
			Container.BindInterfacesAndSelfTo<DisclaimerScreenController>().FromNew().AsSingle();
			
			Container.BindInterfacesAndSelfTo<HowToPlayScreenAdapter>().FromInstance(howToPlayScreenAdapter).AsSingle();
			Container.BindInterfacesAndSelfTo<HowToPlayScreenController>().FromNew().AsSingle();
		}
	}
}