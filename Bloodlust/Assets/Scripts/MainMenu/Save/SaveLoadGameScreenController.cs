using System;
using System.Collections;
using System.Collections.Generic;
using MainMenu.Title;
using Project.Loading;
using Project.Progression;
using Services.Audio;
using UnityEngine;
using Utils;
using Zenject;

namespace MainMenu.Save
{
	public class SaveLoadGameScreenController : IInitializable, IDisposable
	{
		[Inject] public SaveLoadGameScreenAdapter saveLoadGameScreenAdapter { get; private set; }
		[Inject] public ProgressionController progressionController { get; private set; }
		[Inject] public TitleScreenController titleScreenController { get; private set; }
		[Inject] public MainMenuModel mainMenuModel { get; private set; }
		[Inject] public SignalBus signalBus { get; private set; }
		[Inject] public List<SavedGameSlotAdapter> saveSlots { get; private set; }
		[Inject] public AudioManager audioManager { get; private set; }
		[Inject] public AsyncProcessor asyncProcessor { get; private set; }

		private Coroutine _coroutine;
		public void Initialize()
		{
			saveLoadGameScreenAdapter.backButton.onClick.AddListener(OnBackButtonClick);
			for (var i = 0; i < saveSlots.Count; i++)
			{
				var slot = saveSlots[i];
				slot.SaveSlotSelected += OnSaveSlotSelected;
			}

			Hide();
		}

		public void Dispose()
		{
			saveLoadGameScreenAdapter.backButton.onClick.RemoveListener(OnBackButtonClick);
			for (var i = 0; i < saveSlots.Count; i++)
			{
				var slot = saveSlots[i];
				slot.SaveSlotSelected -= OnSaveSlotSelected;
			}
			StopAllCoroutines();
		}

		private void StopAllCoroutines()
		{
			if (_coroutine != null)
			{
				if (asyncProcessor != null)
				{
					asyncProcessor.StopCoroutine(_coroutine);
				}

				_coroutine = null;
			}
		}

		private void UpdateSaveSlots()
		{
			for (var i = 0; i < saveSlots.Count; i++)
			{
				var slot = saveSlots[i];
				var savedGame = progressionController.GetSavedGame(i);
				if (savedGame != null)
				{
					slot.SetNameText(savedGame.name);
					slot.SetDateText(savedGame.date);
					if (savedGame.screenshot != null && savedGame.screenshot.Length > 0)
					{
						slot.SetScreenshotImage(savedGame.screenshot);
					}
					else
					{
						slot.HideScreenshot();
					}
				}
			}
		}

		public void Show()
		{
			UpdateSaveSlots();
			saveLoadGameScreenAdapter.saveGameScreen.gameObject.SetActive(true);
		}

		public void Hide()
		{
			saveLoadGameScreenAdapter.saveGameScreen.gameObject.SetActive(false);
		}

		private void OnBackButtonClick()
		{
			mainMenuModel.screen = MainMenuModel.MainMenuScreen.TitleScreen;

			Hide();
			audioManager.Play(SoundData.SoundId.GENERIC_CLICK);
			titleScreenController.Show();
        }

		private void OnSaveSlotSelected(int index)
		{
			StopAllCoroutines();
			_coroutine = asyncProcessor.StartCoroutine(SaveCoroutine(index));
		}

		private IEnumerator SaveCoroutine(int index)
		{
			audioManager.Play(SoundData.SoundId.GENERIC_CLICK);
			yield return new WaitForSeconds(0.1f);

			signalBus.Fire(new ShowLoadingScreenSignal(true));
			yield return new WaitForSeconds(0.1f);
			
			audioManager.Play(SoundData.SoundId.CINEMATIC_IMPACT);

			if (mainMenuModel.screen == MainMenuModel.MainMenuScreen.SaveGame)
			{
				progressionController.SaveGame(index);
				UpdateSaveSlots();
			}
			else if (mainMenuModel.screen == MainMenuModel.MainMenuScreen.LoadGame)
			{
				progressionController.LoadGame(index);
			}
		}
	}
}