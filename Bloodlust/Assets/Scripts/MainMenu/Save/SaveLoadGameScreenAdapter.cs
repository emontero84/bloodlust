using UnityEngine;
using UnityEngine.UI;

namespace MainMenu.Save
{
	public class SaveLoadGameScreenAdapter : MonoBehaviour
	{
		public RectTransform saveGameScreen;
		public Button backButton;
	}
}