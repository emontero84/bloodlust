using System;
using System.Xml.Schema;
using UnityEngine;
using UnityEngine.UI;

namespace MainMenu.Save
{
	public class SavedGameSlotAdapter : MonoBehaviour
	{
		public Action<int> SaveSlotSelected;
		
		public int index;
		public Text nameText;
		public Text dateText;
		public Button saveSlotButton;
		public Image screenshotImage;
		public void Start()
		{
			saveSlotButton.onClick.AddListener(OnSaveSlotButtonClick);
		}

		private void OnDestroy()
		{
			saveSlotButton.onClick.RemoveListener(OnSaveSlotButtonClick);
		}

		public void SetNameText(string name)
		{
			nameText.text = name;
		}

		public void SetDateText(string date)
		{
			dateText.text = date;
		}

		public void HideScreenshot()
		{
			screenshotImage.gameObject.SetActive(false);	
		}
		
		public void SetScreenshotImage(byte[] bytes)
		{
			int width = Screen.width;
			int height = Screen.height;
            try
            {
                var screenShot = new Texture2D(width, height, TextureFormat.RGB24, false);
                screenShot.LoadImage(bytes);
                screenshotImage.sprite = Sprite.Create(screenShot, new Rect(0, 0, width, height), Vector2.zero);
                screenshotImage.gameObject.SetActive(true);
            } catch (Exception e)
            {
                screenshotImage.gameObject.SetActive(false);
            }
		}

		private void OnSaveSlotButtonClick()
		{
			if (SaveSlotSelected != null)
			{
				SaveSlotSelected(index);
			}
		}
	}
}