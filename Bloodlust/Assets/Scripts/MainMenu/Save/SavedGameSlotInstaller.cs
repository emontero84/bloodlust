using Zenject;

namespace MainMenu.Save
{
	public class SavedGameSlotInstaller : MonoInstaller
	{
		public SavedGameSlotAdapter savedGameSlotAdapter;
		public override void InstallBindings()
		{
			Container.BindInterfacesAndSelfTo<SavedGameSlotAdapter>().FromInstance(savedGameSlotAdapter).AsSingle();
		}
	}
}