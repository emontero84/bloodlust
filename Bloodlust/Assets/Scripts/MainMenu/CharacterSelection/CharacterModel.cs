using System.Collections.Generic;
using Utils;

namespace MainMenu.CharacterSelection
{
    public class CharacterModel
	{
		public int characterIndex = 3;
		public string characterName = "Ash";

        public List<string> characterNames = new List<string>()
        {
            StringConstants.NAME_GIANNA,
            StringConstants.NAME_MATT,
            StringConstants.NAME_CAT,
            StringConstants.NAME_ASH,
            StringConstants.NAME_CHLOE,
            StringConstants.NAME_JESS,
            StringConstants.NAME_STUART,
        };
        public List<string> characterDescriptions = new List<string>()
        {
            StringConstants.DESCRIPTION_GIANNA,
            StringConstants.DESCRIPTION_MATT,
            StringConstants.DESCRIPTION_CAT,
            StringConstants.DESCRIPTION_ASH,
            StringConstants.DESCRIPTION_CHLOE,
            StringConstants.DESCRIPTION_JESS,
            StringConstants.DESCRIPTION_STUART,
        };

        public void InitializeCharacterModel(int index)
        {
            var name = characterNames[index];
            characterIndex = index;
            characterName = name;
        }
    }

    
}