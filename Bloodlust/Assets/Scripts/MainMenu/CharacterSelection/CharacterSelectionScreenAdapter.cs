using System.Collections.Generic;
using Components;
using UnityEngine;
using UnityEngine.UI;

namespace MainMenu.CharacterSelection
{
	public class CharacterSelectionScreenAdapter : MonoBehaviour
	{
		public RectTransform characterSelectionScreen;
		public Text characterNameText;
		public Text characterDescriptionText;
		public Image characterPortraitImage;
		public List<ButtonController> characterButtons;
		public List<Image> characterLightBeams;
		public List<Sprite> characterPortraits;
	}
}