using System;
using System.Collections.Generic;
using MainMenu.Title;
using Services.Audio;
using UnityEngine;
using Utils;
using Zenject;

namespace MainMenu.CharacterSelection
{
	public class CharacterSelectionScreenController : IInitializable, IDisposable
	{
		[Inject] public CharacterSelectionScreenAdapter characterSelectionScreenAdapter { get; private set; }
		[Inject] public TitleScreenController titleScreenController { get; private set; }
		[Inject] public CharacterModel characterModel { get; private set; }
		[Inject] public AudioManager audioManager { get; private set; }

		public void Initialize()
		{
			foreach (var button in characterSelectionScreenAdapter.characterButtons)
			{
				button.ButtonMouseEnter += OnButtonMouseEnter;
				button.ButtonMouseClick += ValidateCharacter;
			}

			foreach (var beam in characterSelectionScreenAdapter.characterLightBeams)
			{
				beam.gameObject.SetActive(false);
			}

			SelectCharacter(3);
			Hide();
		}

		public void Dispose()
		{
			foreach (var button in characterSelectionScreenAdapter.characterButtons)
			{
				button.ButtonMouseEnter -= OnButtonMouseEnter;
				button.ButtonMouseClick -= ValidateCharacter;
			}
		}

		public void Show()
		{
			characterSelectionScreenAdapter.characterSelectionScreen.gameObject.SetActive(true);
		}

		public void Hide()
		{
			characterSelectionScreenAdapter.characterSelectionScreen.gameObject.SetActive(false);
		}

		private void OnButtonMouseEnter(string id)
		{
			audioManager.Play(SoundData.SoundId.DROP);
			int index = Int32.Parse(id);
			SelectCharacter(index);
		}

		private void SelectCharacter(int index)
		{
			foreach (var beam in characterSelectionScreenAdapter.characterLightBeams)
			{
				beam.gameObject.SetActive(false);
			}

			var selectedBeam = characterSelectionScreenAdapter.characterLightBeams[index];
			selectedBeam.gameObject.SetActive(true);
			var portrait = characterSelectionScreenAdapter.characterPortraits[index];
			var name = characterModel.characterNames[index];
			var description = characterModel.characterDescriptions[index];
			characterSelectionScreenAdapter.characterNameText.text = name;
			characterSelectionScreenAdapter.characterDescriptionText.text = description;
			characterSelectionScreenAdapter.characterPortraitImage.sprite = portrait;
		}

		private void ValidateCharacter(string id, bool leftClick)
		{
			audioManager.Play(SoundData.SoundId.CINEMATIC_IMPACT);
			Hide();
			int index = Int32.Parse(id);
            characterModel.InitializeCharacterModel(index);
            titleScreenController.ShowDisclaimerScreen();
		}

       
	}
}