namespace MainMenu
{
	public class MainMenuModel
	{
		public enum MainMenuScreen
		{
			TitleScreen,
			SaveGame,
			LoadGame,
			Options,
			Disclaimer,
			HowToPlay,
			CharacterSelection,
            Gameplay
		}

		public MainMenuScreen screen = MainMenuScreen.Gameplay;
	}
}