using UnityEngine;
using UnityEngine.UI;

namespace MainMenu.Title
{
	public class TitleScreenAdapter : MonoBehaviour
	{
        public RectTransform titleScreen;
        public Button newGameButton;
		public Button saveGameButton;
		public Button loadGameButton;
		public Button optionsButton;
		public Button exitButton;
		public Button continueButton;
		public Text creditsText;
	}
}