using System;
using DG.Tweening;
using Gameplay.Progression;
using MainMenu.CharacterSelection;
using MainMenu.Disclaimer;
using MainMenu.HowToPlay;
using MainMenu.Options;
using MainMenu.Save;
using Project.Loading;
using Project.Progression;
using Services.Audio;
using Services.SceneLoader;
using UnityEngine;
using Utils;
using Zenject;

namespace MainMenu.Title
{
	public class TitleScreenController : IInitializable, IDisposable
	{
		[Inject] public SignalBus signalBus { get; private set; }
		[Inject] public TitleScreenAdapter titleScreenAdapter { get; private set; }
		[Inject] public SceneLoaderService sceneLoaderService { get; private set; }
		[Inject] public SaveLoadGameScreenController saveLoadGameScreenController { get; private set; }
		[Inject] public CharacterSelectionScreenController characterSelectionScreenController { get; private set; }
		[Inject] public DisclaimerScreenController disclaimerScreenController { get; private set; }
		[Inject] public HowToPlayScreenController howToPlayScreenController { get; private set; }
		[Inject] public OptionsScreenController optionsScreenController { get; private set; }
		[Inject] public MainMenuModel mainMenuModel { get; private set; }
		[Inject] public ProgressionController progressionController { get; private set; }
		[Inject] public SavedGamesModel savedGamesModel { get; private set; }
		[Inject] public AudioManager audioManager { get; private set; }

		private Sequence sequence;

		public void Initialize()
		{
			titleScreenAdapter.newGameButton.onClick.AddListener(OnNewGameButtonClick);
			titleScreenAdapter.saveGameButton.onClick.AddListener(OnSaveGameButtonClick);
			titleScreenAdapter.loadGameButton.onClick.AddListener(OnLoadGameButtonClick);
			titleScreenAdapter.optionsButton.onClick.AddListener(OnOptionsButtonClick);
			titleScreenAdapter.exitButton.onClick.AddListener(OnExitButtonClick);
			titleScreenAdapter.continueButton.onClick.AddListener(OnContinueButtonClick);

            ShowTitleScreen();

            progressionController.LoadGameProgression();

			audioManager.StopAllSounds();
			signalBus.Fire(new ShowLoadingScreenSignal(false));
		}

		public void Dispose()
		{
			titleScreenAdapter.newGameButton.onClick.RemoveListener(OnNewGameButtonClick);
			titleScreenAdapter.saveGameButton.onClick.RemoveListener(OnSaveGameButtonClick);
			titleScreenAdapter.loadGameButton.onClick.RemoveListener(OnLoadGameButtonClick);
			titleScreenAdapter.optionsButton.onClick.RemoveListener(OnOptionsButtonClick);
			titleScreenAdapter.exitButton.onClick.RemoveListener(OnExitButtonClick);
			DestroySequence();
		}

        public void Show()
        {
            titleScreenAdapter.titleScreen.gameObject.SetActive(true);
			DestroySequence();
			CreateSequence();
        }

        public void Hide()
        {
            titleScreenAdapter.titleScreen.gameObject.SetActive(false);
			DestroySequence();
        }

        public void ShowContinueButton(bool value)
        {
            titleScreenAdapter.continueButton.gameObject.SetActive(value);
        }

        public void ShowTitleScreen()
        {
            mainMenuModel.screen = MainMenuModel.MainMenuScreen.TitleScreen;
            ShowContinueButton(savedGamesModel.isGameRunning);
            Show();
            characterSelectionScreenController.Hide();
            saveLoadGameScreenController.Hide();
            optionsScreenController.Hide();
            howToPlayScreenController.Hide();
            disclaimerScreenController.Hide();
        }

        public void HideTitleScreen()
        {
            Hide();            
        }

        private void OnNewGameButtonClick()
		{
            HideTitleScreen();
            mainMenuModel.screen = MainMenuModel.MainMenuScreen.CharacterSelection;
            characterSelectionScreenController.Show();
			audioManager.Play(SoundData.SoundId.GENERIC_CLICK);
		}
  
		public void ShowDisclaimerScreen()
		{
            HideTitleScreen();
            mainMenuModel.screen = MainMenuModel.MainMenuScreen.Disclaimer;
			disclaimerScreenController.Show();
		}

		public void ShowHowToPlayScreen()
		{
            HideTitleScreen();
            mainMenuModel.screen = MainMenuModel.MainMenuScreen.HowToPlay;
			howToPlayScreenController.Show();
		}

		public void StartNewGame()
		{
            signalBus.Fire(new ShowLoadingScreenSignal(true));

			progressionController.ResetProgression();
            mainMenuModel.screen = MainMenuModel.MainMenuScreen.Gameplay;
            sceneLoaderService.LoadScene(SceneLoaderModel.GAMEPLAY_SCENE);
		}

        private void OnContinueButtonClick()
        {
            signalBus.Fire(new ShowLoadingScreenSignal(true));

            mainMenuModel.screen = MainMenuModel.MainMenuScreen.Gameplay;
            sceneLoaderService.LoadScene(SceneLoaderModel.GAMEPLAY_SCENE);
        }

        private void OnSaveGameButtonClick()
		{
            HideTitleScreen();
            mainMenuModel.screen = MainMenuModel.MainMenuScreen.SaveGame;
			saveLoadGameScreenController.Show();
			audioManager.Play(SoundData.SoundId.GENERIC_CLICK);
		}

		private void OnLoadGameButtonClick()
		{
            HideTitleScreen();
            mainMenuModel.screen = MainMenuModel.MainMenuScreen.LoadGame;
			saveLoadGameScreenController.Show();
			audioManager.Play(SoundData.SoundId.GENERIC_CLICK);
		}

		private void OnOptionsButtonClick()
		{
            HideTitleScreen();
            mainMenuModel.screen = MainMenuModel.MainMenuScreen.Options;
			optionsScreenController.ShowScreen();
			audioManager.Play(SoundData.SoundId.GENERIC_CLICK);
		}

		private void OnExitButtonClick()
		{
			Application.Quit();
			audioManager.Play(SoundData.SoundId.GENERIC_CLICK);
		}

		private void DestroySequence()
		{
			if (sequence != null)
			{
				sequence.Kill();
				sequence = null;
			}
		}

		private void CreateSequence()
		{
			sequence = DOTween.Sequence();
			sequence.Append(titleScreenAdapter.creditsText.rectTransform.DOMoveX(-30f, 50f));
			sequence.SetLoops(-1);
			sequence.Play();
		}
	}
}