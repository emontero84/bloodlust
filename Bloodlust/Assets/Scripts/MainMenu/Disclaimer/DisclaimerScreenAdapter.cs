using UnityEngine;
using UnityEngine.UI;

namespace MainMenu.Disclaimer
{
	public class DisclaimerScreenAdapter : MonoBehaviour
	{
		public RectTransform disclaimerScreen;
		public Button continueButton;
		public Text pressAnyKeyToContinueText;
		public float pressAnyKeyFadeDuration = 1f;
		public Text disclaimerText;
	}
}