using System;
using System.Collections;
using DG.Tweening;
using MainMenu.HowToPlay;
using MainMenu.Title;
using Services.Audio;
using UnityEngine;
using Utils;
using Zenject;

namespace MainMenu.Disclaimer
{
	public class DisclaimerScreenController : IInitializable, IDisposable, ITickable
	{
		[Inject] public DisclaimerScreenAdapter disclaimerScreenAdapter { get; private set; }
		[Inject] public TitleScreenController titleScreenController { get; private set; }
        [Inject] public MainMenuModel mainMenuModel { get; private set; }
        [Inject] public SignalBus signalBus { get; private set; }
		[Inject] public AsyncProcessor asyncProcessor { get; private set; }
		[Inject] public AudioManager audioManager { get; private set; }

		private Sequence sequence;
		private Coroutine coroutine;

		public void Initialize()
		{
			disclaimerScreenAdapter.continueButton.onClick.AddListener(OnContinueButtonClick);

			Hide();
		}

		public void Dispose()
		{
			disclaimerScreenAdapter.continueButton.onClick.RemoveListener(OnContinueButtonClick);
			DestroySequence();
			StopAllCoroutines();
		}

		public void Tick()
		{
            if (mainMenuModel.screen == MainMenuModel.MainMenuScreen.Disclaimer)
            {
                if (Input.anyKey)
                {
                    if (Input.GetKey(KeyCode.Mouse0) || Input.GetKey(KeyCode.Mouse1))
                    {
                        return;
                    }

                    OnContinueButtonClick();
                }
            }
		}

		private void OnContinueButtonClick()
		{
			signalBus.Fire(new PlayAudioSignal(SoundData.SoundId.GENERIC_CLICK));
			Hide();
			titleScreenController.ShowHowToPlayScreen();
		}

		private void DestroySequence()
		{
			if (sequence != null)
			{
				sequence.Kill();
				sequence = null;
			}
		}

		public void Show()
		{
			disclaimerScreenAdapter.disclaimerScreen.gameObject.SetActive(true);
			DestroySequence();
			sequence = DOTween.Sequence();
			sequence.Append(disclaimerScreenAdapter.pressAnyKeyToContinueText.DOFade(0, disclaimerScreenAdapter.pressAnyKeyFadeDuration));
			sequence.Append(disclaimerScreenAdapter.pressAnyKeyToContinueText.DOFade(1, disclaimerScreenAdapter.pressAnyKeyFadeDuration));
			sequence.SetLoops(-1);
			sequence.Play();

			StartTypewriterEffect();
		}

		public void Hide()
		{
			disclaimerScreenAdapter.disclaimerScreen.gameObject.SetActive(false);
			DestroySequence();
			StopAllCoroutines();
		}

		private void StopAllCoroutines()
		{
			if (coroutine != null)
			{
				if (asyncProcessor != null)
				{
					asyncProcessor.StopCoroutine(coroutine);
				}

				coroutine = null;
			}
		}

		public void StartTypewriterEffect()
		{
			StopAllCoroutines();
			coroutine = asyncProcessor.StartCoroutine(TypewriterEffectCoroutine(disclaimerScreenAdapter.disclaimerText.text));
		}

		private IEnumerator TypewriterEffectCoroutine(string dialogText)
		{
			disclaimerScreenAdapter.disclaimerText.text = StringConstants.COLOR_START_TAG + dialogText + StringConstants.COLOR_END_TAG;

			for (int i = 0; i < dialogText.Length; i++)
			{
				yield return new WaitForSeconds(0.03f);
				audioManager.Play(SoundData.SoundId.DIALOG_BEEP);

				var dialogPre = dialogText.Substring(0, i);
				var dialogPost = dialogText.Substring(i, dialogText.Length - 1 - i);
				disclaimerScreenAdapter.disclaimerText.text = dialogPre + StringConstants.COLOR_START_TAG + dialogPost + StringConstants.COLOR_END_TAG;
			}

			disclaimerScreenAdapter.disclaimerText.text = dialogText;
		}

	}
}