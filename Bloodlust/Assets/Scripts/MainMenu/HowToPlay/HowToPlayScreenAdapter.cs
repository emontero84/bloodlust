using UnityEngine;
using UnityEngine.UI;

namespace MainMenu.HowToPlay
{
	public class HowToPlayScreenAdapter : MonoBehaviour
	{
		public RectTransform howToPlayScreen;
		public Button continueButton;
		public Text pressAnyKeyToContinueText;
		public float pressAnyKeyFadeDuration = 1f;
	}
}