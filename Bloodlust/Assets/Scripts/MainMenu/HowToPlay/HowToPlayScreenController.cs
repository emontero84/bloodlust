using System;
using DG.Tweening;
using MainMenu.Title;
using Services.Audio;
using UnityEngine;
using Zenject;

namespace MainMenu.HowToPlay
{
	public class HowToPlayScreenController : IInitializable, IDisposable, ITickable
	{
		[Inject] public HowToPlayScreenAdapter howToPlayScreenAdapter { get; private set; }
		[Inject] public TitleScreenController titleScreenController { get; private set; }
        [Inject] public MainMenuModel mainMenuModel { get; private set; }
		[Inject] public SignalBus signalBus { get; private set; }

		private Sequence sequence;

		public void Initialize()
		{
            howToPlayScreenAdapter.continueButton.onClick.AddListener(OnContinueButtonClick);

            Hide();
		}

		public void Dispose()
		{
			howToPlayScreenAdapter.continueButton.onClick.RemoveListener(OnContinueButtonClick);
			DestroySequence();
		}

		public void Tick()
		{
            if (mainMenuModel.screen == MainMenuModel.MainMenuScreen.HowToPlay)
            {
                if (Input.anyKey)
                {
                    if (Input.GetKey(KeyCode.Mouse0) || Input.GetKey(KeyCode.Mouse1))
                    {
                        return;
                    }

                    OnContinueButtonClick();
                }
            }
		}

		private void OnContinueButtonClick()
		{
			signalBus.Fire(new PlayAudioSignal(SoundData.SoundId.GENERIC_CLICK));
			Hide();
			titleScreenController.StartNewGame();
		}

		private void DestroySequence()
		{
			if (sequence != null)
			{
				sequence.Kill();
				sequence = null;
			}
		}

		public void Show()
		{
			howToPlayScreenAdapter.howToPlayScreen.gameObject.SetActive(true);
			DestroySequence();
			sequence = DOTween.Sequence();
			sequence.Append(howToPlayScreenAdapter.pressAnyKeyToContinueText.DOFade(0, howToPlayScreenAdapter.pressAnyKeyFadeDuration));
			sequence.Append(howToPlayScreenAdapter.pressAnyKeyToContinueText.DOFade(1, howToPlayScreenAdapter.pressAnyKeyFadeDuration));
			sequence.SetLoops(-1);
			sequence.Play();
        }

        public void Hide()
		{
			howToPlayScreenAdapter.howToPlayScreen.gameObject.SetActive(false);
		}
	}
}