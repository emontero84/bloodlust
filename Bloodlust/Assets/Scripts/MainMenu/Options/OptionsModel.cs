namespace MainMenu.Options
{
	public class OptionsModel
	{
		public float soundVolume = 1f;
		public float musicVolume = 1f;
		public float walkSpeed = 0f;
	}
}