using System;
using System.Xml.Linq;
using DG.Tweening;
using MainMenu.Title;
using Services.Audio;
using UnityEngine;
using Utils;
using Zenject;

namespace MainMenu.Options
{
	public class OptionsScreenController : IInitializable, IDisposable
	{
		[Inject] public OptionsScreenAdapter optionsScreenAdapter { get; private set; }
		[Inject] public MainMenuModel mainMenuModel { get; private set; }
		[Inject] public OptionsModel optionsModel { get; private set; }
		[Inject] public TitleScreenController titleScreenController { get; private set; }
		[Inject] public AudioManager audioManager { get; private set; }
		[Inject] public SignalBus signalBus { get; private set; }

		private Sequence sequence;

		public void Initialize()
		{
			optionsScreenAdapter.backButton.onClick.AddListener(OnBackButtonClick);
			optionsScreenAdapter.soundSlider.onValueChanged.AddListener(OnSoundSliderValueChanged);
			optionsScreenAdapter.musicSlider.onValueChanged.AddListener(OnMusicSliderValueChanged);
			optionsScreenAdapter.walkSpeedSlider.onValueChanged.AddListener(OnWalkSpeedSliderValueChanged);
			Hide();
		}

		public void Dispose()
		{
			optionsScreenAdapter.backButton.onClick.RemoveListener(OnBackButtonClick);
			optionsScreenAdapter.soundSlider.onValueChanged.RemoveListener(OnSoundSliderValueChanged);
			optionsScreenAdapter.musicSlider.onValueChanged.RemoveListener(OnSoundSliderValueChanged);
			optionsScreenAdapter.walkSpeedSlider.onValueChanged.RemoveListener(OnSoundSliderValueChanged);
		}

		private void OnBackButtonClick()
		{
			SaveOptions();
			signalBus.Fire(new PlayAudioSignal(SoundData.SoundId.GENERIC_CLICK));
			mainMenuModel.screen = MainMenuModel.MainMenuScreen.TitleScreen;
			Hide();
            titleScreenController.ShowTitleScreen();

        }

		private void OnSoundSliderValueChanged(float value)
		{
			audioManager.SetSoundVolume(value);
			optionsModel.soundVolume = value;
		}

		private void OnMusicSliderValueChanged(float value)
		{
			audioManager.SetMusicVolume(value);
			optionsModel.musicVolume = value;
		}

		private void OnWalkSpeedSliderValueChanged(float value)
		{
			optionsModel.walkSpeed = value;
		}

		public void ShowScreen()
		{
			LoadOptions();

			optionsScreenAdapter.soundSlider.value = optionsModel.soundVolume;
			optionsScreenAdapter.musicSlider.value = optionsModel.musicVolume;
			optionsScreenAdapter.walkSpeedSlider.value = optionsModel.walkSpeed;
			optionsScreenAdapter.optionsScreen.gameObject.SetActive(true);
		}

		private void SaveOptions()
		{
			PlayerPrefs.SetFloat(StringConstants.SOUND_VOLUME, optionsModel.soundVolume);
			PlayerPrefs.SetFloat(StringConstants.MUSIC_VOLUME, optionsModel.musicVolume);
			PlayerPrefs.SetFloat(StringConstants.WALK_SPEED, optionsModel.walkSpeed);
		}

		private void LoadOptions()
		{
			if (PlayerPrefs.HasKey(StringConstants.SOUND_VOLUME))
			{
				optionsModel.soundVolume = PlayerPrefs.GetFloat(StringConstants.SOUND_VOLUME);
			}

			if (PlayerPrefs.HasKey(StringConstants.MUSIC_VOLUME))
			{
				optionsModel.musicVolume = PlayerPrefs.GetFloat(StringConstants.MUSIC_VOLUME);
			}

			if (PlayerPrefs.HasKey(StringConstants.WALK_SPEED))
			{
				optionsModel.walkSpeed = PlayerPrefs.GetFloat(StringConstants.WALK_SPEED);
			}
		}

		public void Hide()
		{
			optionsScreenAdapter.optionsScreen.gameObject.SetActive(false);
		}
	}
}