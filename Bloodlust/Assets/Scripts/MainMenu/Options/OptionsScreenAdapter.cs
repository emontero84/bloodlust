using UnityEngine;
using UnityEngine.UI;

namespace MainMenu.Options
{
	public class OptionsScreenAdapter : MonoBehaviour
	{
		public RectTransform optionsScreen;
		public Button backButton;
		public Slider soundSlider;
		public Slider musicSlider;
		public Slider walkSpeedSlider;
	}
}