using System.Collections.Generic;
using Gameplay.Adventure.Room;
using Services.Story;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Gameplay.Cheats
{
	public class CheatsAdapter : MonoBehaviour
	{
		[Inject] public SignalBus signalBus;
		[Inject] public StoryService storyService;

		public RoomModel.Room roomToGo;
		public Button goToRoomBbutton;

		public void Start()
		{
			goToRoomBbutton.onClick.AddListener(OnGoToRoomButtonClick);
		}

		private void OnGoToRoomButtonClick()
		{
			signalBus.Fire(new StorySignal() {externalFunction = "GoToRoom", arg1 = "Room_" + roomToGo.ToString()});

			storyService.ChoosePathString(roomToGo.ToString());
		}
	}
}