using Zenject;

namespace Gameplay.Cheats
{
	public class CheatsInstaller : MonoInstaller
	{
		public override void InstallBindings()
		{
			Container.BindInterfacesAndSelfTo<CheatsAdapter>().FromComponentInHierarchy().AsSingle();
		}
	}
}