using System;
using Gameplay.Dialog.Dialog;
using UnityEngine;
using UnityEngine.UI;
using Utils;

namespace Gameplay.Adventure.QuestLog
{
	public class QuestLogAdapter : MonoBehaviour
	{
		public TextAsset questConfiguration;
		public RectTransform questLogPanel;
		public Text questLogTitle;
		public Text questLogDescription;
		public Text questLogIndex;
		public Button closeButton;
		public Button prevButton;
		public Button nextButton;
		public Image questLogUpdatedIcon;

		public Animator questLogInventory;
		private static readonly int QuestLogUpdated = Animator.StringToHash("QuestLogUpdated");
		
		public void ShowQuestLogUI()
		{
			questLogPanel.gameObject.SetActive(true);
		}

		public void HideQuestLogUI()
		{
			questLogPanel.gameObject.SetActive(false);
		}

		public void SetActiveQuest(string title, string description)
		{
			questLogTitle.text = title;
			questLogDescription.text = description;
		}

		public void SetQuestIndex(int currentIndex, int maxIndex)
		{
			questLogIndex.text = StringConstants.QUEST_INDEX_TEXT.Replace("%CURRENT%", currentIndex.ToString()).Replace("%MAX%", maxIndex.ToString());
		}

		public void ShowPrevButton(bool value)
		{
			prevButton.gameObject.SetActive(value);
		}
		
		public void ShowNextButton(bool value)
		{
			nextButton.gameObject.SetActive(value);
		}

		public void PlayQuestLogUpdatedAnimation()
		{
			questLogInventory.Play(QuestLogAdapter.QuestLogUpdated);
		}
	}
}