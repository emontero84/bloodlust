using System;
using System.Collections.Generic;

namespace Gameplay.Adventure.QuestLog.Configuration
{
	[Serializable]
	public class QuestLogConfigurationData
	{
		public List<QuestConfigurationData> quests;
	}
}