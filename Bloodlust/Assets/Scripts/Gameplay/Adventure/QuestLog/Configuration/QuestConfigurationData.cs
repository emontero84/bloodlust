using System;
using System.Collections.Generic;

namespace Gameplay.Adventure.QuestLog.Configuration
{
	[Serializable]
	public class QuestConfigurationData
	{
		public string id;
		public string title;
		public string description;
		public List<QuestStepData> steps;
	}
}