using System.Collections.Generic;

namespace Gameplay.Adventure.QuestLog.Progression
{
	[System.Serializable]
	public class QuestProgressionData
	{
		public string id;
		public List<int> completedSteps;

		public QuestProgressionData()
		{
			completedSteps = new List<int>();
		}

		public QuestProgressionData(string id, List<int> completedSteps)
		{
			this.id = id;
			this.completedSteps = new List<int>();
			for (var i = 0; i < completedSteps.Count; i++)
			{
				var step = completedSteps[i];
				this.completedSteps.Add(step);
			}
		}
	}
}