using System.Collections.Generic;

namespace Gameplay.Adventure.QuestLog.Progression
{
	[System.Serializable]
	public class QuestLogProgressionData
	{
		public List<QuestProgressionData> activeQuests;
		public int currentQuest;

		public QuestLogProgressionData()
		{
			activeQuests = new List<QuestProgressionData>();
		}

		public QuestLogProgressionData(List<QuestProgressionData> quests, int index)
		{
			activeQuests = new List<QuestProgressionData>();
			foreach (var quest in quests)
			{
				activeQuests.Add(new QuestProgressionData(quest.id, quest.completedSteps));
			}

			currentQuest = index;
		}
	}
}