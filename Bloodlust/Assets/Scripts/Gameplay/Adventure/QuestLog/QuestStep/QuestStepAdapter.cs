using UnityEngine;
using UnityEngine.UI;

namespace Gameplay.Adventure.QuestLog.QuestStep
{
	public class QuestStepAdapter : MonoBehaviour
	{
		public RectTransform stepPanel;
		public Text text;
		public Image check;

		public void SetText(string stepText)
		{
			text.text = stepText;
		}
		
		public void ShowStep()
		{
			stepPanel.gameObject.SetActive(true);
		}
		
		public void HideStep()
		{
			stepPanel.gameObject.SetActive(false);
		}

		public void MarkAsComplete(bool value)
		{
			check.gameObject.SetActive(value);
		}
	}
}