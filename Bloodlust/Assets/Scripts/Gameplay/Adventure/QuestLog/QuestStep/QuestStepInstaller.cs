using Zenject;

namespace Gameplay.Adventure.QuestLog.QuestStep
{
	public class QuestStepInstaller : MonoInstaller
	{
		public QuestStepAdapter questStepAdapter;
		public override void InstallBindings()
		{
			Container.BindInterfacesAndSelfTo<QuestStepAdapter>().FromInstance(questStepAdapter).AsSingle();
		}
	}
}