namespace Gameplay.Adventure.QuestLog
{
	public class QuestLogSignal
	{
		public enum Type
		{
			SHOW_QUEST_LOG,
			HIDE_QUEST_LOG,
			ADD_QUEST,
			UPDATE_QUEST
		}
	}
}