using Zenject;

namespace Gameplay.Adventure.QuestLog
{
	public class ShowQuestLogCommand
	{
		[Inject] public QuestLogController questLogController { get; private set; }
		
		public void OnSignal(QuestLogSignal signal)
		{
			questLogController.ShowQuestLogUI();	
		}
	}
}