using System.Collections.Generic;
using Gameplay.Adventure.QuestLog.Configuration;
using Gameplay.Adventure.QuestLog.Progression;

namespace Gameplay.Adventure.QuestLog
{
	public class QuestLogModel
	{
		public bool isInitialized;
		public bool isQuestLogVisible;
		public QuestLogConfigurationData questLogConfigurationData;
		public QuestLogProgressionData questLogProgressionData;
		public string lastQuestUpdated;
	}
}