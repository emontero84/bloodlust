using System;
using System.Collections.Generic;
using Gameplay.Adventure.QuestLog.Configuration;
using Gameplay.Adventure.QuestLog.Progression;
using Gameplay.Adventure.QuestLog.QuestStep;
using Gameplay.Dialog.Dialog;
using MainMenu;
using Services.Analytics;
using Services.Audio;
using Services.Story;
using UnityEngine;
using Utils;
using Zenject;

namespace Gameplay.Adventure.QuestLog
{
	public class QuestLogController : IInitializable, IDisposable, ITickable
	{
		[Inject] public SignalBus signalBus { get; private set; }
		[Inject] public QuestLogAdapter questLogAdapter { get; private set; }
		[Inject] public QuestLogModel questLogModel { get; private set; }
		[Inject] public StoryService storyService { get; private set; }
		[Inject] public List<QuestStepAdapter> questStepAdapters { get; private set; }
        [Inject] public MainMenuModel mainMenuModel { get; private set; }

        public void Initialize()
		{
			signalBus.Subscribe<StorySignal>(OnStorySignal);

			questLogAdapter.closeButton.onClick.AddListener(OnCloseButtonClick);
			questLogAdapter.nextButton.onClick.AddListener(OnNextButtonClick);
			questLogAdapter.prevButton.onClick.AddListener(OnPrevButtonClick);

			HideQuestLogUI();

			if (!questLogModel.isInitialized)
			{
				LoadQuestConfiguration();
				ShowQuestLogUpdatedIcon(false);
			}

			questLogModel.questLogProgressionData = new QuestLogProgressionData();
			questLogModel.questLogProgressionData.activeQuests = new List<QuestProgressionData>();

			questLogModel.isInitialized = true;
			signalBus.Fire<InitializeSignal>();
		}

		public void Dispose()
		{
			signalBus.Unsubscribe<StorySignal>(OnStorySignal);

			questLogAdapter.closeButton.onClick.RemoveListener(OnCloseButtonClick);
			questLogAdapter.nextButton.onClick.RemoveListener(OnNextButtonClick);
			questLogAdapter.prevButton.onClick.RemoveListener(OnPrevButtonClick);
		}

		public void Tick()
		{
            if (mainMenuModel.screen == MainMenuModel.MainMenuScreen.Gameplay)
            {
			    if (Input.GetKeyUp(KeyCode.Q))
			    {
				    if (!questLogModel.isQuestLogVisible)
				    {
					    ShowQuestLogUI();
				    }
				    else
				    {
					    HideQuestLogUI();
				    }
			    }
            }
		}
		
		private void ShowQuestLogUpdatedIcon(bool value)
		{
			questLogAdapter.questLogUpdatedIcon.gameObject.SetActive(value);
		}

		private void OnStorySignal(StorySignal signal)
		{
			switch (signal.externalFunction)
			{
				case StringConstants.ADD_QUEST_TO_LOG:
					var questId = signal.arg1;
					AddQuest(questId);
					ShowQuestLogUpdatedIcon(true);
					questLogModel.lastQuestUpdated = questId;
					break;
				case StringConstants.UPDATE_QUEST_TO_LOG:
					var quest = signal.arg1;
					var stepString = signal.arg2.Split('_')[1];
					int step = Int32.Parse(stepString);
					int index = step - 1;
					var updated = UpdateQuest(quest, index);
					if (updated)
					{
						ShowQuestLogUpdatedIcon(true);
						questLogModel.lastQuestUpdated = quest;
					}
					break;
			}
		}

		private void OnCloseButtonClick()
		{
			signalBus.Fire(new PlayAudioSignal(SoundData.SoundId.CANCEL_CLICK));

			signalBus.Fire(new AnalyticsSignal(AnalyticsModel.QUEST_LOG_CLOSE));

			HideQuestLogUI();
		}

		private void OnNextButtonClick()
		{
			signalBus.Fire(new PlayAudioSignal(SoundData.SoundId.GENERIC_CLICK));

			questLogModel.questLogProgressionData.currentQuest++;
			if (questLogModel.questLogProgressionData.currentQuest >= questLogModel.questLogProgressionData.activeQuests.Count)
			{
				questLogModel.questLogProgressionData.currentQuest = 0;
			}

			ShowCurrentQuest();
		}

		private void OnPrevButtonClick()
		{
			signalBus.Fire(new PlayAudioSignal(SoundData.SoundId.GENERIC_CLICK));

			questLogModel.questLogProgressionData.currentQuest--;
			if (questLogModel.questLogProgressionData.currentQuest < 0)
			{
				questLogModel.questLogProgressionData.currentQuest = questLogModel.questLogProgressionData.activeQuests.Count - 1;
			}

			ShowCurrentQuest();
		}

		public void ShowQuestLogUI()
		{
			if (questLogModel.questLogProgressionData.activeQuests.Count == 0)
			{
				Debug.LogError("[QuestLogController] ShowQuestLogUI: There are no active quests!");
				return;
			}

			signalBus.Fire(new AnalyticsSignal(AnalyticsModel.QUEST_LOG_OPEN));

			// Find last updated quest
			if (questLogModel.lastQuestUpdated != null && questLogModel.lastQuestUpdated.Length > 0)
			{
				var questId = questLogModel.lastQuestUpdated;
				for (int i = 0; i < questLogModel.questLogProgressionData.activeQuests.Count; i++)
				{
					QuestProgressionData quest = questLogModel.questLogProgressionData.activeQuests[i];
					if (quest.id == questId)
					{
						questLogModel.questLogProgressionData.currentQuest = i;
						break;
					}
				}

			} 
				
			ShowCurrentQuest();
								

			if (questLogModel.questLogProgressionData.activeQuests.Count == 1)
			{
				questLogAdapter.ShowNextButton(false);
				questLogAdapter.ShowPrevButton(false);
			}
			else
			{
				questLogAdapter.ShowNextButton(true);
				questLogAdapter.ShowPrevButton(true);
			}
			questLogAdapter.ShowQuestLogUI();
			questLogModel.isQuestLogVisible = true;
			ShowQuestLogUpdatedIcon(false);
			questLogModel.lastQuestUpdated = null;
		}

		public void ShowCurrentQuest()
		{
			if (questLogModel.questLogProgressionData.activeQuests.Count == 0)
			{
				Debug.LogError("[QuestLogController] ShowCurrentQuest: There are no active quests!");
				return;
			}

			var currentActiveQuestIndex = questLogModel.questLogProgressionData.currentQuest;
			var currentQuest = questLogModel.questLogProgressionData.activeQuests[currentActiveQuestIndex];
			var questConfiguration = GetQuestConfiguration(currentQuest.id);

			questLogAdapter.SetActiveQuest(questConfiguration.title, questConfiguration.description);
			questLogAdapter.SetQuestIndex(currentActiveQuestIndex + 1, questLogModel.questLogProgressionData.activeQuests.Count);

			// Hide all steps
			for (var i = 0; i < questStepAdapters.Count; i++)
			{
				var stepController = questStepAdapters[i];
				stepController.HideStep();
			}

			// Show the steps of the current quest
			var steps = questConfiguration.steps;
			for (var i = 0; i < steps.Count; i++)
			{
				var step = steps[i];
				if (i >= questStepAdapters.Count)
				{
					Debug.LogError("[QuestLogController] Too many steps in quest " + currentQuest.id);
					continue;
				}

				var stepController = questStepAdapters[i];
				var stepText = step.step;

				stepController.SetText(stepText);
				stepController.MarkAsComplete(currentQuest.completedSteps.Contains(i));
				stepController.ShowStep();
			}
		}

		public void HideQuestLogUI()
		{
			questLogAdapter.HideQuestLogUI();
			questLogModel.isQuestLogVisible = false;
		}

		private void LoadQuestConfiguration()
		{
			QuestLogConfigurationData configuration = JsonUtility.FromJson<QuestLogConfigurationData>(questLogAdapter.questConfiguration.text);
			questLogModel.questLogConfigurationData = configuration;

#if UNITY_EDITOR
			// Check the length of all the quest texts (only in Editor)
			var quests = questLogModel.questLogConfigurationData.quests;
			for (var i = 0; i < quests.Count; i++)
			{
				var quest = quests[i];
				if (quest.title.Length > DialogModel.MAX_CHARACTERS_PER_QUEST_TILE)
				{
					Debug.LogError("[QuestLogController] LoadQuestConfiguration: quest title too long! length=" + quest.title.Length + "/" + DialogModel.MAX_CHARACTERS_PER_QUEST_TILE + ",  quest=" + quest.id);
				}

				if (quest.description.Length > DialogModel.MAX_CHARACTERS_PER_QUEST_DESCRIPTION)
				{
					Debug.LogError("[QuestLogController] LoadQuestConfiguration: quest description too long! length=" + quest.description.Length + "/" + DialogModel.MAX_CHARACTERS_PER_QUEST_DESCRIPTION + ",  quest=" + quest.id);
				}

				for (var index = 0; index < quest.steps.Count; index++)
				{
					var step = quest.steps[index];
					if (step.step.Length > DialogModel.MAX_CHARACTERS_PER_QUEST_STEP)
					{
						Debug.LogError("[QuestLogController] LoadQuestConfiguration: quest step too long! length=" + step.step.Length + "/" + DialogModel.MAX_CHARACTERS_PER_QUEST_STEP + ",  quest=" + quest.id + ", step=" + index);
					}
				}
			}
#endif
		}

		public void LoadQuestProgression()
		{
			var questsList = storyService.GetVariable(StringConstants.QUESTS_VARIABLE) as Ink.Runtime.InkList;
			var questStepsList = storyService.GetVariable(StringConstants.QUEST_STEPS_VARIABLE) as Ink.Runtime.InkList;
			if (questsList != null && questStepsList != null)
			{
				foreach (var quest in questsList)
				{
					string questId = quest.Key.ToString().Split('.')[1];
					var questSteps = new List<int>();
					foreach (var step in questStepsList)
					{
						string stepString = step.Key.ToString().Split('.')[1];
						var split = stepString.Split('_');
						string questString = split[0];
						if (questId == questString)
						{
							string numberString = split[1];
							int number = Int32.Parse(numberString);
							int index = number - 1;
							questSteps.Add(index);
						}
					}

					questLogModel.questLogProgressionData.activeQuests.Add(new QuestProgressionData(questId, questSteps));
				}
			}
			else
			{
				Debug.LogError("[QuestLogController] There's no quests variable in ink!");
			}

			questLogModel.questLogProgressionData.currentQuest = 0;
		}

		private QuestConfigurationData GetQuestConfiguration(string id)
		{
			var quests = questLogModel.questLogConfigurationData.quests;
			for (var i = 0; i < quests.Count; i++)
			{
				var quest = quests[i];
				if (quest.id == id)
					return quest;
			}

			return null;
		}

		private QuestProgressionData GetQuestProgression(string id)
		{
			var quests = questLogModel.questLogProgressionData.activeQuests;
			for (var i = 0; i < quests.Count; i++)
			{
				var quest = quests[i];
				if (quest.id == id)
					return quest;
			}

			return null;
		}

		private void AddQuest(string id)
		{
			var questConfiguration = GetQuestConfiguration(id);
			if (questConfiguration == null)
			{
				Debug.LogError("[QuestLogController] There's no quest with id: " + id);
				return;
			}

			var questProgression = GetQuestProgression(id);
			if (questProgression != null)
			{
				return;
			}

			var newQuestProgression = new QuestProgressionData();
			newQuestProgression.id = id;
			newQuestProgression.completedSteps = new List<int>();
			questLogModel.questLogProgressionData.activeQuests.Add(newQuestProgression);

			signalBus.Fire(new PlayAudioSignal(SoundData.SoundId.QUEST_UPDATED));
		}

		private bool UpdateQuest(string questId, int step)
		{
			var questConfiguration = GetQuestConfiguration(questId);
			if (step >= 0 && step < questConfiguration.steps.Count)
			{
				var questProgression = GetQuestProgression(questId);
				if (questProgression == null)
				{
					Debug.Log("[QuestLogController] UpdateQuest no progression for quest id: " + questId + " step: " + step);
				}
				else
				{
					if (questProgression.completedSteps.Contains(step))
					{
						Debug.Log("[QuestLogController] UpdateQuest duplicate step progression for quest id: " + questId + " step: " + step);
					}
					else
					{
						questProgression.completedSteps.Add(step);
						
						signalBus.Fire(new PlayAudioSignal(SoundData.SoundId.QUEST_UPDATED));
						return true;
					}
				}
			}
			else
			{
				Debug.Log("[QuestLogController] UpdateQuest no configuration for quest id: " + questId + " step: " + step);
			}
			return false;
		}
	}
}