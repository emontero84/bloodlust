﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Utils;
using Zenject;

namespace Gameplay.Adventure.Inventory
{
	public class InventoryAdapter : MonoBehaviour
	{
		public RectTransform inventoryPanel;
		public List<InventoryData> inventoryPalette;
		public List<Transform> emptyInventorySlots;
		public RectTransform inventoryButtonsContainer;
		public RectTransform inventoryArrowsContainer;
		public Button inventoryUpArrow;
		public Button inventoryDownArrow;
		public Text inventoryItemNameText;
		public Image inventoryItemCursor;

		public void ShowInventoryUI()
		{
			inventoryPanel.gameObject.SetActive(true);
		}

		public void HideInventoryUI()
		{
			inventoryPanel.gameObject.SetActive(false);
		}

		public void ShowInventoryArrows(bool value)
		{
			inventoryArrowsContainer.gameObject.SetActive(value);
		}

		public void SetInventoryNameText(string name)
		{
			inventoryItemNameText.text = name;
		}
	}
}