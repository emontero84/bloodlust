﻿using System;
using System.Collections.Generic;
using Components;
using MainMenu;
using Services.Audio;
using Services.Story;
using UnityEngine;
using Utils;
using Zenject;

namespace Gameplay.Adventure.Inventory
{
	public class InventoryController : IInitializable, IDisposable, ITickable
	{
		[Inject] public SignalBus signalBus { get; private set; }
		[Inject] public InventoryAdapter inventoryAdapter { get; private set; }
		[Inject] public StoryService storyService { get; private set; }
		[Inject] public InventoryModel inventoryModel { get; private set; }
		[Inject] public MainCameraAdapter mainCameraAdapter { get; private set; }
        [Inject] public MainMenuModel mainMenuModel { get; private set; }

        public void Initialize()
		{
			inventoryAdapter.SetInventoryNameText("");
			InitializeDictionaries();
			signalBus.Subscribe<StorySignal>(OnStorySignal);

			inventoryAdapter.inventoryUpArrow.onClick.AddListener(OnInventoryUpArrowClick);
			inventoryAdapter.inventoryDownArrow.onClick.AddListener(OnInventoryDownArrowClick);
		}

		public void Dispose()
		{
			signalBus.Unsubscribe<StorySignal>(OnStorySignal);

			inventoryAdapter.inventoryUpArrow.onClick.RemoveListener(OnInventoryUpArrowClick);
			inventoryAdapter.inventoryDownArrow.onClick.RemoveListener(OnInventoryDownArrowClick);

			if (inventoryModel.inventory != null)
			{
				inventoryModel.inventory.Clear();
				inventoryModel.inventory = null;
			}

			DestroyDictionaries();
		}

		public void Tick()
		{
            if (mainMenuModel.screen == MainMenuModel.MainMenuScreen.Gameplay)
            { 
			    if (inventoryModel.isShowingCursor)
			    {
				    SetMousePositionToCursor();
			    }
            }
		}

		public void LoadInventoryProgression()
		{
			var inventoryList = storyService.GetVariable(StringConstants.INVENTORY_VARIABLE) as Ink.Runtime.InkList;
			inventoryModel.inventory = new List<string>();
			InitInventory();
			foreach (var item in inventoryList)
			{
				string itemId = item.Key.ToString().Split('.')[1];
				AddItemToInventory(itemId);
			}

#if UNITY_EDITOR
			// Check that all inventory items that are present in the story are also present in the palette (only in Editor)
			foreach (var item in inventoryList.all)
			{
				var itemId = item.Key.ToString().Split('.')[1];
				if (!InventoryItemIdIsInDictionary(itemId))
				{
					Debug.LogError("[InventoryController] LoadInventoryProgression: There's no id " + itemId + " in inventory palette dictionary!");
				}
			}
#endif
		}

		private void OnStorySignal(StorySignal signal)
		{
			switch (signal.externalFunction)
			{
				case StringConstants.ADD_ITEM_TO_INVENTORY:
					AddItemToInventory(signal.arg1);
					break;
				case StringConstants.REMOVE_ITEM_FROM_INVENTORY:
					RemoveItemFromInventory(signal.arg1);
					break;
			}
		}

		private void OnInventoryUpArrowClick()
		{
			if (inventoryModel.currentIndex - InventoryModel.INVENTORY_SLOTS_PER_ROW < 0)
				return;

			signalBus.Fire(new PlayAudioSignal(SoundData.SoundId.GENERIC_CLICK));

			for (int i = inventoryModel.currentIndex - InventoryModel.INVENTORY_SLOTS_PER_ROW; i < inventoryModel.currentIndex; i++)
			{
				var inventoryItem = inventoryAdapter.inventoryButtonsContainer.GetChild(i);
				inventoryItem.gameObject.SetActive(true);
			}

			inventoryModel.currentIndex -= InventoryModel.INVENTORY_SLOTS_PER_ROW;

			CheckInventoryArrows();
		}

		private void OnInventoryDownArrowClick()
		{
			if (inventoryModel.currentIndex + InventoryModel.INVENTORY_SLOTS_PER_ROW >= inventoryModel.inventory.Count)
				return;

			signalBus.Fire(new PlayAudioSignal(SoundData.SoundId.GENERIC_CLICK));

			var max = inventoryModel.currentIndex + InventoryModel.INVENTORY_SLOTS_PER_ROW;
			for (int i = inventoryModel.currentIndex; i < max; i++)
			{
				var inventoryItem = inventoryAdapter.inventoryButtonsContainer.GetChild(i);
				inventoryItem.gameObject.SetActive(false);
			}

			inventoryModel.currentIndex += InventoryModel.INVENTORY_SLOTS_PER_ROW;

			CheckInventoryArrows();
		}

		public void InitializeDictionaries()
		{
			DestroyDictionaries();

			// Save inventory palette in two dictionaries by item id and by item name
			inventoryModel.inventoryPaletteDictionaryById = new Dictionary<string, InventoryData>();
			inventoryModel.inventoryPaletteDictionaryByName = new Dictionary<string, InventoryData>();
			for (var i = 0; i < inventoryAdapter.inventoryPalette.Count; i++)
			{
				var data = inventoryAdapter.inventoryPalette[i];
				inventoryModel.inventoryPaletteDictionaryById.Add(data.item, data);
				inventoryModel.inventoryPaletteDictionaryByName.Add(data.name, data);
			}
		}

		public void DestroyDictionaries()
		{
			if (inventoryModel.inventoryPaletteDictionaryById != null)
			{
				inventoryModel.inventoryPaletteDictionaryById.Clear();
				inventoryModel.inventoryPaletteDictionaryById = null;
			}

			if (inventoryModel.inventoryPaletteDictionaryByName != null)
			{
				inventoryModel.inventoryPaletteDictionaryByName.Clear();
				inventoryModel.inventoryPaletteDictionaryByName = null;
			}
		}

		public void InitInventory()
		{
			// Show all inventory items
			foreach (var data in inventoryAdapter.inventoryPalette)
			{
				var transformToShow = data.itemTransform;
				transformToShow.gameObject.SetActive(true);
			}

			// Show all empty slots in the inventory
			foreach (var empty in inventoryAdapter.emptyInventorySlots)
			{
				empty.gameObject.SetActive(true);
			}
		}

		public InventoryData GetInventoryDataById(string item)
		{
			if (inventoryModel.inventoryPaletteDictionaryById.ContainsKey(item))
			{
				var data = inventoryModel.inventoryPaletteDictionaryById[item];
				return data;
			}

			Debug.LogError("[InventoryAdapter] There's no item " + item + " in inventory dictionary!");
			return null;
		}

		public InventoryData GetInventoryDataByName(string name)
		{
			if (inventoryModel.inventoryPaletteDictionaryByName.ContainsKey(name))
			{
				var data = inventoryModel.inventoryPaletteDictionaryByName[name];
				return data;
			}

			Debug.LogError("[InventoryAdapter] There's no item with name " + name + " in inventory dictionary!");
			return null;
		}

		public bool InventoryItemIdIsInDictionary(string itemId)
		{
			return inventoryModel.inventoryPaletteDictionaryById.ContainsKey(itemId);
		}

		public void ShowInventoryItem(string item)
		{
			SetInventoryActive(item, true);
		}

		public void HideInventoryItem(string item)
		{
			SetInventoryActive(item, false);
		}

		private void SetInventoryActive(string item, bool value)
		{
			var data = GetInventoryDataById(item);
			var transformToShow = data.itemTransform;
			transformToShow.gameObject.SetActive(value);
			if (value)
			{
				var button = transformToShow.gameObject.GetComponent<ButtonController>();
				button.id = data.name;
			}
		}

		private void SetInventoryIndex(string item, int index)
		{
			var data = GetInventoryDataById(item);
			var transformToShow = data.itemTransform;
			transformToShow.SetSiblingIndex(index);
		}

		public void ResetInventory()
		{
			inventoryModel.inventory.Clear();
		}

		private void SetMousePositionToCursor()
		{
			var pos = mainCameraAdapter.mainCamera.ScreenToWorldPoint(Input.mousePosition);
			inventoryAdapter.inventoryItemCursor.transform.position = new Vector3(pos.x, pos.y, 0);
		}
		
		public void AddItemToInventory(string item)
		{
			if (!inventoryModel.inventory.Contains(item))
			{
				// Move the item to last available slot
				var indexToAdd = inventoryModel.inventory.Count;

				inventoryModel.inventory.Add(item);
				ShowInventoryItem(item);
				SetInventoryIndex(item, indexToAdd);

				signalBus.Fire(new PlayAudioSignal(SoundData.SoundId.ADD_ITEM_TO_INVENTORY));

				CheckInventoryArrows();
			}
		}

		public void RemoveItemFromInventory(string item)
		{
			if (inventoryModel.inventory.Contains(item))
			{
				inventoryModel.inventory.Remove(item);
				HideInventoryItem(item);

				CheckInventoryArrows();
			}
		}

		public void SetInventoryItemAsCursor(string item)
		{
			var data = GetInventoryDataByName(item);
			inventoryAdapter.inventoryItemCursor.sprite = data.cursorSprite;
			SetMousePositionToCursor();
			inventoryAdapter.inventoryItemCursor.gameObject.SetActive(true);
			inventoryModel.isShowingCursor = true;
			Cursor.visible = false;
		}

		public void SetDefaultCursor()
		{
			inventoryModel.isShowingCursor = false;
			inventoryAdapter.inventoryItemCursor.gameObject.SetActive(false);
			Cursor.visible = true;
		}

		private void CheckInventoryArrows()
		{
			bool needArrows = inventoryModel.inventory.Count > InventoryModel.MAX_INVENTORY_SLOTS || inventoryModel.currentIndex > 0;
			inventoryAdapter.ShowInventoryArrows(needArrows);
			if (needArrows)
			{
				bool needUpArrow = inventoryModel.currentIndex - InventoryModel.INVENTORY_SLOTS_PER_ROW >= 0;
				inventoryAdapter.inventoryUpArrow.gameObject.SetActive(needUpArrow);

				bool needDownArrow = inventoryModel.currentIndex + InventoryModel.INVENTORY_SLOTS_PER_ROW < inventoryModel.inventory.Count - 1;
				inventoryAdapter.inventoryDownArrow.gameObject.SetActive(needDownArrow);
			}
		}

		public void ShowInventoryUI()
		{
			inventoryAdapter.ShowInventoryUI();
		}

		public void HideInventoryUI()
		{
			SetInventoryNameText("");
			inventoryAdapter.HideInventoryUI();
		}

		public void SetInventoryNameText(string name)
		{
			inventoryAdapter.SetInventoryNameText(name);
		}

		public string GetInventoryItemaName(string id)
		{
			var data = GetInventoryDataById(id);
			if (data != null)
			{
				return data.name;
			}

			return "";
		}
	}
}