using System.Collections.Generic;

namespace Gameplay.Adventure.Inventory
{
	public class InventoryModel
	{
		public const int MAX_INVENTORY_SLOTS = 16;
		public const int INVENTORY_SLOTS_PER_ROW = 8;
		public List<string> inventory;
		public int currentIndex;
		public Dictionary<string, InventoryData> inventoryPaletteDictionaryById;
		public Dictionary<string, InventoryData> inventoryPaletteDictionaryByName;
		public bool isShowingCursor;

	}
}