﻿using System;
using UnityEngine;

namespace Gameplay.Adventure.Inventory
{
	[Serializable]
	public class InventoryData
	{
		public string item;
		public string name;
		public Transform itemTransform;
		public Sprite cursorSprite;
	}
}