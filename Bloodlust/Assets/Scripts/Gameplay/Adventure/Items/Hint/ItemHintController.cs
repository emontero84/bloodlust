using System.Collections.Generic;
using Components;
using Gameplay.Adventure.Actions;
using Gameplay.Adventure.Room;
using Gameplay.Adventure.Room.Show;
using MainMenu;
using Services.Story;
using UnityEngine;
using Utils;
using Zenject;

namespace Gameplay.Adventure.Items.Hint
{
	public class ItemHintController : IInitializable, ITickable
	{
		[Inject] public ItemHintAdapter itemHintAdapter { get; private set; }
		[Inject] public ActionsModel actionsModel { get; private set; }
		[Inject] public StoryService storyService { get; private set; }
		[Inject] public ShowPaletteController showPaletteController { get; private set; }
		[Inject] public RoomModel roomModel { get; private set; }
        [Inject] public MainMenuModel mainMenuModel { get; private set; }


        public void Initialize()
		{
			HideAllItemHints();
			HideItemHint();
		}

		public void Tick()
		{
            if (mainMenuModel.screen == MainMenuModel.MainMenuScreen.Gameplay)
            {
			    if (Input.GetKeyDown(KeyCode.Tab))
			    {
				    GetAllItemHints();
			    }
			    else if (Input.GetKeyUp(KeyCode.Tab))
			    {
				    HideAllItemHints();
			    }
            }
		}

		public void ShowItemHint(string itemName, Vector2 position)
		{
			itemHintAdapter.SetItemName(itemName);
			itemHintAdapter.SetItemHintPosition(position);
			itemHintAdapter.SetItemHintVisibility(true);
			actionsModel.isItemHintVisible = true;
		}

		public void HideItemHint()
		{
			itemHintAdapter.SetItemHintVisibility(false);
			actionsModel.isItemHintVisible = false;
		}

		private void GetAllItemHints()
		{
			List<ItemController> allItemHints = new List<ItemController>();
			var itemsToShowList = storyService.GetVariable(StringConstants.ROOM_ITEMS_TO_SHOW_VARIABLE) as Ink.Runtime.InkList;
			var roomId = roomModel.currentRoom;
			foreach (var item in itemsToShowList)
			{
				var itemId = item.Key.ToString().Split('.')[1];
				if (itemId.Split('_')[0] == roomId)
				{
					var roomItem = showPaletteController.GetRoomItemToShowById(itemId);
					var itemController = roomItem.itemController;
					if (itemController != null && itemController.item != "")
					{
						allItemHints.Add(itemController);
					}
				}
			}

			ShowAllItemHints(allItemHints);
		}

		public void ShowAllItemHints(List<ItemController> items)
		{
			for (var index = 0; index < itemHintAdapter.itemHintContainers.Count; index++)
			{
				itemHintAdapter.SetItemHintVisibilityAtIndex(index, false);
			}

			for (var index = 0; index < items.Count; index++)
			{
				var item = items[index];
				itemHintAdapter.SetItemHintPositionAtIndex(item.PositionToSend(), index);
				itemHintAdapter.SetItemHintNameAtIndex(StringConstants.QUESTION_MARK, index);
				itemHintAdapter.SetItemHintVisibilityAtIndex(index, true);
			}

			itemHintAdapter.SetItemName("");
			itemHintAdapter.SetItemHintVisibility(true);
			actionsModel.isItemHintVisible = true;
		}

		public void HideAllItemHints()
		{
			for (var index = 0; index < itemHintAdapter.itemHintContainers.Count; index++)
			{
				itemHintAdapter.SetItemHintVisibilityAtIndex(index, false);
			}

			itemHintAdapter.SetItemHintVisibility(false);
			actionsModel.isItemHintVisible = false;
		}
	}
}