using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Gameplay.Adventure.Items.Hint
{
	public class ItemHintAdapter : MonoBehaviour
	{
		public RectTransform itemHintPanel;
		public RectTransform itemHintContainer;
		public Text itemNameText;
		public List<RectTransform> itemHintContainers;
		public List<Text> itemHintNameTexts;
		
		public void SetItemHintVisibility(bool value)
		{
			itemHintPanel.gameObject.SetActive(value);
		}
		
		public void SetItemName(string name)
		{
			itemNameText.text = name;
		}
		
		public void SetItemHintPosition(Vector2 position)
		{
			itemHintContainer.transform.position = position;
		}

		public void SetItemHintPositionAtIndex(Vector2 position, int index)
		{
			if (index < 0 || index >= itemHintContainers.Count)
			{
				Debug.LogError("[ItemHintAdapter] Wrong item hint index! " + index);
				return;
			}

			var container = itemHintContainers[index];
			container.transform.position = position;
		}
		
		public void SetItemHintNameAtIndex(string name, int index)
		{
			if (index < 0 || index >= itemHintNameTexts.Count)
			{
				Debug.LogError("[ItemHintAdapter] Wrong item hint index! " + index);
				return;
			}

			var itemText = itemHintNameTexts[index];
			itemText.text = name;
		}
		
		public void SetItemHintVisibilityAtIndex(int index, bool value)
		{
			if (index < 0 || index >= itemHintContainers.Count)
			{
				Debug.LogError("[ItemHintAdapter] Wrong item hint index! " + index);
				return;
			}

			var container = itemHintContainers[index];
			container.gameObject.SetActive(value);
		}

	}
}