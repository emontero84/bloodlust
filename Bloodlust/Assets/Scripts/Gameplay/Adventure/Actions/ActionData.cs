﻿using System;

namespace Gameplay.Adventure.Actions
{
	[Serializable]
	public class ActionData
	{
		public Verbs.Verb verb;
		public string item;
		public string item2;
	}
}