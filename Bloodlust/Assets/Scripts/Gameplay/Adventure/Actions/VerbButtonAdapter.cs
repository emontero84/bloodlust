﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace Gameplay.Adventure.Actions
{
	public class VerbButtonAdapter : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler, IPointerUpHandler
	{
		public RectTransform normalText;
		public RectTransform mouseOverText;
		public RectTransform mouseDownText;

		public void Awake()
		{
			ShowNormalText();
		}

		public void ShowNormalText()
		{
			normalText.gameObject.SetActive(true);
			mouseOverText.gameObject.SetActive(false);
			mouseDownText.gameObject.SetActive(false);
		}

		public void ShowMouseOverText()
		{
			normalText.gameObject.SetActive(false);
			mouseOverText.gameObject.SetActive(true);
			mouseDownText.gameObject.SetActive(false);
		}

		private void ShowMouseDownText()
		{
			normalText.gameObject.SetActive(false);
			mouseOverText.gameObject.SetActive(false);
			mouseDownText.gameObject.SetActive(true);
		}

		public void OnPointerEnter(PointerEventData eventData)
		{
			ShowMouseOverText();
		}

		public void OnPointerExit(PointerEventData eventData)
		{
			ShowNormalText();
		}

		public void OnPointerDown(PointerEventData eventData)
		{
			ShowMouseDownText();
		}

		public void OnPointerUp(PointerEventData eventData)
		{
			ShowNormalText();
		}
	}
}