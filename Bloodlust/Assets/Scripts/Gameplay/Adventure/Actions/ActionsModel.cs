using Components;

namespace Gameplay.Adventure.Actions
{
	public class ActionsModel
	{
		public bool isInitialized;
		public bool playerIsGoingToItem;
		public bool isPlayerMovingToAction;
		public bool isItemHintVisible;
		public bool isInventoryItemCombination;
		public bool lookAtInventoryItem;
		public ItemController itemControllerClicked;
		public ActionData action;
	}
}