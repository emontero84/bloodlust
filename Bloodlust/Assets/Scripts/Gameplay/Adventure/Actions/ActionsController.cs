﻿using System;
using System.Collections.Generic;
using Components;
using Gameplay.Adventure.Inventory;
using Gameplay.Adventure.Items.Hint;
using Gameplay.Adventure.QuestLog;
using Gameplay.Adventure.Room;
using Gameplay.Dialog.Choice;
using Gameplay.Dialog.Dialog;
using Services.Analytics;
using Services.Audio;
using Services.SceneLoader;
using Services.Story;
using UnityEngine;
using Utils;
using Zenject;

namespace Gameplay.Adventure.Actions
{
	public class ActionsController : IInitializable, IDisposable
	{
		[Inject] public SignalBus signalBus { get; private set; }
		[Inject] public ActionsModel actionsModel { get; private set; }
		[Inject] public RoomAdapter roomAdapter { get; private set; }
		[Inject] public RoomModel roomModel { get; private set; }
		[Inject] public InventoryAdapter inventoryAdapter { get; private set; }
		[Inject] public InventoryController inventoryController { get; private set; }
		[Inject] public DialogChoiceModel dialogChoiceModel { get; private set; }
		[Inject] public DialogModel dialogModel { get; private set; }
		[Inject] public StoryService storyService { get; private set; }
		[Inject] public DialogController dialogController { get; private set; }
		[Inject] public DialogChoiceController dialogChoiceController { get; private set; }
		[Inject] public MouseClickMover mouseClickMover { get; private set; }
		[Inject] public ItemHintController itemHintController { get; private set; }
		[Inject] public SceneLoaderService sceneLoaderService { get; private set; }

		private ButtonController[] inventoryButtons;
		private List<ItemController> itemControllers;
		
		public void Initialize()
		{
			actionsModel.action = new ActionData();
			actionsModel.playerIsGoingToItem = true;
			ResetAction();

			// Show all rooms
			foreach (var room in roomAdapter.roomPalette)
			{
				room.roomTransform.gameObject.SetActive(true);
			}

			// Add all item controllers
			itemControllers = new List<ItemController>();
			foreach (var room in roomAdapter.roomPalette)
			{
				var itemContainer = room.roomTransform;
				var itemsInContainer = itemContainer.GetComponentsInChildren<ItemController>();
				itemControllers.AddRange(itemsInContainer);
				foreach (var item in itemsInContainer)
				{
					if (item.item == "")
					{
						// Avoid items to show with no item name (open doors, etc).
						continue;
					}

					item.ItemMouseEnter += OnItemMouseEnter;
					item.ItemMouseExit += OnItemMouseExit;
					item.ItemMouseClick += OnItemMouseClick;
				}
			}

			// Inventory buttons
			inventoryButtons = inventoryAdapter.inventoryButtonsContainer.GetComponentsInChildren<ButtonController>();
			foreach (var button in inventoryButtons)
			{
				button.ButtonMouseEnter += OnInventoryButtonMouseEnter;
				button.ButtonMouseExit += OnInventoryButtonMouseExit;
				button.ButtonMouseClick += OnInventoryButtonMouseClick;
				// Hide all items in the inventory
				button.gameObject.SetActive(false);
			}

			dialogChoiceController.HideDialogChoiceUI();

			dialogController.Continue += OnContinue;

			inventoryController.ShowInventoryUI();

			actionsModel.isInitialized = true;
			signalBus.Fire<InitializeSignal>();
		}

		public void Dispose()
		{
			foreach (var item in itemControllers)
			{
				item.ItemMouseEnter -= OnItemMouseEnter;
				item.ItemMouseExit -= OnItemMouseExit;
				item.ItemMouseClick -= OnItemMouseClick;
			}

			foreach (var button in inventoryButtons)
			{
				button.ButtonMouseEnter -= OnInventoryButtonMouseEnter;
				button.ButtonMouseExit -= OnInventoryButtonMouseExit;
				button.ButtonMouseClick -= OnInventoryButtonMouseClick;
			}

			dialogController.Continue -= OnContinue;
			mouseClickMover.PlayerMoveFinish -= OnPlayerMoveFinish;
		}

		private void OnItemMouseEnter(ItemController itemController)
		{
			if (dialogModel.IsShowingDialog || dialogChoiceModel.isShowingDialogChoice)
				return;
			actionsModel.playerIsGoingToItem = false;
			itemHintController.ShowItemHint(itemController.item, itemController.PositionToSend());
		}

		private void OnItemMouseExit()
		{
			if (!actionsModel.isItemHintVisible)
				return;
			actionsModel.playerIsGoingToItem = true;
			itemHintController.HideItemHint();
		}

		private void OnItemMouseClick(ItemController itemController, bool leftClick)
		{
			if (actionsModel.isPlayerMovingToAction)
				return;

			if (dialogModel.IsShowingDialog || dialogChoiceModel.isShowingDialogChoice)
				return;

			itemHintController.HideItemHint();
			inventoryController.SetDefaultCursor();

			actionsModel.itemControllerClicked = itemController;
			var item = itemController.item;
			var verbs = itemController.verbs;
			if (leftClick)
			{
				if (actionsModel.action.item != "")
				{
					if (verbs.Contains(Verbs.Verb.TalkTo))
					{
						SetVerb(Verbs.Verb.Give);
					}
					else
					{
						SetVerb(Verbs.Verb.Use);
					}

					actionsModel.playerIsGoingToItem = true;
					SetItem2(item);
					ProcessAction();
				}
				else
				{
					ResetAction();
					SetItem(item);
					SetVerb(verbs[0]);
					actionsModel.playerIsGoingToItem = true;
					ProcessAction();
				}
			}
			else
			{
				ResetAction();
				SetVerb(Verbs.Verb.LookAt);
				SetItem(item);
				actionsModel.playerIsGoingToItem = true;
				ProcessAction();
			}
		}

		public string GetActionVerb(Verbs.Verb verb)
		{
			switch (verb)
			{
				case Verbs.Verb.WalkTo:
					return StringConstants.WALK_TO;
				case Verbs.Verb.LookAt:
					return StringConstants.LOOK_AT;
				case Verbs.Verb.PickUp:
					return StringConstants.PICK_UP;
				case Verbs.Verb.TalkTo:
					return StringConstants.TALK_TO;
				case Verbs.Verb.Use:
					return StringConstants.USE;
				case Verbs.Verb.Give:
					return StringConstants.GIVE;
			}

			return "";
		}

		private void OnInventoryButtonMouseEnter(string item)
		{
			if (actionsModel.isPlayerMovingToAction)
				return;
			inventoryController.SetInventoryNameText(item);
		}

		private void OnInventoryButtonMouseExit(string item)
		{
			if (actionsModel.isPlayerMovingToAction)
				return;
			inventoryController.SetInventoryNameText("");
		}

		private void OnInventoryButtonMouseClick(string item, bool leftClick)
		{
			if (actionsModel.isPlayerMovingToAction)
				return;

			if (dialogModel.IsShowingDialog || dialogChoiceModel.isShowingDialogChoice)
				return;

			actionsModel.playerIsGoingToItem = false;

			if (actionsModel.action.verb == Verbs.Verb.WalkTo && item == StringConstants.QUEST_LOG_ITEM_NAME)
			{
				ResetAction();
				inventoryController.SetDefaultCursor();
				signalBus.Fire<QuestLogSignal>(QuestLogSignal.Type.SHOW_QUEST_LOG);
			}
			else if (leftClick)
			{
				if (actionsModel.action.item == "")
				{
					SetItem(item);
					inventoryController.SetInventoryItemAsCursor(item);
				}
				else if (actionsModel.action.item2 == "")
				{
					var item1 = actionsModel.action.item;
					inventoryController.SetDefaultCursor();
					
					// Call to look at inventory items to access the tunnel
					ResetAction();
					SetVerb(Verbs.Verb.LookAt);
					SetItem(StringConstants.INVENTORY_ITEMS);
					ProcessAction();
					actionsModel.isInventoryItemCombination = true;

					SetItem(item1);
					SetItem2(item);
					SetVerb(Verbs.Verb.Use);
					ProcessAction();
				}
			}
			else
			{
				// Call to look at inventory items to access the tunnel
				ResetAction();
				SetVerb(Verbs.Verb.LookAt);
				SetItem(StringConstants.INVENTORY_ITEMS);
				ProcessAction();
				actionsModel.lookAtInventoryItem = true;

				SetVerb(Verbs.Verb.LookAt);
				SetItem(item);
				ProcessAction();

			}

			signalBus.Fire(new PlayAudioSignal(SoundData.SoundId.GENERIC_CLICK));
		}

		private void OnContinue()
		{
			Continue();
		}

		public void Continue()
		{
			if (storyService.CanContinue())
			{
				storyService.Continue();
				var tags = storyService.CurrentTags();
				if (tags.Contains(StringConstants.CHOICE_TAG))
				{
					dialogChoiceController.ShowOrHideDialogChoiceArrows();

					var choices = storyService.CurrentChoices();
					List<string> texts = new List<string>();
					foreach (var choice in choices)
					{
						texts.Add(choice.text);
					}

					dialogChoiceController.SetDialogChoices(texts);
					dialogChoiceController.ShowDialogChoices();

					dialogController.HideDialogUI();
					inventoryController.HideInventoryUI();
					dialogChoiceController.ShowDialogChoiceUI();
				}
				else
				{
					var currentText = storyService.CurrentText();
					if (currentText != null && currentText.Length > 0)
					{
						ShowTextOnDialogUI(currentText);
					}
					else
					{
						Continue();
					}
				}
			}
			else
			{
				dialogController.HideDialogUI();
				dialogChoiceController.HideDialogChoiceUI();
				inventoryController.ShowInventoryUI();
				var currentTags = storyService.CurrentTags();
				if (currentTags.Count == 2 && currentTags[1] == StringConstants.END_STORY_TAG)
				{
					sceneLoaderService.LoadScene(SceneLoaderModel.MAIN_MENU_SCENE);
				}
			}
		}

		private void ShowTextOnDialogUI(string currentText)
		{
			var currentCharacter = currentText.Split(':')[0];
			var currentDialog = currentText.Split(':')[1];
			var currentTags = storyService.CurrentTags();
			string currentTag = "";
			if (currentTags.Count > 0)
				currentTag = currentTags[0];
			dialogModel.characterName = currentCharacter;
			dialogModel.dialogText = currentDialog;
			dialogModel.tag = currentTag;
			inventoryController.HideInventoryUI();
			dialogChoiceController.HideDialogChoiceUI();
			dialogController.ShowDialogUI();
			dialogController.SetCharacterName();
			dialogController.SetDialogText();
		}

		public void SetVerb(Verbs.Verb verb)
		{
			actionsModel.action.verb = verb;
		}

		public void SetItem(string item)
		{
			actionsModel.action.item = item;
		}

		public void SetItem2(string item)
		{
			actionsModel.action.item2 = item;
		}

		private void MovePlayerToAction(string item)
		{
			var itemId = roomModel.currentRoom + "." + item;
			var itemController = actionsModel.itemControllerClicked;
			var itemPosition = itemController.playerPosition;
			if (itemPosition != null)
			{
				actionsModel.isPlayerMovingToAction = true;
				mouseClickMover.MoveTo(itemPosition.position);
			}
			else
			{
				if (mouseClickMover.PlayerMoveFinish != null)
					mouseClickMover.PlayerMoveFinish();
			}
		}

		public void ProcessAction()
		{
			string actionText = "";
			if (actionsModel.action.verb != Verbs.Verb.Give && actionsModel.action.item != "" && actionsModel.action.item2 == "")
			{
				actionText = GetActionVerb(actionsModel.action.verb) + " " + actionsModel.action.item;
			}
			else if (actionsModel.action.verb == Verbs.Verb.Give && actionsModel.action.item != "" && actionsModel.action.item2 != "")
			{
				actionText = GetActionVerb(actionsModel.action.verb) + " " + actionsModel.action.item + " to " + actionsModel.action.item2;
			}
			else if (actionsModel.action.verb == Verbs.Verb.Use && actionsModel.action.item != "" && actionsModel.action.item2 != "")
			{
				actionText = GetActionVerb(actionsModel.action.verb) + " " + actionsModel.action.item + " with " + actionsModel.action.item2;
			}

			Debug.Log("Execute action: " + actionText);
			signalBus.Fire(new AnalyticsSignal(AnalyticsModel.EXECUTE_ACTION, "Action", actionText));

			int maxChoices = storyService.GetNumberOfChoices();
			var choices = storyService.CurrentChoices();
			bool choiceSelected = false;
			for (int i = 0; i < maxChoices; i++)
			{
				var choice = choices[i];
				if (choice.text == actionText)
				{
					storyService.ChooseChoiceIndex(i);
					Debug.Log("CHOICE! " + actionText);
					choiceSelected = true;
					break;
				}
			}

			if (!choiceSelected)
			{
				signalBus.Fire(new AnalyticsSignal(AnalyticsModel.WRONG_ACTION, "Action", actionText));

				switch (actionsModel.action.verb)
				{
					case Verbs.Verb.LookAt:
						ShowTextOnDialogUI(StringConstants.DEFAULT_LOOK_AT);
						if (actionsModel.lookAtInventoryItem)
						{
							// Call to last choice to go back from the inventory
							storyService.ChooseChoiceIndex(storyService.GetNumberOfChoices() - 1);
							actionsModel.lookAtInventoryItem = false;
						}
						break;
					case Verbs.Verb.PickUp:
						ShowTextOnDialogUI(StringConstants.DEFAULT_PICK_UP);
						break;
					case Verbs.Verb.TalkTo:
						ShowTextOnDialogUI(StringConstants.DEFAULT_TALK_TO);
						break;
					case Verbs.Verb.Give:
						ShowTextOnDialogUI(StringConstants.DEFAULT_GIVE);
						break;
					default:
						if (actionsModel.action.item2 != "" && actionsModel.isInventoryItemCombination)
						{
							ShowTextOnDialogUI(StringConstants.DEFAULT_COMBINE);
                            // Call to last choice to go back from the inventory
                            storyService.ChooseChoiceIndex(storyService.GetNumberOfChoices() - 1);
                        }
						else
						{
							ShowTextOnDialogUI(StringConstants.DEFAULT_USE);
						}

						break;
				}

				ResetAction();
			}
			else
			{
				if (actionsModel.playerIsGoingToItem)
				{
					mouseClickMover.PlayerMoveFinish += OnPlayerMoveFinish;
					if (actionsModel.action.verb == Verbs.Verb.Give || (actionsModel.action.verb == Verbs.Verb.Use && actionsModel.action.item2 != ""))
					{
						MovePlayerToAction(actionsModel.action.item2);
					}
					else
					{
						MovePlayerToAction(actionsModel.action.item);
					}
				}
				else
				{
					Continue();
					ResetAction();
				}
			}
		}

		private void OnPlayerMoveFinish()
		{
			if (actionsModel.action.verb == Verbs.Verb.WalkTo && actionsModel.action.item == "")
				return;

			mouseClickMover.PlayerMoveFinish -= OnPlayerMoveFinish;

			actionsModel.playerIsGoingToItem = true;
			actionsModel.isPlayerMovingToAction = false;

			Continue();
			ResetAction();
		}

		public void ResetAction()
		{
			actionsModel.action.verb = Verbs.Verb.WalkTo;
			actionsModel.action.item = "";
			actionsModel.action.item2 = "";
			actionsModel.isInventoryItemCombination = false;
		}
	}
}