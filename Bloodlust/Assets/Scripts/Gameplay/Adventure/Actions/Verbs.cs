﻿namespace Gameplay.Adventure.Actions
{
	public class Verbs 
	{
		public enum Verb
		{
			WalkTo,
			LookAt,
			PickUp,
			TalkTo,
			Use,
			Give
		}
	}
}