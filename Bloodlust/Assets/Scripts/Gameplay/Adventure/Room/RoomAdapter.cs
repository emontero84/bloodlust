﻿using System.Collections.Generic;
using Gameplay.Adventure.Room.Transition;
using UnityEngine;

namespace Gameplay.Adventure.Room
{
	public class RoomAdapter : MonoBehaviour
	{
		public List<RoomPaletteData> roomPalette;
		public List<RoomTransitionData> roomTransitions;
	}
}