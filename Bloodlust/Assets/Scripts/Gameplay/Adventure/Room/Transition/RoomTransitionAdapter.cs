using System.Linq.Expressions;
using UnityEngine;
using UnityEngine.UI;

namespace Gameplay.Adventure.Room.Transition
{
	public class RoomTransitionAdapter : MonoBehaviour
	{
		public Image fadeImage;
		public float fadeOut = 0.25f;
		public float fadeIn = 0.25f;

		public void ShowFadeImage(bool value)
		{
			fadeImage.gameObject.SetActive(value);
		}
	}
}