using System;
using System.Collections;
using Components;
using DG.Tweening;
using Gameplay.Adventure.Room.Shadow;
using Services.Audio;
using UnityEngine;
using Utils;
using Zenject;

namespace Gameplay.Adventure.Room.Transition
{
	public class RoomTransitionController : IInitializable, IDisposable
	{
		[Inject] public RoomTransitionAdapter roomTransitionAdapter { get; private set; }
		[Inject] public RoomController roomController { get; private set; }
		[Inject] public PlayerShadowController playerShadowController { get; private set; }
		[Inject] public MouseClickMover mouseClickMover { get; private set; }
		[Inject] public RoomModel roomModel { get; private set; }
		[Inject] public MainCameraAdapter mainCameraAdapter { get; private set; }
		[Inject] public AsyncProcessor asyncProcessor { get; private set; }
		[Inject] public SignalBus signalBus { get; private set; }


		private Coroutine _coroutine;

		public void Initialize()
		{
			roomTransitionAdapter.ShowFadeImage(false);
		}

		public void Dispose()
		{
			StopAllCoroutines();
		}

		private void StopAllCoroutines()
		{
			if (_coroutine != null)
			{
				if (asyncProcessor != null)
				{
					asyncProcessor.StopCoroutine(_coroutine);
				}

				_coroutine = null;
			}
		}

		public void TransitionBetweenRooms(string targetRoomId, Vector2 playerPosition, Vector3 cameraPosition)
		{
			StopAllCoroutines();
			_coroutine = asyncProcessor.StartCoroutine(TransitionBetweenRoomsCoroutine(cameraPosition, targetRoomId, playerPosition));
		}

		public void TransitionToInitialRoom(string targetRoomId, Vector2 playerPosition, Vector3 cameraPosition)
		{
			StopAllCoroutines();
			_coroutine = asyncProcessor.StartCoroutine(TransitionToInitialRoomCoroutine(cameraPosition, targetRoomId, playerPosition));
		}

		private IEnumerator TransitionBetweenRoomsCoroutine(Vector3 cameraPosition, string targetRoom, Vector2 targetPosition)
		{
			roomTransitionAdapter.ShowFadeImage(true);
			roomTransitionAdapter.fadeImage.DOFade(1, roomTransitionAdapter.fadeOut);

			yield return new WaitForSeconds(roomTransitionAdapter.fadeOut);

			string currentRoom = roomModel.currentRoom;
			roomController.HideCurrentRoom();
			roomController.ShowRoom(targetRoom);
			MovePlayerCharacterToRoom(targetRoom, targetPosition);
			mainCameraAdapter.mainCamera.transform.position = cameraPosition;
			roomTransitionAdapter.fadeImage.DOFade(0, roomTransitionAdapter.fadeIn);

			yield return new WaitForSeconds(roomTransitionAdapter.fadeIn);

			roomTransitionAdapter.ShowFadeImage(false);
			roomModel.isGoingToRoom = false;
			signalBus.Fire(new PlayMusicSignal(currentRoom, targetRoom));
		}

		private IEnumerator TransitionToInitialRoomCoroutine(Vector3 cameraPosition, string targetRoom, Vector2 targetPosition)
		{
			roomTransitionAdapter.ShowFadeImage(true);
			roomTransitionAdapter.fadeImage.DOFade(1, 0);

			yield return new WaitForSeconds(roomTransitionAdapter.fadeOut);

			roomController.ShowRoom(targetRoom);
			MovePlayerCharacterToRoom(targetRoom, targetPosition);
			mainCameraAdapter.mainCamera.transform.position = cameraPosition;
			roomTransitionAdapter.fadeImage.DOFade(0, roomTransitionAdapter.fadeIn);

			yield return new WaitForSeconds(roomTransitionAdapter.fadeIn);

			roomTransitionAdapter.ShowFadeImage(false);
			roomModel.isGoingToRoom = false;
			signalBus.Fire(new PlayMusicSignal("", targetRoom));
		}

		private void MovePlayerCharacterToRoom(string roomId, Vector2 targetPosition)
		{
			// Set player shadow
			var currentRoom = EnumUtil.ParseEnum<RoomModel.Room>(roomModel.currentRoom);
			var targetRoom = EnumUtil.ParseEnum<RoomModel.Room>(roomId);
			playerShadowController.SetShadow(targetRoom);

			// Set player position in new room
			mouseClickMover.SetPosition(targetPosition);
			mouseClickMover.Idle();
		}
	}
}