using System;
using UnityEngine;

namespace Gameplay.Adventure.Room.Transition
{
	[Serializable]
	public class RoomTransitionData
	{
		public RoomModel.Room from;
		public RoomModel.Room to;
		public Transform playerCharacterPosition;
	}
}