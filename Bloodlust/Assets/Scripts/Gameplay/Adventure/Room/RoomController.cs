﻿using System;
using System.Collections.Generic;
using Gameplay.Adventure.Room.Transition;
using Project.Progression;
using Services.Analytics;
using Services.Audio;
using Services.Story;
using UnityEngine;
using Utils;
using Zenject;

namespace Gameplay.Adventure.Room
{
	public class RoomController : IInitializable, IDisposable
	{
		[Inject] public SignalBus signalBus { get; private set; }
		[Inject] public RoomAdapter roomAdapter { get; private set; }
		[Inject] public RoomModel roomModel { get; private set; }
		[Inject] public RoomTransitionController roomTransitionController { get; private set; }
		[Inject] public StoryService storyService { get; private set; }
		[Inject] public PlayerModel playerModel { get; private set; }

		private Dictionary<string, RoomPaletteData> roomPaletteDictionary;

		public void Initialize()
		{
			signalBus.Subscribe<StorySignal>(OnStorySignal);

			roomPaletteDictionary = new Dictionary<string, RoomPaletteData>();
			foreach (var data in roomAdapter.roomPalette)
			{
				roomPaletteDictionary.Add(data.room.ToString(), data);
			}

			roomModel.isInitialized = true;
			signalBus.Fire<InitializeSignal>();
		}

		public void Dispose()
		{
			signalBus.Unsubscribe<StorySignal>(OnStorySignal);

			if (roomPaletteDictionary != null)
			{
				roomPaletteDictionary.Clear();
				roomPaletteDictionary = null;
			}
		}

		private void OnStorySignal(StorySignal signal)
		{
			if (signal.externalFunction == StringConstants.GO_TO_ROOM)
			{
				var targetRoomId = signal.arg1.Split('_')[1];
				signalBus.Fire(new AnalyticsSignal(AnalyticsModel.CHANGE_ROOM, "Room", targetRoomId));
				GoToRoom(targetRoomId);
			}
		}

		private void GoToRoom(string targetRoomId)
		{
			roomModel.isGoingToRoom = true;
			var data = GetRoomPaletteData(targetRoomId);
			if (data != null)
			{
				var cameraPosition = data.cameraPosition.position;
				if (roomModel.currentRoom == null)
				{
					roomModel.currentRoom = targetRoomId;
				}

				var currentRoom = EnumUtil.ParseEnum<RoomModel.Room>(roomModel.currentRoom);
				var targetRoom = EnumUtil.ParseEnum<RoomModel.Room>(targetRoomId);
				var targetRoomTransform = GetRoomTransition(currentRoom, targetRoom);
				if (currentRoom != targetRoom)
				{
					roomTransitionController.TransitionBetweenRooms(targetRoomId, targetRoomTransform.position, cameraPosition);
				}
				else
				{
					roomTransitionController.TransitionToInitialRoom(targetRoomId, targetRoomTransform.position, cameraPosition);
				}
			}
		}

		public void ShowRoom(string room)
		{
			roomModel.currentRoom = room;
			EnableRoom(room, true);
		}

		public void HideRoom(string room)
		{
			EnableRoom(room, false);
		}

		public void HideCurrentRoom()
		{
			HideRoom(roomModel.currentRoom);
		}

		private void EnableRoom(string room, bool value)
		{
			var data = GetRoomPaletteData(room);
			if (data != null)
			{
				var roomTransform = data.roomTransform;
				roomTransform.gameObject.SetActive(value);
			}
		}

		public SoundData.SoundId GetRoomMusic(string room)
		{
			var data = GetRoomPaletteData(room);
			if (data != null)
			{
				return data.musicId;
			}

			return SoundData.SoundId.NONE;
		}

		private RoomPaletteData GetRoomPaletteData(string room)
		{
			if (string.IsNullOrEmpty(room))
			{
				Debug.LogError("[RoomController] EnableRoom: The room is null or empty!");
				return null;
			}

			if (roomPaletteDictionary.ContainsKey(room))
			{
				var data = roomPaletteDictionary[room];
				return data;
			}

			Debug.LogError("[RoomController] EnableRoom: There's no room " + room + " in palette dictionary!");
			return null;
		}

		private Transform GetRoomTransition(RoomModel.Room from, RoomModel.Room to)
		{
			for (var i = 0; i < roomAdapter.roomTransitions.Count; i++)
			{
				var transition = roomAdapter.roomTransitions[i];
				if (transition.from == from && transition.to == to)
					return transition.playerCharacterPosition;
			}

			Debug.LogError("[RoomController] There's no room transition from " + from + " to " + to);
			return null;
		}

		public void LoadRoomProgression()
		{
			// Hide all rooms
			foreach (var room in roomAdapter.roomPalette)
			{
				room.roomTransform.gameObject.SetActive(false);
			}

			var roomList = storyService.GetVariable(StringConstants.ROOMS_VARIABLE) as Ink.Runtime.InkList;

			foreach (var room in roomList)
			{
				string roomId = room.Key.ToString().Split('_')[1];
				GoToRoom(roomId);
			}
		}
	}
}