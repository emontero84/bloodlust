using System;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Gameplay.Adventure.Room.Shadow
{
	public class PlayerShadowController : IInitializable, IDisposable
	{
		[Inject] public PlayerShadowAdapter playerShadowAdapter { get; private set; }

		private Dictionary<RoomModel.Room, Color> roomShadowPaletteDictionary;

		public void Initialize()
		{
			roomShadowPaletteDictionary = new Dictionary<RoomModel.Room, Color>();
			for (var i = 0; i < playerShadowAdapter.roomShadowPalette.Count; i++)
			{
				var data = playerShadowAdapter.roomShadowPalette[i];
				roomShadowPaletteDictionary.Add(data.room, data.color);
			}
		}

		public void Dispose()
		{
			if (roomShadowPaletteDictionary != null)
			{
				roomShadowPaletteDictionary.Clear();
				roomShadowPaletteDictionary = null;
			}
		}

		public void SetShadow(RoomModel.Room room)
		{
			var color = GetShadow(room);
			playerShadowAdapter.shadow.color = color;
		}
		
		public Color GetShadow(RoomModel.Room room)
		{
			if (roomShadowPaletteDictionary.ContainsKey(room))
			{
				var roomColor = roomShadowPaletteDictionary[room];
				return roomColor;
			}

			Debug.LogError("[PlayerShadowController] GetShadow: There's no room " + room + " in shadow palette dictionary!");
			return Color.white;
		}
	}
}