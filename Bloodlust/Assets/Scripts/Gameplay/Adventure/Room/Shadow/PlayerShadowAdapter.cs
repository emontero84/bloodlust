using System.Collections.Generic;
using UnityEngine;

namespace Gameplay.Adventure.Room.Shadow
{
	public class PlayerShadowAdapter : MonoBehaviour
	{
		public SpriteRenderer shadow;
		public List<RoomShadowData> roomShadowPalette;
	}
}