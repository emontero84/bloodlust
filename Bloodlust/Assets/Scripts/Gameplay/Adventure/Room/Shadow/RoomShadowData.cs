using System;
using UnityEngine;

namespace Gameplay.Adventure.Room.Shadow
{
	[Serializable]
	public class RoomShadowData
	{
		public RoomModel.Room room;
		public Color color;
	}
}