﻿using System;
using Services.Audio;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace Gameplay.Adventure.Room
{
	[Serializable]
	public class RoomPaletteData
	{
		public RoomModel.Room room;
		public Transform roomTransform;
		public Transform cameraPosition;
		public SoundData.SoundId musicId;
	}
}