using System;
using System.Collections.Generic;
using Components;
using Services.Story;
using UnityEngine;
using Utils;
using Zenject;

namespace Gameplay.Adventure.Room.Show
{
	public class ShowPaletteController : IInitializable, IDisposable
	{
		[Inject] public SignalBus signalBus { get; private set; }
		[Inject] public ShowPaletteModel showPaletteModel { get; private set; }
		[Inject] public StoryService storyService { get; private set; }

		public void Initialize()
		{
			signalBus.Subscribe<StorySignal>(OnStorySignal);
			showPaletteModel.showPaletteDictionary = new Dictionary<string, ShowPaletteData>();
		}

		public void Dispose()
		{
			signalBus.Unsubscribe<StorySignal>(OnStorySignal);

			if (showPaletteModel.showPaletteDictionary != null)
			{
				showPaletteModel.showPaletteDictionary.Clear();
				showPaletteModel.showPaletteDictionary = null;
			}
		}

		private void OnStorySignal(StorySignal signal)
		{
			switch (signal.externalFunction)
			{
				case StringConstants.SHOW:
					var itemToShow = signal.arg1;
					Show(itemToShow);
					break;
				case StringConstants.HIDE:
					var itemToHide = signal.arg1;
					Hide(itemToHide);
					break;
			}
		}

		public void Show(string id)
		{
			SetActive(id, true);
		}

		public void Hide(string id)
		{
			SetActive(id, false);
		}

		private void SetActive(string id, bool value)
		{
			if (showPaletteModel.showPaletteDictionary.ContainsKey(id))
			{
				var transformToShow = showPaletteModel.showPaletteDictionary[id].transformToShow;
				transformToShow.gameObject.SetActive(value);
			}
			else
			{
				Debug.LogError("[ShowPaletteController] SetActive: There's no id " + id + " in palette dictionary!");
			}
		}

		public ShowPaletteData GetRoomItemToShowById(string id)
		{
			if (showPaletteModel.showPaletteDictionary.ContainsKey(id))
				return showPaletteModel.showPaletteDictionary[id];
			return null;
		}

		public void LoadRoomItemsToShowProgression()
		{
			var itemsToShowList = storyService.GetVariable(StringConstants.ROOM_ITEMS_TO_SHOW_VARIABLE) as Ink.Runtime.InkList;
			// Create palette of items to show. 
			var itemControllers = GameObject.FindObjectsOfType<ItemController>();
			foreach (var itemController in itemControllers)
			{
				var gameObject = itemController.gameObject;
				var itemId = gameObject.name;
				var showPaletteData = new ShowPaletteData();
				showPaletteData.id = itemId;
				showPaletteData.itemController = itemController;
				showPaletteData.transformToShow = gameObject.transform;
				showPaletteModel.showPaletteDictionary.Add(itemId, showPaletteData);
			}

#if UNITY_EDITOR
			// Check that all the items to show in the story are also present in the scene (only in Editor).
			foreach (var item in itemsToShowList.all)
			{
				var itemId = item.Key.ToString().Split('.')[1];
				var itemToShowGameObject = GameObject.Find(itemId);
				if (itemToShowGameObject == null)
				{
					Debug.LogError("[ShowPaletteController] There's no game object with id " + itemId + " in items to show palette dictionary!");
				}
				else
				{
					if (!showPaletteModel.showPaletteDictionary.ContainsKey(itemId))
					{
						Debug.LogError("[ShowPaletteController] Adding game object with id " + itemId + " in items to show palette dictionary!");
						var showPaletteData = new ShowPaletteData();
						showPaletteData.id = itemId;
						showPaletteData.itemController = itemToShowGameObject.GetComponent<ItemController>();
						showPaletteData.transformToShow = itemToShowGameObject.transform;
						showPaletteModel.showPaletteDictionary.Add(itemId, showPaletteData);
					}
				}
			}
#endif

			// Hide all items in the rooms
			foreach (var item in showPaletteModel.showPaletteDictionary)
			{
				string itemId = item.Key;
				Hide(itemId);
			}

			// Show the visible items in the rooms
			foreach (var item in itemsToShowList)
			{
				string itemId = item.Key.ToString().Split('.')[1];
				Show(itemId);
			}
		}
	}
}