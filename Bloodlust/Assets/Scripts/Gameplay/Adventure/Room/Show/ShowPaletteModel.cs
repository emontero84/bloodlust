﻿using System.Collections.Generic;
using UnityEngine;

namespace Gameplay.Adventure.Room.Show
{
	public class ShowPaletteModel : MonoBehaviour
	{
		// A dictionary for run-time access
		public Dictionary<string, ShowPaletteData> showPaletteDictionary;
	}
}