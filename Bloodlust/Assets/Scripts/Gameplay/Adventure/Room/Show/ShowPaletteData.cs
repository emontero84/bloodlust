﻿using System;
using Components;
using UnityEngine;

namespace Gameplay.Adventure.Room.Show
{
	[Serializable]
	public class ShowPaletteData
	{
		public string id;
		public Transform transformToShow;
		public ItemController itemController;
	}
}