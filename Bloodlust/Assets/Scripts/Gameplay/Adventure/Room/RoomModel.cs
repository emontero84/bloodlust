﻿namespace Gameplay.Adventure.Room
{
	public class RoomModel
	{
		public enum Room
		{
			Apartment101,
			LodgingHallway,
			Apartment102,
			MainStreet,
			SuitesHallway,
			SuitesMercurius,
			SuitesCarlton,
			ParkingGarage,
			Beach,
			BeachHouseExterior,
			BeachHouseInterior,
			Pawnshop,
			NoirGallery,
			Hospital,
			HospitalOffice,
			SecondStreet,
			MadhouseClub,
			Elevator,
			MadhouseOffice,
			Diner,
			Bailbonds,
			Silo,
			Introduction
		}

		public bool isInitialized;
		public string currentRoom;
		public bool isGoingToRoom;
	}
}