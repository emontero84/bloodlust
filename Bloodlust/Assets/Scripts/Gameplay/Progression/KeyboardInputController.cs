using System.Collections;
using System.IO;
using MainMenu;
using Project.Loading;
using Project.Progression;
using Services.Audio;
using Services.SceneLoader;
using UnityEngine;
using Utils;
using Zenject;

namespace Gameplay.Progression
{
	public class KeyboardInputController : ITickable
	{
		[Inject] public SceneLoaderService sceneLoaderService { get; private set; }
		[Inject] public ScreenshotModel screenshotModel { get; private set; }
		[Inject] public AudioManager audioManager { get; private set; }
		[Inject] public AsyncProcessor asyncProcessor { get; private set; }
		[Inject] public SignalBus signalBus { get; private set; }
        [Inject] public MainMenuModel mainMenuModel { get; private set; }
        [Inject] public SavedGamesModel savedGamesModel { get; private set; }

        public void Tick()
		{
            if (mainMenuModel.screen == MainMenuModel.MainMenuScreen.Gameplay)
            {
                if (Input.GetKey(KeyCode.Escape))
                {
                    asyncProcessor.StartCoroutine(CaptureScreenshotPNG());
                }
            }	
		}

		private IEnumerator CaptureScreenshotPNG()
		{
			// We should only read the screen buffer after rendering is complete
			yield return new WaitForEndOfFrame();

			// Create a texture the size of the screen, RGB24 format
			int width = Screen.width;
			int height = Screen.height;
			screenshotModel.screenshot = new Texture2D(width, height, TextureFormat.RGB24, false);

			// Read screen contents into the texture
			screenshotModel.screenshot.ReadPixels(new Rect(0, 0, width, height), 0, 0);
			screenshotModel.screenshot.Apply();

			signalBus.Fire(new ShowLoadingScreenSignal(true));

			audioManager.StopAllSounds();

            // After taking the screenshot, go to main menu
            savedGamesModel.isGameRunning = true;
            sceneLoaderService.LoadScene(SceneLoaderModel.MAIN_MENU_SCENE);
		}
	}
}