﻿using System;
using System.Collections;
using Components;
using Gameplay.Dialog.Portrait;
using MainMenu.CharacterSelection;
using Services.Analytics;
using Services.Audio;
using Services.Story;
using UnityEngine;
using Utils;
using Zenject;

namespace Gameplay.Dialog.Dialog
{
	public class DialogController : IInitializable, IDisposable
	{
		[Inject] public DialogAdapter dialogAdapter { get; private set; }
		[Inject] public DialogModel dialogModel { get; private set; }
		[Inject] public PortraitController portraitController { get; private set; }
		[Inject] public AudioManager audioManager { get; private set; }
		[Inject] public CharacterModel characterModel { get; private set; }

		[Inject] public AsyncProcessor asyncProcessor { get; private set; }
		[Inject] public SignalBus signalBus { get; private set; }

		public Action Continue;

		private string currentDialogText;
		private bool isTypingDialog;
		private Coroutine coroutine;

		public void Initialize()
		{
			dialogAdapter.continueButton.onClick.AddListener(OnContinueButtonClick);

			dialogAdapter.HideDialogUI();
			dialogAdapter.HideContinueButton();
		}

		public void Dispose()
		{
			StopAllCoroutines();
			dialogAdapter.continueButton.onClick.RemoveListener(OnContinueButtonClick);
		}

		private void StopAllCoroutines()
		{
			if (coroutine != null)
			{
				if (asyncProcessor != null)
				{
					asyncProcessor.StopCoroutine(coroutine);
				}

				coroutine = null;
			}
		}

		public void ShowDialogUI()
		{
			dialogAdapter.ShowDialogUI();
			dialogAdapter.ShowContinueButton();
			dialogModel.IsShowingDialog = true;
		}

		public void HideDialogUI()
		{
			dialogAdapter.HideDialogUI();
			dialogAdapter.HideContinueButton();
			portraitController.HidePortrait();
			dialogModel.IsShowingDialog = false;
		}

		public void SetCharacterName()
		{
			if (dialogModel.characterName == StringConstants.PLAYER_PORTRAIT)
			{
				dialogAdapter.characterNameText.text = characterModel.characterName;
				portraitController.ShowPortrait(characterModel.characterName);	
			}
			else
			{
				dialogAdapter.characterNameText.text = dialogModel.characterName;
				portraitController.ShowPortrait(dialogModel.characterName);
			}
		}

		public void SetDialogText()
		{
			if (dialogModel.dialogText.Length > DialogModel.MAX_CHARACTERS_PER_DIALOG)
			{
				Debug.LogError("[DialogController] SetDialogText: text too long! length=" + dialogModel.dialogText.Length + "/" + DialogModel.MAX_CHARACTERS_PER_DIALOG + ",  text=" + dialogModel.dialogText);
			}

			currentDialogText = dialogModel.dialogText;
			StartTypewriterEffect(dialogModel.dialogText);
			portraitController.PlayTalkAnimation();
		}

		public void StartTypewriterEffect(string dialogText)
		{
			StopAllCoroutines();
			coroutine = asyncProcessor.StartCoroutine(TypewriterEffectCoroutine(dialogText));
		}

		private IEnumerator TypewriterEffectCoroutine(string dialogText)
		{
			isTypingDialog = true;
			dialogAdapter.dialogText.text = StringConstants.COLOR_START_TAG + dialogText + StringConstants.COLOR_END_TAG;

			var voiceId = portraitController.GetVoiceSoundId(dialogModel.characterName);
			bool defaultVoice = (voiceId == SoundData.SoundId.DIALOG_BEEP || !audioManager.HasSound(voiceId));
			if (!defaultVoice)
			{
				audioManager.Play(voiceId);
			}

			for (int i = 0; i < dialogText.Length; i++)
			{
				yield return new WaitForSeconds(portraitController.GetDialogSpeed(dialogModel.characterName));
				if (defaultVoice)
					audioManager.Play(SoundData.SoundId.DIALOG_BEEP);

				var dialogPre = dialogText.Substring(0, i);
				var dialogPost = dialogText.Substring(i, dialogText.Length - 1 - i);
				dialogAdapter.dialogText.text = dialogPre + StringConstants.COLOR_START_TAG + dialogPost + StringConstants.COLOR_END_TAG;
			}

			if (!defaultVoice)
			{
				audioManager.Stop(voiceId);
			}

			isTypingDialog = false;
			dialogAdapter.dialogText.text = dialogText;
			SetIdleAnimation();
		}

		private void OnContinueButtonClick()
		{
			if (dialogModel.IsShowingDialog)
			{
				if (isTypingDialog)
				{
					StopAllCoroutines();
					var voiceId = portraitController.GetVoiceSoundId(dialogModel.characterName);
					bool defaultVoice = (voiceId == SoundData.SoundId.DIALOG_BEEP || !audioManager.HasSound(voiceId));
					if (!defaultVoice)
					{
						audioManager.Stop(voiceId);
					}

					isTypingDialog = false;
					dialogAdapter.dialogText.text = currentDialogText;
					SetIdleAnimation();
				}
				else
				{
					signalBus.Fire(new AnalyticsSignal(AnalyticsModel.DIALOG_CONTINUE_CLICK));

					if (Continue != null)
						Continue();
				}
			}
		}

		private void SetIdleAnimation()
		{
			if (dialogModel.tag == "")
			{
				portraitController.PlayIdleAnimation();
			}
			else
			{
				switch (dialogModel.tag)
				{
					case StringConstants.PORTRAIT_BLOOD_TAG:
						portraitController.PlayBloodAnimation();
						break;
					case StringConstants.PORTRAIT_SAD_TAG:
						portraitController.PlaySadAnimation();
						break;
					case StringConstants.PORTRAIT_ANGRY_TAG:
						portraitController.PlayAngryAnimation();
						break;
					case StringConstants.PORTRAIT_SMILE_TAG:
						portraitController.PlaySmileAnimation();
						break;
					case StringConstants.PORTRAIT_SURPRISE_TAG:
						portraitController.PlaySurpriseAnimation();
						break;
					case StringConstants.PORTRAIT_CONFUSED_TAG:
						portraitController.PlayConfusedAnimation();
						break;
					default:
						portraitController.PlayIdleAnimation();
						break;
				}
			}
		}
	}
}