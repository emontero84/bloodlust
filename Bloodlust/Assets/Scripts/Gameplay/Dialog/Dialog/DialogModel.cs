namespace Gameplay.Dialog.Dialog
{
	public class DialogModel
	{
		public bool IsShowingDialog;
		public string characterName;
		public string dialogText;
		public string tag;

		public const int MAX_CHARACTERS_PER_DIALOG = 180;
		public const int MAX_CHARACTERS_PER_DIALOG_CHOICE = 75;
		public const int MAX_CHARACTERS_PER_QUEST_TILE = 35;
		public const int MAX_CHARACTERS_PER_QUEST_DESCRIPTION = 150;
		public const int MAX_CHARACTERS_PER_QUEST_STEP = 41;
	}
}