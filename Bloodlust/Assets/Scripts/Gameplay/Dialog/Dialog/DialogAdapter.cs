﻿using UnityEngine;
using UnityEngine.UI;

namespace Gameplay.Dialog.Dialog
{
	public class DialogAdapter : MonoBehaviour
	{
		public RectTransform dialogUI;
		public RectTransform overlay;
		public Text characterNameText;
		public Text dialogText;
		public Button continueButton;

		public void ShowDialogUI()
		{
			dialogUI.gameObject.SetActive(true);
			overlay.gameObject.SetActive(true);
			characterNameText.text = "";
			dialogText.text = "";
		}

		public void HideDialogUI()
		{
			dialogUI.gameObject.SetActive(false);
			overlay.gameObject.SetActive(false);
		}

		public void ShowContinueButton()
		{
			continueButton.gameObject.SetActive(true);
		}
		
		public void HideContinueButton()
		{
			continueButton.gameObject.SetActive(false);
		}
	}
}