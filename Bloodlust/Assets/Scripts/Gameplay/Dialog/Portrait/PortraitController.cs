﻿using Services.Audio;
using Zenject;

namespace Gameplay.Dialog.Portrait
{
	public class PortraitController : IInitializable
	{
		[Inject] public PortraitAdapter portraitAdapter { get; private set; }

		public void Initialize()
		{
			HidePortrait();
		}

		public void HidePortrait()
		{
			portraitAdapter.HidePortrait();
		}

		public void ShowPortrait(string character)
		{
			portraitAdapter.ShowPortrait(character);
		}

		public void PlayIdleAnimation()
		{
			portraitAdapter.PlayAnimation(PortraitAnimations.PortraitIdleAnimation);
		}

		public void PlayTalkAnimation()
		{
			portraitAdapter.PlayAnimation(PortraitAnimations.PortraitTalkAnimation);
		}

		public void PlayBloodAnimation()
		{
			portraitAdapter.PlayAnimation(PortraitAnimations.PortraitBloodAnimation);
		}

		public void PlaySadAnimation()
		{
			portraitAdapter.PlayAnimation(PortraitAnimations.PortraitSadAnimation);
		}

		public void PlayAngryAnimation()
		{
			portraitAdapter.PlayAnimation(PortraitAnimations.PortraitAngryAnimation);
		}
		
		public void PlaySmileAnimation()
		{
			portraitAdapter.PlayAnimation(PortraitAnimations.PortraitSmileAnimation);
		}

		public void PlaySurpriseAnimation()
		{
			portraitAdapter.PlayAnimation(PortraitAnimations.PortraitSurpriseAnimation);
		}

		public void PlayConfusedAnimation()
		{
			portraitAdapter.PlayAnimation(PortraitAnimations.PortraitConfusedAnimation);
		}

		public float GetDialogSpeed(string character)
		{
			var data = portraitAdapter.GetPortraitData(character);
			if (data != null)
			{
				return data.dialogSpeed;
			}
			return PortraitData.DEFAULT_DIALOG_SPEED;
		}

		public SoundData.SoundId GetVoiceSoundId(string character)
		{
			var data = portraitAdapter.GetPortraitData(character);
			if (data != null)
			{
				return data.voice;
			}
			return SoundData.SoundId.DIALOG_BEEP;
		}
	}
}