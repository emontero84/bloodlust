using UnityEngine;

namespace Gameplay.Dialog.Portrait
{
	public class PortraitAnimations
	{
		public static readonly int PortraitIdleAnimation = Animator.StringToHash("PortraitIdleAnimation");
		public static readonly int PortraitTalkAnimation = Animator.StringToHash("PortraitTalkAnimation");
		public static readonly int PortraitBloodAnimation = Animator.StringToHash("PortraitBloodAnimation");
		public static readonly int PortraitSadAnimation = Animator.StringToHash("PortraitSadAnimation");
		public static readonly int PortraitAngryAnimation = Animator.StringToHash("PortraitAngryAnimation");
		public static readonly int PortraitSmileAnimation = Animator.StringToHash("PortraitSmileAnimation");
		public static readonly int PortraitConfusedAnimation = Animator.StringToHash("PortraitConfusedAnimation");
		public static readonly int PortraitSurpriseAnimation = Animator.StringToHash("PortraitSurpriseAnimation");

	}
}