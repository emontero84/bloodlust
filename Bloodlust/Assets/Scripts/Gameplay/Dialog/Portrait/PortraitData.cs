﻿using System;
using Services.Audio;
using UnityEngine;

namespace Gameplay.Dialog.Portrait
{
	[Serializable]
	public class PortraitData
	{
		public static float DEFAULT_DIALOG_SPEED = 0.03f;
		
		public string character;
		public RuntimeAnimatorController  characterPortrait;
		[Range(0.01f, 0.1f)]
		public float dialogSpeed = DEFAULT_DIALOG_SPEED;
		public SoundData.SoundId voice;
	}
}