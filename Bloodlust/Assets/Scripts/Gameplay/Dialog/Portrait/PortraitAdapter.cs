﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Gameplay.Dialog.Portrait
{
	public class PortraitAdapter : MonoBehaviour
	{
		public List<PortraitData> portraitPalette;
		public Image portrait;
		public Animator animator;

		private Dictionary<string, PortraitData> portraitPaletteDictionary;

		public void Awake()
		{
			portraitPaletteDictionary = new Dictionary<string, PortraitData>();
			foreach (var data in portraitPalette)
			{
				portraitPaletteDictionary.Add(data.character, data);
			}
		}

		public void HidePortrait()
		{
			portrait.gameObject.SetActive(false);
		}

		public PortraitData GetPortraitData(string character)
		{
			if (portraitPaletteDictionary.ContainsKey(character))
			{
				var data = portraitPaletteDictionary[character];
				return data;
			}
			Debug.LogWarning("[PortraitAdapter] There's no character " + character + " in portrait dictionary!");
			return null;
		}

		public void ShowPortrait(string character)
		{
			if (portraitPaletteDictionary.ContainsKey(character))
			{
				var portraitToShow = portraitPaletteDictionary[character];
				animator.runtimeAnimatorController = portraitToShow.characterPortrait;
				animator.Play(0);
				portrait.gameObject.SetActive(true);
			}
			else
			{
				portrait.gameObject.SetActive(false);
				Debug.LogWarning("[PortraitAdapter] There's no character " + character + " in portrait dictionary!");
			}
		}

		public void PlayAnimation(int animation)
		{
			animator.Play(animation);
		}
	}
}