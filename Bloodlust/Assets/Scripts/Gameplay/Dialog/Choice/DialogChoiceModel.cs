
using System.Collections.Generic;

namespace Gameplay.Dialog.Choice
{
	public class DialogChoiceModel
	{
		public bool isShowingDialogChoice;
		public int startingChoiceIndex;
		public List<string> choiceTexts;
		public const int MAX_CHOICES = 4;

	}
}