﻿using System;
using System.Collections.Generic;
using Components;
using Gameplay.Adventure.Actions;
using Gameplay.Dialog.Portrait;
using MainMenu;
using MainMenu.CharacterSelection;
using Services.Analytics;
using Services.Audio;
using Services.Story;
using UnityEngine;
using Utils;
using Zenject;

namespace Gameplay.Dialog.Choice
{
	public class DialogChoiceController : IInitializable, IDisposable, ITickable
	{
		[Inject] public SignalBus signalBus { get; private set; }
		[Inject] public DialogChoiceAdapter dialogChoiceAdapter { get; private set; }
		[Inject] public DialogChoiceModel dialogChoiceModel { get; private set; }
		[Inject] public StoryService storyService { get; private set; }
		[Inject] public PortraitController portraitController { get; private set; }
		[Inject] public ActionsController actionsController { get; private set; }
		[Inject] public CharacterModel characterModel { get; private set; }
        [Inject] public MainMenuModel mainMenuModel { get; private set; }


        private ButtonController[] dialogChoiceButtons;

		public void Initialize()
		{
			dialogChoiceAdapter.upArrowButton.onClick.AddListener(OnUpArrowButtonMouseClick);
			dialogChoiceAdapter.downArrowButton.onClick.AddListener(OnDownArrowButtonMouseClick);

			dialogChoiceButtons = dialogChoiceAdapter.dialogChoicesContainer.GetComponentsInChildren<ButtonController>();
			foreach (var choice in dialogChoiceButtons)
			{
				choice.ButtonMouseEnter += OnDialogChoiceMouseEnter;
				choice.ButtonMouseExit += OnDialogChoiceMouseExit;
				choice.ButtonMouseClick += OnDialogChoiceClick;
			}
		}

		public void Dispose()
		{
			dialogChoiceAdapter.upArrowButton.onClick.RemoveListener(OnUpArrowButtonMouseClick);
			dialogChoiceAdapter.downArrowButton.onClick.RemoveListener(OnDownArrowButtonMouseClick);

			if (dialogChoiceButtons != null)
			{
				foreach (var choice in dialogChoiceButtons)
				{
					choice.ButtonMouseEnter -= OnDialogChoiceMouseEnter;
					choice.ButtonMouseExit -= OnDialogChoiceMouseExit;
					choice.ButtonMouseClick -= OnDialogChoiceClick;
				}

				dialogChoiceButtons = null;
			}
		}

		public void Tick()
		{
            if (mainMenuModel.screen == MainMenuModel.MainMenuScreen.Gameplay)
            {
			    if (dialogChoiceModel.isShowingDialogChoice)
			    {
				    for (int i = 1; i <= dialogChoiceModel.choiceTexts.Count; i++)
				    {
					    if (Input.GetKeyUp(i.ToString()))
					    {
						    DialogChoiceByKeyboard(i);
					    }
				    }

				    if (dialogChoiceModel.choiceTexts.Count > DialogChoiceModel.MAX_CHOICES)
				    {
					    if (Input.GetKeyUp(KeyCode.UpArrow) || Input.GetAxis(StringConstants.MOUSE_SCROLL_WHEEL) > 0f)
					    {
						    SetDialogChoiceUpArrow();
					    }
					    else if (Input.GetKeyUp(KeyCode.DownArrow) || Input.GetAxis(StringConstants.MOUSE_SCROLL_WHEEL) < 0f)
					    {
						    SetDialogChoiceDownArrow();
					    }
				    }
			    }
            }
		}

		private void OnUpArrowButtonMouseClick()
		{
			SetDialogChoiceUpArrow();
		}

		private void OnDownArrowButtonMouseClick()
		{
			SetDialogChoiceDownArrow();
		}

		public void ShowDialogChoiceUI()
		{
			dialogChoiceAdapter.ShowDialogChoiceUI();
			portraitController.ShowPortrait(characterModel.characterName);
			dialogChoiceModel.isShowingDialogChoice = true;
		}

		public void HideDialogChoiceUI()
		{
			dialogChoiceAdapter.HideDialogChoiceUI();
			portraitController.HidePortrait();
			dialogChoiceModel.isShowingDialogChoice = false;
		}

		public void ShowChoiceText(int index, int realIndex, string text)
		{
			dialogChoiceAdapter.SetChoiceText(index, realIndex, text);
		}

		public void ShowChoiceMouseOver(int index, int realIndex, string text)
		{
			dialogChoiceAdapter.ShowChoiceMouseOver(index, realIndex, text);
		}

		public void HideChoiceAtIndex(int index)
		{
			dialogChoiceAdapter.HideChoiceAtIndex(index);
		}

		public void SetDialogChoices(List<string> choices)
		{
			dialogChoiceModel.startingChoiceIndex = 0;
			dialogChoiceModel.choiceTexts = choices;
		}

		public void ShowDialogChoices()
		{
			int i = 0;
			for (; i < dialogChoiceModel.choiceTexts.Count && i < DialogChoiceModel.MAX_CHOICES; i++)
			{
				ShowChoiceText(i, dialogChoiceModel.startingChoiceIndex + i + 1, dialogChoiceModel.choiceTexts[dialogChoiceModel.startingChoiceIndex + i]);
			}

			for (; i < DialogChoiceModel.MAX_CHOICES; i++)
			{
				HideChoiceAtIndex(i);
			}
		}

		public void ShowArrowButtons()
		{
			dialogChoiceAdapter.ShowArrowButtons();
		}

		public void HideArrowButtons()
		{
			dialogChoiceAdapter.HideArrowButtons();
		}

		private void OnDialogChoiceMouseEnter(string id)
		{
			int index = Int32.Parse(id);
			ShowChoiceMouseOver(index, dialogChoiceModel.startingChoiceIndex + index + 1, dialogChoiceModel.choiceTexts[dialogChoiceModel.startingChoiceIndex + index]);
		}

		private void OnDialogChoiceMouseExit(string id)
		{
			int index = Int32.Parse(id);
			ShowChoiceText(index, dialogChoiceModel.startingChoiceIndex + index + 1, dialogChoiceModel.choiceTexts[dialogChoiceModel.startingChoiceIndex + index]);
		}

		private void OnDialogChoiceClick(string id, bool leftClick)
		{
			if (!dialogChoiceModel.isShowingDialogChoice)
				return;

			int index = Int32.Parse(id);
			signalBus.Fire(new AnalyticsSignal(AnalyticsModel.DIALOG_CHOICE_CLICK, "Index", index.ToString()));

			SetDialogChoice(dialogChoiceModel.startingChoiceIndex + index);
		}

		private void DialogChoiceByKeyboard(int number)
		{
			int index = number - 1;
			if (index < 0 || index >= dialogChoiceModel.choiceTexts.Count)
			{
				Debug.LogError("[DialogChoiceController] Keyboard press outside of dialog choices! " + number);
				return;
			}

			signalBus.Fire(new AnalyticsSignal(AnalyticsModel.DIALOG_CHOICE_KEY, "Index", index.ToString()));

			SetDialogChoice(index);
		}

		private void SetDialogChoice(int index)
		{
			Debug.Log("DIALOG CHOICE! " + index);
			storyService.ChooseChoiceIndex(index);
			actionsController.Continue();

			signalBus.Fire(new PlayAudioSignal(SoundData.SoundId.GENERIC_CLICK));
		}

		private void SetDialogChoiceUpArrow()
		{
			dialogChoiceModel.startingChoiceIndex--;
			if (dialogChoiceModel.startingChoiceIndex < 0)
			{
				dialogChoiceModel.startingChoiceIndex = 0;
			}

			ShowDialogChoices();
			signalBus.Fire(new PlayAudioSignal(SoundData.SoundId.GENERIC_CLICK));
		}

		private void SetDialogChoiceDownArrow()
		{
			dialogChoiceModel.startingChoiceIndex++;
			if (dialogChoiceModel.startingChoiceIndex > storyService.GetNumberOfChoices() - DialogChoiceModel.MAX_CHOICES)
			{
				dialogChoiceModel.startingChoiceIndex = storyService.GetNumberOfChoices() - DialogChoiceModel.MAX_CHOICES;
			}

			ShowDialogChoices();
			signalBus.Fire(new PlayAudioSignal(SoundData.SoundId.GENERIC_CLICK));
		}

		public void ShowOrHideDialogChoiceArrows()
		{
			var choices = storyService.CurrentChoices();
			if (choices.Count > DialogChoiceModel.MAX_CHOICES)
			{
				ShowArrowButtons();
			}
			else
			{
				HideArrowButtons();
			}
		}
	}
}