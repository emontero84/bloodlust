﻿using System.Collections.Generic;
using Components;
using Gameplay.Dialog.Dialog;
using UnityEngine;
using UnityEngine.UI;

namespace Gameplay.Dialog.Choice
{
	public class DialogChoiceAdapter : MonoBehaviour
	{
		public RectTransform dialogChoiceUI;
		public RectTransform overlay;

		public RectTransform dialogChoicesContainer;
		public List<Text> dialogChoiceTexts;
		public Button upArrowButton;
		public Button downArrowButton;

		private string colorStartTag = "<color=000000>";
		private string colorEndTag = "</color>";

		public void ShowDialogChoiceUI()
		{
			dialogChoiceUI.gameObject.SetActive(true);
			overlay.gameObject.SetActive(true);
		}

		public void HideDialogChoiceUI()
		{
			dialogChoiceUI.gameObject.SetActive(false);
			overlay.gameObject.SetActive(false);
		}

		public void SetChoiceText(int index, int realIndex, string text)
		{
			if (text.Length > DialogModel.MAX_CHARACTERS_PER_DIALOG_CHOICE)
			{
				Debug.LogError("[DialogChoiceAdapter] SetChoiceText: choice too long! length=" + text.Length + "/" + DialogModel.MAX_CHARACTERS_PER_DIALOG_CHOICE + ",  text=" + text);
			}
			if (index >= 0 && index < dialogChoiceTexts.Count)
			{
				var dialogChoiceText = dialogChoiceTexts[index];
				dialogChoiceText.text = realIndex + ". " + colorStartTag + text + colorEndTag;
				dialogChoiceText.gameObject.SetActive(true);
			}
		}

		public void ShowChoiceMouseOver(int index, int realIndex, string text)
		{
			if (index >= 0 && index < dialogChoiceTexts.Count)
			{
				var dialogChoiceText = dialogChoiceTexts[index];
				dialogChoiceText.text = realIndex + ". " +  text ;
				dialogChoiceText.gameObject.SetActive(true);
			}
		}

		public void HideChoiceAtIndex(int index)
		{
			if (index >= 0 && index < dialogChoiceTexts.Count)
			{
				var dialogChoiceText = dialogChoiceTexts[index];
				dialogChoiceText.gameObject.SetActive(false);
			}
		}

		public void ShowArrowButtons()
		{
			upArrowButton.gameObject.SetActive(true);
			downArrowButton.gameObject.SetActive(true);
		}

		public void HideArrowButtons()
		{
			upArrowButton.gameObject.SetActive(false);
			downArrowButton.gameObject.SetActive(false);
		}
	}
}