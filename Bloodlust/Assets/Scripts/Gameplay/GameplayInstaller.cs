using Components;
using Gameplay.Adventure.Actions;
using Gameplay.Adventure.Inventory;
using Gameplay.Adventure.Items.Hint;
using Gameplay.Adventure.QuestLog;
using Gameplay.Adventure.Room;
using Gameplay.Adventure.Room.Shadow;
using Gameplay.Adventure.Room.Show;
using Gameplay.Adventure.Room.Transition;
using Gameplay.Dialog.Choice;
using Gameplay.Dialog.Dialog;
using Gameplay.Dialog.Portrait;
using Gameplay.Progression;
using Services.Audio;
using Services.Story;
using Utils;
using Zenject;

namespace Gameplay
{
	public class GameplayInstaller : MonoInstaller
	{
		public ShowPaletteModel showPaletteModel;
		public RoomAdapter roomAdapter;
		public RoomTransitionAdapter roomTransitionAdapter;
		public InventoryAdapter inventoryAdapter;
		public DialogChoiceAdapter dialogChoiceAdapter;
		public DialogAdapter dialogAdapter;
		public PortraitAdapter portraitAdapter;
		public QuestLogAdapter questLogAdapter;
		public PlayerShadowAdapter playerShadowAdapter;
		public MainCameraAdapter mainCameraAdapter;
		public ItemHintAdapter itemHintAdapter;

		public override void InstallBindings()
		{
			Container.BindSignal<InitializeSignal>().ToMethod<InitializeStoryCommand>(x => x.OnSignal).FromResolve();
			Container.BindInterfacesAndSelfTo<InitializeStoryCommand>().AsSingle();

			Container.BindInterfacesAndSelfTo<ShowPaletteController>().FromNew().AsSingle();
			Container.BindInterfacesAndSelfTo<ShowPaletteModel>().FromInstance(showPaletteModel).AsSingle();

			Container.BindInterfacesAndSelfTo<RoomModel>().FromNew().AsSingle();
			Container.BindInterfacesAndSelfTo<RoomAdapter>().FromInstance(roomAdapter).AsSingle();
			Container.BindInterfacesAndSelfTo<RoomController>().FromNew().AsSingle();
			Container.BindInterfacesAndSelfTo<RoomTransitionAdapter>().FromInstance(roomTransitionAdapter).AsSingle();
			Container.BindInterfacesAndSelfTo<RoomTransitionController>().FromNew().AsSingle();

			Container.BindInterfacesAndSelfTo<ActionsModel>().FromNew().AsSingle();
			Container.BindInterfacesAndSelfTo<ActionsController>().FromNew().AsSingle();

			Container.BindInterfacesAndSelfTo<ItemHintController>().FromNew().AsSingle();
			Container.BindInterfacesAndSelfTo<ItemHintAdapter>().FromInstance(itemHintAdapter).AsSingle();

			Container.BindInterfacesAndSelfTo<InventoryModel>().FromNew().AsSingle();
			Container.BindInterfacesAndSelfTo<InventoryController>().FromNew().AsSingle();
			Container.BindInterfacesAndSelfTo<InventoryAdapter>().FromInstance(inventoryAdapter).AsSingle();

			Container.BindInterfacesAndSelfTo<DialogAdapter>().FromInstance(dialogAdapter).AsSingle();
			Container.BindInterfacesAndSelfTo<DialogController>().FromNew().AsSingle();
			Container.BindInterfacesAndSelfTo<DialogModel>().FromNew().AsSingle();

			Container.BindInterfacesAndSelfTo<DialogChoiceController>().FromNew().AsSingle();
			Container.BindInterfacesAndSelfTo<DialogChoiceModel>().FromNew().AsSingle();
			Container.BindInterfacesAndSelfTo<DialogChoiceAdapter>().FromInstance(dialogChoiceAdapter).AsSingle();

			Container.BindInterfacesAndSelfTo<PortraitController>().FromNew().AsSingle();
			Container.BindInterfacesAndSelfTo<PortraitAdapter>().FromInstance(portraitAdapter).AsSingle();

			Container.BindInterfacesAndSelfTo<MouseClickMover>().FromComponentInHierarchy().AsSingle();

			Container.BindInterfacesAndSelfTo<QuestLogModel>().FromNew().AsSingle();
			Container.BindInterfacesAndSelfTo<QuestLogController>().FromNew().AsSingle();
			Container.BindInterfacesAndSelfTo<QuestLogAdapter>().FromInstance(questLogAdapter).AsSingle();
			Container.DeclareSignal<QuestLogSignal>().WithId(QuestLogSignal.Type.SHOW_QUEST_LOG);
			Container.BindInterfacesAndSelfTo<ShowQuestLogCommand>().AsSingle();
			Container.BindSignal<QuestLogSignal>().WithId(QuestLogSignal.Type.SHOW_QUEST_LOG).ToMethod<ShowQuestLogCommand>(x => x.OnSignal).FromResolve();

			Container.BindInterfacesAndSelfTo<PlayerShadowAdapter>().FromInstance(playerShadowAdapter).AsSingle();
			Container.BindInterfacesAndSelfTo<PlayerShadowController>().FromNew().AsSingle();

			Container.BindInterfacesAndSelfTo<KeyboardInputController>().FromNew().AsSingle();
			Container.BindInterfacesAndSelfTo<MainCameraAdapter>().FromInstance(mainCameraAdapter).AsSingle();
			
			Container.DeclareSignal<PlayMusicSignal>();
			Container.BindInterfacesAndSelfTo<PlayMusicCommand>().FromNew().AsSingle();
			Container.BindSignal<PlayMusicSignal>().ToMethod<PlayMusicCommand>(x => x.OnSignal).FromResolve();

		}
	}
}