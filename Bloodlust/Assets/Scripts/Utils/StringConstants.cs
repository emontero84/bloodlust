namespace Utils
{
	public class StringConstants
	{
		// Story external functions
		public const string SHOW = "ShowItem";
		public const string HIDE = "HideItem";
		public const string ADD_ITEM_TO_INVENTORY = "AddItemToInventory";
		public const string REMOVE_ITEM_FROM_INVENTORY = "RemoveItemFromInventory";
		public const string ADD_QUEST_TO_LOG = "AddQuestToLog";
		public const string UPDATE_QUEST_TO_LOG = "UpdateQuestToLog";
		public const string GO_TO_ROOM = "GoToRoom";
		public const string PLAY_SOUND = "PlaySound";
		public const string STOP_SOUND = "StopSound";
		public const string PLAYER_ANIMATION = "PlayerAnimation";
		public const string WAIT = "Wait";

		// Player animations
		public const string PLAYER_INTERACT = "Interact";
		public const string PLAYER_KICK = "Kick";
		public const string PLAYER_HACK = "Hack";

		// Story tags
		public const string CHOICE_TAG = "choice";
		public const string PORTRAIT_BLOOD_TAG = "portrait:blood";
		public const string PORTRAIT_SAD_TAG = "portrait:sad";
		public const string PORTRAIT_ANGRY_TAG = "portrait:angry";
		public const string PORTRAIT_SMILE_TAG = "portrait:smile";
		public const string PORTRAIT_SURPRISE_TAG = "portrait:surprise";
		public const string PORTRAIT_CONFUSED_TAG = "portrait:confused";
		public const string END_STORY_TAG = "end";
		public const string INVENTORY_ITEMS = "inventory items";

		// Story variables
		public const string QUESTS_VARIABLE = "Quests";
		public const string QUEST_STEPS_VARIABLE = "QuestSteps";
		public const string INVENTORY_VARIABLE = "Inventory";
		public const string ROOMS_VARIABLE = "Rooms";
		public const string ROOM_ITEMS_TO_SHOW_VARIABLE = "RoomItemsToShow";

		// Player character names and descriptions
		public const string NAME_ASH = "Ash";
		public const string NAME_CAT = "Cat";
		public const string NAME_MATT = "Matt";
		public const string NAME_STUART = "Stuart";
		public const string NAME_CHLOE = "Chloe";
		public const string NAME_GIANNA = "Gianna";
		public const string NAME_JESS = "Jess";

		public const string DESCRIPTION_ASH = "Raw, loud and straight to the gut.";
		public const string DESCRIPTION_CAT = "Deadly and lonesome as a stray animal.";
		public const string DESCRIPTION_MATT = "Witness of the Abyss and other unspeakable horrors.";
		public const string DESCRIPTION_STUART = "Deep in the shadows, a rat whispers in your ear.";
		public const string DESCRIPTION_CHLOE = "Mistress of finesse and seduction.";
		public const string DESCRIPTION_GIANNA = "Keeper of arcane mysteries and traditions.";
		public const string DESCRIPTION_JESS = "Your blood is weak but your will is strong.";

		// Skill variables
		public const string SKILL_INTIMIDATE = "SkillIntimidate";
		public const string SKILL_PERSUADE = "SkillPersuade";
		public const string SKILL_SEDUCE = "SkillSeduce";
		public const string SKILL_KICK = "SkillKick";
		public const string SKILL_STEAL = "SkillSteal";
		public const string SKILL_HACK = "SkillHack";
		public const string SKILL_LOCKPICKING = "SkillLockpicking";
		public const string SKILL_BRIBE = "SkillBribe";

		// Quest log
		public const string QUEST_INDEX_TEXT = "%CURRENT% of %MAX%";
		public const string QUEST_LOG_ITEM_NAME = "notebook";

		// Save games
		public const string SAVE_GAMES_FILE_ID = "savedgames";
		public const string SAVE_GAMES_FILE_PATH = "/savedgames.json";
		public const string SAVE_GAMES_EMPTY = "{\"savedGames\": [{\"name\": \"Empty Slot 1\",\"json\": \"\",\"date\": \"--\",\"x\": 0.0,\"y\": 0.0,\"screenshot\": []},{\"name\": \"Empty Slot 2\",\"json\": \"\",\"date\": \"--\",\"x\": 0.0,\"y\": 0.0,\"screenshot\": []},{\"name\": \"Empty Slot 3\",\"json\": \"\",\"date\": \"--\",\"x\": 0.0,\"y\": 0.0,\"screenshot\": []},{\"name\": \"Empty Slot 4\",\"json\": \"\",\"date\": \"--\",\"x\": 0.0,\"y\": 0.0,\"screenshot\": []}],\"index\": -1}";

		// Portraits
		public const string PLAYER_PORTRAIT = "Player";

		// Actions
		public const string WALK_TO = "Walk to";
		public const string LOOK_AT = "Look at";
		public const string PICK_UP = "Pick up";
		public const string TALK_TO = "Talk to";
		public const string GIVE = "Give";
		public const string USE = "Use";
		public const string INTIMIDATE = "Intimidate";
		public const string PERSUADE = "Persuade";
		public const string SEDUCE = "Seduce";
		public const string KICK = "Kick";
		public const string STEAL = "Steal";
		public const string HACK = "Hack";
		public const string LOCKPICKING = "Lockpick";
		public const string BRIBE = "Bribe";

		// Default wrong action texts
		public const string DEFAULT_LOOK_AT = "Player: I don't see anything special about that.";
		public const string DEFAULT_PICK_UP = "Player: I'm not carrying that with me.";
		public const string DEFAULT_TALK_TO = "Player: Probably not a good idea.";
		public const string DEFAULT_GIVE = "Player: Probably not a good idea.";
		public const string DEFAULT_USE = "Player: That doesn't seem to work.";
		public const string DEFAULT_COMBINE = "Player: I can't make these two things work together.";

		// String constants
		public const string QUESTION_MARK = "?";
		public const string COLOR_START_TAG = "<color=#00000000>";
		public const string COLOR_END_TAG = "</color>";

		// Volume mixer exposed parameters (and PlayerPrefs for options)
		public const string MUSIC_VOLUME = "MusicVolume";
		public const string SOUND_VOLUME = "SoundVolume";
		public const string DIALOG_VOLUME = "DialogVolume";
		public const string WALK_SPEED = "WalkSpeed";

		// Unity mouse wheel action
		public const string MOUSE_SCROLL_WHEEL = "Mouse ScrollWheel";
	}
}