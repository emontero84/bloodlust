using DG.Tweening;
using UnityEngine;

namespace Components
{
	public class LightAnimation : MonoBehaviour
	{
		private Light light;
		private float minIntensity;
		public float maxIntensity;
		public float duration;

		private void Start()
		{
			light = GetComponent<Light>();
			minIntensity = light.intensity;
			light.DOKill();

			var sequence = DOTween.Sequence();
			sequence.Append(light.DOIntensity(maxIntensity, duration/2));
			sequence.Append(light.DOIntensity(minIntensity, duration/2));
			sequence.SetLoops(-1);
			sequence.Play();
		}

		public void OnDestroy()
		{
			if (light != null)
			{
				light.DOKill();
				light = null;
			}
		}
	}
}