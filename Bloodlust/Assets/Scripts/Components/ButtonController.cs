﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Components
{
	public class ButtonController : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
	{
		public string id;
		public Action<string> ButtonMouseEnter;
		public Action<string> ButtonMouseExit;
		public Action<string, bool> ButtonMouseClick;

		public void OnPointerEnter(PointerEventData eventData)
		{
			if (ButtonMouseEnter != null)
				ButtonMouseEnter(id);
		}

		public void OnPointerExit(PointerEventData eventData)
		{
			if (ButtonMouseExit != null)
				ButtonMouseExit(id);
		}

		public void OnPointerClick(PointerEventData eventData)
		{
			bool leftClick = eventData.button == PointerEventData.InputButton.Left;
			if (ButtonMouseClick != null)
				ButtonMouseClick(id, leftClick);
		}
	}
}