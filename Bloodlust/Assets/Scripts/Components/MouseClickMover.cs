using System;
using System.Collections;
using System.Collections.Generic;
using Gameplay.Adventure.Actions;
using Gameplay.Adventure.Inventory;
using Gameplay.Adventure.QuestLog;
using Gameplay.Adventure.Room;
using Gameplay.Dialog.Choice;
using Gameplay.Dialog.Dialog;
using MainMenu.CharacterSelection;
using MainMenu.Options;
using Project.Progression;
using Services.Audio;
using Services.Story;
using UnityEngine;
using UnityEngine.AI;
using Utils;
using Zenject;

namespace Components
{
	public class MouseClickMover : MonoBehaviour
	{
		[Inject] public OptionsModel optionsModel { get; private set; }
		[Inject] public DialogModel dialogModel { get; private set; }
		[Inject] public ActionsModel actionsModel { get; private set; }
		[Inject] public DialogChoiceModel dialogChoiceModel { get; private set; }
		[Inject] public QuestLogModel questLogModel { get; private set; }
		[Inject] public PlayerModel playerModel { get; private set; }
		[Inject] public MainCameraAdapter mainCameraAdapter { get; private set; }
		[Inject] public ActionsController actionsController { get; private set; }
		[Inject] public InventoryController inventoryController { get; private set; }
		[Inject] public CharacterModel characterModel { get; private set; }
		[Inject] public RoomModel roomModel { get; private set; }
		[Inject] public SignalBus signalBus { get; private set; }
		[Inject] public AudioManager audioManager { get; private set; }
		[Inject] public AsyncProcessor asyncProcessor { get; private set; }

		public Action PlayerMoveFinish;

		public SpriteRenderer characterSpriteRenderer;
		public Animator characterAnimator;
		public NavMeshAgent agent;
		private float speed = 4f;
		private float maxSpeed = 20f;
		public List<RuntimeAnimatorController> characterAnimators;

		private float MIN_DISTANCE = 0.01f;
		private float verbsPanelTop;
		private Vector2 clickPosition;
		private static readonly int WalkAnimation = Animator.StringToHash("PlayerWalkAnimation");
		private static readonly int IdleAnimation = Animator.StringToHash("PlayerIdleAnimation");
		private static readonly int InteractAnimation = Animator.StringToHash("PlayerInteractAnimation");
		private static readonly int KickAnimation = Animator.StringToHash("PlayerKickAnimation");
		private static readonly int HackAnimation = Animator.StringToHash("PlayerHackAnimation");

		private Coroutine _coroutine;

		public void Start()
		{
			agent.updateRotation = false;
			agent.updateUpAxis = false;
			verbsPanelTop = Screen.height * 0.28f;
			var index = characterModel.characterIndex;
			characterAnimator.runtimeAnimatorController = characterAnimators[index];
			signalBus.Subscribe<StorySignal>(OnStorySignal);
		}

		private void OnDestroy()
		{
			signalBus.Unsubscribe<StorySignal>(OnStorySignal);
			StopAllCoroutines();
		}

		private void StopAllCoroutines()
		{
			if (_coroutine != null)
			{
				if (asyncProcessor != null)
				{
					asyncProcessor.StopCoroutine(_coroutine);
				}

				_coroutine = null;
			}
		}

		private void OnStorySignal(StorySignal signal)
		{
			if (signal.externalFunction == StringConstants.PLAYER_ANIMATION)
			{
				switch (signal.arg1)
				{
					case StringConstants.PLAYER_INTERACT:
						Interact();
						break;
					case StringConstants.PLAYER_KICK:
						Kick();
						break;
					case StringConstants.PLAYER_HACK:
						Hack();
						break;
				}
			}
		}

		public void Update()
		{
			if (dialogModel.IsShowingDialog || dialogChoiceModel.isShowingDialogChoice || questLogModel.isQuestLogVisible || roomModel.isGoingToRoom)
				return;

			if (Input.mousePosition.y < verbsPanelTop)
				return;

			if (actionsModel.isPlayerMovingToAction)
				return;

			if (Input.GetMouseButtonDown(0) && actionsModel.playerIsGoingToItem)
			{
				clickPosition = mainCameraAdapter.mainCamera.ScreenToWorldPoint(Input.mousePosition);
				MoveTo(clickPosition);
				actionsController.ResetAction();
				inventoryController.SetDefaultCursor();
			}
		}

		private void FixedUpdate()
		{
			if (agent.speed > 0)
			{
				characterSpriteRenderer.flipX = agent.velocity.x < 0;
			}

			if (agent.speed > 0 && !agent.pathPending && agent.remainingDistance < MIN_DISTANCE)
			{
				OnPlayerMoveFinished();
			}
		}

		public void SetPosition(Vector2 target)
		{
			characterSpriteRenderer.flipX = false;
			agent.speed = 0;
			agent.Warp(target);
			Idle();
			SavePlayerPosition();
		}

		public void MoveTo(Vector2 target)
		{
			Walk();
			agent.speed = speed + optionsModel.walkSpeed * maxSpeed;
			agent.SetDestination(target);
			audioManager.Play(SoundData.SoundId.STEP);
			_coroutine = asyncProcessor.StartCoroutine(WalkTimerCoroutine());
		}

		private IEnumerator WalkTimerCoroutine()
		{
			yield return new WaitForSeconds(10);
			Debug.LogError("[MouseClickMover] ERROR! Can't reach destination after 10 seconds.");
			OnPlayerMoveFinished();
		}

		private void OnPlayerMoveFinished()
		{
			StopAllCoroutines();
			agent.speed = 0;
			Idle();
			SavePlayerPosition();
			audioManager.Stop(SoundData.SoundId.STEP);

			if (PlayerMoveFinish != null)
				PlayerMoveFinish();
		}

		private void SavePlayerPosition()
		{
			var position = agent.transform.position;
			playerModel.x = position.x;
			playerModel.y = position.y;
			playerModel.isInitialized = true;
		}

		public void LoadPlayerPosition()
		{
			SetPosition(new Vector2(playerModel.x, playerModel.y));
		}

		public void Idle()
		{
			characterAnimator.Play(IdleAnimation);
		}

		public void Walk()
		{
			characterAnimator.Play(WalkAnimation);
		}

		public void Interact()
		{
			characterAnimator.Play(InteractAnimation);
		}
		
		public void Kick()
		{
			characterAnimator.Play(KickAnimation);
		}
		
		public void Hack()
		{
			characterAnimator.Play(HackAnimation);
		}
	}
}