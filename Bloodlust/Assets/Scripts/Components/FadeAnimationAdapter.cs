using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace Components
{
	public class FadeAnimationAdapter : MonoBehaviour
	{
		public Image view;
		public float initialAlpha = 0f;
		public float finalAlpha = 1f;
		public float duration;

		private void Start()
		{
			view.DOFade(initialAlpha, 0);
			view.DOFade(finalAlpha, duration);
		}
	}
}