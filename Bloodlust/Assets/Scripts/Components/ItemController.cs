﻿using System;
using System.Collections.Generic;
using Gameplay.Adventure.Actions;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Components
{
	public class ItemController : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
	{
		public string item;
		public List<Verbs.Verb> verbs;
		public Transform playerPosition;
		public Transform textPosition;

		public Action<ItemController> ItemMouseEnter;
		public Action ItemMouseExit;
		public Action<ItemController, bool> ItemMouseClick;

		private Transform positionToSend;

		public Vector2 PositionToSend()
		{
			return positionToSend.position;
		}

		private void Start()
		{
			var playerPositions = GetComponentInChildren<PlayerPosition>();
			var itemHintPositions = GetComponentInChildren<ItemHintPosition>();
			if (playerPositions != null)
			{
				playerPosition = playerPositions.transform;
			}
			if (itemHintPositions != null)
			{
				textPosition = itemHintPositions.transform;
			}
			positionToSend = textPosition != null ? textPosition : gameObject.transform;
		}

		public void OnPointerEnter(PointerEventData eventData)
		{
			if (ItemMouseEnter != null)
				ItemMouseEnter(this);
		}

		public void OnPointerExit(PointerEventData eventData)
		{
			if (ItemMouseExit != null)
				ItemMouseExit();
		}

		public void OnPointerClick(PointerEventData eventData)
		{
			bool leftClick = eventData.button == PointerEventData.InputButton.Left;
			if (ItemMouseClick != null)
				ItemMouseClick(this, leftClick);
		}
	}
}