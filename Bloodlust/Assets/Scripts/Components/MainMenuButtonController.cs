using UnityEngine;
using UnityEngine.EventSystems;

namespace Components
{
	public class MainMenuButtonController : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler, IPointerUpHandler
	{
		public RectTransform selector;
		public RectTransform normalText;
		public RectTransform mouseOverText;
		public RectTransform mouseClickText;

		private void Awake()
		{
			ShowNormalText();
		}

		public void ShowNormalText()
		{
			selector.gameObject.SetActive(false);
			normalText.gameObject.SetActive(true);
			mouseOverText.gameObject.SetActive(false);
			mouseClickText.gameObject.SetActive(false);
		}
		
		public void ShowMouseOverText()
		{
			selector.gameObject.SetActive(true);
			normalText.gameObject.SetActive(false);
			mouseOverText.gameObject.SetActive(true);
			mouseClickText.gameObject.SetActive(false);
		}

		public void ShowMouseDownText()
		{
			selector.gameObject.SetActive(true);
			normalText.gameObject.SetActive(false);
			mouseOverText.gameObject.SetActive(false);
			mouseClickText.gameObject.SetActive(true);
		}
		
		public void OnPointerEnter(PointerEventData eventData)
		{
			ShowMouseOverText();
		}

		public void OnPointerExit(PointerEventData eventData)
		{
			ShowNormalText();
		}

		public void OnPointerDown(PointerEventData eventData)
		{
			ShowMouseDownText();
		}

		public void OnPointerUp(PointerEventData eventData)
		{
			ShowNormalText();
		}
	}
}