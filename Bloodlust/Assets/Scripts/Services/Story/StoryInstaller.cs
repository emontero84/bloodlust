using UnityEngine;
using Zenject;

namespace Services.Story
{
	public class StoryInstaller : MonoInstaller<StoryInstaller>
	{
		public TextAsset storyJson;

		public override void InstallBindings()
		{
			Container.BindInterfacesAndSelfTo<StoryService>().FromNew().AsSingle();
			Container.BindInterfacesAndSelfTo<TextAsset>().FromInstance(storyJson).AsSingle();
			
			Container.DeclareSignal<StorySignal>();
			Container.DeclareSignal<InitializeSignal>();
		}
	}
}