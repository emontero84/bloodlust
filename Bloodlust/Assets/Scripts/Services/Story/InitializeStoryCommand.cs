using Components;
using Gameplay.Adventure.Actions;
using Gameplay.Adventure.Inventory;
using Gameplay.Adventure.QuestLog;
using Gameplay.Adventure.Room;
using Gameplay.Adventure.Room.Show;
using Ink.Runtime;
using MainMenu.CharacterSelection;
using Project.Loading;
using UnityEngine;
using Utils;
using Zenject;

namespace Services.Story
{
	public class InitializeStoryCommand
	{
		[Inject] public SignalBus signalBus { get; private set; }
		[Inject] public RoomModel roomModel { get; private set; }
		[Inject] public ActionsModel actionsModel { get; private set; }
		[Inject] public QuestLogModel questLogModel { get; private set; }
		[Inject] public StoryService storyService { get; private set; }
		[Inject] public QuestLogController questLogController { get; private set; }
		[Inject] public InventoryController inventoryController { get; private set; }
		[Inject] public RoomController roomController { get; private set; }
		[Inject] public ShowPaletteController showPaletteController { get; private set; }
		[Inject] public MouseClickMover mouseClickMover { get; private set; }
		[Inject] public ActionsController actionsController { get; private set; }
		[Inject] public CharacterModel characterModel { get; private set; }

		public void OnSignal(InitializeSignal signal)
		{
			if (roomModel.isInitialized && actionsModel.isInitialized && questLogModel.isInitialized)
			{
				if (!storyService.isInitialized)
				{
					storyService.BindExternalFunctionOneParam(StringConstants.ADD_ITEM_TO_INVENTORY);
					storyService.BindExternalFunctionOneParam(StringConstants.REMOVE_ITEM_FROM_INVENTORY);
					storyService.BindExternalFunctionOneParam(StringConstants.SHOW);
					storyService.BindExternalFunctionOneParam(StringConstants.HIDE);
					storyService.BindExternalFunctionOneParam(StringConstants.GO_TO_ROOM);
					storyService.BindExternalFunctionOneParam(StringConstants.ADD_QUEST_TO_LOG);
					storyService.BindExternalFunctionTwoParams(StringConstants.UPDATE_QUEST_TO_LOG);
					storyService.BindExternalFunctionOneParam(StringConstants.PLAY_SOUND);
					storyService.BindExternalFunctionOneParam(StringConstants.STOP_SOUND);
					storyService.BindExternalFunctionOneParam(StringConstants.PLAYER_ANIMATION);
					storyService.BindExternalFunctionOneParam(StringConstants.WAIT);
					storyService.isInitialized = true;	
				}	
				
				// Read progression from the story state
				showPaletteController.LoadRoomItemsToShowProgression();
				inventoryController.LoadInventoryProgression();
				questLogController.LoadQuestProgression();
				roomController.LoadRoomProgression();
				mouseClickMover.LoadPlayerPosition();

                var isGameStarted = (int) storyService.GetVariable("IsGameStarted");
                if (isGameStarted == 0)
                {
                    actionsController.Continue();
                    storyService.ChooseChoiceIndex(characterModel.characterIndex);
                    actionsController.Continue();
                }
				
				signalBus.Fire(new ShowLoadingScreenSignal(false));
			}
		}
	}
}