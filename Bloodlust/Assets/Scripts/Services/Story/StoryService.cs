﻿using System;
using System.Collections;
using System.Collections.Generic;
using Ink.Runtime;
using UnityEngine;
using Utils;
using Zenject;

namespace Services.Story
{
	public class StoryService : IInitializable, IDisposable
	{
		[Inject] public TextAsset storyJson { get; private set; }
		[Inject] public SignalBus signalBus { get; private set; }
		[Inject] public AsyncProcessor asyncProcessor { get; private set; }

		private Ink.Runtime.Story _story;
		public bool isInitialized;
		private Coroutine _coroutine;
		private bool _isWaiting;
		private List<StorySignal> _waitingSignals;

		public void Initialize()
		{
			_story = new Ink.Runtime.Story(storyJson.text);
			_waitingSignals = new List<StorySignal>();
		}

		public void Dispose()
		{
			_story = null;
			if (_coroutine != null)
			{
				asyncProcessor.StopCoroutine(_coroutine);
				_coroutine = null;
			}
		}

		private IEnumerator WaitCoroutine(string seconds)
		{
			int wait = Int32.Parse(seconds);
			_isWaiting = true;
			yield return new WaitForSeconds(wait);
			_isWaiting = false;
			if (_waitingSignals.Count > 0)
			{
				for (var i = 0; i < _waitingSignals.Count; i++)
				{
					var signal = _waitingSignals[i];
					signalBus.Fire(signal);
				}
				_waitingSignals.Clear();
			}
		}

		public void ResetStory()
		{
			_story.ResetState();
		}

		public void LoadStory(string json)
		{
			_story.state.LoadJson(json);
		}

		public string SaveStory()
		{
			return _story.state.ToJson();
		}

		public bool CanContinue()
		{
			return _story.canContinue;
		}

		public string Continue()
		{
			return _story.Continue().Trim();
		}

		public string CurrentText()
		{
			return _story.currentText.Trim();
		}

		public List<Choice> CurrentChoices()
		{
			return _story.currentChoices;
		}

		public List<string> CurrentTags()
		{
			return _story.currentTags;
		}

		public int GetNumberOfChoices()
		{
			return _story.currentChoices.Count;
		}

		public void ChooseChoiceIndex(int index)
		{
			_story.ChooseChoiceIndex(index);
		}

		public void BindExternalFunctionOneParam(string functionName)
		{
			_story.BindExternalFunction(functionName, (string arg1) =>
			{
				if (functionName == StringConstants.WAIT)
				{
					_coroutine = asyncProcessor.StartCoroutine(WaitCoroutine(arg1));
					return;
				}

				var signal = new StorySignal() {externalFunction = functionName, arg1 = arg1};
				if (!_isWaiting)
				{
					signalBus.Fire(signal);
				}
				else
				{
					_waitingSignals.Add(signal);
				}
			});
		}

		public void BindExternalFunctionTwoParams(string functionName)
		{
			_story.BindExternalFunction(functionName, (string arg1, string arg2) => { signalBus.Fire(new StorySignal() {externalFunction = functionName, arg1 = arg1, arg2 = arg2}); });
		}

		public void SetVariable(string variableName, System.Object value)
		{
			_story.variablesState[variableName] = value;
		}

		public System.Object GetVariable(string variableName)
		{
			return _story.variablesState[variableName];
		}

		public void ChoosePathString(string path)
		{
			_story.ChoosePathString(path);
		}
	}
}