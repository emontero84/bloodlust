using Zenject;

namespace Services.SceneLoader
{
	public class SceneLoaderInstaller : Installer<SceneLoaderInstaller>
	{
		public override void InstallBindings()
		{
			Container.BindInterfacesAndSelfTo<SceneLoaderService>().FromNew().AsSingle();
		}
	}
}