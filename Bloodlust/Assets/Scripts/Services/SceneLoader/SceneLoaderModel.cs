namespace Services.SceneLoader
{
	public class SceneLoaderModel
	{
		public const string MAIN_MENU_SCENE = "MainMenu";
		public const string GAMEPLAY_SCENE = "Gameplay";
	}
}