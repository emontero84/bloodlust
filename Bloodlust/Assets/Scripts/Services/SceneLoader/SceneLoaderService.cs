using UnityEngine.SceneManagement;

namespace Services.SceneLoader
{
	public class SceneLoaderService
	{
		public void LoadScene(string sceneName)
		{
			SceneManager.LoadScene(sceneName);
		}
	}
}