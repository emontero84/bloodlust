using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

namespace Services.FileLoader
{
	public class FileLoaderService
	{
		public Action<string, string, string> FileLoaded;

		public void LoadStreamingAsset(string fileName, string fileId)
		{
			string filePath = System.IO.Path.Combine(Application.streamingAssetsPath, fileName);

			string result;
			if (filePath.Contains("://") || filePath.Contains(":///"))
			{
				result = PlayerPrefs.GetString(fileId);
			}
			else
			{
				result = System.IO.File.ReadAllText(filePath);
			}

			if (FileLoaded != null)
			{
				FileLoaded(fileName, fileId, result);
			}
		}

		public void SaveStreamingAsset(string json, string fileName, string fileId)
		{
			string filePath = System.IO.Path.Combine(Application.streamingAssetsPath, fileName);

			if (filePath.Contains("://") || filePath.Contains(":///"))
			{
				PlayerPrefs.SetString(fileId, json);
			}
			else
			{
				System.IO.File.WriteAllText(fileName, json);
			}
		}
	}
}