using System;
using Gameplay.Adventure.Room;
using UnityEngine;
using Utils;
using Zenject;

namespace Services.Audio
{
	public class PlayMusicCommand
	{
		[Inject] public AudioManager audioManager { get; private set; }
		[Inject] public RoomController roomController { get; private set; }

		public void OnSignal(PlayMusicSignal signal)
		{
			// The initial music plays without fade in
			if (String.IsNullOrEmpty(signal.currentRoom))
			{
				var initialMusicId = GetMusicRoomId(signal.nextRoom);
				audioManager.Play(initialMusicId);
				return;
			}

			// Stop music of the current room, play the music of the next room
			var currentRoomMusicId = GetMusicRoomId(signal.currentRoom);
			var nextRoomMusicId = GetMusicRoomId(signal.nextRoom);
			if (currentRoomMusicId != nextRoomMusicId)
			{
				audioManager.FadeOut(currentRoomMusicId, audioManager.fadeOutDuration);
				audioManager.FadeIn(nextRoomMusicId, audioManager.fadeInDuration);
			}
		}

		private SoundData.SoundId GetMusicRoomId(string roomId)
		{
			var currentRoom = EnumUtil.ParseEnum<RoomModel.Room>(roomId);
			var musicId = roomController.GetRoomMusic(roomId);
			return musicId;
		}
	}
}