using Zenject;

namespace Services.Audio
{
	public class AudioInstaller : MonoInstaller<AudioInstaller>
	{
		public AudioManager audioManager;

		public override void InstallBindings()
		{
			Container.BindInterfacesAndSelfTo<AudioManager>().FromInstance(audioManager).AsSingle();
			
			Container.DeclareSignal<PlayAudioSignal>();
			Container.DeclareSignal<StopAudioSignal>();
			Container.BindSignal<PlayAudioSignal>().ToMethod<PlayAudioCommand>(x => x.OnSignal).FromResolve();
			Container.BindSignal<StopAudioSignal>().ToMethod<StopAudioCommand>(x => x.OnSignal).FromResolve();
			Container.BindInterfacesAndSelfTo<PlayAudioCommand>().FromNew().AsSingle();
			Container.BindInterfacesAndSelfTo<StopAudioCommand>().FromNew().AsSingle();
		}
	}
}