namespace Services.Audio
{
	public class StopAudioSignal
	{
		public SoundData.SoundId sound;

		public StopAudioSignal(SoundData.SoundId soundId)
		{
			sound = soundId;
		}
	}
}