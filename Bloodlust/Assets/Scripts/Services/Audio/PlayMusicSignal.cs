namespace Services.Audio
{
	public class PlayMusicSignal
	{
		public string currentRoom;
		public string nextRoom;

		public PlayMusicSignal(string room, string next)
		{
			currentRoom = room;
			nextRoom = next;
		}
	}
}