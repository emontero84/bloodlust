using Zenject;

namespace Services.Audio
{
	public class PlayAudioCommand
	{
		[Inject] public AudioManager audioManager { get; private set; }

		public void OnSignal(PlayAudioSignal signal)
		{
			var sound = signal.sound;
			audioManager.Play(sound);
		}
	}
}