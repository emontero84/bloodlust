namespace Services.Audio
{
	public class PlayAudioSignal
	{
		public SoundData.SoundId sound;

		public PlayAudioSignal(SoundData.SoundId soundId)
		{
			sound = soundId;
		}
	}
}