using Zenject;

namespace Services.Audio
{
	public class StopAudioCommand
	{
		[Inject] public AudioManager audioManager { get; private set; }
		
		public void OnSignal(StopAudioSignal signal)
		{
			var sound = signal.sound;
			audioManager.Stop(sound);
		}
	}
}