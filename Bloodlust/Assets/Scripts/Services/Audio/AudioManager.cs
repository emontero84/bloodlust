using System;
using System.Collections.Generic;
using DG.Tweening;
using Services.Story;
using UnityEngine;
using UnityEngine.Audio;
using Utils;
using Zenject;

namespace Services.Audio
{
	public class AudioManager : MonoBehaviour, IInitializable, IDisposable
	{
		[Inject] public SignalBus signalBus { get; private set; }

		public List<SoundData> sounds;
		public AudioMixer masterAudioMixer;
		public float fadeInDuration;
		public float fadeOutDuration;

		private Dictionary<SoundData.SoundId, SoundData> soundsDictionary;

		public void Initialize()
		{
			soundsDictionary = new Dictionary<SoundData.SoundId, SoundData>();
			foreach (SoundData sound in sounds)
			{
				var source = gameObject.AddComponent<AudioSource>();
				source.clip = sound.clip;
				source.volume = sound.volume;
				source.loop = sound.loop;
				source.outputAudioMixerGroup = sound.output;
				sound.source = source;
				soundsDictionary.Add(sound.id, sound);
			}

			signalBus.Subscribe<StorySignal>(OnStorySignal);
		}

		public void Dispose()
		{
			if (soundsDictionary != null)
			{
				StopAllSounds();

				soundsDictionary.Clear();
				soundsDictionary = null;
			}

			signalBus.Unsubscribe<StorySignal>(OnStorySignal);
		}

		private void OnStorySignal(StorySignal signal)
		{
			switch (signal.externalFunction)
			{
				case StringConstants.PLAY_SOUND:
					var soundEnum = EnumUtil.ParseEnum<SoundData.SoundId>(signal.arg1);
					Play(soundEnum);
					break;
				case StringConstants.STOP_SOUND:
					var soundEnum2 = EnumUtil.ParseEnum<SoundData.SoundId>(signal.arg1);
					Stop(soundEnum2);
					break;
			}
		}

		private SoundData GetSoundData(SoundData.SoundId id)
		{
			if (soundsDictionary.ContainsKey(id))
			{
				return soundsDictionary[id];
			}
			else
			{
				Debug.LogError("[AudioManager] There's no sound with id " + id);
				return null;
			}
		}

		private AudioSource GetAudioSource(SoundData.SoundId id)
		{
			var data = GetSoundData(id);
			if (data != null)
				return data.source;
			return null;
		}

		public bool HasSound(SoundData.SoundId id)
		{
			var data = GetSoundData(id);
			return data != null;
		}
		
		public void Play(SoundData.SoundId id)
		{
			AudioSource source = GetAudioSource(id);
			source.Play();
		}

		public void Stop(SoundData.SoundId id)
		{
			AudioSource source = GetAudioSource(id);
			source.Stop();
		}

		public void StopAllSounds()
		{
			foreach (var value in soundsDictionary.Values)
			{
				if (value != null)
				{
					value.source.Stop();
				}
			}
		}

		public void FadeIn(SoundData.SoundId id, float time)
		{
			var data = GetSoundData(id);
			float targetVolume = data.volume;
			var source = data.source;
			source.volume = 0;
			source.Play();
			source.DOFade(targetVolume, time);
		}

		public void FadeOut(SoundData.SoundId id, float time)
		{
			AudioSource source = GetAudioSource(id);
			source.DOFade(0, time);
		}

		public void SetMusicVolume(float volume)
		{
			masterAudioMixer.SetFloat(StringConstants.MUSIC_VOLUME, volume);
		}

		public void SetSoundVolume(float volume)
		{
			masterAudioMixer.SetFloat(StringConstants.SOUND_VOLUME, volume);
		}

		public void SetDialogVolume(float volume)
		{
			masterAudioMixer.SetFloat(StringConstants.DIALOG_VOLUME, volume);
		}
	}
}