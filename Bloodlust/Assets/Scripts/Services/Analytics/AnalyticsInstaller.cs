using Zenject;

namespace Services.Analytics
{
	public class AnalyticsInstaller : Installer<AnalyticsInstaller>
	{
		public override void InstallBindings()
		{
			Container.BindInterfacesAndSelfTo<AnalyticsService>().FromNew().AsSingle();
			Container.DeclareSignal<AnalyticsSignal>();
			Container.BindSignal<AnalyticsSignal>().ToMethod<SendCustomEventCommand>(x => x.OnSignal).FromResolve();
			Container.BindInterfacesAndSelfTo<SendCustomEventCommand>().AsSingle();
		}
	}
}