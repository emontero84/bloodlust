using System.Collections.Generic;

namespace Services.Analytics
{
	public class AnalyticsSignal
	{
		public string eventName;
		public Dictionary<string, object> parameters;

		public AnalyticsSignal(string eventName, string paramId, string parameters)
		{
			this.eventName = eventName;
			this.parameters = new Dictionary<string, object>
			{
				{paramId, parameters}
			};
		}

		public AnalyticsSignal(string eventName, Dictionary<string, object> parameters = null)
		{
			this.eventName = eventName;
			this.parameters = parameters;
		}
	}
}