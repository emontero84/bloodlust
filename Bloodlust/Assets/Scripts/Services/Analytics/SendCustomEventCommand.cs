using Zenject;

namespace Services.Analytics
{
	public class SendCustomEventCommand
	{
		[Inject] public AnalyticsService analyticsService { get; private set; }

		public void OnSignal(AnalyticsSignal signal)
		{
			analyticsService.SendCustomEvent(signal.eventName, signal.parameters);
		}
	}
}