namespace Services.Analytics
{
	public class AnalyticsModel
	{
		public const string WALK_TO_POINT = "WalkToPoint";
		public const string ITEM_CLICK = "ItemClick";
		public const string ITEM_SECONDARY_CLICK = "ItemSecondaryClick";
		public const string INVENTORY_ITEM_CLICK = "InventoryItemClick";
		public const string INVENTORY_ITEM_SECONDARY_CLICK = "InventoryItemSecondaryClick";
		public const string EXECUTE_ACTION = "ExecuteAction";
		public const string WRONG_ACTION = "WrongAction";
		public const string QUEST_LOG_OPEN = "QuestLogOpen";
		public const string QUEST_LOG_CLOSE = "QuestLogClose";
		public const string DIALOG_CONTINUE_CLICK = "DialogContinueClick";
		public const string DIALOG_CHOICE_CLICK = "DialogChoiceClick";
		public const string DIALOG_CHOICE_KEY = "DialogChoiceKey";
		public const string CHANGE_ROOM = "ChangeRoom";
	}
}