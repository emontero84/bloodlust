using System;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Services.Analytics
{
	public class AnalyticsService : IInitializable, IDisposable
	{
		public void Initialize()
		{
			Application.logMessageReceived += OnLogMessageReceived;
		}

		public void Dispose()
		{
			Application.logMessageReceived -= OnLogMessageReceived;
		}

		public void SendCustomEvent(string eventName, Dictionary<string, object> parameters)
		{
#if !UNITY_EDITOR
			UnityEngine.Analytics.Analytics.CustomEvent(eventName, parameters);
#endif
		}

		private void OnLogMessageReceived(string logString, string stackTrace, LogType logType)
		{
			if (logType == LogType.Error || logType == LogType.Exception)
			{
				var parameters = new Dictionary<string, object>();
				parameters.Add("LogString", logString);
				parameters.Add("StackTrace", stackTrace);
				parameters.Add("LogType", logType.ToString());
				SendCustomEvent("Error", parameters);
			}
		}
	}
}